---
title: "Bacteria"
description: "Fun Facts relating to bacteria"
slug: "bacteria"
image: "bacteria.jpg"
style:
    background: "#2a9d8f"
    color: "#fff"
---