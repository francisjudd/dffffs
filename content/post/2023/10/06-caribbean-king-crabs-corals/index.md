+++
author = "Flora Judd"
title = "Crabs to the Rescue"
date = "2023-10-06"
description = "Innovative approaches in marine conservation show hope for coral reefs."
tags = ["Caribbean King Crab","Marine Conservation","Coral Reefs","Habitat Restoration","Predator Conditioning","Florida","Marine Ecosystems","Biodiversity","Mote Marine Lab","Conservation Strategies"]
categories = ["Marine Conservation","Coral Reefs","Ecology","Conservation"]
image = "crabs_with_an_appetite_for_seaweed_could.jpeg"
+++

Hello and welcome to another
 *{{<text size="18.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

Today we have a story that gives us{{<highlight size="16.0pt" bg="yellow">}}optimism{{</highlight>}}, something that we can be a bit short of when looking at the marine conservation world. Some scientists have a plan to **{{<text size="18.0pt" fg="#00B0F0">}}help Florida's dying coral reefs{{</text>}}** ! And the plan looks like this.

![Caribbean king crab, Cayos Cochinos, Honduras - a photo on Flickriver](caribbean_king_crab_cayos_cochinos_hondu.jpeg)

This is a **{{<text size="20.0pt" fg="#FFC000">}}Caribbean king crab ( *Maguimithrax spinosissimus* ).{{</text>}}** I actually love that Latin name. Doesn't that sound like a James Bond villain?
![image](689e484c145a1c58.png)
*Again, **shockingly** , I am not a professional photoshop artist.*

Annyyyhooo.

Coral reefs are on a pretty steep decline and in most places it is so bad that
 ***{{<text size="20.0pt" fg="#92D050">}}natural recovery{{</text>}}*** {{<text size="20.0pt" fg="#92D050">}}{{</text>}}is **{{<text size="24.0pt" bg="gray">}}pretty much impossible{{</text>}}**.

{{<text size="22.0pt" fg="#7030A0">}}Overfishing{{</text>}},
 **{{<text size="24.0pt" fg="#00B0F0">}}climate change{{</text>}}** , *{{<text size="24.0pt" fg="#ED7D31">}}disease{{</text>}}* {{<text size="24.0pt" fg="#ED7D31">}}{{</text>}}and ***{{<text size="24.0pt" fg="#7F6000">}}eutrophication{{</text>}}*** {{<text size="24.0pt" fg="#7F6000">}}{{</text>}}have meant that now much of what used to be coral reef in the Caribbean is just
 ***{{<text size="24.0pt" fg="#00B050" bg="lime">}}algae{{</text>}}***.

![Do Coral-Algal Phase Shifts Result in Decreases in Productivity onCaribbean Reefs? | by KOBI TALMA | Medium](do_coral-algal_phase_shifts_result_in_de.jpeg)

*{{<text size="16.0pt">}}Shifting that algae is difficult{{</text>}}*. But until it is shifted new corals cannot grow.
 **{{<text size="16.0pt" fg="#FBC0ED">}}Baby corals{{</text>}}** {{<text size="16.0pt">}}{{</text>}}spend their early days swimming around in the ocean, trying to find a spot to
 *{{<text size="20.0pt" fg="#92D050">}}settle on{{</text>}}* and {{<text size="18.0pt" fg="#8FAADC">}}start a colony{{</text>}}. And it's hard to do that when there is so much algae --- not much space left over to claim for themselves.

![Does Imgur have a flag? - GIFs - Imgur](does_imgur_have_a_flag_-_gifs_-_imgur.gif)

Another big problem is that the {{<highlight size="18.0pt" bg="yellow">}}algae also blocks the sunlight{{</highlight>}}{{<text size="18.0pt">}}{{</text>}}that the pre-existing corals need to grow, and
 **{{<text size="22.0pt" fg="#FFC000">}}crowds them out{{</text>}}**.

The animals that would *{{<text size="18.0pt" fg="#F4B183">}}normally{{</text>}}* {{<text size="18.0pt" fg="#F4B183">}}{{</text>}}eat the algae, and keep it under control, have **{{<text size="20.0pt" fg="#7030A0">}}declined hugely{{</text>}}** {{<text size="20.0pt" fg="#7030A0">}}{{</text>}}in the last few decades. In the 1980s, an unknown disease almost totally wiped out
 ***{{<highlight size="20.0pt" bg="aqua">}}longspined sea urchins{{</highlight>}}*** {{<text size="20.0pt">}}{{</text>}}in the Caribbean, and these dudes would normally go to TOWN on all that algae.

![Long-Spined Sea Urchin | Tetiaroa Society](long-spined_sea_urchin_tetiaroa_society.jpeg)
Try to show {{<text size="14.0pt" bg="fuchsia">}}love{{</text>}}{{<text size="14.0pt">}}{{</text>}}to the sea urchins --- they do important work!

With no herbivores to keep the algae in check --- it can grow **{{<text size="16.0pt" fg="#FFC000">}}unchecked{{</text>}}** {{<text size="16.0pt" fg="#FFC000">}}{{</text>}}and *{{<text size="18.0pt" fg="#4472C4">}}unstopped{{</text>}}*.

So, a team at the Mote Marine Lab and Aquarium in Florida has been working on a
 **{{<text size="18.0pt" fg="#70AD47">}}solution{{</text>}}** {{<text size="18.0pt" fg="#70AD47">}}{{</text>}}to this. They discovered that the Caribbean king crab has a *{{<text size="20.0pt" fg="#00CCA3">}}voracious appetite for algae{{</text>}}* , in fact they exceed nearly all other fish or invertebrate grazers in the Caribbean, (<a href="https://www.cell.com/current-biology/fulltext/S0960-9822(20)31674-2?_returnURL=https%3A%2F%2Flinkinghub.elsevier.com%2Fretrieve%2Fpii%2FS0960982220316742%3Fshowall%3Dtrue">Spadaro
 and Butler</a>, 2020).

![Crabs with an appetite for seaweed could save Caribbean coral reefs | NewScientist](crabs_with_an_appetite_for_seaweed_could.jpeg)

They decided to test how effective these dudes might be at munching some algae, so they
{{<text size="18.0pt" fg="#A9D18E">}}transplanted some crabs onto patches of reef{{</text>}}.

The crabs reduced the cover of algae by **{{<text size="20.0pt" fg="#FFC000" bg="navy">}}50-80%!{{</text>}}** This led to a
 **{{<text size="20.0pt" fg="#F0126B">}}3-5x{{</text>}}** increase in *{{<text size="20.0pt" fg="#7030A0">}}coral recruitment{{</text>}}* {{<text size="20.0pt" fg="#7030A0">}}{{</text>}}and **{{<text size="20.0pt" fg="#00B0F0">}}fish abundance{{</text>}}** {{<text size="20.0pt" fg="#00B0F0">}}{{</text>}}and ***{{<text size="22.0pt" fg="#F47F3E">}}fish diversity!{{</text>}}***

![Nice Joey Thumbs Up GIF | GIFDB.com](nice_joey_thumbs_up_gif_gifdbcom.gif)

What is even better is that these crabs are {{<text size="20.0pt" fg="#A9D18E">}}native{{</text>}}to Florida (just in **{{<text size="14.0pt" fg="#00CCA3">}}very low numbers{{</text>}}** {{<text size="14.0pt" fg="#00CCA3">}}{{</text>}}because everything eats them) and adding large quantities of them to the reef is
{{<text size="18.0pt" fg="#FFC000">}}very unlikely to have unexpected negative consequences{{</text>}}.

Their plan is pretty ambitious. They want to breed **{{<text size="26.0pt" fg="#F0126B">}}a quarter of a million{{</text>}}** {{<text size="26.0pt" fg="#F0126B">}}{{</text>}}Caribbean king crabs *{{<highlight size="18.0pt" bg="yellow">}}every single year{{</highlight>}}*.

![A person's open hand with a tiny crab sitting in the middle.](a_persons_open_hand_with_a_tiny_crab_sit.jpeg) **
*Okay they are really cute when they are small.*

But there is one crucial step before the lab-raised crabs are released into the sea. This is my favourite part of the whole story.

These crabs **{{<text size="20.0pt" fg="red">}}have no experience of predators{{</text>}}** , right, they have just grown up in a tank. They
 *{{<text size="20.0pt" fg="#F47F3E">}}need to be conditioned to fear things that might eat them{{</text>}}* , like octopuses, snappers or groupers.

The team decided to use **{{<text size="20.0pt" fg="#7030A0">}}puppets{{</text>}}** {{<text size="20.0pt" fg="#7030A0">}}{{</text>}}modelled on the predators that they could put in the tank *{{<text size="20.0pt" fg="#F0126B">}}while poking the crabs{{</text>}}* {{<text size="20.0pt" fg="#F0126B">}}{{</text>}}to create an aversion. But the best part of all --- the lab ***{{<highlight size="22.0pt" bg="aqua">}}partnered with a local school{{</highlight>}}*** {{<text size="22.0pt">}}{{</text>}}and had the school kids make the hand puppets.

Luckily the crabs **{{<text size="16.0pt" fg="#00B0F0">}}do not have very good vision {{</text>}}**

![image](cdd788b79e70d01b.png)

So {{<highlight size="18.0pt" bg="yellow">}}maybe these crabs can play a role in helping the Caribbean corals recover{{</highlight>}}. It is not enough by itself, but anything that helps is always worth celebrating!

Hope you enjoyed a **{{<text size="20.0pt" fg="#FFC000">}}fun{{</text>}}** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}and ***{{<text size="20.0pt" fg="#7030A0" bg="fuchsia">}}funky{{</text>}}*** {{<text size="20.0pt" fg="#7030A0">}}{{</text>}}critter helping us help the planet.

Lots of love,

Flora

![Animals Falling GIFs | Tenor](animals_falling_gifs_tenor.gif)