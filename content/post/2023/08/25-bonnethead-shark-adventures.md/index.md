+++
author = "Flora Judd"
title = "Bonnethead Shark Wonders"
date = "2023-08-25"
description = "Exploring the surprising diet of the bonnethead shark and its unique characteristics."
tags = ["Bonnethead Shark","Omnivores","Marine Diet","Shark Behavior","Ocean Ecosystems","Conservation","Dietary Studies","Shark Anatomy","Nutritional Value","Biodiversity"]
categories = ["Marine Biology","Shark Species","Wildlife Education","Conservation"]
image = "a_profile_of_the_bonnethead_shark.jpeg"
+++

Hello and welcome to another
 *{{<text size="18.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

Today we are going to look at a shark that is after my own heart. This is the ***{{<text size="20.0pt" fg="#5B9BD5">}}bonnethead shark{{</text>}}***.

![A Profile of the Bonnethead Shark](a_profile_of_the_bonnethead_shark.jpeg)

This is what they look like.

Sort of like evolution got
 **{{<text size="16.0pt" fg="#00B6D6">}}partway towards a hammerhead shark{{</text>}}** {{<text size="16.0pt" fg="#00B6D6">}}{{</text>}}and then got bored and{{<text size="18.0pt" fg="#0070C0">}}took a break{{</text>}}. It has the smallest **{{<highlight size="20.0pt" bg="aqua">}}cephalofoil{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}(the proper name for the hammer-head) of any of the hammerhead sharks. It is actually a pretty small dude (for shark standards at least), typically ranging **{{<text size="18.0pt" fg="#00E5C1">}}80-90cm{{</text>}}**.

And, come on, are they not just ***{{<text size="20.0pt" fg="#8FAADC">}}BLOOMIN" adorable{{</text>}}*** ??

![Bonnethead Shark pup | CRITTERS | Pinterest](bonnethead_shark_pup_critters_pinterest.jpeg)

But they are ***{{<text size="18.0pt" fg="#92D050">}}very unusual in what they eat{{</text>}}***. According to
<a href="https://www.youtube.com/watch?v=WnJUEQyRsU0">this highly reputable video</a>
{{< youtube WnJUEQyRsU0 >}} on YouTube (which has
 **{{<highlight bg="yellow">}}3 whole views{{</highlight>}}** ) they are **{{<text size="22.0pt" fg="#00B050">}}omnivores{{</text>}}** !

![image](27438e7a9ebc7168.png)
![image](d1ee7e4a983d2a76.png)

Shoutout to Jameson and his excellent content.

But do not worry, I have not based this fact on the home-produced video of a{{<text size="14.0pt" fg="#7030A0">}}5-year-old YouTube star{{</text>}}, there are more people than just Jameson who are aware of this shark's unusual dietary requirements.

Now, we have known that bonnetheads *{{<text size="14.0pt" fg="#FFC000">}}occasionally{{</text>}}* munch on some seagrass for a little while, but scientists first assumed that it was **{{<text size="18.0pt" fg="#ED7D31">}}incidental{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}or *{{<text size="18.0pt" fg="#843C0C">}}did not really provide much nutrition{{</text>}}* for the sharks. Kind of like how **{{<text size="18.0pt" fg="#70AD47">}}cats eat grass{{</text>}}** {{<text size="18.0pt" fg="#70AD47">}}{{</text>}}(we think to get rid of gut parasites and hair balls by increasing muscle activity in the gut!! So many fun facts today!!).

![Funny Cats GIFs | USAGIF.com](funny_cats_gifs_usagifcom.gif)

But when some research was done by scientists at the University of California and Florida International University they found that, alongside eating some fish, shrimp and crabs, **{{<text size="20.0pt" bg="lime">}}typically around 60% of their diet{{</text>}}** {{<text size="20.0pt">}}{{</text>}}is actually seagrass.

*{{<text size="18.0pt" fg="#1CC92C">}}60 whole percent!!{{</text>}}* {{<text size="18.0pt">}}{{</text>}} **{{<u size="16.0pt" fg="#FFC000">}}MORE than HALF{{</u>}}** {{<text size="16.0pt" fg="#FFC000">}}{{</text>}}of their food!!

Therefore, these sharks are actually using the seagrass for its **{{<text size="16.0pt" fg="#00B0F0">}}nutritional value{{</text>}}** , not just *{{<text size="16.0pt" fg="#00B6D6">}}accidentally snagging some greens{{</text>}}* {{<text size="16.0pt" fg="#00B6D6">}}{{</text>}}alongside their main course.

But, as anyone who's eaten cabbage knows, vegetables can be pretty ***{{<text size="16.0pt" fg="#1CC92C">}}fibrous{{</text>}}***. There is a lot of structure that needs to be broken down to access their **{{<text size="14.0pt" fg="#F4B183">}}vetegably goodness{{</text>}}** {{<text size="14.0pt" fg="#F4B183">}}{{</text>}}and **{{<text size="16.0pt" fg="#A9D18E">}}shark teeth are not designed for that{{</text>}}**. Think of a sheep or cows teeth, very *{{<text size="14.0pt" fg="#5B9BD5">}}flat{{</text>}}* {{<text size="14.0pt" fg="#5B9BD5">}}{{</text>}}and *{{<text size="14.0pt" fg="#2E75B6">}}sturdy{{</text>}}* {{<text size="14.0pt" fg="#2E75B6">}}{{</text>}}to grind tough grass down to a pulp.

And now look at what the poor bonnethead is working with.

![image](d7dc4fd822a8b9df.png)

Yeah. So it seems most likely that they have ***{{<text size="16.0pt" fg="#7030A0" bg="fuchsia">}}incredibly{{</text>}}*** **{{<text size="16.0pt" fg="#7030A0" bg="fuchsia">}}strong stomach acid{{</text>}}** **{{<text size="16.0pt" fg="#7030A0">}}{{</text>}}** that is able to break down the *{{<text size="18.0pt" fg="#EF68A2">}}cellulose and the starch{{</text>}}* in the seagrass and access all of those tasty nutrients.

In addition, the research team (<a href="https://royalsocietypublishing.org/doi/10.1098/rspb.2018.1583">study here</a>) used a very cool trick of lacing the seagrass with a **{{<text size="18.0pt" fg="#FFF2CC" bg="black">}}specific carbon isotope{{</text>}}** , so they could track *{{<text size="14.0pt" fg="#00B6D6">}}where the seagrass was going in the shark's body{{</text>}}* {{<text size="14.0pt" fg="#00B6D6">}}{{</text>}}and therefore what it was being used for (metabolically speaking). Turns out the shark's **{{<text size="18.0pt" fg="#ED7D31">}}blood and liver tissue{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}was very high in the altered carbon isotope, showing that those digested compounds were *{{<text size="16.0pt" fg="#007F9D">}}actually being used to build and maintain{{</text>}}* the sharks.

Definitely not an accidental monching.

![Food Eat GIF - Food Eat Ron Swanson - Discover & Share GIFs](food_eat_gif_-_food_eat_ron_swanson_-_di.gif)

Now this has pretty wild implications for sharks. Previously, only **{{<text size="16.0pt" fg="#7030A0">}}fictitious{{</text>}}** {{<text size="16.0pt" fg="#7030A0">}}{{</text>}}sharks were **{{<text size="18.0pt" fg="#70AD47">}}vegetarianally inclined{{</text>}}**.

![image](ad5874d6ecfd6b96.jpeg)![Fish-are-friends-not-food GIFs - Get the best GIF on GIPHY](fish-are-friends-not-food_gifs_-_get_the.gif)

And I am so here to dispel the myth that sharks are all blood-thirsty killing machines. Some of them just like to snack on some{{<text size="16.0pt" fg="#A9D18E">}}good ol" fashioned vegetables{{</text>}}.

Also, we can add another thing to the (extremely) long list of **{{<text size="16.0pt" fg="#EF68A2">}}why we need to love our seagrass meadows{{</text>}}** {{<text size="16.0pt" fg="#EF68A2">}}{{</text>}}and keep them safe from human problematic-ness. More on seagrasses later.

Thanks for stopping by.

Lots of veggie, sharky love,

Flora xxx

![falls over tortoise gif | WiffleGif](falls_over_tortoise_gif_wifflegif.gif)