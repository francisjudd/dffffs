+++
author = "Flora Judd"
title = "Icefish Adaptations"
date = "2023-08-24"
description = "Exploring the extraordinary adaptations of the icefish in extreme environments."
tags = ["Icefish","Haemoglobin","Cold Water Adaptations","Evolutionary Strategies","Blood Physiology","Extreme Environments","Marine Biology","Species Diversity","Research","Survival Tactics"]
categories = ["Marine Biology","Evolution","Wildlife","Adaptations"]
image = "how_does_the_antarctic_icefish_live_with.jpeg"
+++

Hello and welcome to another *{{<text size="16.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,
Today we are looking at a particularly wild and whacky fish known as an
 **{{<highlight size="16.0pt" bg="aqua">}}icefish{{</highlight>}}**.

Now, that name may make you think that I am going to tell you they are so cool because they live in
 *{{<text size="16.0pt" fg="#0070C0">}}very chilly places{{</text>}}* {{<text size="16.0pt" fg="#0070C0">}}{{</text>}}(which they do). And you might think of this:

![Best Ice Fishing GIFs | Gfycat](best_ice_fishing_gifs_gfycat.gif)

But they actually look like this:

![Scaly, see-through thing surprises fisherman - CNN.com](scaly_see-through_thing_surprises_fisher.jpeg)

And these, my friend, are some **{{<text size="18.0pt" fg="#00B0F0">}}pretty hardcore fish{{</text>}}**. They are just swimmin" around, living their lives, without
 *{{<text size="18.0pt" fg="red" bg="maroon">}}ANY haemoglobin in their bloodstream{{</text>}}*. These guys are the
 **{{<text size="20.0pt" fg="#C00000">}}ONLY known vertebrates{{</text>}}** {{<text size="20.0pt" fg="#C00000">}}{{</text>}}that totally lack haemoglobin as adults. That means no red blood cells. Nada.

This means that, yes, {{<text size="18.0pt" fg="white" bg="black">}}their blood is totally clear{{</text>}}.

And this is not just a quick swap. Can't go from having haemoglobin to not having haemoglobin in a pinch. From an evolutionary perspective this is a
 *{{<text size="18.0pt" fg="#ED7D31">}}total body and circulatory system overhaul{{</text>}}*.

In (most) vertebrates, haemoglobin is **{{<text size="16.0pt" fg="red">}}pretty darn essential{{</text>}}**. It binds to that
 *{{<highlight size="18.0pt" bg="yellow">}}handy oxygen stuff{{</highlight>}}* {{<text size="18.0pt">}}{{</text>}}and carries **{{<text size="18.0pt" fg="#ED7D31">}}A LOT{{</text>}}** of it through the body.

![Carrying GIFs - Get the best GIF on GIPHY](carrying_gifs_-_get_the_best_gif_on_giph.gif)

Yep, that's exactly what it's like. Your poor haemoglobin lugging all that oxygen around.

Except these guys have **{{<text size="18.0pt" fg="#FFC000">}}lost the genes{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}for **{{<text size="18.0pt" fg="red">}}haemoglobin{{</text>}}** {{<text size="18.0pt" fg="red">}}{{</text>}}(and also *{{<highlight size="16.0pt" bg="yellow">}}myoglobin{{</highlight>}}* {{<text size="18.0pt">}}{{</text>}}--- for the science nerds out there).

So, in order to have a system that doesn't rely on haemoglobin
 *{{<text size="18.0pt" fg="#7030A0">}}at all anymore{{</text>}}* {{<text size="18.0pt" fg="#7030A0">}}{{</text>}}the icefish had to go through some **{{<highlight size="18.0pt" bg="aqua">}}pretty major anatomical and chemical adaptations{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}compared to other similar fish (even fish from the same family).

Firstly the icefish's ***{{<text size="14.0pt" fg="#00E5C1">}}gills are HUGE{{</text>}}*** {{<text size="14.0pt">}}{{</text>}}relative to its body size, it has **{{<text size="16.0pt" fg="#004B6A">}}no scales{{</text>}}** {{<text size="16.0pt">}}{{</text>}}(which might help it to absorb some of that sweet sweet oxygen from the surrounding water) and
 **{{<text size="18.0pt" fg="#007F9D">}}a heart that is four times the size{{</text>}}** {{<text size="18.0pt">}}{{</text>}}of similar fish. It also makes **{{<text size="18.0pt" fg="#00B6D6">}}antifreeze proteins{{</text>}}** {{<text size="18.0pt">}}{{</text>}}like lots of other cold water fish that hang out in sub-zero sea temperatures (oof).

![How Does The Antarctic Icefish Live Without Red Blood Cells?](how_does_the_antarctic_icefish_live_with.jpeg)

This means that oxygen exists entirely in a **{{<text size="16.0pt" fg="#92D050">}}physical solution{{</text>}}** {{<text size="16.0pt" fg="#92D050">}}{{</text>}}in icefish blood, unlike other vertebrates where the oxygen is biologically bound up. This means their blood can carry only
 **{{<text size="20.0pt" fg="#00B050">}}10% of the oxygen{{</text>}}** {{<text size="20.0pt" fg="#00B050">}}{{</text>}}that their haemoglobin-possessing relatives have.

Right, so we have had a look at just how much this fish has had to adapt (massive gills and huge heart and whatnot) just to get
 *{{<text size="16.0pt" bg="fuchsia">}}rid of something{{</text>}}* that actually is
 *{{<text size="18.0pt" bg="fuchsia">}}very useful{{</text>}}*.

Evolution?

![What The Hell Are You Doing GIFs | Tenor](what_the_hell_are_you_doing_gifs_tenor.gif)

It is pretty crucial to note here that haemoglobin performs **{{<text size="18.0pt" fg="#8FAADC">}}a whole lot worse{{</text>}}** {{<text size="18.0pt" fg="#8FAADC">}}{{</text>}}at colder temperatures. And seawater is **{{<text size="20.0pt" fg="#2F5597">}}can hold more oxygen{{</text>}}** as it gets colder. That means that when biologists first found that icefish had clear blood in the 1950s, they thought it was an adaption
 to the cold. Makes sense.

![The Evolutionary Quirk That Allows Antarctica's Icefish To Survive ...](the_evolutionary_quirk_that_allows_antar.jpeg)

*{{<text size="16.0pt" fg="#ED7D31">}}But that doesn't seem to be the case{{</text>}}*. It seems that actually the loss of haemoglobin genes was probably an accident, and it was
 **{{<text size="18.0pt" fg="#FFC000">}}lucky that the icefish pulled through at all{{</text>}}**. In most environments, losing your haemoglobin genes would be totally fatal. You"d be a goner.

![YARN | You're what we call "a goner." | Scrubs (2001) - S02E05 Drama |Video clips by quotes | d5a87d1a | ](yarn_youre_what_we_call_a_goner_scrubs_2.gif)

But because the freezing Antarctic waters hold a lot of that tasty, dissolved oxygen, it seems they were able to survive without it.

However, it might **{{<text size="16.0pt" fg="#92D050">}}not have been for the best{{</text>}}**. One paper in 2006 said that the losses of the haemoglobin genes do not appear to be of adaptive value . Ouch. Instead they led to
 higher energetic costs for circulating blood and reduced cardiac performance as well as having to modify --- you know --- their
 **{{<text size="20.0pt" fg="#EF68A2">}}WHOLE cardiovascular system{{</text>}}**.

![How the Icefish Got Its Transparent Blood and See-Through Skull - The ...](how_the_icefish_got_its_transparent_bloo.jpeg)

So, what really stands out when you start looking into Antarctic icefish is that you see a lot of phrases like
 **{{<text size="18.0pt" fg="#9DC3E6">}} disadvantageous traits, {{</text>}}** 
 *{{<highlight size="18.0pt" bg="aqua">}} non-advantageous,{{</highlight>}}* 
 **{{<text size="16.0pt" fg="#00E5C1">}} extraordinary biology, {{</text>}}** {{<text size="16.0pt" fg="#00E5C1">}}{{</text>}}and ***{{<text size="18.0pt" fg="#0070C0">}} lucky accident, {{</text>}}***. It is pretty clear that these guys are a bit of an
 ***{{<text size="14.0pt" fg="#002060">}}evolutionary conundrum{{</text>}}***.

It isn't all bad news though. One developmental biologist, John Postlethwait, at the University of Oregon said that they had finally evolved a
 **{{<text size="18.0pt" fg="#7030A0">}}cure for anaemia{{</text>}}**. No haemoglobin, no red blood cells, no anaemia --- what a win!

All in all they are pretty cool dudes. They abandoned haemoglobin, pushed on through their own
 *sheer force of will* , in spite of several **{{<text size="18.0pt" bg="fuchsia">}} sub-lethal {{</text>}}** {{<text size="18.0pt">}}{{</text>}}mutations and morphological changes.

![Amanda rosewater GIF on GIFER - by Gajurus](amanda_rosewater_gif_on_gifer_-_by_gajur.gif)

Thanks again for coming by.

Love Flora xxx


![Fail Polar Bear GIF by BBC Earth - Find & Share on GIPHY](fail_polar_bear_gif_by_bbc_earth_-_find_.gif)