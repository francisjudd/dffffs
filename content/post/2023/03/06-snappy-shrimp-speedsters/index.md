+++
author = "Flora Judd"
title = "Speedy Snapping Shrimp"
date = "2023-03-06"
description = "Exploring the remarkable speed of the juvenile bigclaw snapping shrimp."
tags = ["Snapping Shrimp","bigclaw","Speed","Marine Life","Underwater Research","Cavitation Bubbles","Biomechanics","Acceleration","Light Production","Ecological Impact"]
categories = ["Marine Biology","Crustaceans","Nature","Research"]
image = "f8b462ac04f898f4.png"
+++

Hello and welcome to another
 *{{<text size="18.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

Today, we are going to keep things ***{{<highlight size="16.0pt" bg="yellow">}}snappy{{</highlight>}}***. And luckily for you, I don't mean quick, I mean **{{<text size="18.0pt" bg="fuchsia">}}pincers{{</text>}}**.

![play gifs Page 13 | WiffleGif](play_gifs_page_13_wifflegif.gif)

I have decided that, to do this justice, we need to look at two *{{<text size="14.0pt" fg="#ED7D31">}}different{{</text>}}* {{<text size="14.0pt" fg="#ED7D31">}}{{</text>}}species of crustacean. One that is the
 ***{{<text size="20.0pt" fg="#FFC000">}}speediest{{</text>}}*** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}(spoiler alert: it's NOT a mantis shrimp) and one that is the ***{{<text size="18.0pt" fg="#0070C0">}}strongest{{</text>}}***. When I started this, I was going to tackle both in this email, but I got so overexcited by the speedy one that we will tackle the strong one another
 day.

I had too much fun, **{{<text size="16.0pt" fg="#A9D18E">}}sorry, not sorry.{{</text>}}**

Let's talk speeeeedy.

We are looking at a ***{{<text size="20.0pt" fg="#8FAADC">}}juvenile bigclaw snapping shrimp{{</text>}}***.
 You would expect this shrimp to be fast, given that he comes from a family of animals called the **{{<text size="20.0pt" fg="#00B0F0">}}pistol shrimps{{</text>}}**. Isn't that brilliant? ***{{<text size="20.0pt" fg="#00FFE3">}}PISTOL SHRIMPS{{</text>}}***. (okay --- this one HAS to be a band name please can someone start band called the pistol shrimps with me???)

*Side note --- I just checked to see if anyone already has a band called Pistol Shrimp and this is the closest I could find.
 **Luckily I do not feel intimidated by their success.***
**
*![image](9facd77e99f4d0ed.png)* **
**
*But I want this as a logo*
*![image](73337f53b408e0c9.png)* **
**
**{{<highlight size="16.0pt" bg="yellow">}}SORRY --- back on track{{</highlight>}}** **{{<text size="16.0pt">}}.{{</text>}}**

This is a juvenile bigclaw snapping shrimp.

![image](f8b462ac04f898f4.png)

And if I had just broken the
 **{{<text size="18.0pt" fg="#FFC000">}}acceleration record for a repeatable body movement underwater{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}I would probably look a whole lot more pleased with myself than this little dude does. This would be me 

![image](b008216f4fc715dd.png)
*Yes I did this myself, and no I am not a professional photoshop artist .*
**
Alright, alright. So how fast are we actually talking here?

Well how's **{{<text size="18.0pt" fg="#A142EE">}}600,000 metres per second per second{{</text>}}** {{<text size="18.0pt">}}{{</text>}}(and yes, there are supposed to be two per second s). *{{<text size="18.0pt" fg="#DD72F2">}}THAT IS SIMILAR TO A BULLET LEAVING A GUN{{</text>}}*.

This easily beats the adults of the same species, which go about **{{<text size="16.0pt" fg="#A9D18E">}}20 times slower{{</text>}}** , and also is ***{{<text size="18.0pt" fg="#ED7D31">}}even speedier than the mantis shrimp{{</text>}}***.

And he's just a tiny little crustacean. Like seriously tiny, he is a **{{<text size="14.0pt" fg="yellow" bg="gray">}}few millimetres long{{</text>}}**.

The story is phenomenal. The researchers started off filming at **{{<text size="16.0pt" fg="#8FAADC">}}50,000 frames per second{{</text>}}** , the same they would use for an adult shrimp, and the claw movement
 was still a ***{{<text size="16.0pt" fg="#4472C4">}}total blur{{</text>}}***. The lead researcher on the team describes it beautifully. He said **{{<text size="18.0pt" fg="#A9D18E">}} Wow, these guys are really cooking. {{</text>}}**

![image](d86437d7d0ed5606.gif)

It was only when they increased the framerate to **{{<text size="20.0pt" fg="#00FFE3" bg="gray">}}300,000 frames per second{{</text>}}** {{<text size="20.0pt" fg="#00FFE3">}}{{</text>}}that they were able to actually see what on earth was going on.

So the whole snap takes just
 ***{{<text size="14.0pt" fg="#00B0F0">}}300 microseconds{{</text>}}***. For context --- a blink of an eye lasts around **{{<text size="18.0pt" fg="#C00000">}}500 times that long{{</text>}}**.

Very few animals can reach speeds that are this fast. One exception to that is the **{{<text size="18.0pt" fg="red" bg="black">}}Dracula ant{{</text>}}** {{<text size="18.0pt">}}{{</text>}}(mmm seems like another ant fact is needed soon) which can shut its jaws in only **{{<text size="16.0pt" fg="#FFC000">}}23 microseconds{{</text>}}**. BUT it is totally cheating because moving through air is easy peasy compared to moving through water.

So how do they do it? Well, these guys can do this because they have a **{{<text size="16.0pt" fg="#00B050">}}spring-like mechanism{{</text>}}** {{<text size="16.0pt" fg="#00B050">}}{{</text>}}in the *{{<text size="16.0pt" fg="#92D050">}}larger of their two claws{{</text>}}*. Funnily their Latin genus name is
 *Alpheus heterochaelis* --- hetero = different, chaelis = claw and when they say different, they ain't lying.

![Morphology and DNA analyses reveal a new cryptic snapping shrimp of the Alpheusheterochaelis Say, 1818 (Decapoda: Alpheidae) species complex from thewestern Atlantic - Scientific Publications of the Mus um national d'Histoire](morphology_and_dna_analyses_reveal_a_new.jpeg)

Look at the **{{<text size="14.0pt" fg="#F4B183">}}CHUNKY{{</text>}}** {{<text size="14.0pt" fg="#F4B183">}}{{</text>}}claw. I love the sea. You really can't make this stuff up.

Anyways, they have this spring-like mechanism, and when it is released it whips the claw closed. This creates a **{{<text size="16.0pt" fg="#B4C7E7" bg="blue">}}high-speed water jet{{</text>}}** {{<text size="16.0pt" fg="#203864">}}{{</text>}}and makes a very **{{<text size="18.0pt" fg="yellow" bg="black">}}loud popping sound{{</text>}}** {{<text size="18.0pt" fg="yellow">}}{{</text>}}which then startles potential predators and also stuns prey.

![Cat GIF - Cat Balloon Pop - Discover & Share GIFs](cat_gif_-_cat_balloon_pop_-_discover_sha.gif)

The noise is because of the formation of a very short-lived **{{<text size="18.0pt" fg="#DD72F2">}}cavitation bubble{{</text>}}** {{<text size="18.0pt" fg="#DD72F2">}}{{</text>}}(for the science-ly minded this is where the static pressure of a liquid reduces to below the liquid's vapour pressure, leading to the formation of small vapor-filled cavities in the liquid). Some think this is what happens
 when you crack your knuckles --- there are these **{{<text size="14.0pt" fg="#0070C0">}}cavitation bubbles{{</text>}}** {{<text size="14.0pt" fg="#0070C0">}}{{</text>}}forming in the **{{<text size="16.0pt" fg="#FFA830">}}synovial fluid{{</text>}}** {{<text size="16.0pt">}}{{</text>}}in your joints.

I, once more, digress.

This snap is SO FAST that is actually produces **{{<text size="18.0pt" fg="#FFD966">}}light{{</text>}}** {{<text size="18.0pt" fg="#FFD966">}}{{</text>}}and **{{<text size="18.0pt" fg="#FB7A00">}}plasma{{</text>}}** {{<text size="18.0pt" fg="red">}}{{</text>}}as well.

I"m going to say that again. It is ***{{<text size="18.0pt" fg="#A142EE">}}SO FAST{{</text>}}*** , it produces **{{<text size="18.0pt" fg="yellow" bg="blue">}}LIGHT{{</text>}}** {{<text size="18.0pt" fg="yellow">}}{{</text>}}and **{{<highlight size="18.0pt" fg="#FB7A00" bg="yellow">}}PLASMA{{</highlight>}}**.

![How Thick Can You Cut with Plasma Underwater](how_thick_can_you_cut_with_plasma_underw.jpeg)

This process has been mimicked by researchers to create **{{<text size="16.0pt" fg="#00FFE3">}}underwater plasma{{</text>}}** {{<text size="16.0pt" fg="#00FFE3">}}{{</text>}}that reaches nearly ***{{<text size="16.0pt" fg="red">}}1,700 degrees C{{</text>}}***. This is very handy for stuff like underwater drilling used to create **{{<text size="14.0pt" fg="#F4B183">}}geothermal wells{{</text>}}** {{<text size="14.0pt" fg="#F4B183">}}{{</text>}}that tap into the earth's natural heat.

And if you want to see more, there is some **{{<text size="16.0pt" fg="#DD72F2">}}highly ethically questionably content{{</text>}}** {{<text size="16.0pt" fg="#DD72F2">}}{{</text>}}on YouTube of people competing pistol shrimp against mantis shrimp I am not sure how I feel about it but 3.7 million people decided it was worth watching so if you want to here it is.
<a href="https://www.youtube.com/watch?v=kWLpiogS3IQ">![image](82671d87de25b052.png)</a>
{{< youtube kWLpiogS3IQ >}}

Thanks for tuning in. If you made it this far, here is your reward.

![lol dog gifs | WiffleGif](lol_dog_gifs_wifflegif.gif)

Best fishes,

Flora