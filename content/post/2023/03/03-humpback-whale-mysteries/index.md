+++
author = "Flora Judd"
title = "Humpback Whales Unveiled"
date = "2023-03-03"
description = "Exploring the fascinating development of humpback whale foetuses and their unique evolutionary traits."
tags = ["Humpback Whales","Foetuses","Baleen","Evolutionary Biology","Filter Feeders","Whale Anatomy","Marine Research","Natural History","Developmental Biology","Conservation Studies"]
categories = ["Marine Biology","Whales","Evolution","Cetacean Studies"]
image = "ca432bab4460384b.png"
+++

Hello lovely people and welcome to a *{{<text size="16.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

It has been a wee while hasn't it? I would promise greater consistency, but I don't want to. So let's focus on what really matters. ***{{<highlight size="14.0pt" fg="#ED7D31" bg="yellow">}}Humpback whale mouths{{</highlight>}}***.

![Humpback Whale Feeding GIFs | Tenor](humpback_whale_feeding_gifs_tenor.gif)
*Omnomnomnoooooom*
**
Humpback whales are unfaultably epic. They are **{{<text size="36.0pt" fg="#0070C0">}}ginormous{{</text>}}** , spend their time *{{<text size="16.0pt" fg="#00F4C9">}}slurping up the critters of the sea{{</text>}}* , and **{{<text size="16.0pt" fg="#00B0F0">}}sing to each other{{</text>}}** {{<text size="16.0pt" fg="#00B0F0">}}{{</text>}}as a means of flirting (I learnt from the best).

I bet you didn't think they could get cooler.

{{<u fg="#C00000">}}WRONG{{</u>}}.

![Upside Down Humpback Whale GIF - Whale Ani Animal - Discover & Share GIFs |Whale, Ocean animals, Animals](upside_down_humpback_whale_gif_-_whale_a.gif)
*Don't worry this is upside down --- I am just trying to melt your brain.*

I came across some research done on **{{<text size="18.0pt" fg="#FFC000">}}humpback whale foetuses{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}in the collection at the Natural History Museum in London. Weirdly enough, foetal development can often tell us a lot about the ***{{<text size="14.0pt" fg="#A9D18E">}}evolution{{</text>}}*** {{<text size="14.0pt" fg="#A9D18E">}}{{</text>}}of a species. Sometimes animals that evolve to ***{{<text size="16.0pt" fg="#ED7D31">}}lose{{</text>}}*** **{{<text size="16.0pt" fg="#ED7D31">}}a certain trait{{</text>}}** , still **{{<text size="16.0pt" fg="#70AD47">}}grow that trait in the womb{{</text>}}**. It just doesn't last all the way to birth.

At the NHM in London, they have seven humpback whale foetuses at ***{{<text size="14.0pt" fg="#7030A0">}}various stages of development{{</text>}}***. I know you are dying to see what a whale foetus looks like, so I have collated these pics from the Natural History Museum
 website.

![image](4fc61907daf217d7.png)

And this is the oldest one:

![image](ca432bab4460384b.png)

He ***{{<highlight size="18.0pt" bg="aqua">}}LOOKS LIKE A PERFECT BUT TINY WHALE{{</highlight>}}***. Isn't that so adorable?! And like only a
 tiiiiiny bit creepy 

Important information here is that humpback whales are **{{<text size="16.0pt" fg="#00B0F0">}}filter-feeders{{</text>}}** , meaning they take a BIG gulp of food, use **{{<text size="16.0pt" fg="#0070C0">}}special bristly plates{{</text>}}** {{<text size="16.0pt" fg="#0070C0">}}{{</text>}}(side note I am now developing a crockery company called
 *Special Bristly Plates* if anyone wants to invest just hit me up) to strain the water out, and then **{{<text size="16.0pt" fg="#00EDCF">}}monch on the tasty krill{{</text>}}** {{<text size="16.0pt" fg="#00EDCF">}}{{</text>}}and small fish left in their mouth. This is what the baleen look like:

![What is baleen? - Whale and Dolphin Conservation](what_is_baleen_-_whale_and_dolphin_conse.jpeg)

*{{<text fg="#0070C0">}}Not all whales{{</text>}}* {{<text fg="#0070C0">}}{{</text>}}have these funky plates. Some of them (like sperm whales and killer whales) **{{<text size="16.0pt" fg="#9DC3E6">}}have teeth instead{{</text>}}** {{<text size="16.0pt" fg="#9DC3E6">}}{{</text>}}and this is thought to be, what we Biologists like to call, the **{{<highlight size="14.0pt" bg="yellow">}}ancestral state{{</highlight>}}**. Meaning that all early whales will have *{{<text size="14.0pt" fg="#2F5597">}}started with teeth{{</text>}}* , and some species *{{<text size="16.0pt" fg="#50ABEE">}}went on{{</text>}}* {{<text size="16.0pt" fg="#50ABEE">}}{{</text>}}to evolve these baleen plates, rather than the other way around.

Amazingly, analysis of these little foetuses has showed that humpbacks do in fact **{{<text size="18.0pt" fg="#00B050">}}grow teeth{{</text>}}**. But they ***{{<highlight size="18.0pt" bg="aqua">}}REABSORB THEM INTO THEIR JAWS{{</highlight>}}*** , and then grow baleen plates. This transition from teeth-to-baleen occurs in the **{{<text size="16.0pt" fg="#00B0F0">}}final third{{</text>}}** {{<text size="16.0pt" fg="#00B0F0">}}{{</text>}}of their foetus lives (aka the period of gestation).

In slightly more science-y terms we are seeing **{{<text size="14.0pt" fg="#0070C0">}}mineralized tooth germs{{</text>}}** {{<text size="14.0pt" fg="#0070C0">}}{{</text>}}(these are pre-teeth growths) forming in the gums, but they never erupt. Instead, the whales reabsorb them and produce baleen instead.

Is that not *{{<text size="16.0pt" bg="fuchsia">}}absolute madness{{</text>}}* ??

![Whale GIFs | Tenor](whale_gifs_tenor.gif)

Gosh they are so cool.

Best fishes,

Flora

Here is your GIF of an animal falling over:

![Leopard Fall GIF - Leopard Fall Ground - Discover & Share GIFs](leopard_fall_gif_-_leopard_fall_ground_-.gif)