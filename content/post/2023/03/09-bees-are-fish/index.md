+++
author = "Flora Judd"
title = "Bees Are Fish"
date = "2023-03-09"
description = "Explore the quirky California classification of bumblebees as fish."
tags = ["Bees","Fish Classification","Endangered Species Act","Wildlife Protection","Pollinators","California Law","Biodiversity","Insect Conservation","Ecological Impact","Environmental Policy"]
categories = ["Nature","Wildlife","Environmental Policy","Ecology"]
image = "bumblebee_goby_brachygobius_doriae_speci.jpeg"
+++

Hello and welcome to another
 *{{<text size="16.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

This one is going to be
 **{{<highlight size="16.0pt" bg="aqua">}}exceptionally brief{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}and **{{<text size="16.0pt" bg="fuchsia">}}exceptionally beautiful{{</text>}}**.

We are looking at **{{<highlight size="18.0pt" bg="yellow">}}bees{{</highlight>}}**.

![Caribbean Reef Fish School | Stocksy United](caribbean_reef_fish_school_stocksy_unite.jpeg)

*{{<text size="14.0pt" fg="#00B0F0">}}You know, bees.{{</text>}}*

![School Of Fish Photograph by Danilovi | Fine Art America](school_of_fish_photograph_by_danilovi_fi.jpeg)

Oh, sorry, **{{<text size="18.0pt" fg="#A9D18E">}}this isn't California{{</text>}}** ?
 Ohhhh my mistake!

In California, and California only, in June 2022, some species of bumblebee ***{{<text size="20.0pt" fg="#FFC000">}}were classified as species of fish{{</text>}}*** , legally speaking.

![Episode 1 What GIF by The X-Files - Find & Share on GIPHY](episode_1_what_gif_by_the_x-files_-_find.gif)

Is that not **{{<text size="18.0pt" fg="#ED7D31">}}hilarious{{</text>}}** !?

There is logic behind the madness --- do not fear!

They did it because the
 **{{<text size="20.0pt" fg="#B740CF">}}California Endangered Species Act (CESA){{</text>}}** only protects *{{<text size="16.0pt" fg="#00B050">}}birds, mammals, fish, amphibians, reptiles, and plants{{</text>}}* 
 --- **{{<text size="14.0pt" fg="#0070C0">}}not insects{{</text>}}**. Bummer for all the insects.

Therefore, reclassifying bees as fish is a *{{<text size="16.0pt" fg="#9DC3E6">}}sneaky way{{</text>}}* {{<text size="16.0pt" fg="#9DC3E6">}}{{</text>}}of getting them protected because, the definition of fish (famously **{{<text size="16.0pt" fg="#92D050">}}there is no such thing as a fish{{</text>}}** , but according to the California Fish and Game Code) is any mollusc, crustacean, invertebrate (or) amphibian, .

So guys, you heard it here first, **{{<text size="18.0pt" fg="#00E5ED">}}bees are fish{{</text>}}**.

![Bumblebee Goby (Brachygobius doriae) Species Profile & Care Guide](bumblebee_goby_brachygobius_doriae_speci.jpeg)
*I mean this bumblebee goby is the next best thing right?*

Lots of love and best fishes (best bees?),

Flora

Here is your GIF
![Baby-sloth-falls-over](baby-sloth-falls-over.gif)