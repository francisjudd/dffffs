+++
author = "Flora Judd"
title = "Coconut Crab Power"
date = "2023-03-08"
description = "Exploring the incredible strength of coconut crabs and their unique biology."
tags = ["Coconut Crab","Strength","Crustacean Biology","Pinch Force","Arthropod Flexibility","Predator","Biological Adaptations","Ecological Role","Conservation","Field Studies"]
categories = ["Animals","Nature","Science","Crustacean Biology"]
image = "coconut_crabs_bone-crushing_grip_is_10_t.jpeg"
+++

Hello and welcome to another *{{<text size="18.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

We are back on our **{{<text size="18.0pt" fg="#ED7D31">}}pincer hype train{{</text>}}** and today we look at the ***{{<text size="16.0pt" fg="#FFC000">}}strongest of the strong{{</text>}}*** {{<text size="16.0pt" fg="#FFC000">}}{{</text>}}--- the **{{<highlight size="22.0pt" bg="yellow">}}coconut crab{{</highlight>}}** {{<text size="14.0pt">}}.{{</text>}}
{{<text size="14.0pt">}}{{</text>}}
Phwoar, it just
 *sounds* hench doesn't it?

![Claws celebre: Coconut crabs have world's strongest pinch, study ...](claws_celebre_coconut_crabs_have_worlds_.jpeg)

You can see that they are pretty **{{<text size="20.0pt" fg="#0070C0">}}beefy looking animals{{</text>}}**. They can get up to *{{<text size="22.0pt" fg="#00B0F0">}}50cm across{{</text>}}* {{<text size="22.0pt" fg="#00B0F0">}}{{</text>}}(seriously though think about how big that is) and weigh **{{<text size="20.0pt" fg="#9DC3E6">}}4 kilos{{</text>}}**. That is larger than *{{<highlight size="18.0pt" bg="aqua">}}any other arthropod living on land{{</highlight>}}*.

**{{<text bg="silver">}}SIDE NOTE{{</text>}}** {{<text bg="silver">}}: The largest land arthropod
 that has{{</text>}} ***{{<text size="20.0pt" fg="#70AD47" bg="silver">}}ever lived{{</text>}}*** {{<text size="20.0pt" fg="#70AD47" bg="silver">}}{{</text>}}{{<text bg="silver">}}was a{{</text>}} **{{<text size="18.0pt" fg="#00B050" bg="silver">}}millipede-looking animal{{</text>}}** {{<text size="18.0pt" fg="#00B050" bg="silver">}}{{</text>}}{{<text bg="silver">}}that lived about{{</text>}} **{{<text size="18.0pt" fg="#0DB029" bg="silver">}}300 million years ago{{</text>}}** {{<text size="18.0pt" bg="silver">}}{{</text>}}{{<text bg="silver">}}called{{</text>}} *{{<text size="18.0pt" fg="#548235" bg="silver">}}Arthropleura{{</text>}}* {{<text bg="silver">}}.
 Its fossil was found on a beach in north England and it was{{</text>}} **{{<text size="20.0pt" fg="#92D050">}}2.6m long{{</text>}}** {{<text size="20.0pt" fg="#92D050">}}{{</text>}}{{<text bg="silver">}}(the same length as a small car) and is believed to have weighed the same as a{{</text>}} **{{<text size="20.0pt" fg="#385723" bg="silver">}}large dog{{</text>}}** {{<text bg="silver">}}.
 I believe the scientific response to that is * **eugh** *. Also I adore the NHM's recreation of this animal because it looks like a school cut and stick project{{</text>}}:

![An artist's impression of Arthropleura on a beach](an_artists_impression_of_arthropleura_on.jpeg)

**{{<text size="16.0pt" bg="fuchsia">}}~BACK TO COCONUT CRABS~{{</text>}}** **{{<text size="16.0pt">}}{{</text>}}**

Firstly, I am seriously ***{{<text size="16.0pt" fg="#7030A0">}}baffled{{</text>}}*** {{<text size="16.0pt" fg="#7030A0">}}{{</text>}}by some people on the internet (I mean that's nothing new). There are videos where people give coconut crabs **{{<text size="18.0pt" fg="#C00000">}}items to crush{{</text>}}** {{<text size="18.0pt" fg="#C00000">}}{{</text>}}to show their
 *{{<text size="18.0pt" fg="yellow" bg="navy">}}incredible claw power{{</text>}}* {{<text fg="yellow">}}.{{</text>}}Of all the objects that they could have given this coconut crab to destroy in an impressive show of strength, they chose a **{{<text size="20.0pt" fg="#ED7D31">}}ball point pen{{</text>}}**.

![WATCH: The monster coconut crab with a pinch MORE powerful than a LION ...](watch_the_monster_coconut_crab_with_a_pi.jpeg)

You know, one of the *{{<text size="16.0pt" fg="#2E75B6">}}very few{{</text>}}* {{<text size="16.0pt" fg="#2E75B6">}}{{</text>}}items I can snap all by myself is a **{{<text size="14.0pt" fg="#9DC3E6">}}ball point pen{{</text>}}** ? And even more bizarre
 is **{{<highlight size="16.0pt" bg="yellow">}}HOW CLOSE THIS PERSON GETS THEIR FINGERS TO THESE LETHAL CRUSHING CLAWS{{</highlight>}}**.

![image](c9609c35309d3e56.png)
*Some people's brains intrigue me, they really do.*

Anyhoo.

Coconut crabs are related to **{{<text size="18.0pt" fg="#ED7D31">}}hermit crabs{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}of all things. And actually the juveniles ***{{<text size="16.0pt" fg="#FFC000">}}live in little shells{{</text>}}*** {{<text size="16.0pt" fg="#FFC000">}}{{</text>}}underwater for the first portion of their life. Then they climb on land, discard the shell and their carapace (exoskeleton) hardens. In fact, when they are juveniles they actually look *{{<text size="18.0pt" fg="#C55A11">}}thoroughly adorable{{</text>}}*.

![Juvenile coconut crabs (Birgus latro) use empty gastropod shells for ...](juvenile_coconut_crabs_birgus_latro_use_.jpeg)

It's only when they grow up that they start looking so **{{<text size="18.0pt" fg="#00B0F0">}}ridiculously hench{{</text>}}**. Remind you of anyone?

![Coconut Crab In Water / Water Juvenile Coconut Crab Relative Hermit Red ...](coconut_crab_in_water_water_juvenile_coc.jpeg)![Larry the Lobster | Nickelodeon | Fandom](larry_the_lobster_nickelodeon_fandom.png)

Just saying.

So some researchers decided to measure the pinching strength of these bad boys. Those claws are pretty huge --- makes sense.

What the researchers were not expecting to find is that they can pinch with over **{{<text size="20.0pt" fg="#FFC000">}}330kg of force{{</text>}}**. I am going to say that again.

**{{<text size="20.0pt" fg="yellow" bg="black">}}330KG OF FORCE.{{</text>}}** **{{<text size="20.0pt" fg="yellow">}}{{</text>}}**

That's the same as the **{{<text size="18.0pt" fg="#00B050">}}bite force of a LION{{</text>}}** **{{<text size="18.0pt">}}.{{</text>}}** **{{<text size="18.0pt" fg="#00B050">}}{{</text>}}**

If you take that **{{<text size="18.0pt" fg="#ED7D31">}}in relation to its body size{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}its stronger than all other animals, apart from only one. The animal that beats it is a ***{{<text size="18.0pt" fg="#92D050">}}saltwater crocodile{{</text>}}*** {{<text size="18.0pt" fg="#92D050">}}{{</text>}}(which to be fair has a bite to body size ratio similar to that of a
 *T. rex* so you win some you lose some you know).

***{{<text size="18.0pt" fg="#00CDCF">}}BUT STRONGER THAN A LION{{</text>}}***. Man, that's cool.

![Coconut crab's bone-crushing grip is 10 times stronger than ours | New ...](coconut_crabs_bone-crushing_grip_is_10_t.jpeg)
*Squeeze it. Squeeze it. Squeeeeeeeze it.*

The researchers did say that the research was challenging because they **{{<text size="18.0pt" fg="#FF893B">}}did get nipped a few times{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}themselves. That is especially challenging because the crab often *{{<text size="16.0pt" fg="#9B6EFB">}}doesn't want to let go{{</text>}}* {{<text size="16.0pt">}}{{</text>}}once it has got you.

![image](7deb41383815e4c4.png)
*Once again I did this without any professional software. I know.*

But there is a trick! If you ever get pinched by **{{<text size="16.0pt" bg="fuchsia">}}the strongest crustacean in the world{{</text>}}** {{<text size="16.0pt">}}{{</text>}}--- here is the advice. The Micronesians of the Line Islands have a strategy: ***{{<text size="20.0pt" fg="#00CDCF">}} gentle titillation of the under soft parts of the body with any light material will cause the crab to loosen its hold .{{</text>}}*** {{<text size="20.0pt" fg="#00CDCF">}}{{</text>}}

Gentle titillation guys, you know what that means.

**{{<text size="18.0pt" fg="#FF893B">}}TICKLE THE CRAB.{{</text>}}**

I love science, man.

![Are these the 20 most boring facts ever discovered? | Metro News](are_these_the_20_most_boring_facts_ever_.gif)
*Don't worry I know this is not a crab. This is a slow loris surprisingly there are very few GIFs of people tickling crabs on the internet.*

Okay, but Flora why does this crab need such crazy strength?

They are **{{<text size="18.0pt" fg="#FFC000">}}solitary animals{{</text>}}** and **{{<text size="18.0pt" fg="#C00000">}}very aggressive{{</text>}}**. And because, unlike their
 hermit cousins, they are *{{<text size="18.0pt" bg="silver">}}not constrained by having to stay small to fit in a shell{{</text>}}* ,
 they could evolve to be **{{<text size="22.0pt" fg="#00F132">}}massive{{</text>}}** {{<text size="22.0pt" fg="#00F132">}}{{</text>}}and **{{<text size="20.0pt" fg="#9B6EFB">}}hella strong{{</text>}}**.

They are also **{{<text size="18.0pt" bg="red">}}voracious predators{{</text>}}** , ripping the tough husks of coconuts apart (hence the name)
 and eating just about anything they can find. Sometimes this means taking down birds and small mammals.

![image](0c3c0120ca77f82a.png)
*OOF. Full video
<a href="https://www.youtube.com/watch?v=XIRfCoauxbo">here</a>
{{< youtube XIRfCoauxbo >}} if that is your bag. Not the best camera skillz but still pretty knarly.*
**
Phwoar that's a chunky crustacean.

Hope you enjoyed.

Best fishes,

Flora

Here is your GIF:
![Time Out GIF - Red Pandas Pandas Fall - Discover & Share GIFs](time_out_gif_-_red_pandas_pandas_fall_-_.gif)