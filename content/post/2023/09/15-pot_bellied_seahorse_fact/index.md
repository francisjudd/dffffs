+++
author = "Flora Judd"
title = "Pregnant Seahorse Males"
date = "2023-09-15"
description = "Exploring the astonishing role of male seahorses in pregnancy."
tags = ["Pot-bellied Seahorse","Male Pregnancy","Courtship","Embryogenesis","Convergent Evolution","Seagrass Meadows","Gender Roles","Marine Life","Reproductive Behavior","Unique Biology"]
categories = ["Marine Biology","Seahorses","Nature","Reproductive Biology"]
image = "big_bellied_seahorse_seahorse_world_tasm.jpeg"
+++

Hello and welcome to another
 *{{<text size="16.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,
Today, we look at a particularly exceptional creature, the **{{<text size="18.0pt" bg="fuchsia">}}pot-bellied (or big belly) seahorse.{{</text>}}**

![Big Bellied Seahorse --- Seahorse World | Tasmania | Australia](big_bellied_seahorse_seahorse_world_tasm.jpeg)

This little guy is actually one of the *{{<text size="16.0pt" fg="#00B0F0">}}largest seahorse species in the world{{</text>}}* , getting up to **{{<text size="20.0pt" fg="#ED7D31">}}35cm long{{</text>}}**. They live in shallow waters and really **{{<text size="18.0pt" fg="#00B050">}}love a good seagrass meadow{{</text>}}**.

Now, without a doubt, the coolest biological feature of the seahorse is that it is ***{{<highlight size="18.0pt" bg="yellow">}}the male that gets pregnant{{</highlight>}}***. That really never fails to blow my mind.

![Blow Mind Mind Blown GIF - Blow Mind Mind Blown Explode - Discover & ShareGIFs](blow_mind_mind_blown_gif_-_blow_mind_min.gif)
*This is actually me. True life.*

Some people find that a difficult concept. Surely, if one animal *{{<text size="18.0pt" fg="#9DC3E6">}}is getting pregnant{{</text>}}* , that means{{<text size="16.0pt" fg="#0070C0">}}it must be a female{{</text>}}? Not so! We declare things male or female based on the **{{<text size="18.0pt" fg="#00B0F0">}}size of the sex cells{{</text>}}** (or gametes) that they produce. If it makes
 **{{<text size="18.0pt" fg="#FFC000">}}sperm{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}(the smaller gamete), it's a male, no matter what happens next. If it makes **{{<text size="20.0pt" fg="#70AD47">}}eggs{{</text>}}** {{<text size="20.0pt" fg="#70AD47">}}{{</text>}}(the bigger gamete) it's a female.

But before that happens, there is still **{{<text size="18.0pt" fg="#7030A0">}}a lot of woo-ing to be done{{</text>}}** , including a very lengthy courtship dance. The male starts off by inflating his pouch to (and I quote){{<text size="22.0pt" fg="#ED7D31">}} balloon-like proportions{{</text>}}{{<text size="20.0pt" fg="#ED7D31">}} {{</text>}}. Poor man.

![Big-belly Seahorse - Species encyclopedia - Aquarium La Rochelle](big-belly_seahorse_-_species_encyclopedi.jpeg)

Mmmm so sexy.

He then uses **{{<text size="20.0pt" fg="#00B0F0">}}c{{</text>}}** **{{<text size="20.0pt" fg="#A9D18E">}}o{{</text>}}** **{{<text size="20.0pt" fg="#FFC000">}}l{{</text>}}** **{{<text size="20.0pt" fg="#00B0F0">}}o{{</text>}}** **{{<text size="20.0pt" fg="#ED7D31">}}u{{</text>}}** **{{<text size="20.0pt" fg="red">}}r{{</text>}}** **{{<text size="20.0pt">}}{{<text fg="#7030A0">}}c{{</text>}}{{<text fg="#00C700">}}h{{</text>}}{{<text fg="#FFD966">}}a{{</text>}}{{<text fg="#0070C0">}}n{{</text>}}{{<text fg="#8FAADC">}}g{{</text>}}{{<text fg="#A9D18E">}}e{{</text>}}{{</text>}}** {{<text size="20.0pt">}}{{</text>}}to woo her!! (I really wish human men had more interesting courtship techniques --- why aren't y"all changing colour?) He makes his pouch{{<text size="18.0pt" fg="#FFE699" bg="gray">}}a white/light yellow{{</text>}}{{<text size="18.0pt" fg="#FFE699">}}{{</text>}}(like in that picture above) and makes himself **{{<highlight size="18.0pt" bg="yellow">}}very yellow{{</highlight>}}**.

He approaches the female he likes the look of again and again, with *{{<text size="16.0pt" fg="#F4B183">}}his head tucked{{</text>}}* {{<text size="16.0pt" fg="#F4B183">}}{{</text>}}and his **{{<text size="18.0pt" fg="#00B050">}}fins fluttering{{</text>}}** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}(okay that sounds more like some men I know).

![Pot-bellied Seahorse - Hippocampus abdominalis](pot-bellied_seahorse_-_hippocampus_abdom.jpeg)

If the female likes him back she reciprocates with some of her own head tucking and fin fluttering and colour changing. Once it has been established that all parties are pretty keen, they start **{{<text size="18.0pt" fg="#00B0F0">}}swimming together{{</text>}}** , sometimes *{{<text size="20.0pt" fg="#0070C0">}}entwining their tails{{</text>}}* together, in a{{<text size="24.0pt" fg="#92D050">}} dance .{{</text>}}

![Synchronized-dancing GIFs - Get the best GIF on GIPHY](synchronized-dancing_gifs_-_get_the_best.gif)

They then dash to the surface and the female deposits her eggs into man's dilated pouch. He will release his sperm within the pouch to fertilise the eggs and he will continue to look after their kiddiwinkles
 for only about **{{<text size="18.0pt" fg="#7030A0">}}30 days{{</text>}}** {{<text size="18.0pt" fg="#7030A0">}}{{</text>}}until they are ready to go. He can have
 *{{<text size="20.0pt" fg="#A53DC6">}}up to 1,000 of them{{</text>}}* {{<text size="20.0pt">}}{{</text>}}in there!!

Now, there is a problem that occurs in any pregnancy. How do you get{{<text size="18.0pt" fg="#A9D18E">}}nutrients and oxygen{{</text>}}to your rapidly developing offspring and make sure that you get rid of all of the pesky *{{<text size="18.0pt" fg="#767171">}}carbon and nitrogen waste products{{</text>}}* {{<text size="18.0pt" fg="#767171">}}{{</text>}}that they produce? If you are an egg-layer it is easy, the **{{<text size="20.0pt" fg="#BE8FC9">}}shells are porous{{</text>}}** {{<text size="20.0pt">}}{{</text>}}so all of the gases can come and go without your input.

![image](6597a7b52a1ebc09.png)

Well, for us humans, we have a very handy thing --- the **{{<text size="20.0pt" fg="#C15442">}}placenta{{</text>}}**. So, how do seahorses do it if it is the males that are doing the pregnant-ing.

They actually develop some
 **{{<text size="18.0pt" fg="#FFC000">}}complex placental structures{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}over time, in a way that is actually
 *{{<text size="18.0pt" fg="#00B0F0">}}very similar{{</text>}}* {{<text size="18.0pt" fg="#00B0F0">}}{{</text>}}to human pregnancy.

They develop **{{<text size="18.0pt" fg="#BE8FC9">}}lots of small blood vessels{{</text>}}** {{<text size="18.0pt" fg="#BE8FC9">}}{{</text>}}in the pouch, like those that we see in human placentas, and so the distance between the father's bloody supply and the embryos decreases dramatically as the pregnancy goes on. This is important, because as the little fishies
 get ***{{<text size="20.0pt" fg="#7030A0">}}older{{</text>}}*** {{<text size="20.0pt" fg="#7030A0">}}{{</text>}}they need **{{<text size="18.0pt" fg="#A53DC6">}}more and more oxygen{{</text>}}**.

![Embryogenesis in the dwarf seahorse, Hippocampus zosterae (Syngnathidae) |Semantic Scholar](embryogenesis_in_the_dwarf_seahorse_hipp.png)
*This is the* *{{<text size="16.0pt" bg="fuchsia">}}embryogenesis{{</text>}}* *{{<text size="16.0pt">}}{{</text>}}* *of the seahorses --- starting from when they are basically a freckle on an egg yolk, all the way to teeny tiny seahorse.*

The dads will also supply very **{{<text size="16.0pt" fg="#92D050">}}energy-rich fats{{</text>}}** {{<text size="16.0pt" fg="#92D050">}}{{</text>}}and{{<text size="20.0pt" fg="#00C700">}}calcium{{</text>}}to help the embryos to build their tiny skeletons and even produce ***{{<text size="18.0pt" fg="yellow" bg="black">}}antibacterial{{</text>}}*** {{<text size="18.0pt" fg="yellow">}}{{</text>}}and ***{{<text size="18.0pt" fg="red" bg="maroon">}}antifungal{{</text>}}*** {{<text size="18.0pt" fg="red">}}{{</text>}}molecules to keep them safe from infection.

And then, birth. What happens there??

Well, with a week or so to go, the seahorse dads start producing **{{<text size="18.0pt" fg="#ED7D31">}}hatching signals{{</text>}}** , which cause the little tiny baby seahorses to *{{<text size="18.0pt" fg="#92D050">}}break out of their thin membranes{{</text>}}* {{<text size="18.0pt" fg="#92D050">}}{{</text>}}and start to **{{<highlight size="20.0pt" bg="aqua">}}swim around freely{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}inside the brood pouch. **{{<text size="16.0pt" fg="#FFC000">}}Oestrogen{{</text>}}** {{<text size="16.0pt" fg="#FFC000">}}{{</text>}}also plays a role in producing a cascade of signals that help to induce birth.

![Seahorses Giving Birth Is The Craziest Thing You'll See Today](seahorses_giving_birth_is_the_craziest_t.gif)
*{{<highlight bg="yellow">}}IS THAT NOT THE WILDEST THING YOU HAVE SEEN ALL DAY??{{</highlight>}}*

And amazingly, the genes involved in pregnancy are remarkably similar in both mammals, seahorses and other fish that also have pregnancies (the females though!) even though they **{{<text size="20.0pt" fg="#BE8FC9">}}all evolved separately{{</text>}}**. It seems to be a really remarkable case of *{{<text size="20.0pt" bg="fuchsia">}}convergent evolution{{</text>}}* {{<text size="20.0pt">}}{{</text>}}--- where the same final state evolves more than once from different starting points.

Now, the mums are not
 **{{<text size="20.0pt" fg="#FFC000">}}totally absent{{</text>}}** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}from the process, the eggs that get passed over to the dad do contain a *{{<highlight size="18.0pt" bg="yellow">}}good number of nutrients{{</highlight>}}* {{<text size="18.0pt">}}{{</text>}}in the egg yolk that feeds the developing embryo.

But it definitely does remain a bit of a mystery as to just
 *why* the females have handed the pregnancy role to males.

![image](4554098e7c905452.jpeg)

If I find out, I"ll let you know.

Hope you enjoyed another fun fact.

Lots of love,

Flora

![GIF of Pandas - Animal Gifs - gifs - funny animals - funny gifs](gif_of_pandas_-_animal_gifs_-_gifs_-_fun.gif)