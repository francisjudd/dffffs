+++
author = "Flora Judd"
title = "Feminism in Animal Sex"
date = "2022-04-04"
description = "Exploring cryptic female choice in animal reproduction."
tags = ["Cryptic Female Choice","Sexual Coercion","Animal Behavior","Paternity","Dominance Hierarchy","Sperm Selection","Runaway Selection","Reproductive Strategies","Evolution","Females in Nature"]
categories = ["Biology","Feminism","Animal Behavior","Sexual Selection"]
image = "why_fruit_fly_sperm_are_giant.jpeg"
+++

Hello and welcome to a
 *{{<text size="18.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* {{<text size="18.0pt">}},{{</text>}}

*CONTENT WARNING* ---
 *this email contains images of fruit flies having SEX (*audible gasp*) you"ve been warned* * * **

Today we are going to have a look at one of the very few examples of **{{<text size="20.0pt" fg="red">}}feminism{{</text>}}** {{<text size="20.0pt" fg="red">}}{{</text>}}in *{{<text size="20.0pt" fg="#00B0F0">}}animal sex{{</text>}}* {{<text size="20.0pt" fg="#00B0F0">}}{{</text>}}within the natural world. **{{<highlight size="24.0pt" bg="yellow">}}Cryptic female choice{{</highlight>}}**.

It sounds cool right?

I think this has come up in a fact before, but for a quick recap --- *{{<text size="20.0pt" fg="#0070C0">}}cryptic female choice{{</text>}}* (or CFC to us *{{<text size="14.0pt">}}kool{{</text>}}* {{<text size="14.0pt">}}*kids*{{</text>}}of biology) is a **{{<highlight size="18.0pt" bg="aqua">}}female-driven{{</highlight>}}** , *{{<text size="22.0pt" fg="#FFD966">}}post-mating{{</text>}}* {{<text size="22.0pt" fg="#FFD966">}}{{</text>}}mechanism that females use to **{{<text size="24.0pt" fg="#ED7D31">}}systematically bias{{</text>}}** {{<text size="24.0pt" fg="#ED7D31">}}{{</text>}}the fertilisation of her eggs in favour of a ***{{<text size="20.0pt" fg="#A9D18E">}}certain male{{</text>}}***.

This essentially allows females to have a say in the **{{<text size="18.0pt" fg="#00B050">}}paternity{{</text>}}** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}of their offspring. This is pretty good for them, as in the animal kingdom there is a LOT of ***{{<text size="20.0pt" bg="fuchsia">}}sexual coercion{{</text>}}***. So even though a female might be forced to mate with a male that she doesn't want to, she can choose that that
 male *{{<text size="20.0pt" fg="#FFC000">}}does not fertilise her eggs{{</text>}}*.

**{{<text size="16.0pt" fg="#548235">}}PRETTY neat{{</text>}}** {{<text size="16.0pt" fg="#548235">}}{{</text>}}if you ask me.

This can be seen in jungle fowl (which kinda look like the wild ancestors of chickens).

![To cull or not to cull --- the Red Junglefowl? --- Bird Ecology Study Group](to_cull_or_not_to_cull_the_red_junglefow.jpeg)

Here, females will always mate with **{{<text size="20.0pt" fg="#0070C0">}}dominant{{</text>}}** {{<text size="20.0pt" fg="#0070C0">}}{{</text>}}males if they can. If subdominant males force females into having sex with them, the females will quite simply *{{<text size="20.0pt" fg="#ED7D31">}}eject their sperm{{</text>}}*. They don't want it. They won't have it.

And they can eject up to about **{{<text size="22.0pt" fg="#FFD966" bg="blue">}}80%{{</text>}}** of that sperm which is pretty badass!

Now put your biology hat on.

![image](d4356ce745c60cd3.png)
( *side note I edited this image in my notes and now the image won't delete --- this is not a joke send help my exam revision notes now look like this and I can't change them):*
**
*![image](d0b17a015fbab67b.png)* **

**{{<text size="16.0pt">}}ANYWAY.{{</text>}}**

Now you"ve got your biology hat on we can think about this a bit more sensibly. How do we know that the{{<text size="16.0pt" fg="#70AD47">}}sperm{{</text>}}from these subdominant males isn't just a bit **{{<text size="18.0pt" fg="#ED7D31">}}rubbish{{</text>}}** ? How do we know that it is the *{{<highlight size="18.0pt" bg="aqua">}}DOMINANCE{{</highlight>}}* {{<text size="18.0pt">}}{{</text>}}of the males that is causing the ejection?

Well if you do some experimental **{{<highlight size="16.0pt" bg="yellow">}}jiggery pokery{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}and *{{<text size="18.0pt" fg="#A9D18E">}}change the social dominance hierarchy{{</text>}}* {{<text size="18.0pt" fg="#A9D18E">}}{{</text>}}in the group (i.e. taking out the big dominant males so that subordinate males take over) then you see that the females **{{<text size="20.0pt" fg="#4472C4">}}CHANGE{{</text>}}** {{<text size="20.0pt" fg="#4472C4">}}{{</text>}}which sperm they choose to eject.

WOWZA.

A very interesting consequence of this is that it can lead to what we like to call{{<text size="24.0pt" bg="red">}} {{</text>}}{{<text size="28.0pt" bg="red">}}runaway selection{{</text>}}{{<text size="24.0pt" bg="red">}} .{{</text>}}{{<text size="24.0pt">}}{{</text>}}

![Collection of my favorite gifs | Running away gif, Funny animals, Animals](collection_of_my_favorite_gifs_running_a.gif)

This means that if females keep picking males with *{{<text size="18.0pt" fg="#00B050">}}certain qualities{{</text>}}* {{<text size="18.0pt" fg="#00B050">}}{{</text>}}to fertilise their eggs, then that trait is going to get selected and amplified **{{<text size="22.0pt">}}again{{</text>}}** and **{{<text size="36.0pt">}}again{{</text>}}**.

This happens because females produce sons that *{{<text size="18.0pt" fg="#00B0F0">}}have the trait{{</text>}}* {{<text size="18.0pt" fg="#00B0F0">}}{{</text>}}and daughters that *{{<text size="18.0pt" fg="#0070C0">}}find the trait attractive{{</text>}}* {{<text size="18.0pt" fg="#0070C0">}}{{</text>}}so it gets bigger and bigger. This is why you have some crazy sexual ornaments that evolve 

![Being Explicit about Resources will (has?) Revolutionize(d) SexualSelection --- Ambika Kamath](being_explicit_about_resources_will_has_.jpeg)![Sexual selection in birds - Wikipedia](sexual_selection_in_birds_-_wikipedia.jpeg)![Biological Ornamentation | DragonflyIssuesInEvolution13 Wiki | Fandom](biological_ornamentation_dragonflyissues.jpeg)![Researchers rack up "tough" secrets from deer antlers](researchers_rack_up_tough_secrets_from_d.jpeg)
*(PS I do not recommend Googling images for sexual ornaments unless you want a very raunchy Christmas tree next year)*

But this doesn't only happen on the scale of a **{{<highlight size="16.0pt" bg="aqua">}}whole animal{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}(like those photos above) but also at the
 **{{<highlight size="18.0pt" bg="yellow">}}gametic level{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}--- i.e. at the level of the sex cells themselves.

If you are a fruit fly (a *Drosophila* ) --- I"m afraid that **{{<text size="18.0pt" fg="#FFC000">}}size does matter{{</text>}}**. CFC seems to select males that have *{{<text size="18.0pt" fg="#00B0F0">}}longer sperm{{</text>}}* and the males that have longer sperm are also more likely to be able to **{{<text size="24.0pt" fg="#4472C4">}}displace{{</text>}}** any sperm from males that she might have already mated with. In one species,
 *Drosophila bifurca* , this has led to evolution of the **{{<text size="48.0pt" fg="#ED7D31">}}longest sperm cell{{</text>}}** {{<text size="48.0pt" fg="#ED7D31">}}{{</text>}}in the animal kingdom.

This little guy is **{{<text size="22.0pt" fg="#00B050">}}3mm{{</text>}}** {{<text size="22.0pt" fg="#00B050">}}{{</text>}}long.

![Why fruit fly sperm are giant](why_fruit_fly_sperm_are_giant.jpeg)

Wanna know how long his sperm cells are?

UP TO **{{<highlight size="26.0pt" bg="yellow">}}58.3mm{{</highlight>}}** {{<text size="26.0pt">}}{{</text>}}long.

![Fruit fly's giant sperm is quite an exaggeration | Science News](fruit_flys_giant_sperm_is_quite_an_exagg.jpeg)

*{{<text size="20.0pt" bg="fuchsia">}}YES THAT IS TWENTY TIMES HIS LENGTH.{{</text>}}* *{{<text size="20.0pt">}}{{</text>}}*

That is like the average male human having a sperm cell that is over **{{<text size="20.0pt" fg="#ED7D31">}}30 METRES LONG{{</text>}}**.
That's a lot.

Hope you enjoyed and feel more empowered if you are a woman and more insignificant at the length of your sperm cells if you are a man.

Lots of love,

Flora

P.S. Here's your animal-falling-over GIF.

![Wash hand , after eating : r/gifs](wash_hand_after_eating_rgifs.gif)