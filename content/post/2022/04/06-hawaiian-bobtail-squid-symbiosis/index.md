+++
author = "Flora Judd"
title = "Glow of the Squid"
date = "2022-04-06"
description = "Discover the remarkable life of Hawaiian bobtail squid and their glowing symbionts."
tags = ["Hawaiian Bobtail Squid","Vibrio fischeri","Bioluminescence","Symbiotic Bacteria","Moonlight Mimicry","Night Hunting","Marine Ecosystem","Bacterial Symbiosis","Adaptations","Ecological Relationships"]
categories = ["Marine Biology","Symbiosis","Squid","Bioluminescence"]
image = "hawaiian_bobtail_squid_takes_down_a_larg.jpeg"
+++

Hello and welcome to another *{{<text size="18.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* {{<text size="18.0pt">}},{{</text>}}

Today, because I need it, we are returning to the glorious,{{<highlight size="18.0pt" bg="yellow">}}glowing{{</highlight>}}{{<text size="18.0pt">}}{{</text>}}marine realm and taking a look at the
 **{{<text size="22.0pt" fg="#ED7D31">}}Hawaiian bobtail squid{{</text>}}**. Yes --- it is totally as adorable as it sounds.


![Bacterial World](bacterial_world.jpeg)

*He's* *{{<text size="20.0pt">}}SO SMOL{{</text>}}* *.*
**
Look at him taking down this shrimp like a badass

![Hawaiian bobtail squid takes down a larger shrimp. : pics](hawaiian_bobtail_squid_takes_down_a_larg.jpeg)

Now, I"m not even **{{<text size="20.0pt" fg="red">}}squidding{{</text>}}** , these guys get even cooler. Because --- like everything near and dear to me --- they ***{{<highlight size="22.0pt" bg="aqua">}}glow{{</highlight>}}*** {{<text size="22.0pt">}}{{</text>}}(apart from Clare --- you need to work on that).

The reason that they do this is very cool, they hunt at night and therefore hunt at **{{<text size="18.0pt" fg="#4472C4">}}moonlight{{</text>}}**. But if they **{{<text size="16.0pt" fg="#8FAADC">}}block{{</text>}}** out the very little light that there is when they are about to attack their prey,
 that is not very subtle, and so they glow to *{{<text size="18.0pt" fg="#00B0F0">}}mimic the moonlight{{</text>}}* {{<text size="18.0pt" fg="#00B0F0">}}{{</text>}}so that their prey cannot tell that they are there.

**{{<highlight size="18.0pt" bg="aqua">}}COOL, HUH?{{</highlight>}}** **{{<text size="18.0pt">}}{{</text>}}**

![Hawaiian Bobtail Squid | Beautiful sea creatures, Deep sea ...](hawaiian_bobtail_squid_beautiful_sea_cre.jpeg)

But what is even cooler is that they harbour these little **{{<text size="18.0pt" bg="fuchsia">}}bioluminescent bacteria{{</text>}}** {{<text size="18.0pt">}}{{</text>}}to do the glowing part for them. These guys are called *{{<text size="24.0pt" fg="#7030A0">}}Vibrio fischeri{{</text>}}* {{<text size="24.0pt" fg="#7030A0">}}{{</text>}}and they live inside a **{{<text size="22.0pt" fg="#FFC000">}}specialised organ{{</text>}}**.

But how do they get there?

Some animals that are super reliant on symbionts actually pass them down **{{<text size="20.0pt" fg="#A9D18E">}}from parent to child{{</text>}}**. This is a way of making sure that your offspring have *{{<text size="20.0pt" fg="#C00000">}}exactly what they need{{</text>}}* (I mean MY parents didn't give me any bioluminescent symbionts, but whatever I"m not that bitter about it or anything).

This is **{{<text size="16.0pt" fg="#44546A">}}especially important{{</text>}}** {{<text size="16.0pt" fg="#44546A">}}{{</text>}}if you're --- I don't know --- an ***{{<text size="18.0pt" fg="#00B050">}}aphid{{</text>}}*** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}and you eat something *{{<text size="20.0pt" fg="#4472C4">}}really dumb{{</text>}}* {{<text size="20.0pt" fg="#4472C4">}}{{</text>}}and nutritionally devoid like **{{<text size="18.0pt" fg="#92D050">}}plant sap{{</text>}}**. You{{<text size="22.0pt" fg="#FFC000">}}REALLY{{</text>}}need those bacteria to make your essential nutrients for you so you get them all the way from birth.

![Universitat de Barcelona - The UB forms part of the ...](universitat_de_barcelona_-_the_ub_forms_.jpeg)
*Don't ask me why there's another aphid coming out of its butt. They lay eggs --- it makes no sense.*

But this guy doesn't do that. He's born with 0 bacteria in his little light organ. So he has to do something **{{<text size="18.0pt" fg="#00B0F0">}}pretty remarkable{{</text>}}**.

A baby bobtail squid hatches out and has to suss out getting some of these neat little bacteria in its light organ. These bacteria are present in the seawater at a concentration of about **{{<text size="24.0pt" fg="#ED7D31">}}100 per ml{{</text>}}**.

That might sound like quite a lot. But there are{{<text size="22.0pt" fg="#70AD47">}}1,000,000{{</text>}}bacteria in every ml of seawater (enjoy thinking of that on your next holiday).

![Beautiful young smiling woman swimming underwater in the ...](beautiful_young_smiling_woman_swimming_u.jpeg)
*Would she still be smiling if she knew how many bacteria were in her eyes?*

**{{<text size="16.0pt" bg="fuchsia">}}The odds aren't so good.{{</text>}}** **{{<text size="16.0pt">}}{{</text>}}**

But the squid **{{<text size="22.0pt" fg="#92D050">}}does not give up.{{</text>}}** {{<text size="22.0pt" fg="#92D050">}}{{</text>}}

![NEVER GONNA GIVE YOU UP](never_gonna_give_you_up.gif)

When its born it encounters a couple of friendly bacteria in the first few seconds. It then uses these *{{<text size="20.0pt" fg="#ED7D31">}}weird hairy arm-looking things{{</text>}}* {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}to waft the bacteria into those **{{<highlight size="18.0pt" bg="yellow">}}pores{{</highlight>}}** at the bottom there (don't worry --- once the bacteria is established
 the weird hairy arms fall off although actually I don't know if that makes things better or worse).

![image](7b578c6a0312ca36.png)

And once inside the squid has to make sure that **{{<text size="26.0pt" fg="#ED7D31">}}only{{</text>}}** the lovely, glowing
 *Vibrio fischeri* are the ones that grow.

We really **{{<text size="18.0pt" fg="#FFC000">}}don't understand{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}this yet. We think it maybe **{{<text size="18.0pt" fg="#C00000">}}kills{{</text>}}** {{<text size="18.0pt" fg="#C00000">}}{{</text>}}some of the competing cells with its immune system, we think that maybe the squid **{{<text size="18.0pt" fg="#4472C4">}}feeds{{</text>}}** the bacteria with some yummy mucus but really *{{<text size="20.0pt" fg="#70AD47">}}we have no idea.{{</text>}}*

But almost every time, the squid is able to pick one bacteria, just **{{<highlight size="22.0pt" bg="aqua">}}ONE{{</highlight>}}** {{<text size="22.0pt">}}{{</text>}}out of literally **{{<highlight size="26.0pt" bg="aqua">}}THOUSANDS{{</highlight>}}** , and encourage it to grow and colonise its light organ. There are NO OTHER
 SPECIES that grow inside these little organs. WoW.

*{{<text size="20.0pt">}}SIDE NOTE{{</text>}}*
I think this is **hilarious**. At dawn, when the squid stop hunting, they eject **{{<text size="22.0pt" fg="#FFC000">}}95% of these bacteria{{</text>}}**. SPLOOP. Out you go.

And then they bury themselves in the sand to rest.

<a href="http://fuckyeahcuttlefish.tumblr.com/page/8">![fuck yeah, cuttlefish!](fuck_yeah_cuttlefish.gif)</a>{{<text size="10.0pt" fg="whitesmoke">}}<a href="http://fuckyeahcuttlefish.tumblr.com/page/8">{{<text fg="blue">}}{{</text>}}</a>{{</text>}}
By the time it's the next day, that remaining **{{<text size="18.0pt" bg="fuchsia">}}5%{{</text>}}** has reproduced like *{{<text size="18.0pt">}}billy-o{{</text>}}* {{<text size="18.0pt">}}{{</text>}}and the squid is all **{{<text size="20.0pt" bg="lime">}}recharged{{</text>}}** and *{{<text size="18.0pt" bg="lime">}}ready to go again{{</text>}}*.

This is probably for two important reasons --- to help **{{<text size="22.0pt" fg="#7030A0">}}control{{</text>}}** {{<text size="22.0pt" fg="#7030A0">}}{{</text>}}the bacterial population and CRUCIALLY to make sure that there are ***{{<highlight size="22.0pt" bg="yellow">}}enough{{</highlight>}}*** {{<text size="22.0pt">}}{{</text>}}of the friendly glowing bacteria in the water for baby squid to find some when they hatch. Kind of like a daily top up of bacteria into the water to help out your mates 

It's such a weird and wonderful world out there.

![Bobtails + Bacteria = BFF - Science Friday](bobtails_bacteria_bff_-_science_friday.gif)

Lots of love,

Flora xxx

Here's your animal falling over:

![GIF of Pandas - Animal Gifs - gifs - funny animals - funny gifs](gif_of_pandas_-_animal_gifs_-_gifs_-_fun.gif)