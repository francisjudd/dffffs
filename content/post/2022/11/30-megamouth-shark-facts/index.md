+++
author = "Flora Judd"
title = "Meet the Megamouth"
date = "2022-11-30"
description = "Discover the quirky world of the elusive megamouth shark."
tags = ["Megamouth Shark","Planktivorous","Marine Life","Biodiversity","Rare Species","Underwater","Distinctive Features","Ecological Role","Shark Behavior","Conservation"]
categories = ["Marine Biology","Sharks","Sea Creatures","Wildlife"]
image = "rare_megamouth_shark_washes_up_in_philip.jpeg"
+++

Hello and welcome to another *{{<text size="20.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

I would say sorry for doing
{{<u size="18.0pt" fg="#4472C4">}}another one about fish{{</u>}}, but you know who I am and you know what I"m like and so you know what you signed up for.

*{{<highlight bg="yellow">}}No you cannot unsubscribe.{{</highlight>}}* **
This is about the amazing and elusive **{{<text size="20.0pt" fg="#00B0F0">}}megamouth shark{{</text>}}**.

![Megamouth shark | Prehistoric Wiki | Fandom](megamouth_shark_prehistoric_wiki_fandom.jpeg)

They have these slightly weird looking massive mouths and generally look kind of comical. Less **{{<text size="18.0pt" fg="red" bg="black">}}stealth-killer-shark{{</text>}}** {{<text size="18.0pt" fg="red">}}{{</text>}}and more **{{<text size="24.0pt" fg="#EEAEF1">}}blobfish{{</text>}}**.

![Rare Megamouth Shark Washes Up in Philippines](rare_megamouth_shark_washes_up_in_philip.jpeg)![image](81fede18f3d47ca9.png)

I think the Wikipedia page sums it up quite nicely: a relatively poor swimmer, the megamouth has a **{{<text size="22.0pt" fg="#92D050">}}soft, flabby body{{</text>}}** and a *{{<text size="20.0pt" fg="#12CAED">}}long, wide, bulbous head{{</text>}}*. It swims at an average speed of about **{{<text size="22.0pt" bg="fuchsia">}}2 km per hour{{</text>}}**.

![Shark National Geographic GIF - Shark National Geographic Slow Shark -Discover & Share GIFs](shark_national_geographic_gif_-_shark_na.gif)

It is the smallest of the
 **{{<text size="18.0pt" fg="#ED7D31">}}three planktivorous sharks{{</text>}}** , the others being ***{{<text size="14.0pt" fg="#5B9BD5">}}basking sharks{{</text>}}*** {{<text size="14.0pt" fg="#5B9BD5">}}{{</text>}}and ***{{<text size="16.0pt" fg="#00FDF5">}}whale sharks{{</text>}}*** , reaching a maximum of about **{{<u size="18.0pt" fg="#002060">}}5m{{</u>}}**. It was only discovered in **{{<highlight size="18.0pt" bg="yellow">}}1976{{</highlight>}}** , which really is not very long ago at all (as I"m sure Chris and Clare would be able to confirm). Amazingly, when it was discovered
 this shark was not only a new *{{<highlight size="20.0pt" bg="yellow">}}species{{</highlight>}}* , but also a new **{{<text size="22.0pt" fg="#FFC000">}}genus{{</text>}}** {{<text size="22.0pt" fg="#FFC000">}}{{</text>}}and a whole **{{<text size="26.0pt" fg="yellow" bg="blue">}}new family{{</text>}}**.

This makes it, along with the **{{<text size="16.0pt" fg="#00F59F">}}coelacanth{{</text>}}** {{<text size="16.0pt">}}{{</text>}}(which is another phenomenal fish that might need its own fun fact), one of the ***{{<text size="20.0pt" bg="lime">}}two major excitements{{</text>}}*** {{<text size="20.0pt">}}{{</text>}}for the world's fish nerds in the 20th century.

![Megamouth shark | Wildlife Online](megamouth_shark_wildlife_online.jpeg)

As of 2018, only **{{<u size="20.0pt" fg="#7CA8F0">}}99 individuals{{</u>}}** {{<text size="20.0pt" fg="red">}}{{</text>}}had been *{{<highlight size="20.0pt" bg="aqua">}}caught or sighted{{</highlight>}}* ever. Isn't that crazy?? We know so little about this fabulous oddity.

What I did find though, was an amazing encounter where a scuba diver was lucky enough to see one during the day in Komodo, Indonesia.

![seatrench Megamouth Shark (source)](seatrench_megamouth_shark_source.gif)

**{{<text size="20.0pt" fg="#92D050">}}IMAGINE HOW BLOWN YOUR MIND WOULD BE.{{</text>}}**

Okay you"ll have to imagine how blown
 *my* mind would be.

Another cool thing that they have is these crazy **{{<text size="20.0pt" fg="white" bg="black">}}white bands{{</text>}}** {{<text size="20.0pt" fg="white">}}{{</text>}}on their upper lip. This white band, hidden in a groove between their snout and the jaw, is only visible when their upper jaw is stickin" out.

Scientists got to thinking that this is probably linked to ***{{<text size="18.0pt" fg="#FFC000">}}feeding{{</text>}}***.

![Megachasma pelagios --- Discover Fishes](megachasma_pelagios_discover_fishes.jpeg)

Some research carried out by a team of Belgian scientists found that, instead of producing light like some people thought, the band seems to be super reflective. This means it even reflects the light **{{<text size="20.0pt" fg="yellow" bg="gray">}}produced by bioluminescent planktonic prey{{</text>}}**.

So whilst it first SEEMED like it was producing light, it is just **{{<text size="20.0pt" fg="#FFC000">}}so reflective{{</text>}}** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}it bounces back almost any light that reaches it. This probably then attracts ***{{<text size="18.0pt" fg="#92D050">}}MORE plankton{{</text>}}***. Neat, right?

Hope you enjoyed another weirdo from the sea (and I"m not talking about me).

Lots of love,

Flora


![Latest Polar Bear GIFs | Gfycat](latest_polar_bear_gifs_gfycat.gif)