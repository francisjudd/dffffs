+++
author = "Flora Judd"
title = "Chainsaw-Faced Wonders"
date = "2022-11-16"
description = "Explore the fascinating differences between sawsharks and sawfish."
tags = ["Sawshark","Sawfish","Marine Life","Fish Classification","Wildlife Education","Behavioral Differences","Morphological Traits","Ecological Roles","Marine Ecosystems","Fish Species Diversity"]
categories = ["Marine Biology","Wildlife Education","Fish Species","Ecology"]
image = "c348eca033040421.png"
+++

Hello and welcome to a
 *{{<text size="16.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

Today you are going to have a wild and crazy fact about two kinds of fish that **{{<text size="14.0pt" fg="#00B0F0">}}LOOK{{</text>}}** {{<text size="14.0pt" fg="#00B0F0">}}{{</text>}}very similar but are actually **{{<text size="16.0pt" fg="#0070C0">}}DIFFERENT{{</text>}}** {{<text size="16.0pt" fg="#0070C0">}}{{</text>}}species, but both of them are freaking gnarly.

Okay, so first things first, today we are having a look at the gloriously wonderful ***{{<highlight size="16.0pt" bg="aqua">}}sawshark{{</highlight>}}***.

This is what a sawshark looks like:
![image](c348eca033040421.png)

As you can see, he essentially has a **{{<text size="18.0pt" fg="#FFD966" bg="maroon">}}chainsaw coming out of the front of his face{{</text>}}**.
 If that isn't cool, I literally do not know what is.

*{{<text size="14.0pt" fg="#00B050">}}BUT HE"S NOT THE ONLY ONE.{{</text>}}*

This is what a ***{{<text size="18.0pt" fg="#DEEBF7" bg="blue">}}sawfish{{</text>}}*** {{<text size="18.0pt" fg="#DEEBF7">}}{{</text>}}looks like:

![Green Sawfish | NOAA Fisheries](green_sawfish_noaa_fisheries.jpeg)

You would be forgiven for mistaking one for the other because I can see how the **{{<text size="16.0pt" fg="#ED7D31">}}giant weapon{{</text>}}** {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}on the front of its head could be distracting when undergoing any further species identification. But they key difference here is that **{{<text size="18.0pt" fg="#FFC000">}}sawsharks{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}are (you guessed it) a kind of **{{<text size="18.0pt" fg="#00FAF5">}}shark{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}and **{{<text size="22.0pt" fg="#548235">}}sawfish{{</text>}}** {{<text size="22.0pt" fg="#548235">}}{{</text>}}are A type of **{{<text size="20.0pt" fg="#9FF766">}}ray{{</text>}}** ! Obviously.

The key differences that allow us to tell them apart are the fact that the sawshark has these little ***{{<text size="18.0pt" fg="#9DC3E6">}}barbels{{</text>}}*** {{<text size="18.0pt" fg="#9DC3E6">}}{{</text>}}(not to be confused with barbells Charlie) coming out the side of its rostrum.

![image](161d31f82161ccf9.png)
![Sawfish or Sawshark?](sawfish_or_sawshark.jpeg)
*Which could it be ?*

Also the{{<text size="18.0pt" fg="#39ABFD">}}gill slits{{</text>}}. The sawshark, like all sharks, has the gill slits on the ***{{<text size="18.0pt" bg="fuchsia">}}sides of its body{{</text>}}*** , whilst the sawfish, like all rays, has its gill slits on the ***{{<highlight size="20.0pt" bg="yellow">}}underside of its body{{</highlight>}}*** like this.

![image](ffbaa54b5e0ae730.png)

Sawfish are also freaking
 **{{<text size="24.0pt">}}MASSIVE{{</text>}}**. Whilst a sawshark can reach about **{{<text size="20.0pt" fg="#70AD47">}}1.5m{{</text>}}** {{<text size="20.0pt" fg="#70AD47">}}{{</text>}}long, the sawfish reaches a ridonkulous
 **{{<text size="22.0pt" fg="#FFC000">}}7m{{</text>}}** long.

But both of them rely on using their crazy face blade for attacking prey and rivals, as you can see here:

![Sawfish GIFs - Find & Share on GIPHY](sawfish_gifs_-_find_share_on_giphy.gif)
*Omnomnomnomnom*
**
So what are these pointy things on its face? Are they **{{<text size="18.0pt" fg="#FFC000">}}teeth{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}that have **{{<text size="18.0pt" fg="#ED7D31">}}migrated out of its mouth{{</text>}}** ? Well, not quite.

We need to take this back a second. You might have heard that shark skin is super rough --- almost sharp --- and that is because of the structure of their scales . These are very special and are called **{{<highlight size="20.0pt" bg="yellow">}}dermal denticles{{</highlight>}}** **{{<text size="20.0pt">}}{{</text>}}** (anyone want to start a band with me called dermal denticles???) because they are actually modified teeth, complete with dentine and enamel.

![image](2e99cf75442f242e.jpeg)

This is what they look like. They act kind of like **{{<text size="18.0pt" fg="#92D050">}}chainmail{{</text>}}** , providing protection, and also hugely reducing drag to allow for **{{<text size="20.0pt" fg="#FC43D1">}}sPeEdY sWiMmInG{{</text>}}**.

Research published in 2015, seems to think that these teeth on the rostrum are modified dermal denticles. So they are **{{<highlight size="20.0pt" bg="yellow">}}modified, modified teeth{{</highlight>}}**.

![Antique 20th Century Edwardian Mounted Sawfish Rostrum, circa 1900 at ...](antique_20th_century_edwardian_mounted_s.jpeg)

But both sawsharks and sawfish give birth to **{{<text size="14.0pt">}}LIVE YOUNG{{</text>}}**. Ouch. Ouch. Ouch. Those
 poor mothers.{{<text size="13.5pt">}}{{</text>}}
{{<text size="13.5pt">}}{{</text>}}
Do not fear! Evolution to the rescue.{{<text size="13.5pt">}}{{</text>}}
{{<text size="13.5pt">}}{{</text>}}
Sawsharks are born with their spikes all **{{<text size="16.0pt" fg="#5B9BD5">}}folded over{{</text>}}** {{<text size="16.0pt" fg="#5B9BD5">}}{{</text>}}nice
 and neat so that they do not injure their mothers on the way out. Sawfish are born with little **{{<text size="18.0pt" fg="#00CAF5">}}gel coverings{{</text>}}** {{<text size="18.0pt" fg="#00CAF5">}}{{</text>}}over
 their rostrums that protect the mother that look like this.{{<text size="13.5pt">}}{{</text>}}
{{<text size="13.5pt">}}{{</text>}}
![cid1679088146*image005.png@01D8F5C2.728C2F80](cid1679088146image005png01d8f5c2728c2f80.png){{<text size="13.5pt">}}{{</text>}}
*Isn't he ADORABLE?!* {{<text size="13.5pt">}}{{</text>}}

![IT'S SO FLUFFY I'M GOING TO DIE | Tumblr](its_so_fluffy_im_going_to_die_tumblr.gif)

This picture above is taken from an amazing video of a research group that caught and tagged a female sawfish (see what I mean about how huge they can get???), only to find that there were little rostrums poking
 out of her cloaca because she was literally **{{<highlight size="20.0pt" bg="aqua">}}about to give birth{{</highlight>}}** {{<text size="13.5pt">}}{{</text>}}
{{<text size="13.5pt">}}{{</text>}}
![cid1679088146*image007.png@01D8F5C2.9D0A0610](cid1679088146image007png01d8f5c29d0a0610.png)

(watch the video<a href="https://www.youtube.com/watch?time_continue=183&v=gt-sE14dYXs&feature=emb_title">here</a>
{{< youtube gt-sE14dYXs >}}if
 you want to put off your work and bask in the glory of cartilaginous fish for a couple of minutes).

Hope you enjoyed this educational foray into the world of chainsaw-faced-fish.

Lots of love,

Flora

Here is your animal falling over GIF:

{{<text size="13.5pt">}}![Marine Mammal Rescue Plop GIF - Marine Mammal Rescue Plop Oops - Discover &Share GIFs](marine_mammal_rescue_plop_gif_-_marine_m.gif){{</text>}}
{{<text size="13.5pt">}}{{</text>}}