+++
author = "Flora Judd"
title = "Ancient Lice Remedy"
date = "2022-12-02"
description = "Discover the oldest readable sentence related to head lice from an ancient comb."
tags = ["Oldest Sentence","Canaanite Language","Head Lice","Writing Origins","Alphabet Development","Archaeological Findings","Linguistic History","Cultural Artifacts","Research","Historical Linguistics"]
categories = ["History","Language","Archaeology","Archaeological Linguistics"]
image = "an_ivory_comb_with_an_ancient_message_ge.jpeg"
+++

Hello and welcome to another
 *{{<text size="22.0pt">}}Fun Fact From Flora{{</text>}}* ,

**{{<highlight size="16.0pt" bg="aqua">}}This one is NOT about fish.{{</highlight>}}** **{{<text size="16.0pt">}}{{</text>}}**

![I Don't Want It GIFs | Tenor](i_dont_want_it_gifs_tenor.gif)

I know, Nicki Manaj, subscriber to this email chain, I hear you. But this is cool trust me.

The **{{<text size="18.0pt" fg="#FFC000">}}oldest readable sentence{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}written using the *{{<text size="20.0pt" fg="#ED7D31">}}first ever alphabet{{</text>}}* {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}has been found on the side of an ivory comb.

![The ivory comb](the_ivory_comb.jpeg)

**{{<highlight size="18.0pt" bg="yellow">}} What does it say?! {{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}I hear you cry **{{<text size="16.0pt" fg="#A9D18E">}} What secrets does it tell us about the past life of humans gone by? {{</text>}}** **{{<text size="14.0pt">}}{{</text>}}**

Well, before we get there let's have a little think about what we know about **{{<text size="18.0pt" fg="#00B0F0">}}written language{{</text>}}**. Writing first emerged in ***{{<text size="16.0pt" fg="#FE9D23">}}Mesopotamia{{</text>}}*** {{<text size="16.0pt">}}{{</text>}}and{{<highlight size="22.0pt" bg="aqua">}}Egypt{{</highlight>}}{{<text size="22.0pt">}}{{</text>}}an estimated *{{<text size="18.0pt" fg="#00B050">}}5200 years ago{{</text>}}*. But those first systems{{<text size="16.0pt" fg="#FFC000">}}didn't have an alphabet{{</text>}}. Think of ancient Egyptian hieroglyphics. They were typically based on **{{<highlight size="20.0pt" bg="yellow">}}signs{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}to represent *{{<text size="20.0pt" fg="#FFB006">}}words and syllables{{</text>}}*. The alphabet, as a concept, came later.

![Hieroglyphic Cartoons and Comics - funny pictures from CartoonStock](hieroglyphic_cartoons_and_comics_-_funny.jpeg)
But the alphabet was likely
 **{{<text size="20.0pt" fg="#00F776">}}invented in or near Egypt{{</text>}}** , as many of the hieroglyphics were *{{<text size="18.0pt" bg="lime">}}repurposed{{</text>}}* {{<text size="18.0pt">}}{{</text>}}into the alphabetic letters that we might recognise today.

![image](69c41c140b42e1c5.png)

Scientists are pretty argumentative about when exactly that happened, mostly due to{{<u size="18.0pt" fg="#FFC000" bg="gray">}}a lack of archaeological evidence{{</u>}}. People tend to settle on around **{{<text size="16.0pt" fg="#ED7D31">}}4200 --- 3800 years ago{{</text>}}**.

But back to the comb.

![An Ivory Comb With an Ancient Message: Get Rid of Beard Lice. - The New ...](an_ivory_comb_with_an_ancient_message_ge.jpeg)

The message on this comb is written in an **{{<text size="20.0pt" fg="#F4B183">}}ancient Canaanite language{{</text>}}** {{<text size="20.0pt" fg="#F4B183">}}{{</text>}}spoken at *{{<text size="18.0pt" fg="#A9D18E" bg="blue">}}Tel Lachish{{</text>}}* {{<text size="18.0pt" fg="#A9D18E">}}{{</text>}}in southern Israel and is dated to be about ***{{<text size="20.0pt" fg="#FFC000">}}3700 years old{{</text>}}***. The next oldest sentence we have is about 400 years younger, so this is pretty exciting stuff.

Alright I"ve built the suspense enough.

![Tell me more tell me more gif 11 GIF Images Download](tell_me_more_tell_me_more_gif_11_gif_ima.gif)

*{{<text size="18.0pt" fg="#9DC3E6">}}Do you want to know what this ancient message is about?{{</text>}}* {{<text fg="#9DC3E6">}}{{</text>}}

**{{<text size="18.0pt" fg="#C00000">}}Head lice.{{</text>}}**

Yup.

I know.

The words are carefully carved in 1-3mm letters and it reads
***{{<highlight size="20.0pt" bg="yellow">}} May this tusk root out the lice of the hair and the beard. {{</highlight>}}***

Fantastic, eh?

Amusingly, this deciphering was aided by the fact that there are **{{<text size="26.0pt" fg="#FE9D23">}}still pieces of exoskeleton from lice{{</text>}}** that were actually removed using this comb.

**{{<text size="20.0pt" bg="fuchsia">}}ISN"T THAT AMAZING?!{{</text>}}** {{<text size="20.0pt">}}{{</text>}}If that isn't cool --- I am sorry I don't know what is.

Hope you enjoyed this land-based foray into the scientific world.

Lots of love,

Flora

A bug themed falling over:
![Lady Bug Fall GIF - Lady Bug Fall Raindrop - Discover & Share GIFs](lady_bug_fall_gif_-_lady_bug_fall_raindr.gif)