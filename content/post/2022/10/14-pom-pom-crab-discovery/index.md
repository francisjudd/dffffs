+++
author = "Flora Judd"
title = "Crab Symbiosis Explained"
date = "2022-10-14"
description = "Discover the fascinating relationship between the pom pom crab and its anemone companions."
tags = ["Lybia edmondsoni","Pom Pom Crab","Symbiotic Relationship","Anemone Defense","Reproductive Strategies","Ecological Impact","Marine Life","Crustacean Biology","Mutualism","Underwater Ecosystems"]
categories = ["Marine Biology","Crustaceans","Symbiosis","Ecology"]
image = "five_amazing_crab_species_scuba_diver_li.jpeg"
+++

Well hello there,

After, yet another, lengthy hiatus we return with another instalment of a *{{<text size="18.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}*.

Today, the animal that I have viewed to be so **{{<text size="20.0pt" fg="#FFC000">}}crucial{{</text>}}** , so *{{<text size="26.0pt" fg="#00B050">}}worth-knowing{{</text>}}* , so ***{{<text size="26.0pt" bg="fuchsia">}}earth-shatteringly awesome{{</text>}}*** that it is worth emerging from my email-less slumber is this:
 ***Lybia edmondsoni***.

Or more commonly known as
 **{{<highlight size="22.0pt" fg="#4472C4" bg="aqua">}}the pom pom crab{{</highlight>}}**.

![Five Amazing Crab Species Scuba Diver Life](five_amazing_crab_species_scuba_diver_li.jpeg)

***{{<highlight size="22.0pt" bg="yellow">}}ISN"T HE GORGEOUS?!{{</highlight>}}*** ***{{<text size="22.0pt">}}{{</text>}}***

Basically, if you can't see from the image, he is grasping **{{<text size="16.0pt" fg="fuchsia">}}two little anemones{{</text>}}** {{<text size="16.0pt">}}{{</text>}}in his claws, or his ***{{<text size="20.0pt" fg="#A37FFF">}}chelae{{</text>}}*** {{<text size="20.0pt">}}{{</text>}}(if you are a snazzy biologist or got too many vowels playing Scrabble). And this species of anemone is **{{<text size="20.0pt" fg="yellow" bg="lime">}}symbiotic{{</text>}}** {{<text size="20.0pt" fg="yellow">}}{{</text>}}with this crab, meaning that they both benefit from the arrangement.

The anemone is essentially
 *{{<text size="16.0pt" fg="#ED7D31">}}brandished in the face of anything scary and/or edible{{</text>}}* , as the anemones have a nasty sting.

This is pretty handy, because this crab has very small claws and so, as the Wikipedia page for this species so scathingly puts it, is ***{{<text size="20.0pt" fg="#00B0F0">}} {{</text>}}*** ***{{<text size="22.0pt" fg="#00B0F0">}}neither able to defend itself well nor feed itself efficiently{{</text>}}*** ***{{<text size="20.0pt" fg="#00B0F0">}} {{</text>}}***.

I mean same. But still. Harsh.

But luckily this crab has
{{<text size="16.0pt" fg="#6FF54A">}}something up its sleeve{{</text>}}. Or rather **{{<text size="20.0pt" fg="#00E7FF">}}on its sleeve {{</text>}}**

![Blackadder Baldrick GIF - Blackadder Baldrick Cunning - Discover & ShareGIFs](blackadder_baldrick_gif_-_blackadder_bal.gif)

Its weedy little claws turn out to be an advantage here because they are able to grasp onto these little anemones *{{<text size="20.0pt" fg="#DEEBF7" bg="purple">}}very delicately{{</text>}}* {{<text size="20.0pt" fg="#DEEBF7">}}{{</text>}}(I mean they are also held in place by spines, but the delicate holding of the anemone is a nicer image).

The anemones can then be used as **{{<text size="18.0pt" fg="#C00000" bg="silver">}}defence against predators{{</text>}}** or **{{<text size="18.0pt" fg="#ED7D31" bg="olive">}}other crabs{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}and also to *{{<text size="22.0pt" fg="#92D050" bg="green">}}catch particles of food{{</text>}}* *{{<text size="22.0pt" fg="#92D050">}}{{</text>}}* that the crab can then scrape off and snack on, or even *{{<text size="20.0pt" fg="#BDD7EE" bg="teal">}}stun larger prey items{{</text>}}*.

![Tumblr](tumblr.gif)

And what is super cool, is that the anemone that is used here (usually
 *Triactis producta* ), reproduces{{<text size="22.0pt" fg="#FF8E3D">}}asexually{{</text>}}{{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}by ***{{<text size="24.0pt" fg="yellow" bg="maroon">}}fission{{</text>}}***. And what that means is that the anemone essentially splits in half
 to reproduce and does not require another anemone to produce offspring. **{{<text size="18.0pt" fg="#00B050">}}It can do it all by itself{{</text>}}**.

In experiments where one of the pom poms gets removed, the crabs will slowly pull the remaining anemone apart, holding one of the fractions in each claw. Each of these fractions **{{<text size="20.0pt" fg="#8FAADC">}}then develops into a full anemone again{{</text>}}**.

![Blonde Brain Power Pom Pom Crab](blonde_brain_power_pom_pom_crab.gif)

*{{<highlight size="18.0pt" bg="aqua">}}HOW NIFTY IS THAT?!{{</highlight>}}* *{{<text size="18.0pt">}}{{</text>}}*

This is the only example that we know of in the animal kingdom, where one animal **{{<text size="20.0pt" fg="#DEEBF7" bg="navy">}}actively initiates asexual reproduction{{</text>}}** in another.
 In the same way that we might take a plant cutting and stick it in a jam jar of water until it grows roots (or maybe that's just Chris --- do other people do that too?), these crabs are essentially *{{<text size="20.0pt" fg="fuchsia">}}taking cuttings of these anemones{{</text>}}*.

Rather amusingly, when used to fight other crabs, it seems that the anemones are mostly used **{{<text size="20.0pt" fg="yellow" bg="red">}}in weapon-waving{{</text>}}** , but little
 *actual contact*. Kind of an **{{<highlight size="16.0pt" fg="#4472C4" bg="yellow">}}all-mouth-and-no-trousers{{</highlight>}}** {{<text size="16.0pt" fg="#4472C4">}}{{</text>}}sort of situation except an ***{{<text size="22.0pt" bg="lime">}}all-anemone-no-chelae{{</text>}}*** {{<text size="22.0pt">}}{{</text>}}sort of situation 

![Friends Rachel Green GIF - Friends Rachel Green Cheerleader - Discover &Share GIFs](friends_rachel_green_gif_-_friends_rache.gif)
*Terrifying --- right?*
**
And whilst the benefits to the crab are very obvious, the benefits to the anemone remain **{{<text size="18.0pt" fg="#4472C4">}}less clear{{</text>}}**. Maybe being waved around gives the anemone *{{<text size="16.0pt" fg="#00E7FF">}}better feeding opportunities{{</text>}}* {{<text size="16.0pt" fg="#00E7FF">}}{{</text>}}or **{{<text size="18.0pt" fg="fuchsia">}}greater oxygen concentrations{{</text>}}**. Or maybe it **{{<text size="20.0pt" fg="#A37FFF" bg="green">}}stops it from being buried{{</text>}}** {{<text size="20.0pt" fg="#A37FFF">}}{{</text>}}in shifting sand.

They may be my new favourite crustacean.

Lots of love to you and happy Friday!

Flora xxx

Here is your GIF of an animal falling over.

![Falling Lion GIF - Falling Lion Sliding - Discover & Share GIFs](falling_lion_gif_-_falling_lion_sliding_.gif)