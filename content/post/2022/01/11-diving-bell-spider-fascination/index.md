+++
author = "Flora Judd"
title = "Underwater Spider Wonders"
date = "2022-01-11"
description = "Explore the unique life of the diving bell spider and its fascinating underwater adaptations."
tags = ["Diving Bell Spider","Underwater Adaptations","Habitat","Oxygen Acquisition","Silk Utilization","Arachnids","Unique Spiders","Flora","Ecosystem Role","Invertebrates"]
categories = ["Spiders","Marine Life","Arthropods","Nature"]
image = "f95ea1c955771fe8.png"
+++

Hello and welcome to a
 *{{<text size="16.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

Today we are going to have a look at a very cool example of a not so loved arthropod --- the **{{<text size="26.0pt" fg="red">}}spider{{</text>}}**. Now spiders get a lot of criticism for being I don't know --- too hairy and too leggy and too moving-around-ish? Unsure.

But look how much fun this
 ***{{<highlight size="20.0pt" fg="#4472C4" bg="aqua">}}peacock spider{{</highlight>}}*** {{<text size="20.0pt" fg="#4472C4">}}{{</text>}}is having.

![Maracas Spider GIF - Maracas Spider Mexican - Discover ...](maracas_spider_gif_-_maracas_spider_mexi.gif)

That could be you too!! Plus (not what this fact is about) but peacock spiders are actually ***{{<text size="26.0pt" fg="#FF46D9">}}really{{</text>}}*** **{{<text size="26.0pt" fg="#FF46D9">}}sexy{{</text>}}**.

![Awesome Animated Spider Gifs at Best Animations](awesome_animated_spider_gifs_at_best_ani.gif)

*Look at the pretty colours*.

Anyways. I am gonna tell you about a very, very cool spider. The ***{{<text size="36.0pt" fg="#A9D18E">}}diving bell spider{{</text>}}*** {{<text size="36.0pt" fg="#A9D18E">}}{{</text>}}(see it sounds romantic already).

Now what is kinda funky about these spiders is that they spend their ** **{{<text size="28.0pt" fg="#FFD966" bg="blue">}}entire lives underwater{{</text>}}** {{<text bg="blue">}}.{{</text>}}

***{{<text size="28.0pt" fg="#00B050">}}I KNOW{{</text>}}***. WILD. They are the ONLY spider that we know about that does that.

I can tell what you're thinking --- of course Flora only likes this spider *{{<text size="14.0pt" fg="#0070C0">}}because it lives underwater{{</text>}}*. Figures. But it's actually a VERY cool dude.

This is what it looks like:

![image](f95ea1c955771fe8.png)

YES that{{<text size="20.0pt" bg="silver">}}silvery looking thing{{</text>}}{{<text size="20.0pt">}}{{</text>}}around his abdomen is in fact **{{<text size="26.0pt" fg="#9DC3E6">}}AN AIR BUBBLE{{</text>}}** {{<text size="26.0pt" fg="#9DC3E6">}}{{</text>}}that he has trapped there using very
 *{{<text size="28.0pt" fg="#2E75B6">}}hydrophobic hairs{{</text>}}* {{<text size="28.0pt" fg="#2E75B6">}}{{</text>}}all around his abdomen known as a **{{<text size="48.0pt" fg="#00B0F0">}}plastron{{</text>}}**. That really is an impressive amount of hair.

![Fur Hairy Chest GIF - Fur HairyChest AustinPowers ...](fur_hairy_chest_gif_-_fur_hairychest_aus.gif)

Oh and I forgot to include the *{{<text size="20.0pt" fg="#385723">}}coolest part{{</text>}}* {{<text size="20.0pt" fg="#385723">}}{{</text>}}of this image. Whoops. Silly me.

![image](3170c0ae0e040ae0.png)

**{{<text size="36.0pt" bg="fuchsia">}}HE HAS BUILT HIMSELF AN UNDERWATER LAIR IN WHICH TO DWELL{{</text>}}**.

Wowza.

The way he does this is by
 **{{<text size="18.0pt" fg="#A9D18E">}}constructing the underwater web{{</text>}}** {{<text size="18.0pt" fg="#A9D18E">}}{{</text>}}(easier said than done) and then taking
 *{{<text size="16.0pt" fg="#00B0F0">}}lots of trips{{</text>}}* {{<text size="16.0pt" fg="#00B0F0">}}{{</text>}}to the surface, gathering all of that sweet sweet air on his hairy body, and then travelling down into the web and shaking all those air bubbles off. They are then **{{<text size="20.0pt" fg="#F4B183">}}trapped{{</text>}}** {{<text size="20.0pt">}}{{</text>}}inside the ***{{<text size="24.0pt" fg="#FFD966">}}diving bell{{</text>}}*** {{<text size="24.0pt" fg="#FFD966">}}{{</text>}}(that's what it's called --- isn't that heavenly?) and up he goes to the surface again to get more. He will do as many trips as it takes to fill that diving bell.

![realmonstrosities | Water plants, Fish pet, Fauna](realmonstrosities_water_plants_fish_pet_.gif)

This takes a *{{<text size="20.0pt" fg="#7030A0">}}whole bunch{{</text>}}* of different kinds of silk. Some that **{{<highlight size="36.0pt" bg="aqua">}}anchor{{</highlight>}}** the diving bell to the plants around it, some are used for the **{{<highlight size="24.0pt" bg="yellow">}}actual nest{{</highlight>}}** , some are used as **{{<text size="26.0pt" bg="lime">}} {{</text>}}** **{{<text size="28.0pt" bg="lime">}}trip wires{{</text>}}** **{{<text size="26.0pt" bg="lime">}} {{</text>}}** {{<text size="26.0pt">}}{{</text>}}that detect the vibrations of insects nearby. When an unfortunate insect comes too close, these guys will ambush them and retreat into their lovely airy nest.

I imagine it feels exactly like this.

![image](5f135b9cb6ee0df6.png)
*No I know I"ve never even had any professional photoshop training.*

What is also amazing is that the **{{<text size="20.0pt" fg="#ED7D31">}}walls{{</text>}}** of the diving bell are ***{{<text size="36.0pt" fg="#0070C0">}}permeable{{</text>}}*** to gases and so oxygen can infuse into the diving bell and carbon dioxide can diffuse out. The spider still needs to fetch fresh air every now
 and then, they're very good at **{{<text size="22.0pt" fg="#FF46D9">}}monitoring the oxygen concentration{{</text>}}** {{<text size="22.0pt" fg="#FF46D9">}}{{</text>}}around them.

![The Book of Barely Imagined Beings: The diving bell spider ...](the_book_of_barely_imagined_beings_the_d.jpeg)

ISN"T HE SO COOL.

Hope you enjoyed this
 *{{<text size="22.0pt" fg="#5B9BD5">}}crazy spider fact.{{</text>}}*

Lots of love,

Flora xxx

![Turtle Falling GIF by Cheezburger - Find &amp; Share on GIPHY](turtle_falling_gif_by_cheezburger_-_find.gif)