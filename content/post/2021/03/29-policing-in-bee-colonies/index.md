+++
author = "Flora Judd"
title = "Bee Behavior Unveiled"
date = "2021-03-29"
description = "Exploring conflict resolution among eusocial insects, particularly bees."
tags = ["Bees","Conflict Resolution","Policing","Queen Dynamics","Larvae Competition","Honeybees","Stingless Bees","Evolutionary Biology","Worker Dynamics","Ecosystem Roles"]
categories = ["Insects","Ecology","Animal Behavior","Social Insects"]
image = "4f2905bd021d0849.jpeg"
+++

Hello and welcome to you
 *{{<text size="18.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* ,

What we are going to have a look at today is **{{<text size="20.0pt" fg="#4472C4">}}policing{{</text>}}** {{<text size="20.0pt" fg="#4472C4">}}{{</text>}}and ***{{<text size="20.0pt" fg="#ED7D31">}}conflict resolution{{</text>}}*** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}in the eusocial insects (these are your bees, wasps, ants and termites). Pretty epic stuff.

We had a look last week at what ants do when an *{{<text size="18.0pt" fg="#70AD47">}}infection{{</text>}}* {{<text size="18.0pt" fg="#70AD47">}}{{</text>}}enters their colony (it wasn't pretty) so imagine what it's like when **{{<highlight size="20.0pt" bg="yellow">}}colony members start misbehaving{{</highlight>}}** !!

![Oh Behave GIF - AustinPowers MikeMyers OhBehave - Discover ...](oh_behave_gif_-_austinpowers_mikemyers_o.gif)

Well in these colonies what you tend to have is **{{<text size="20.0pt" fg="#ED7D31">}}one reproducing female{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}(the (yassss) ***{{<highlight size="24.0pt" bg="yellow">}}Queen{{</highlight>}}*** ) who has *{{<text size="16.0pt">}}all the babies{{</text>}}* and lays *{{<text size="18.0pt">}}all the eggs{{</text>}}*. This means that there is a lot of ***{{<text size="22.0pt" fg="#92D050">}}competition{{</text>}}*** {{<text size="22.0pt" fg="#92D050">}}{{</text>}}around who gets to be Queen.

I mean evolutionarily speaking it's kind of hitting the **{{<text size="20.0pt" fg="#FFC000">}}jackpot{{</text>}}** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}isn't it? You get to sit around and have people *{{<text size="18.0pt" fg="#ED7D31">}}bring you food{{</text>}}* {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}and **{{<text size="22.0pt" fg="#5B9BD5">}}look after your children{{</text>}}** {{<text size="22.0pt" fg="#5B9BD5">}}{{</text>}}as you have{{<u size="20.0pt" fg="#92D050">}}as much sex as possible{{</u>}}and produce **{{<text size="36.0pt" fg="#0070C0">}}SO MANY OFFSPRING{{</text>}}** {{<text size="36.0pt" fg="#FFC000">}}{{</text>}}(up to ***{{<text size="48.0pt" fg="#00B050">}}2,000 PER DAY{{</text>}}*** ).

Not a bad life.

Some colonies have sussed this conflict out pretty well. Let's take a look at ***{{<text size="18.0pt" fg="#FFC000">}}honeybees{{</text>}}***.

Honeybees will have **{{<highlight size="20.0pt" bg="yellow">}}one queen at a time{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}but it is the ***{{<text size="20.0pt" fg="#ED7D31">}}workers who decide{{</text>}}*** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}which larvae become workers and which become queens. They do this by making special **{{<text size="22.0pt" fg="#00B0F0">}}Queen cells{{</text>}}** {{<text size="22.0pt" fg="#00B0F0">}}{{</text>}}that are maintained by workers and it is
 *{{<text size="20.0pt" fg="#7030A0">}}only the individuals raised in these cells{{</text>}}* {{<text size="20.0pt" fg="#7030A0">}}{{</text>}}that can become queen. They also get fed
 **{{<highlight size="22.0pt" bg="aqua">}}royal jelly{{</highlight>}}** {{<text size="22.0pt">}}{{</text>}}which is what allows them to grow much larger than worker bees and live much longer (and I mean MUCH longer --- if our royal family did the same we"d **{{<highlight size="20.0pt" bg="yellow">}}still be under the reign of Queen Elizabeth I{{</highlight>}}** ).

![image](4f2905bd021d0849.jpeg)

Here are some queen larvae **{{<text size="18.0pt" fg="#FFC000">}}floating on their royal jelly{{</text>}}**.

Now these queens develop and the first queen that emerges starts making noises. She **{{<text size="18.0pt" bg="fuchsia">}}"toots"{{</text>}}** **{{<text size="18.0pt">}}{{</text>}}** to let the other workers know that she is alive and healthy.

The other queens that are still inside their queen cells will start **{{<text size="20.0pt" bg="lime">}}"quacking"{{</text>}}** {{<text size="20.0pt">}}{{</text>}}in response, but the other workers won't let them out until the first queen is dead or if there is a swarm.

And I say **{{<text size="18.0pt" bg="fuchsia">}}"tooting"{{</text>}}** {{<text size="18.0pt" bg="fuchsia">}}{{</text>}}and **{{<text size="18.0pt" bg="lime">}}"quacking"{{</text>}}** {{<text size="18.0pt" bg="lime">}}{{</text>}}--- you're probably thinking this is all nonsense. Nope. You can literally hear it. Please click the photo below and enjoy for yourself.

<a href="https://www.youtube.com/watch?v=9naKEy1v6Lw">![image](cd1fcf7fe8483286.png)</a>
{{< youtube 9naKEy1v6Lw >}}


*{{<text size="16.0pt" fg="#ED7D31">}}ISN"T THAT MENTAL.{{</text>}}*

It makes me chuckle every time.

Now that's the example of how to do it right sadly not all colonies have it quite so sussed.

In the stingless bee, it's
 **{{<text size="20.0pt" fg="#92D050">}}the larva themselves{{</text>}}** {{<text size="20.0pt" fg="#92D050">}}{{</text>}}that get to determine whether they become queens or workers. This means that even though it is *{{<text size="18.0pt" fg="#00B0F0">}}SUPER UNLIKELY{{</text>}}* {{<text size="18.0pt" fg="#00B0F0">}}{{</text>}}you will become queen (remember there is only one) a lot of workers will have a go ***{{<text size="18.0pt">}}just in case{{</text>}}*** {{<text size="18.0pt">}}{{</text>}}because the **{{<text size="20.0pt" fg="#0070C0">}}evolutionary pay-offs{{</text>}}** {{<text size="20.0pt" fg="#0070C0">}}{{</text>}}are so huge.

This means that **{{<highlight size="24.0pt" bg="yellow">}}TWENTY PERCENT{{</highlight>}}** of larvae try and have a go at being queen. That is
 A LOT of bees.

![Stingless bee honey discovery could create conservation ...](stingless_bee_honey_discovery_could_crea.jpeg)

But sadly, the other workers don't want more queens. **{{<text size="16.0pt">}}They want more workers{{</text>}}** ! (the more I learn the more I realise that bees are just *{{<text size="14.0pt" bg="red">}}communists{{</text>}}* )

AND SO --- do you know what they do instead? They ***{{<text size="24.0pt" fg="#ED7D31">}}decapitate the queens they don't want{{</text>}}***.

Firstly, the scientists involved were very pleased because they finally figured out why there was a **{{<text size="18.0pt" fg="#92D050">}}massive pile of heads{{</text>}}** in the middle of the colony but SECONDLY it is totally bizarre that this is a stable strategy for these bees. **{{<text size="22.0pt">}}THEY LITERALLY {{<highlight bg="yellow">}}DECAPITATE{{</highlight>}} {{<text bg="red">}}20%{{</text>}} {{<highlight bg="yellow">}}OF THEIR POPULATION{{</highlight>}}{{</text>}}**.

*{{<text size="16.0pt">}}Wat.{{</text>}}*
*{{<text size="16.0pt">}}{{</text>}}*
![Wat GIFs - Find & Share on GIPHY](wat_gifs_-_find_share_on_giphy.gif)

Bees. Explain.

Hope you enjoyed today's fact.

Try not to decapitate anyone.

Love to you,

Flora xx