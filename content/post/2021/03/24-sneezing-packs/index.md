+++
author = "Flora Judd"
title = "Voting with Sneezes"
date = "2021-03-24"
description = "Explore how African wild dogs use sneezes to make group decisions."
tags = ["African Wild Dogs","Group Living","Decision Making","Sneezing","Voting Mechanism","Cooperation","Pack Behavior","Animal Dynamics","Social Structures","Behavioral Studies"]
categories = ["Animals","Behavior","Group Dynamics","Social Behavior"]
image = "african_wild_dogs_use_sneezes_to_vote_in.png"
+++

Hello and welcome your favourite thing --- you're *{{<text size="18.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* ,

Today we are going to have a little look at ***{{<text size="20.0pt" fg="#ED7D31">}}group-living animals{{</text>}}***.

Now, a lot of animals live in groups. It's a behaviour that we see across a **{{<highlight size="20.0pt" bg="yellow">}}huge{{</highlight>}}** **{{<highlight size="16.0pt" bg="yellow">}}range of different taxonomies{{</highlight>}}** , which suggests that it's a pretty *{{<text size="16.0pt" fg="#00B050">}}evolutionarily successful{{</text>}}* {{<text size="16.0pt" fg="#00B050">}}{{</text>}}way of doing things.

Animals don't necessarily live in groups constantly --- sometimes they come together for **{{<text size="16.0pt">}}certain times{{</text>}}** in their lives or times of year and then ***{{<text size="18.0pt" fg="#4472C4">}}disperse again{{</text>}}***. Some animals live *{{<text size="18.0pt" fg="#00B0F0">}}cooperatively{{</text>}}* {{<text size="18.0pt" fg="#00B0F0">}}{{</text>}}in groups (i.e. working together to get the job done)

![AND NOW FOR SOMETHING COMPLETELY DIFFERENT: ANIMAL GROUPS ...](and_now_for_something_completely_differe.jpeg)

But some do not live in cooperative groups. They just happen to be in the same place using the same resources at the same time.

Cooperative living is a very interesting biological phenomenonemonen with many advantages and disadvantages, but what I want to talk about today is ***{{<highlight size="18.0pt" bg="yellow">}}how cooperative groups of animals make decisions{{</highlight>}}***.

After all, groups are made up of **{{<text size="18.0pt" fg="#ED7D31">}}individuals{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}and individuals have their own personal
{{<text size="18.0pt" fg="#4472C4">}}goals{{</text>}},{{<text size="18.0pt" fg="#4472C4">}}preferences{{</text>}}and{{<text size="18.0pt" fg="#4472C4">}}instincts{{</text>}}. How do you incorporate all of those into an outcome for the group that causes the smallest amount of conflict?

Well one way that humans come to a consensus is through ***{{<text size="16.0pt" fg="#FFC000">}}voting{{</text>}}***.

What if I told you that we aren't the only animals that seem to do this?

![Elections | The IECA](elections_the_ieca.jpeg)

Okay maybe not quite like that but this is *{{<highlight size="22.0pt" bg="aqua">}}SUPER cool{{</highlight>}}*.

**{{<text size="22.0pt" fg="#ED7D31">}}African wild dogs{{</text>}}** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}are wonderful animals that look like this.

![Happy Painted Wolf GIF by BBC America - Find & Share on GIPHY](happy_painted_wolf_gif_by_bbc_america_-_.gif)

They live in **{{<text size="18.0pt" fg="#FFC000">}}packs of up to about 20{{</text>}}** {{<text fg="#FFC000">}}{{</text>}}dogs and they are ***{{<text size="16.0pt" fg="red">}}despotic{{</text>}}*** {{<text size="16.0pt" fg="red">}}{{</text>}}by nature. By that I mean that there is
{{<text size="16.0pt" fg="#4472C4">}}one dominant breeding pair{{</text>}}that rules over the rest of the pack who are all{{<text size="16.0pt" fg="#00B0F0">}}subordinates{{</text>}}.

These guys **{{<text size="16.0pt" fg="#ED7D31">}}hunt for their food{{</text>}}** {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}and before they go out for a hunt they go through a very high energy ritual that we call a **{{<text size="22.0pt" fg="#C00000">}}"rally"{{</text>}}**. This involves a lot of *{{<text size="18.0pt" fg="#92D050">}}tail wagging{{</text>}}* , *{{<text size="18.0pt" fg="#92D050">}}head touching{{</text>}}* {{<text size="18.0pt" fg="#92D050">}}{{</text>}}and generally *{{<text size="18.0pt" fg="#92D050">}}running around{{</text>}}* {{<text size="18.0pt" fg="#92D050">}}{{</text>}}quite a lot.

Sometimes these rallies end with all the dogs going off to hunt.

![Wild Dogs GIFs - Find & Share on GIPHY](wild_dogs_gifs_-_find_share_on_giphy.gif)

Sometimes they end with all the dogs lying down and having a bit of a nap.

![African Wild Dog Facts - Animal Facts Encyclopedia](african_wild_dog_facts_-_animal_facts_en.jpeg)

So it seems that **{{<highlight size="20.0pt" bg="yellow">}}some sort of decision making process{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}is happening here. But what is it that determines the outcomes of these rallies? Who is deciding whether or not to start the hunt?

Firstly, researchers found that the rallies are *{{<text size="16.0pt" fg="#4472C4">}}more likely to be successful{{</text>}}* {{<text size="16.0pt" fg="#4472C4">}}{{</text>}}if there have been **{{<text size="16.0pt" fg="#00B0F0">}}other failed rallies before{{</text>}}**. i.e. if there were two rallies before that didn't lead to a hunt the third
 is much more likely to end up in a hunt.

![Volunteer with African Wild Dogs | Top 10 Projects 2020](volunteer_with_african_wild_dogs_top_10_.jpeg)

Also, when a **{{<text size="18.0pt" fg="#92D050">}}higher-ranking individual{{</text>}}** {{<text size="18.0pt" fg="#92D050">}}{{</text>}}initiated the rally it was more likely to be successful (i.e. go for the hunt), but still the researchers were still puzzled. This didn't explain enough of what they were seeing.

Then they started counting
 **{{<highlight size="22.0pt" bg="aqua">}}sneezes{{</highlight>}}**.

The ***{{<text size="18.0pt" fg="#ED7D31">}}more sneezes{{</text>}}*** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}that took place during a rally, the
 ***{{<text size="20.0pt" fg="#ED7D31">}}more likely{{</text>}}*** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}the dogs were to start hunting. The researchers concluded (and I promise you this is real research --- a paper called "Sneeze to leave" published in the
 *<a href="https://royalsocietypublishing.org/doi/full/10.1098/rspb.2017.0347#xref-ref-4-1">Proceedings of the Royal Society B</a>* ) that the sneezes were being used *{{<highlight size="22.0pt" bg="yellow">}}"as a voting mechanism to establish group consensus"{{</highlight>}}*.

***{{<u size="24.0pt">}}Isn't that MENTAL!?{{</u>}}***

![African Wild Dogs Use Sneezes to Vote in Democratic-Like ...](african_wild_dogs_use_sneezes_to_vote_in.png)

Dogs use a **{{<text size="14.0pt" fg="#FFC000">}}VOTING MECHANISM{{</text>}}** {{<text size="14.0pt" fg="#FFC000">}}{{</text>}}like we do to come to a *{{<text size="14.0pt" fg="#70AD47">}}group decision{{</text>}}* !!

What was also very interesting to note was that this process wasn't ***{{<text size="16.0pt" fg="#ED7D31">}}entirely democratic{{</text>}}***. If it was one of the big-dog dogs that initiated the rally, they only needed an **{{<text size="20.0pt" fg="#4472C4">}}average of 3 sneezes{{</text>}}** {{<text size="20.0pt" fg="#4472C4">}}{{</text>}}to get the pack going. If it was a subordinate member of the pack they needed **{{<text size="20.0pt" fg="#00B0F0">}}at least 10 sneezes{{</text>}}** {{<text size="20.0pt" fg="#00B0F0">}}{{</text>}}for the hunt to go.

Sneezing has never before been documented as a **{{<text size="18.0pt" fg="#FFC000">}}communicative function{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}so this is *{{<text size="16.0pt">}}PRETTY NEAT{{</text>}}*.

But maybe don't try it in the ballot okay?

Lots of love,

Flora