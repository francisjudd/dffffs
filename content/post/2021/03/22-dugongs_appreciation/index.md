+++
author = "Flora Judd"
title = "Dugongs Deserve Love"
date = "2021-03-22"
description = "Discover why dugongs are fascinating yet vulnerable marine mammals."
tags = ["Dugongs","Marine Mammal","Seagrass","Herbivorous Diet","Conservation Status","Vulnerable Species","Communication","Maternity Behavior","Ecological Role","Marine Conservation"]
categories = ["Marine Life","Wildlife Conservation","Science","Aquatic Biology"]
image = "dugong_photos_high_resolution_stock_unde.png"
+++

![New trending GIF on Giphy | Bruno mars, Michael jackson neverland, Singer](new_trending_gif_on_giphy_bruno_mars_mic.gif)

**{{<text size="18.0pt">}}HELLO WONDERFUL HUMANS{{</text>}}** {{<text size="18.0pt">}}{{</text>}}and welcome for the almighty return of your *{{<text size="22.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* ,

I missed y"all a lot and I am VERY EXCITED to be bringing some funky science back into your inbox every day.

Today I am going to introduce you to a species that you might already know a bit about but I think that they are in **{{<highlight size="16.0pt" bg="yellow">}}MUCH NEED OF APPRECIATION{{</highlight>}}**.

![The Old Reader](the_old_reader.gif)

No --- this is not the love child of an **{{<text size="16.0pt" bg="silver">}}elephant{{</text>}}** , a *{{<highlight size="20.0pt" fg="#00B050" bg="aqua">}}mermaid{{</highlight>}}* {{<text size="20.0pt" fg="#00B050">}}{{</text>}}and a{{<u size="22.0pt" fg="red">}}hoover{{</u>}}. This is a **{{<text size="20.0pt" bg="fuchsia">}}dugong{{</text>}}** !

Sometimes known as sea cows alongside their evolutionary cousins the manatees, they are the only *{{<text size="16.0pt" fg="#70AD47">}}totally herbivorous marine mammals{{</text>}}* {{<text size="16.0pt" fg="#70AD47">}}{{</text>}}(apart from me of course). In fact they eat between **{{<text size="22.0pt" fg="#ED7D31">}}50kg and 80kg{{</text>}}** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}of seagrass EVERY DAY. They evolved about{{<highlight size="16.0pt" bg="yellow">}}50-60 million years ago{{</highlight>}}{{<text size="16.0pt">}}{{</text>}}when some animals that kind of looked like elephants ventured into the water and started living there. Cos you know --- why not?

Their nostrils are on the top of their head (you can see them in the GIF above) and they can close them using some handy little **{{<text size="16.0pt" fg="#ED7D31">}}valves{{</text>}}**. They have tail flukes and fins that actually look a lot like those of dolphins.

![Dugong photos, high resolution stock underwater pictures ...](dugong_photos_high_resolution_stock_unde.png)

Isn't that a sexy outline.

They also have between ***{{<highlight size="20.0pt" bg="yellow">}}57 and 60 vertebrae{{</highlight>}}*** {{<text size="20.0pt">}}{{</text>}}which is pretty nuts. We have 33. Pretty rubbish in comparison really. Also weirdly enough their ribs contain *{{<text size="22.0pt" fg="red">}}almost no bone marrow{{</text>}}* , making them some of the densest bones in the animal kingdom. This is thought to help them
 stay submerged under the water. Maybe that's also why they can weight **{{<text size="20.0pt" fg="#4472C4">}}up to a ton{{</text>}}**. Dense bones --- yeah suuuuuure
 Susan.

![Mythical Creatures with Real Origins - Aquiziam](mythical_creatures_with_real_origins_-_a.png)

They can live up to the age of **{{<text size="48.0pt" fg="#ED7D31">}}73{{</text>}}** {{<text size="48.0pt" fg="#ED7D31">}}{{</text>}}and tend to live solitary lives even though they are **{{<text size="18.0pt" fg="#4472C4">}}social animals{{</text>}}** {{<text size="18.0pt" fg="#4472C4">}}{{</text>}}and will communicate using chirps, whistles and barks. They are especially dependent on social contact early in life and calves and mothers are *{{<highlight size="20.0pt" bg="yellow">}}almost in constant physical contact{{</highlight>}}*. Calves even reach out and touch their mothers
 with their flippers for reassurance.

Gestation takes about **{{<text size="16.0pt" bg="fuchsia">}}13-15 months{{</text>}}** {{<text size="16.0pt">}}{{</text>}}(oof) and usually to just one calf at a time. Newborns are already 1.2m long and are fed milk from their mothers for the first 14-18 months of their life. Weirdly enough their *{{<text size="16.0pt" fg="#00B050">}}nipples are found in their armpits{{</text>}}* {{<text size="16.0pt" fg="#00B050">}}{{</text>}}like this.

![image](9a47793b2a81b8d2.png)
(this is taken from a genuine scientific paper --- I"m not a creeper I promise)

This is it in action (okay this is a manatee not a dugong but it was the best I could find I"m sorry). Looks kind of painful I can't lie.
![image](9e66ce966c28333d.png)

What is also incredible is that when they need to be nursed, calves have been seen to **{{<text size="20.0pt" fg="#ED7D31">}}suck their flippers{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}in the same way humans might **{{<text size="20.0pt" fg="#FFC000">}}suck their thumb{{</text>}}**.

*{{<highlight size="18.0pt" bg="aqua">}}ISN"T THAT SO CRAZY.{{</highlight>}}* *{{<text size="18.0pt">}}{{</text>}}*

We need to be careful with our lovely dugongs though. Their very long lives and the fact that they reach sexual maturity at about **{{<text size="20.0pt" fg="#A5A5A5">}}18 years old{{</text>}}** {{<text size="20.0pt" fg="#A5A5A5">}}{{</text>}}means their populations are *very vulnerable.* If even a small proportion of individuals are killed it will take a{{<text size="20.0pt">}}LONG ASS TIME{{</text>}}for populations to recover. Also because they love to live in the shallow waters where their favourite seagrass is found they are at risk of **{{<text size="16.0pt" fg="red">}}boat strikes{{</text>}}** , getting ***{{<text size="20.0pt" fg="#00B0F0">}}trapped in fishing nets{{</text>}}*** {{<text size="20.0pt" fg="#00B0F0">}}{{</text>}}and getting bombarded by **{{<text size="20.0pt" fg="#92D050">}}tourist boats{{</text>}}** with poor practices. This can chase them into deeper water and out of the safe shallow waters they love so dearly.

Where I was in the Red Sea, there are now **{{<text size="22.0pt" fg="#92D050">}}only 32{{</text>}}** of them left. And I was lucky enough to see one which was *{{<text size="18.0pt" fg="#ED7D31">}}FRICKING NUTS{{</text>}}*.

Hope you enjoyed learning about them as much as I did.

Love to you,

Flora

![manatee smoosh gif | WiffleGif](manatee_smoosh_gif_wifflegif.gif)