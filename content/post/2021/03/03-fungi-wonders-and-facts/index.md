+++
author = "Flora Judd"
title = "Fungal Fascination"
date = "2021-03-03"
description = "Explore the intriguing role fungi play in our ecosystems."
tags = ["Fungi","Decomposition","Lignin","Ecosystem Dynamics","Humungous Fungus","Opisthokonts","Multicellular Organisms","Photosynthetic Interactions","Root Structures","Biodiversity"]
categories = ["Nature","Biology","Fungi","Ecosystems"]
image = "the_largest_living_organism_armillaria_o.jpeg"
+++

Hello and welcome to your
 *{{<text size="20.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* ,

Not a huge amount of time today so we're keeping it brief but beautiful!

We're back on to ***{{<text size="22.0pt" fg="#00B050" bg="lime">}}Wednesday's What Was the World Doing Ages and Ages Ago{{</text>}}***

So let's see where we are in our calendar year 

We saw ***{{<highlight size="20.0pt" bg="yellow">}}life begin{{</highlight>}}*** {{<text size="20.0pt">}}{{</text>}}on
 the **{{<text size="26.0pt" fg="#4472C4">}}25th February{{</text>}}**.
*{{<text size="22.0pt" bg="lime">}}Photosynthesis started{{</text>}}* {{<text size="22.0pt">}}{{</text>}}on
 the **{{<text size="22.0pt" fg="#4472C4">}}28thMarch{{</text>}}**.
***{{<text size="22.0pt" bg="fuchsia">}}Multicellular organisms{{</text>}}*** {{<text size="22.0pt">}}{{</text>}}appeared
 on the **{{<text size="22.0pt" fg="#4472C4">}}16th August{{</text>}}**.

**{{<text size="26.0pt" fg="red" bg="maroon">}}Sexual reproduction{{</text>}}** {{<text size="26.0pt" fg="red">}}{{</text>}}came
 about on the **{{<text size="22.0pt" fg="#0070C0">}}17thSeptember{{</text>}}**.

Now we have{{<text size="24.0pt" bg="olive">}}fungi{{</text>}}{{<text size="24.0pt">}}{{</text>}}appearing on the **{{<text size="22.0pt" fg="#0070C0">}}15thNovember{{</text>}}**.

![Fungi Species List : rare Charitable Research Reserve](fungi_species_list_rare_charitable_resea.jpeg)


Fungi fall into a group called the **{{<text size="18.0pt" fg="red">}}Opisthokonts{{</text>}}** {{<text size="14.0pt" fg="red">}}{{</text>}}(get that into a game of Bananagrams --- I dare you).

They have a lot of ***{{<highlight size="16.0pt" bg="yellow">}}features in common{{</highlight>}}*** {{<text size="16.0pt">}}{{</text>}}with us animals and a few that are **{{<text size="16.0pt">}}just unique to them{{</text>}}**.

So both animals and fungi are ***{{<text size="16.0pt" fg="#ED7D31">}}heterotrophic{{</text>}}*** {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}--- meaning that we need to get our carbon from other sources (we can't make it ourselves like{{<text size="16.0pt" fg="#00B050">}}those lovely plants{{</text>}}do). And both animals and fungi ***{{<text size="16.0pt" fg="#92D050">}}lack chloroplasts{{</text>}}*** {{<text size="16.0pt" fg="#92D050">}}{{</text>}}(the organelles in plants that contain chlorophyll --- the pigment that makes plants green).

They do have a couple of things that make them unique --- which is that their cell walls are very rigid and combine ***{{<text size="18.0pt" fg="#4472C4">}}glucans{{</text>}}*** {{<text size="18.0pt" fg="#4472C4">}}{{</text>}}(normally only found in plants) with ***{{<text size="16.0pt" fg="#4472C4">}}chitins{{</text>}}*** {{<text size="16.0pt" fg="#4472C4">}}{{</text>}}(which are normally only found in insect exoskeletons). Kind of weird.

Fungi play an incredibly important role in ecosystems --- decomposing and recycling all dem nutrients and an *{{<highlight size="22.0pt" bg="yellow">}}overwhelming{{</highlight>}}* *{{<text size="22.0pt">}}{{</text>}}* number of fungi occur in the soil.

Lignin --- the stuff that makes trees "woody" **{{<text size="20.0pt">}}can
 *only* be digested by fungi{{</text>}}**. Isn't that crazy!? **{{<highlight size="18.0pt" bg="aqua">}}No other organisms{{</highlight>}}** are capable of doing it. Given that trees
 are ***{{<text size="18.0pt" fg="#70AD47">}}20-25% lignin{{</text>}}*** , that's quite a lot of digesting that needs to be done!

![Why fungi matter | Grow Wild](why_fungi_matter_grow_wild.gif)


They digest things extracellularly --- meaning that they ***{{<highlight size="16.0pt" bg="yellow">}}release enzymes{{</highlight>}}*** {{<text size="16.0pt">}}{{</text>}}into the environment that break down the compounds into simple sugars. These can then be{{<u size="16.0pt">}}reabsorbed{{</u>}}{{<text size="16.0pt">}}{{</text>}}by the fungus.

Not only that, but ***{{<text size="24.0pt" fg="#5B9BD5">}}95% of ALL TERRESTRIAL PLANTS{{</text>}}*** need fungi to improve the efficiency of their root structures.

The biggest fungus? Might just be the largest living thing on earth. Known as the **{{<highlight size="18.0pt" bg="yellow">}}"Humungous Fungus"{{</highlight>}}** , it produces fruiting bodies (mushrooms)
 that look like this.

![The largest living organism (Armillaria ostoyae) covers over 2,385 acres and produces honey mushrooms (pictured) in the fall.](the_largest_living_organism_armillaria_o.jpeg)

But whilst these look small, the underground network of super thin hyphae (kind of analogous to roots but for fungi) covers a vast area of over **{{<text size="20.0pt" bg="fuchsia">}}10km{{</text>}}**. It also weighs ***{{<text size="20.0pt" fg="#ED7D31">}}20,000kg{{</text>}}***.

It also might take the bacon for oldest organism in the world, at an estimated **{{<highlight size="22.0pt" bg="yellow">}}8,500 years old{{</highlight>}}** !!

Hope you enjoyed a tiny little look into the fungal world.


![8 Mesmerizing Timelapse GIFs Showing How Mushrooms Grow ...](8_mesmerizing_timelapse_gifs_showing_how.gif)


Lots of love,

Flora xx