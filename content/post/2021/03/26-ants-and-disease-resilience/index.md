+++
author = "Flora Judd"
title = "Ants vs. Disease"
date = "2021-03-26"
description = "Exploring how ants combat disease within their colonies."
tags = ["Ants","Disease Resistance","Colony Health","Social Behavior","Chemical Detection","Infection Treatment","Super-organism","Ecosystem Dynamics","Fungal Spores","Survival Strategies"]
categories = ["Biology","Ants","Disease","Animal Behavior"]
image = "the_ants_treat_infections_with_extreme_p.jpeg"
+++

HELLO **{{<text size="14.0pt">}}today is a good day{{</text>}}** {{<text size="14.0pt">}}{{</text>}}to be a fun fact and today you get a
 ***{{<text size="20.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}*** ,

Today we are going back to my much beloved subject --- **{{<text size="24.0pt">}}ants{{</text>}}** **{{<text size="24.0pt">}} {{</text>}}** **{{<text size="24.0pt">}}{{</text>}}**. ***{{<highlight size="20.0pt" bg="yellow">}}I"M SORRY{{</highlight>}}*** {{<text size="20.0pt">}}{{</text>}}if you find ants boring but if you do think that ants are boring then you are{{<u size="18.0pt" fg="#ED7D31">}}simply just wrong{{</u>}}{{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}and I"m going to show you why.

To all my ant enthusiasts --- this one is for you. ( ***{{<text size="16.0pt" fg="#C00000">}}Enthusiants?{{</text>}}*** I"ll get some T-shirts made)

![image](312e391656bafc79.png)

(due to recent budget cuts the VR headset is not included we"ve also had to let our graphics designer go )

{{<text size="14.0pt">}}ANYWAYS{{</text>}}.

**{{<text size="18.0pt">}}Ants{{</text>}}**.

We spoke about animal group living earlier this week and it's a very interesting topic.

One of the disadvantages of group living, which we are experiencing particularly keenly at the moment, is the ***{{<highlight size="18.0pt" bg="yellow">}}spread of disease{{</highlight>}}*** {{<text size="18.0pt">}}{{</text>}}(sorry). But this isn't just a human problem. There are other species that have to face it as well and they aren't going into **{{<text size="18.0pt" fg="#5B9BD5">}}animal-enforced lockdowns{{</text>}}** {{<text size="18.0pt" fg="#5B9BD5">}}{{</text>}}are they?

Well some do enforce
 ***{{<text size="18.0pt" fg="#4472C4">}}social distancing{{</text>}}***. The *{{<text size="16.0pt" fg="#ED7D31">}}Caribbean spiny lobster{{</text>}}* {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}lives in **{{<text size="14.0pt" fg="#92D050">}}cooperative burrows{{</text>}}** {{<text size="14.0pt" fg="#92D050">}}{{</text>}}with usually a few other individuals.

![image](da54bf1fb31ee09a.jpeg)

But when one of them **{{<text size="16.0pt" fg="#ED7D31">}}gets sick{{</text>}}** {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}it will den on its own. Now there are two reasons this could be the case --- it might be that this is *{{<text size="18.0pt" fg="#FFC000">}}being enforced by other lobsters{{</text>}}* {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}refusing to den with the sick one, or it could be that the sick individual *{{<text size="18.0pt" fg="#C55A11">}}retreats from social interactions{{</text>}}* {{<text size="18.0pt" fg="#C55A11">}}{{</text>}}when it isn't feeling so hot.

![Your lobster cost a fiver? Discount supermarkets Asda ...](your_lobster_cost_a_fiver_discount_super.gif)

So some researchers did some cool experiments where they gave spiny lobsters the choice of who they denned with. The **{{<text size="16.0pt" fg="#9DC3E6">}}sick ones went with anyone{{</text>}}** , but
 the **{{<text size="16.0pt" fg="#2F5597">}}healthy ones would only go with other healthy ones{{</text>}}** --- suggesting that it's a decision being made by healthy individuals.

Now how do they know one of them is sick? It's not exactly super obvious from the outside. Well actually the others can ***{{<text size="18.0pt" fg="#92D050">}}detect chemicals{{</text>}}*** {{<text size="18.0pt" fg="#92D050">}}{{</text>}}emitted in the
 **{{<text size="18.0pt" fg="#00B050">}}sick lobster's urine{{</text>}}** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}(a whole lot cooler than a PCR test I can tell you that for free).

Lobsters will *{{<text size="16.0pt" fg="#ED7D31">}}refuse to den with sick individuals{{</text>}}* , even if it means they have to find a whole new area in which to live,
 increasing the risk that they are **{{<text size="16.0pt" fg="#FFC000">}}attacked by a predator{{</text>}}**. This meant when there was the spread of a virus
 called **{{<text size="18.0pt" fg="#4472C4">}}PaV1{{</text>}}** that has a *{{<text size="20.0pt" fg="#ED7D31">}}90% mortality{{</text>}}* , the lobsters were actually fine. No major population declines. Winner!

But the really interesting example here is, of course, ants. Ants live in **{{<text size="18.0pt" fg="#A9D18E">}}highly integrated colonies{{</text>}}** {{<text size="18.0pt" fg="#A9D18E">}}{{</text>}}and cohesive societies that can essentially be compared to a *{{<text size="20.0pt" fg="#ED7D31">}}super-organism{{</text>}}*. The risk of transmission is incredibly high and there isn't a lot of room for social distancing.
 Imagine your cells trying to social distance from each other in your body not really an option.

And yet --- ant colonies ***{{<text size="20.0pt" fg="#FFC000">}}don't seem to suffer from disease{{</text>}}***.
 **Ever**.

*{{<highlight size="16.0pt" bg="yellow">}}What fuckery is going on here{{</highlight>}}* *{{<text size="16.0pt">}}.{{</text>}}*

Well some researchers exposed a **{{<text size="16.0pt" fg="#ED7D31">}}pupa{{</text>}}** {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}(an ant larva still in its cocoon) to ***{{<text size="18.0pt" fg="#92D050">}}some fungal spores{{</text>}}*** , put it back with the other ants and waited to see what happened.

What the ants did is they started to *{{<text size="16.0pt" fg="#4472C4">}}cut the pupa out of its silk cocoon{{</text>}}*.

![The Ants Treat Infections with 'Extreme Prejudice ...](the_ants_treat_infections_with_extreme_p.jpeg)

They then started to **{{<text size="18.0pt" fg="#00B0F0">}}bite holes into its{{</text>}}** *{{<text size="22.0pt" fg="#0070C0">}}cuticle{{</text>}}* {{<text size="22.0pt" fg="#0070C0">}}{{</text>}}(its "skin" if you will) and ***{{<text size="16.0pt" fg="#92D050">}}sprayed poison{{</text>}}*** {{<text size="16.0pt" fg="#92D050">}}{{</text>}} *inside* the pupa from the tip of their abdomens.

They then **{{<text size="20.0pt" fg="#ED7D31">}}bit the limbs off{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}and sprayed them with poison too. For several minutes they just *{{<text size="16.0pt" fg="#FFC000">}}stabbed more holes{{</text>}}* {{<text size="16.0pt" fg="#FFC000">}}{{</text>}}in this poor little ant and **{{<text size="16.0pt" fg="#2E75B6">}}filled it with poison{{</text>}}** , essentially
 disinfecting it from the inside out.

Oh and the pupa is **{{<highlight size="26.0pt" bg="yellow">}}still alive{{</highlight>}}** {{<text size="26.0pt">}}{{</text>}}at this point. This is what it looks like before.
![image](41202e59fa3bd1cb.png)

{{<text size="18.0pt">}}All curled up inside it's little cocoon how nice{{</text>}}.

This is what it looks like afterwards.

![image](d1804fa23018dece.png)

*{{<text size="26.0pt">}}AHHHHHHHHH.{{</text>}}*

![Disgusted My Eyes GIF - Find & Share on GIPHY](disgusted_my_eyes_gif_-_find_share_on_gi.gif)

And if you were wondering (you know you were) this is the pH level inside before and after.

![image](bcefed9b3538d305.png)


*{{<text size="18.0pt">}}OH SHIT{{</text>}}* {{<text size="18.0pt">}}{{</text>}}--- that is
 **{{<text size="26.0pt" fg="#ED7D31">}}ACIDIC{{</text>}}**.

Lockdown ain't so bad now is it?

But seriously don't go sterilising yourself from the inside out. It's a bad idea.

![Sanitizer GIFs | Tenor](sanitizer_gifs_tenor.gif)

Lots of love,

Flora