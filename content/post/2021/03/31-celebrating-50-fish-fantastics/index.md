+++
author = "Flora Judd"
title = "50 Fishy Facts"
date = "2021-03-31"
description = "Diving into the wonders and oddities of early fish evolution."
tags = ["Fish","Evolution","Agnatha","Hagfish","Lampreys","Multicellular Organisms","Photosynthesis","Sexual Reproduction","Marine Evolution","History of Fish"]
categories = ["Science","Evolution","Marine Life","Fish"]
image = "truck_full_of_slime_eels_overturns_on_hi.gif"
+++

Hello and welcome to your *Daily Fun Fact From Flora* ,

WE"VE REACHED 50!! Whooooop what a milestone.

![Excited Season 4 GIF by Friends - Find & Share on GIPHY](excited_season_4_gif_by_friends_-_find_s.gif)


We're back on to ***{{<text size="22.0pt" fg="#00B050" bg="lime">}}Wednesday's What Was the World Doing Ages and Ages
 Ago{{</text>}}***

So let's see where we are in our calendar year 

We saw ***{{<highlight size="20.0pt" bg="yellow">}}life begin{{</highlight>}}*** {{<text size="20.0pt">}}{{</text>}}on
 the **{{<text size="26.0pt" fg="#4472C4">}}25th February{{</text>}}**.

*{{<text size="22.0pt" bg="lime">}}Photosynthesis started{{</text>}}* {{<text size="22.0pt">}}{{</text>}}on
 the **{{<text size="22.0pt" fg="#4472C4">}}28thMarch{{</text>}}**.

***{{<text size="22.0pt" bg="fuchsia">}}Multicellular organisms{{</text>}}*** {{<text size="22.0pt">}}{{</text>}}appeared
 on the **{{<text size="22.0pt" fg="#4472C4">}}16th August{{</text>}}**.

**{{<text size="26.0pt" fg="red" bg="maroon">}}Sexual reproduction{{</text>}}** {{<text size="26.0pt" fg="red">}}{{</text>}}came
 about on the **{{<text size="22.0pt" fg="#0070C0">}}17thSeptember{{</text>}}**.

{{<text size="24.0pt" bg="olive">}}Fungi{{</text>}}{{<text size="24.0pt">}}{{</text>}}starting
 being funky on the **{{<text size="22.0pt" fg="#0070C0">}}15thNovember{{</text>}}**.

Now we have **{{<highlight-u size="28.0pt" fg="#0070C0" bg="aqua">}}fish{{</highlight-u>}}** {{<text size="28.0pt">}}{{</text>}}coming onto the scene on the **{{<text size="22.0pt" fg="#0070C0">}}20thNovember{{</text>}}**.

Anyone who knows me will know just how much I love fish. So today (the 20th November I mean) is a very special day.

First things first, *{{<text size="20.0pt" fg="#ED7D31">}}there is no such thing as a fish{{</text>}}*.

We usually like to define species by being able to group things together on the **{{<text size="20.0pt" fg="#70AD47">}}evolutionary family tree{{</text>}}** {{<text size="20.0pt" fg="#70AD47">}}{{</text>}}--- kind of along the lines of "everything after THIS branch is a reptile" and "everything after THIS branch is a mammal" etc etc. But the problem with that is that *{{<text size="18.0pt" fg="#00B0F0">}}we evolved from fish{{</text>}}*. So if you take that approach then we are all fish as well hmmm not so
 ideal.

Okay so is there another way of defining fish? Well we have to include sharks, stingrays, hagfish, sturgeon, lungfish, goldfish and tuna. Maybe they are all animals that **{{<text size="18.0pt" fg="#8FAADC">}}breathe through gills{{</text>}}** (or at least they do usually sorry lungfish), **{{<text size="18.0pt" fg="#2F5597">}}have bodies covered in scales{{</text>}}** {{<text size="18.0pt" fg="#2F5597">}}{{</text>}}(but not sometimes like in sharks, stingrays and hagfish) and **{{<text size="18.0pt" fg="#2E75B6">}}limbs in the form of fins{{</text>}}** {{<text size="18.0pt" fg="#2E75B6">}}{{</text>}}(most of the time).

WOW what a *{{<text size="20.0pt" bg="fuchsia">}}great definition{{</text>}}*.

![7 Reasons To Name Your Child After You, Other Than The ...](7_reasons_to_name_your_child_after_you_o.gif)

Now that we"ve sorted that we can have a look at what the **{{<text size="18.0pt" fg="#4472C4">}}earliest fish{{</text>}}** {{<text size="18.0pt" fg="#4472C4">}}{{</text>}}looked like (spoiler --- they weren't very pretty).

The earliest group of fish that we know about are called the ***{{<highlight size="24.0pt" bg="yellow">}}Agnatha{{</highlight>}}*** {{<text size="24.0pt">}}{{</text>}}--- or the jawless fish. Guess what. They don't have jaws.

The only remnants of this branch of the family tree are **{{<text size="18.0pt" fg="#FFC000">}}hagfish{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}and
 **{{<text size="18.0pt" fg="#FFC000">}}lampreys{{</text>}}**. They are hellish looking beings (and I"m saying that as a massive lover of all things fish).

This is a hagfish

![A hagfish with it's head poking out of a burrow](a_hagfish_with_its_head_poking_out_of_a_.jpeg)

![Truck Full of "Slime Eels" Overturns on Highway in ...](truck_full_of_slime_eels_overturns_on_hi.gif)

Hagfish have a **{{<text size="18.0pt" fg="#ED7D31">}}long cylindrical body{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}and
 **{{<text size="18.0pt" fg="#FFC000">}}no eyes{{</text>}}**. They tend to *{{<text size="16.0pt" fg="#548235">}}burrow into their food face-first{{</text>}}* {{<text fg="#548235">}},{{</text>}}as instead of having jaws they have **{{<text size="24.0pt" fg="#2F5597">}}"rasping tooth plates"{{</text>}}** {{<text size="24.0pt" fg="#2F5597">}}{{</text>}}that move together and apart to snack on dead fish. That's what you can see in the MONSTROUS GIF above. They are also capable of producing **{{<u size="18.0pt" fg="#A9D18E">}}huge amounts of slime{{</u>}}**.

The slime is actually 99% seawater and acts like a really fine sieve rather than a cohesive gel. It's a predator defence mechanism and it looks{{<text size="18.0pt" bg="fuchsia">}}really freaking weird{{</text>}}.

![image](0996104c166e2f0e.png)

So as you can imagine it was a bit of a problem when a truck carrying about **{{<text size="20.0pt" fg="#ED7D31">}}4 tonnes of hagfish{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}(who the fuck wanted 4 tonnes of hagfish) collided with another vehicle and chucked all of these "slime-eels" onto the **{{<text size="20.0pt" fg="#4472C4">}}US 101 highway in Oregon{{</text>}}**.

These hagfish got pretty stressed so there was a LOT of slime.

![Eel-Body-Image-1-07132017](eel-body-image-1-07132017.jpeg)

Ew.

Lampreys are similarly gross. They have a **{{<text size="18.0pt" fg="#ED7D31">}}very large suction device{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}that they use as a mouth that is very difficult to remove. This allows them to break the skin of their host and just enjoy ***{{<text size="18.0pt" fg="#FFC000">}}parasitising{{</text>}}*** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}off of them. Like the worst leech ever.

![Lamprey](lamprey.jpeg)

But what is kind of cool is that they have an ***{{<text size="20.0pt" fg="red">}}anadromous lifecycle{{</text>}}*** , meaning they can swim upriver to spawn. AND TO GET THERE they use those
 weird mouths to ***{{<highlight size="22.0pt" bg="yellow">}}suction themselves onto waterfalls{{</highlight>}}*** {{<text size="22.0pt">}}{{</text>}}and then fling their little bodies up the waterfall.

Let's just be glad that fish evolved jaws and stopped looking so horrendous.

Lots of love,

Flora