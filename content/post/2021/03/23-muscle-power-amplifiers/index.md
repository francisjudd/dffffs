+++
author = "Flora Judd"
title = "Muscle Power Unleashed"
date = "2021-03-23"
description = "Explore the astonishing power of muscles and their amplifying capabilities."
tags = ["Muscles","Power Amplifier","Elastic Energy","Horse Muscles","Mantis Shrimp","Predation Techniques","Acceleration","Biomechanics","Movement Efficiency","Animal Adaptations"]
categories = ["Science","Biology","Muscles","Movement Dynamics"]
image = "clicking_fingers_snapping_fingers_gif_wi.gif"
+++

Hello and welcome to today's
 *{{<text size="20.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}*

We are going to take a little look at **{{<highlight size="20.0pt" bg="yellow">}}muscles{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}and how they can achieve some pretty remarkable things that you wouldn't necessarily think they could.

Let's take our first example of *{{<text size="16.0pt" fg="#ED7D31">}}clicking{{</text>}}*. Most people can click their fingers (sorry to rub it in if you can't). **{{<text size="14.0pt" fg="#00B0F0">}}It's pretty cool{{</text>}}**.

![clicking fingers snapping fingers gif | WiffleGif](clicking_fingers_snapping_fingers_gif_wi.gif)

But have you ever thought about *{{<text size="18.0pt" fg="#00B050">}}why{{</text>}}* {{<text size="18.0pt" fg="#00B050">}}you can click your fingers{{</text>}}. If you try and create the same effect by just tapping your hand with your fingers
 you can't get anywhere near enough ***{{<text size="18.0pt" bg="lime">}}power{{</text>}}*** {{<text size="18.0pt">}}{{</text>}}to generate a loud noise.

SO what is happening here?

Well muscles are pretty awesome because they can be used as what is known as a ***{{<text size="24.0pt" fg="#FFC000">}}power amplifier{{</text>}}***. The muscle acts as a store of **{{<text size="20.0pt" bg="fuchsia">}}elastic energy{{</text>}}**. You can build this energy up slowly as you start to press down on your thumb and then BAM.

You{{<text size="18.0pt" fg="#5B9BD5">}}suddenly unload the force{{</text>}}and it results in a movement that is way faster than you could ever get with muscle power alone.

Think of a **{{<text size="16.0pt" fg="#ED7D31">}}bow and arrow{{</text>}}**. You could never chuck an arrow at the speed that they fly through the air, instead
 you slowly pull back the string (is it called a string ? I bet there's a fancy name I don't know about) and then suddenly release it and ***{{<text size="20.0pt" fg="#92D050">}}PHWOAR{{</text>}}*** {{<text size="20.0pt" fg="#92D050">}}{{</text>}}the arrow goes flying.

![Workouts Inspired by Fictional Characters | Quirk Books ...](workouts_inspired_by_fictional_character.gif)

Or at least that's what is supposed to happen. When I try it looks a bit more like this 

![Archery GIFs | Tenor](archery_gifs_tenor.gif)

Now this use of muscles as a
 ***{{<text size="20.0pt" fg="#FFC000">}}power amplifier{{</text>}}*** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}can be seen across the animal kingdom and I"m gonna show you two pretty epic examples.

***{{<text size="24.0pt">}}NUMERO UNO{{</text>}}*** **{{<text size="24.0pt">}}---
{{<text fg="#ED7D31">}}Horses{{</text>}}{{</text>}}**

**{{<highlight size="16.0pt" bg="yellow">}}Horse{{</highlight>}}** {{<highlight size="16.0pt" bg="yellow">}}**biceps**{{</highlight>}}{{<text size="16.0pt">}}{{</text>}}act as an *{{<text size="16.0pt" fg="#00B050">}}elastic energy store{{</text>}}*. While the horse is standing the muscle is passively stretched and this builds up energy. That means the leg can then be **{{<text size="18.0pt" fg="#92D050">}}catapulted{{</text>}}** {{<text size="18.0pt" fg="#92D050">}}{{</text>}}forwards when the horse runs.

Therefore, the horse can
 **{{<text size="18.0pt" fg="#5B9BD5">}}accelerate at crazy speeds{{</text>}}**. In fact the horse bicep would need to be **{{<text size="28.0pt" fg="#4472C4">}}50kg{{</text>}}** (FIFTY BRO WHAT EVEN) to get that fast by muscle power alone. The bicep actually **{{<text size="24.0pt" fg="#00B050">}}0.4kg{{</text>}}**. *{{<text size="18.0pt">}}NUTS{{</text>}}*. That is super important because it allows the horse to be light and rapid in its running.

***{{<text size="22.0pt">}}NUMERO DOS{{</text>}}*** **{{<text size="22.0pt">}}---
{{<text fg="#00B0F0">}}Mantis shrimp{{</text>}}{{</text>}}**

Now the mantis shrimp is one of my all-time favourite animals. If you haven't seen one before they look like this.

![The Mantis Shrimp | SiOWfa15: Science in Our World ...](the_mantis_shrimp_siowfa15_science_in_ou.jpeg)

*{{<highlight size="18.0pt" bg="yellow">}}Psychedelic{{</highlight>}}* *{{<text size="18.0pt" bg="fuchsia">}}looking{{</text>}}* *{{<highlight size="18.0pt" bg="aqua">}}bastards{{</highlight>}}* {{<text size="18.0pt">}}{{</text>}}I know.

Now they are **{{<text size="16.0pt">}}predators{{</text>}}** {{<text size="16.0pt">}}{{</text>}}and they live in *{{<text size="16.0pt">}}burrows{{</text>}}*. They wait and they wait and they wait until their prey is in striking distance and then ***{{<u size="72.0pt" fg="red">}}WHABAM{{</u>}}***.

They "punch" the prey SO FAST. Like SO FAST. Like accelerating **{{<highlight size="20.0pt" bg="yellow">}}faster than a .22 calibre bullet{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}kind of fast. When they collide with the prey they deliver ***{{<text size="20.0pt" fg="#ED7D31">}}160 lbs of force{{</text>}}***.

In fact if you could **{{<text size="18.0pt">}}throw a ball{{</text>}}** {{<text size="18.0pt">}}{{</text>}}at **{{<text size="36.0pt" fg="#C00000">}}1/10{{</text>}}** {{<text size="36.0pt" fg="#C00000">}}{{</text>}}of the speed that these shrimp punch their prey --- ***{{<text size="36.0pt" fg="#00B0F0" bg="blue">}}it would go into orbit{{</text>}}***.

*{{<text size="18.0pt">}}WHAT THE FUCK{{</text>}}*.

Anyways these guys are also reliant on the wonderfully elastic properties of their muscles. The *{{<text size="18.0pt" fg="#ED7D31">}}"hammer"{{</text>}}* {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}they use for punching (known much more badassly as the **{{<highlight size="20.0pt" bg="yellow">}}raptorial appendage{{</highlight>}}** ) has a large muscle that pulls the hammer back.

This compresses their exoskeleton. They then effectively put a ***{{<text size="18.0pt" fg="#7030A0">}}latch{{</text>}}*** {{<text size="18.0pt" fg="#7030A0">}}{{</text>}}on this muscle and just wait. When the prey comes by they can flick the latch and rapidly **{{<text size="20.0pt" fg="#FFC000">}}release all of that energy{{</text>}}**.

This is an (obviously) very slowed down version.

![mantis shrimp on Tumblr](mantis_shrimp_on_tumblr.gif)

COOL, EH?

So --- in conclusion --- *{{<highlight size="18.0pt" bg="yellow">}}when you click your fingers you are basically a mantis shrimp{{</highlight>}}*.

**{{<text size="24.0pt" bg="fuchsia">}}Noice{{</text>}}**.

Lots of love,

Flora