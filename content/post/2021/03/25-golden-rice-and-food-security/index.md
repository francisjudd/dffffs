+++
author = "Flora Judd"
title = "Golden Rice: A Solution"
date = "2021-03-25"
description = "Exploring the potential of Golden Rice in combating vitamin A deficiency."
tags = ["Golden Rice","Genetic Modification","Vitamin A Deficiency","Food Security","GMO","Beta-Carotene","Malnutrition","Agricultural Innovations","Child Health","Sustainable Agriculture"]
categories = ["Genetic Modification","Food Security","Health","Agriculture"]
image = "e4995018b9f47f2a.png"
+++

Helloo and welcome the incredible inbox extravaganza that is your *{{<text size="16.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* ,

Today's fact is part **{{<text size="18.0pt" fg="red">}}fact{{</text>}}** {{<text size="18.0pt" fg="red">}}{{</text>}}and part ***{{<text size="20.0pt" fg="#C00000">}}rant{{</text>}}***. We are going to have a look at one example of the awesome potential that *{{<highlight size="20.0pt" bg="yellow">}}genetic modification{{</highlight>}}* {{<text size="20.0pt">}}{{</text>}}has in helping with global food security.

Now firstly, GM crops get
 ***{{<text size="14.0pt" fg="#92D050">}}a lot of shit{{</text>}}***.

![Scientific American declares it's against GMO labeling ...](scientific_american_declares_its_against.jpeg)![I Am a Struggling Farmer And Anti-GM Activists Do Not ...](i_am_a_struggling_farmer_and_anti-gm_act.jpeg)
![Viewpoint: The faulty logic behind popular anti-GMO meme ...](viewpoint_the_faulty_logic_behind_popula.jpeg)![Charles and Anne at odds over GM crops? Princess Royal ...](charles_and_anne_at_odds_over_gm_crops_p.jpeg)

But WHY? We"ve been genetically modifying crops for **{{<text size="16.0pt" fg="#00B050">}}THOUSANDS OF YEARS{{</text>}}**. Want to know what the *{{<text size="16.0pt" fg="#ED7D31">}}ancestors of the potato{{</text>}}* {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}looked like?

![Glasgow Botanic Gardens Food Security and Climate Change](glasgow_botanic_gardens_food_security_an.png)

Doesn't really look like the potatoes you find in the supermarket, does it? We bred the potatoes that had the traits we liked and thus ***{{<text size="18.0pt" fg="#4472C4">}}modified their genetics{{</text>}}***. But no one is protesting those potatoes are they.

The example we are looking at today is **{{<text size="22.0pt" fg="#FFC000">}}Golden Rice{{</text>}}**. Now rice is consumed all over the world, but there is a problem with how it is processed.

![rice mill manufacturers | Rice, Polished rice](rice_mill_manufacturers_rice_polished_ri.jpeg)
When you first get rice you remove the **{{<highlight size="20.0pt" bg="yellow">}}outer husk{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}which *{{<text size="16.0pt" fg="#ED7D31">}}isn't edible{{</text>}}*. This leaves you with **{{<text size="18.0pt" fg="#7F6000">}}brown rice{{</text>}}**. Great we love brown
 rice.

But the problem is that it's ***{{<text size="18.0pt" fg="#70AD47">}}very unstable{{</text>}}*** {{<text fg="#70AD47">}}{{</text>}}(what a mood) as a food source. Fats can accumulate in the outer layers and when the rice is **{{<text size="16.0pt">}}de-husked{{</text>}}** {{<text size="16.0pt">}}{{</text>}}these fats mix with enzymes and creates a *{{<highlight size="22.0pt" bg="aqua">}}toxic by-product{{</highlight>}}*.

Essentially, a few days after de-husking the rice is ***{{<text size="18.0pt" fg="#4472C4">}}totally inedible{{</text>}}***.

Now don't go rushing to your cupboards!! You won't be poisoned I promise. Your brown rice is fine because it has been *{{<text size="18.0pt" fg="#ED7D31">}}heat treated{{</text>}}* {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}to **{{<text size="24.0pt" fg="#70AD47">}}denature those enzymes{{</text>}}** {{<text size="24.0pt" fg="#70AD47">}}{{</text>}}which stabilises the rice. This is quite expensive though.

The cheaper option is to **{{<text size="14.0pt" fg="#4472C4">}}take off that brown layer{{</text>}}** {{<text size="14.0pt" fg="#4472C4">}}{{</text>}}and just use the ***{{<text size="22.0pt" fg="#00B0F0">}}white grain{{</text>}}*** {{<text size="22.0pt" fg="#00B0F0">}}{{</text>}}underneath and as long as you keep it dry, it can stay good to eat for genuinely *{{<text size="18.0pt">}}hundreds of years{{</text>}}*.

![How to cook delicious rice in your Instant Pot - CNET](how_to_cook_delicious_rice_in_your_insta.gif)

But there is a problem with this. All the **{{<text size="22.0pt" fg="#ED7D31">}}vitamins, minerals and amino acids{{</text>}}** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}are in the brown layer that you just removed and you are just left with a grain that is *{{<text size="18.0pt" fg="#FFC000">}}95% carbohydrate{{</text>}}*. So for the people who rely on rice as a key food source and can't afford to heat treat the
 brown rice --- this is a big problem. They need proteins, fats, minerals, vitamins!!

One particularly heart-breaking vitamin deficiency is **{{<text size="20.0pt" fg="#4472C4">}}vitamin A deficiency{{</text>}}**. If you don't eat enough vitamin A it leads to{{<text size="24.0pt">}}blindness{{</text>}}
and{{<text size="24.0pt">}}death{{</text>}}, especially in *{{<text size="16.0pt" fg="#70AD47">}}children{{</text>}}*. In South East Asia, **{{<highlight size="22.0pt" bg="yellow">}}250 million children{{</highlight>}}** {{<text size="22.0pt">}}{{</text>}}under the age of 5 suffer from vitamin A deficiency.

Of those, **{{<text size="24.0pt" fg="#4472C4">}}25 million will go blind{{</text>}}** {{<text size="24.0pt" fg="#4472C4">}}{{</text>}}and ***{{<text size="36.0pt" fg="yellow" bg="black">}}2.5 million will die{{</text>}}***.

*{{<text size="14.0pt" fg="#00B0F0">}}FOR LITERALLY NO OTHER REASON THAN NOT GETTING ENOUGH VITAMIN A.{{</text>}}*

To tackle this, some very cool science dudes decided to put the gene for **{{<text size="22.0pt" fg="#ED7D31">}}beta-carotene{{</text>}}** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}(a precursor of vitamin A) into rice. (Yes beta-carotene is found in carrots and yes it helps prevent blindness so yes carrots do help you see in the dark). And by putting this gene in the right
 place you can make sure the vitamin ends up ***{{<text size="16.0pt" fg="#70AD47">}}in the grain{{</text>}}*** {{<text size="16.0pt" fg="#70AD47">}}{{</text>}}(important it isn't in the roots or the flowers or somewhere else totally useless)

They used two different genes and you can see the result here.

![image](e4995018b9f47f2a.png)

**{{<text size="22.0pt" fg="#FFC000">}}Golden Rice 1{{</text>}}** --- they tried using a gene from a **{{<text size="16.0pt">}}daffodil{{</text>}}**. This did make some beta-carotene in the rice grain but not very much. In fact to get your daily allowance of vitamin
 A you need to eat **{{<text size="20.0pt" fg="#4472C4">}}3kg{{</text>}}** of rice every day not so ideal.

**{{<text size="22.0pt" fg="#ED7D31">}}Golden Rice 2{{</text>}}** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}--- they tried using a gene from the soil bacterium *Erwinia uredovora.* As you can see. Much better. This time you only need to eat
 **{{<text size="24.0pt" fg="#4472C4">}}150g{{</text>}}** of rice to get your daily vitamin A.

![Journal retracts study on benefits of GMO rice Quartz](journal_retracts_study_on_benefits_of_gm.jpeg)

Now I bet you're all thinking WOW THIS IS FANTASTIC. What a lot of kiddie-winkles can be saved. This must be hitting the fields{{<text size="22.0pt">}}all over Asia{{</text>}}and the{{<text size="36.0pt">}}world{{</text>}}immediately! 

Well not so much. Trials are needed to prove beyond any doubt that the rice is safe to use and farm and there are no detrimental effects. You basically just gotta show that it's **{{<text size="18.0pt" fg="#70AD47">}}the same as normal rice{{</text>}}** {{<text size="18.0pt" fg="#70AD47">}}{{</text>}}for all intents and purposes.

But ***{{<text size="20.0pt" fg="#00B050">}}Greenpeace{{</text>}}*** have been paying people to vandalise these trials. And people are still hugely and weirdly
 opposed to genetic modification.

![Activists destroy](activists_destroy.jpeg)

Golden rice was produced in **{{<highlight size="24.0pt" bg="yellow">}}2005{{</highlight>}}**.

It wasn't until **{{<text size="26.0pt" fg="red">}}2018{{</text>}}** {{<text size="26.0pt" fg="red">}}{{</text>}}that the US approved it for cultivation and in **{{<text size="26.0pt" fg="#C00000">}}2019{{</text>}}** {{<text size="26.0pt" fg="#C00000">}}{{</text>}}it was approved for use for humans in the Philippines. BUT it still has to go through more checks to allow it to be commercially farmed.

*{{<highlight size="22.0pt" bg="yellow">}}WHAT THE HELL PEOPLE!?{{</highlight>}}* *{{<text size="22.0pt">}}{{</text>}}*

We have a **{{<text size="18.0pt" fg="#ED7D31">}}LEGITIMATE{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}and **{{<text size="24.0pt" fg="#FFC000">}}SAFE WAY{{</text>}}** {{<text size="24.0pt" fg="#FFC000">}}{{</text>}}of saving
 *{{<text size="24.0pt" fg="#92D050">}}2.5 MILLION CHILDREN EACH YEAR{{</text>}}* {{<text size="24.0pt" fg="#92D050">}}{{</text>}}who are needlessly dying of deficiencies of ***{{<text size="22.0pt" fg="#9DC3E6">}}JUST ONE VITAMIN{{</text>}}***.

**{{<text size="22.0pt">}}WHY IS THIS NOT BEING IMPLEMENTED.{{</text>}}**
**{{<text size="22.0pt">}}{{</text>}}**
{{<text size="20.0pt">}}WHY ARE PEOPLE STILL LETTING{{</text>}} **{{<text size="24.0pt" fg="#4472C4">}}OUTDATED FEARS{{</text>}}** {{<text size="24.0pt" fg="#4472C4">}}{{</text>}}{{<text size="20.0pt">}}HOLD US BACK FROM IMPLEMENTING{{</text>}} ***{{<text size="24.0pt" fg="#ED7D31">}}SERIOUSLY BENEFICIAL{{</text>}}*** {{<text size="20.0pt">}}TECHNOLOGIES.{{</text>}}

* **ahem** *

Rant over.

Hopefully time will show that it is safe and people will learn that GM is something to be embraced and not to be feared.

Hope you're having a lovely day,

Flora xx