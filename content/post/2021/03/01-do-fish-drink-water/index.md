+++
author = "Flora Judd"
title = "Do Fish Drink Water?"
date = "2021-03-01"
description = "A surprising exploration into the drinking habits of fish."
tags = ["Fish","Drinking Habits","Saltwater","Freshwater","Biology","Adaptations","Marine Life","Hydration","Aquatic Adaptations","Behavioral Ecology"]
categories = ["Marine Biology","Nature","Wildlife","Fish"]
image = "ocean_fish_gif_-_find_share_on_giphy.gif"
+++

Hello!!

Don't worry. I didn't forget about your *{{<text size="20.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}*.

But, as it is getting late (lockdown has turned me into a granny I can't help it), this shall be a brief one.

Now I know that I have a habit of yakking on about fish quite a lot when no one really asked me too but there is one fact that remains neglected **{{<highlight size="22.0pt" bg="yellow">}}YET IT IS ONE OF THE MOST CRUCIAL{{</highlight>}}**.

{{<text size="20.0pt">}}Flora --- *do fish*{{</text>}} ***{{<text size="24.0pt">}}drink{{</text>}}*** ***{{<highlight size="48.0pt" bg="aqua">}}wate{{</highlight>}}*** **{{<highlight size="48.0pt" bg="aqua">}}r{{</highlight>}}** {{<text size="20.0pt">}}?{{</text>}}

![Fish GIF - Find & Share on GIPHY](fish_gif_-_find_share_on_giphy.gif)

Finally you will have the answer.

{{<highlight size="48.0pt" bg="yellow">}}And the answer is {{</highlight>}}{{<text size="48.0pt">}}{{</text>}}

It depends.

***{{<text size="18.0pt">}}AND THAT"S NOT A COP-OUT I PROMISE{{</text>}}***

Are they a ***{{<text size="20.0pt" fg="#0070C0">}}salty fish{{</text>}}*** {{<text size="20.0pt" fg="#0070C0">}}{{</text>}}(at this point I feel like a pretty salty fish) or a ***{{<text size="20.0pt" fg="#0070C0">}}freshwater fish{{</text>}}*** {{<text fg="#0070C0">}}?{{</text>}}

And I see what you're thinking. Oh Flora is it that easy? The fish that live in *{{<highlight size="16.0pt" bg="yellow">}}sweet sweet freshwater{{</highlight>}}* {{<text size="16.0pt">}}{{</text>}}drink and those stuck in what is essentially a **{{<text size="16.0pt">}}MASSIVE{{</text>}}** {{<text size="16.0pt">}}{{</text>}}bowl of *{{<highlight size="14.0pt" bg="yellow">}}oversalted soup{{</highlight>}}* {{<text size="14.0pt">}}{{</text>}}cannot. Right? 

**{{<text size="20.0pt">}}Wrong{{</text>}}**.

*{{<text size="18.0pt">}}Salt water fish do drink.{{</text>}}*

*{{<text size="20.0pt" fg="red">}}Freshwater fish do not.{{</text>}}*

**{{<text size="28.0pt">}}I KNOW.{{</text>}}**

![Dead Fish GIFs | Tenor](dead_fish_gifs_tenor.gif)

Okay if I"ve just blown your tiny mind let me explain.

Fish{{<text size="20.0pt">}}skin{{</text>}}is pretty much ***{{<text size="20.0pt" fg="#00B0F0">}}impermeable{{</text>}}*** , meaning that{{<u size="16.0pt" bg="fuchsia">}}water cannot pass through{{</u>}}. But the{{<text size="22.0pt">}}gills{{</text>}}are very ***{{<text size="20.0pt" fg="#4472C4">}}permeable{{</text>}}***. They have to let{{<u size="16.0pt">}}dissolved gases{{</u>}}in and out and they *{{<text size="14.0pt">}}also let water in and out.{{</text>}}*

That means that in a **{{<text size="16.0pt" fg="#92D050">}}saltwater environment{{</text>}}** , the concentration of the surrounding sea is ***{{<highlight size="18.0pt" bg="yellow">}}pretty much the same{{</highlight>}}*** {{<text size="18.0pt">}}{{</text>}}as the **{{<text size="16.0pt">}}concentration of stuff in the cells{{</text>}}**. That means the water
 *overall* doesn't diffuse in or out of the cells.

This means that to get enough water, **{{<highlight size="16.0pt" bg="aqua">}}these fish drink{{</highlight>}}** (not to forget), pass a *{{<text size="16.0pt">}}small amount of urine{{</text>}}* {{<text size="16.0pt">}}{{</text>}}and{{<u size="18.0pt">}}secrete any excess salt{{</u>}}at their gills.

In ***{{<text size="22.0pt" fg="#00B0F0">}}freshwater{{</text>}}*** {{<text size="22.0pt" fg="#00B0F0">}}{{</text>}}on the other hand, the environment is
 *{{<text size="16.0pt">}}so much less salty{{</text>}}* {{<text size="16.0pt">}}{{</text>}}than the fish's cells that the **{{<highlight size="20.0pt" bg="yellow">}}water will diffuse into the cells{{</highlight>}}**.

That means these fish
 *{{<text size="20.0pt" fg="red">}}don't NEED to drink{{</text>}}* {{<text size="20.0pt" fg="red">}}{{</text>}}because the water is making its way into their cells by itself.

This means that **{{<highlight size="16.0pt" bg="aqua">}}they do not drink{{</highlight>}}** , they *{{<text size="18.0pt">}}pass copious amounts of urine{{</text>}}* {{<text size="18.0pt">}}{{</text>}}and they{{<u size="16.0pt">}}get the salts they need from their food{{</u>}}.

![image](29982a2964d413af.gif)

Hope you enjoyed a brief but
 **{{<text size="16.0pt">}}beautiful foray{{</text>}}** {{<text size="16.0pt">}}{{</text>}}into the fish world.

Love Flora xxx

PS GIF of me and all my groupies xxx

![Ocean Fish GIF - Find & Share on GIPHY](ocean_fish_gif_-_find_share_on_giphy.gif)