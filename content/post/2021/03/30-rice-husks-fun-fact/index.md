+++
author = "Flora Judd"
title = "Rice Husk Wonders"
date = "2021-03-30"
description = "Discover the surprising uses of rice husks in our world."
tags = ["Rice","Husks","Biofuel","Fireworks","Human Consumption","Agricultural Practices","Southeast Asia","Sustainability","Waste Utilization","Ecological Applications"]
categories = ["Food","Sustainability","Agriculture","Traditional Uses"]
image = "reforestation_and_rice_hull_stoves_.jpeg"
+++

Hello and welcome to your better-late-than-never *{{<text size="18.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* ,
This fact is going to be
 **{{<text size="20.0pt" fg="#ED7D31">}}brief{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}and *{{<highlight size="22.0pt" bg="yellow">}}beautiful{{</highlight>}}*.

Now you may remember we spoke about rice a bit last week. Rice is *{{<text size="18.0pt" fg="#92D050">}}unusual{{</text>}}* {{<text size="18.0pt" fg="#92D050">}}{{</text>}}as crops go because **{{<text size="18.0pt" fg="#00B050">}}almost all rice is used for human consumption{{</text>}}** {{<text fg="#00B050">}}{{</text>}}--- either directly as rice or it's made into noodles, flour, rice cakes, rice krispies, rice pudding, syrup, risotto, vinegar (seriously so many rice food products exist).

The husks, stems and stalks are not very ***{{<text size="16.0pt" fg="#FFC000">}}nutritionally viable{{</text>}}*** {{<text size="16.0pt" fg="#FFC000">}}{{</text>}}and don't break down very easily.

![UNAM discovers human bone substitute developed from agro ...](unam_discovers_human_bone_substitute_dev.jpeg)

**{{<text size="20.0pt" fg="#0070C0">}}Wheat stems{{</text>}}** {{<text size="20.0pt" fg="#0070C0">}}{{</text>}}can be used as ***{{<text size="16.0pt" bg="teal">}}straw{{</text>}}*** {{<text size="16.0pt">}}{{</text>}}and **{{<text size="22.0pt" fg="#70AD47">}}maize{{</text>}}** {{<text size="22.0pt" fg="#70AD47">}}{{</text>}}is mulched to make ***{{<text size="16.0pt" bg="olive">}}animal food{{</text>}}***. Rice husks --- not so much.

In Southeast Asia you can occasionally find ***{{<highlight size="22.0pt" fg="#ED7D31" bg="yellow">}}rice hull burners{{</highlight>}}*** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}which use rice husks as a form of biofuel, but they aren't super common.

![Reforestation and Rice Hull Stoves | , ...](reforestation_and_rice_hull_stoves_.jpeg)

INSTEAD --- what they are used for is ***{{<text size="28.0pt" fg="yellow" bg="red">}}fireworks{{</text>}}***.

Every "dot" of colour that you see in a firework is a *{{<text size="24.0pt" fg="#0070C0">}}rice husk{{</text>}}* {{<text fg="#0070C0">}}{{</text>}}covered in **{{<text size="24.0pt" fg="#00B0F0">}}gunpowder{{</text>}}** {{<text size="24.0pt" fg="#00B0F0">}}{{</text>}}(and a ***{{<text size="20.0pt" fg="#FFC000">}}metal salt{{</text>}}*** to make a pretty colour) that has then been set on fire. They are literally used as
 **{{<text size="22.0pt" fg="#ED7D31">}}main bursting charge{{</text>}}** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}in aerial fireworks shells.

![Questions for "Fireworks shower the sky with science ...](questions_for_fireworks_shower_the_sky_w.gif)

I KNOW.

What a world we live in.

Lots of love,

Flora