+++
author = "Flora Judd"
title = "Polyandry Unpacked"
date = "2021-03-02"
description = "Exploring the evolutionary twists of polyandry versus monogamy in the animal kingdom."
tags = ["Polyandry","Monogamy","Sexual Selection","Reproductive Success","Courtship Behaviors","Exaggerated Traits","Good Genes Hypothesis","Evolutionary Strategies","Behavioral Ecology","Animal Kingdom"]
categories = ["Evolution","Sexual Selection","Animal Behavior","Biology"]
image = "nerd_alert_-_austin_powers_gif_-_dork_ne.gif"
+++

Hello and welcome to another *{{<text size="18.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* ,

Today we are going to have a look at the evolutionary origins (and advantages) of
 ***{{<text size="18.0pt" bg="fuchsia">}}polyandry{{</text>}}*** {{<text size="18.0pt">}}{{</text>}}vs ***{{<text size="18.0pt" fg="#ED7D31">}}monogamy{{</text>}}***.

We spoke about sexual selection last week, where females picking the sexiest male leads to what we like to call
 **{{<text size="16.0pt" fg="red">}}pre-copulatory selection{{</text>}}**. The lady can pick which male she thinks is best after he's fought off some of the other less good males (either through literal
 *{{<text size="14.0pt">}}direct fighting{{</text>}}* {{<text size="14.0pt">}}{{</text>}}or through other *{{<text size="16.0pt">}}sexually selected traits{{</text>}}* {{<text size="16.0pt">}}{{</text>}}like **vocalisations** of those frogs or **ornaments** like those crazy long-tailed widow birds).

Now, we are going to have a look at **{{<highlight size="18.0pt" bg="yellow">}}post-copulatory selection{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}in the form of polyandry.

![Nerd Alert - Austin Powers GIF - Dork Nerdalert ...](nerd_alert_-_austin_powers_gif_-_dork_ne.gif)

***{{<text size="20.0pt" bg="fuchsia">}}Polyandry{{</text>}}*** {{<text size="20.0pt">}}{{</text>}}occurs when a female *{{<text size="14.0pt">}}mates with multiple males{{</text>}}*. In the animal kingdom
 **{{<highlight size="16.0pt" bg="yellow">}}this is the rule{{</highlight>}}** rather than the exception in
 **{{<text size="14.0pt">}}sexually reproducing organisms{{</text>}}**. It happens in
 ***{{<text size="14.0pt" fg="#00B050">}}angiosperms{{</text>}}*** {{<text size="14.0pt" fg="#00B050">}}{{</text>}}(those are flowering plants), {{<u size="18.0pt" fg="#7030A0">}}invertebrates{{</u>}}{{<text size="18.0pt" fg="#7030A0">}}{{</text>}}and the vast majority of **{{<text size="18.0pt" fg="#00B0F0">}}vertebrates as well{{</text>}}**.

This is interesting and is something of a **{{<text size="14.0pt">}}conundrum{{</text>}}** , because it is not entirely clear ** ***{{<text size="16.0pt" fg="red">}}why{{</text>}}*** **{{<text size="16.0pt" fg="red">}}females are polyandrous{{</text>}}**.

It makes evolutionary sense for ***{{<text size="16.0pt">}}males to be polyandrous{{</text>}}*** {{<text size="16.0pt">}}{{</text>}}--- the more ladies they mate with the more offspring they have
 the more genes they propagate into the next generation.
{{<highlight bg="yellow">}}Winner, winner, sexy dinner{{</highlight>}}.

But females, largely, once they have mated with a male and had their eggs fertilised --- well what's the point in having any more sex? You're done.
 *{{<text size="14.0pt">}}Box ticked{{</text>}}*. **{{<text size="14.0pt">}}Move on{{</text>}}**.

But this isn't what we see.

There are both **{{<text size="18.0pt" fg="red">}}non-adaptive{{</text>}}** {{<text size="18.0pt" fg="red">}}{{</text>}}and **{{<text size="18.0pt" fg="#00B050">}}adaptive{{</text>}}** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}explanations for why this may be. Let's start with non-adaptive.

**{{<text size="14.0pt" fg="red">}}NON-ADAPTIVE{{</text>}}**

*{{<text size="18.0pt">}}The trait is male-driven{{</text>}}*

In a **{{<highlight size="14.0pt" bg="yellow">}}finite population{{</highlight>}}** {{<text size="14.0pt">}}{{</text>}}where males want to mate with multiple females (as we mentioned above) maybe it's just the case that *{{<text size="14.0pt">}}eventually many males will mate with the same female{{</text>}}*. Those males don't
 *know* that she's already been fertilised so they might just go for it.

*{{<text size="20.0pt">}}Correlated evolution{{</text>}}*

If there is a ***{{<text size="16.0pt" fg="#C00000">}}genetic component{{</text>}}*** {{<text size="16.0pt" fg="#C00000">}}{{</text>}}to mating with multiple partners then this will be strongly selected for in males. This is because any males with genes that say
 ** {{<text bg="red">}}HAVE SEX WITH LOADS OF GIRLS{{</text>}}** are probably going to have a lot of offspring. And guess what --- those offspring are all also likely to have genes that say **{{<text bg="red">}}HAVE
 SEX{{</text>}}** {{<text bg="red">}}**WITH LOADS OF GIRLS**{{</text>}} . And on it goes until all the males in a population want to
 ** {{<text bg="red">}}HAVE SEX WITH LOADS OF GRILS{{</text>}}** sounds a bit like boarding school tbh.

**{{<text size="18.0pt" fg="#00B050">}}ADAPTIVE{{</text>}}**

*{{<text size="20.0pt">}}Convenience{{</text>}}*

This one is super depressing. It might be that ***{{<text size="16.0pt">}}levels of {{<text fg="#70AD47">}}male harassment{{</text>}}{{</text>}}*** {{<text size="16.0pt" fg="#70AD47">}}{{</text>}}are **{{<text size="22.0pt" fg="#92D050">}}so high{{</text>}}** {{<text size="22.0pt" fg="#92D050">}}{{</text>}}that just giving in and having sex with them is going to{{<u size="16.0pt">}}cause you less damage{{</u>}}{{<text size="16.0pt">}}{{</text>}}than trying to resist them. Oof. Animal kingdom is brutal.

*{{<text size="20.0pt">}}Acquisition of resources.{{</text>}}*

This one is MUCH more
 **{{<text size="18.0pt" fg="#00B050">}}feminist{{</text>}}**. A lot of the time in the animal world, males will provide their sexual partners with presents, known as ***{{<text size="22.0pt" fg="#E2F0D9" bg="green">}}nuptial gifts{{</text>}}***. If females are enjoying this
 gift-giving it might be in her interest to have a lot of sexual partners.

There is a wonderful example of this seen in butterflies. Butterflies undergo a form of behaviour called **{{<text size="24.0pt" fg="#00B050">}}"puddling"{{</text>}}** {{<text size="24.0pt" fg="#00B050">}}{{</text>}}(I know it's adorable). They sip moisture from puddles to extract salt and minerals.

![Group Of Butterfly On The Ground Stock Image - Image of ...](group_of_butterfly_on_the_ground_stock_i.jpeg)

It's mostly males that do this because they **{{<text size="18.0pt" fg="#00B050">}}incorporate the salts into their sperm{{</text>}}** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}and transfer it to the lady in a little bundle called a *{{<text size="22.0pt" fg="#92D050">}}spermatophore{{</text>}}*. Now females can mate every 2 or 3 days and so can basically{{<u size="16.0pt">}}exist entirely off these gifts{{</u>}}. Pretty ideal really.

Sometimes this benefits the males as well and in a type of grasshopper known as a **{{<text size="18.0pt">}}katydid{{</text>}}** , females eat their present *{{<text size="18.0pt" fg="#92D050">}}while the male inseminates{{</text>}}* {{<text size="18.0pt" fg="#92D050">}}{{</text>}}her. The bigger the present, the longer the male gets to put as much sperm in her as he can, increasing his odds of **{{<text size="16.0pt" fg="#00B050">}}successful fertilisation{{</text>}}**.

![Katydid_2189](katydid_2189.jpeg)

But sometimes that means that males will ***{{<text size="18.0pt" fg="#00B050">}}trick females{{</text>}}*** , like empidid flies, which put **{{<text size="16.0pt" fg="#92D050">}}seed fluff{{</text>}}** {{<text size="16.0pt" fg="#92D050">}}{{</text>}}in their food packets to distract her while he mate with her.

![Empidid fly | Project Noah](empidid_fly_project_noah.jpeg)
SOME male gifts even have ***{{<text size="20.0pt" fg="#92D050">}}compounds{{</text>}}*** {{<text size="20.0pt" fg="#92D050">}}{{</text>}}that induce *{{<text size="16.0pt" fg="#00B050">}}refractoriness{{</text>}}* {{<text size="16.0pt" fg="#00B050">}}{{</text>}}in the females MAKING THEM *{{<text size="18.0pt">}}LESS LIKELY TO MATE AGAIN{{</text>}}*. That is definite *{{<text size="14.0pt">}}low-key insect roofying{{</text>}}*.

Sorry --- got off topic.

*{{<text size="20.0pt">}}Sperm replenishment{{</text>}}*

I guess if you're a female you don't want to (quite literally) put ***{{<text size="16.0pt" fg="#00B050">}}all your eggs in one basket{{</text>}}***. What if the male you mated with is infertile or low in sperm? Then your eggs aren't actually so fertilised are they hmmm {{<u size="18.0pt" fg="#92D050">}}might as well have a back-up{{</u>}}.


When you have polyandry in a system it introduces a **{{<text size="16.0pt" fg="#ED7D31">}}new form of sexual competition{{</text>}}**. In monandry --- each male is
 ***{{<highlight size="18.0pt" bg="yellow">}}guaranteed full fertilisation{{</highlight>}}*** {{<text size="18.0pt">}}{{</text>}}of the eggs that the female produces. He can have a lot of sex with her and do a lot of fertilising. That means he fights other males for
 ***{{<text size="16.0pt" fg="#ED7D31">}}access{{</text>}}*** **{{<text size="16.0pt" fg="#ED7D31">}}to females{{</text>}}** , but then the fighting is done.

In polyandry things are a bit different. If a female is mating with multiple males so that their ejaculates
 **{{<text size="18.0pt">}}overlap{{</text>}}** {{<text size="18.0pt">}}{{</text>}}both *{{<text size="18.0pt" fg="#00B0F0">}}spatially and temporally{{</text>}}* {{<text size="18.0pt" fg="#00B0F0">}}{{</text>}}then those sperm cells are going to be competing with each other for access to her eggs.

NOW you have sexual selection **{{<text size="16.0pt">}}AFTER{{</text>}}** {{<text size="16.0pt">}}{{</text>}}mating as well as **{{<text size="16.0pt">}}BEFORE{{</text>}}**. There is now
 **{{<highlight size="14.0pt" bg="yellow">}}post-copulatory selection{{</highlight>}}.** There is ***{{<text size="18.0pt" fg="#ED7D31">}}sperm competition{{</text>}}***. Females can even bias the outcome of sperm competition to favour the ejaculates of some males over others if she thinks they're better options. This is known as
 **{{<text size="20.0pt" fg="#0070C0">}}cryptic female choice{{</text>}}**.

Hope I"ve converted all of you to polyandry (apart from one of you --- you know who you are).

Love to you,

Flora xxx