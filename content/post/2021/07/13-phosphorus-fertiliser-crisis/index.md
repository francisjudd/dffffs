+++
author = "Flora Judd"
title = "Phosphorus Predicament"
date = "2021-07-13"
description = "A shocking look at our dwindling phosphorus supply and its implications."
tags = ["Phosphorus","Fertilizers","Resource Sustainability","Food Production","Agriculture","Environmental Impact","Ecosystem Health","Decline","Prices","Resource Conservation"]
categories = ["Environment","Sustainability","Agriculture","Resource Management"]
image = "funny_cat_gifs_animals.gif"
+++

Hello and welcome to another *{{<text size="20.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

Today we are going to have a little look at something that is
{{<highlight bg="yellow">}}very important{{</highlight>}} that people don't tend to know very much about. I"m afraid it's not good news but I can insert random GIFs of cats interacting with lizards at inappropriate points to ease the journey.

Now we love
 **{{<text size="20.0pt" fg="#ED7D31">}}fertilisers{{</text>}}**. Plants love
 **{{<text size="20.0pt" fg="#ED7D31">}}fertilisers{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}(some plants at least ).

![Feed Me Seymour GIFs | Tenor](feed_me_seymour_gifs_tenor.gif)

You might have heard of *{{<text size="16.0pt" fg="#00B0F0">}}nitrogen fertilisers{{</text>}}* {{<text size="16.0pt" fg="#00B0F0">}}{{</text>}}--- those are quite well known --- but you might not know that **{{<text size="20.0pt" fg="#0070C0">}}phosphorus{{</text>}}** {{<text size="20.0pt" fg="#0070C0">}}{{</text>}}is also a very important fertiliser. Sadly, we are {{<highlight size="16.0pt" bg="yellow">}}not{{</highlight>}}{{<text size="16.0pt">}}{{</text>}}being very sensible in *how* we are using that phosphorus (I mean are we ever sensible in our use of planetary resources).

It looks like we only have about {{<text size="22.0pt" fg="#FFE699" bg="navy">}}100-300 years{{</text>}}{{<text size="22.0pt" fg="#FFE699">}}{{</text>}}of phosphorus left.

Now that's **{{<text size="16.0pt" fg="red" bg="maroon">}}pretty shocking{{</text>}}**.

![Funny Cat GIFs | Animals](funny_cat_gifs_animals.gif)

There may be more --- as a mineral it is very *{{<text size="16.0pt" fg="#92D050">}}asymmetrically distributed{{</text>}}*. It is mostly found in the
 **{{<text size="16.0pt" fg="#FFC000">}}Sahara{{</text>}}** {{<text size="16.0pt" fg="#FFC000">}}{{</text>}}and **{{<text size="16.0pt" fg="#FFC000">}}Morocco{{</text>}}** {{<text size="16.0pt" fg="#FFC000">}}{{</text>}}who then export it out to other countries, but there could be more elsewhere that we don't know about.

As these supplies start to dwindle we are seeing a huuuuge ***{{<text size="22.0pt" fg="#7030A0" bg="fuchsia">}}price spike{{</text>}}*** {{<text size="22.0pt" fg="#7030A0">}}{{</text>}}in phosphorus fertilisers. You know who that's gonna hit the hardest --- developing countries.

To put it in perspective, *{{<text size="16.0pt">}}globally{{</text>}}* ,
 **{{<text size="20.0pt" fg="#00B050">}}10kg{{</text>}}** of phosphorus are added per hectare of agricultural land.

In Africa that's only **{{<text size="20.0pt" fg="#9DC3E6">}}3kg per hectare{{</text>}}**.

In Europe it reaches **{{<text size="20.0pt" fg="#2E75B6">}}25kg per hectare{{</text>}}**.

And what is even more MAD is that only *{{<text size="22.0pt" fg="#ED7D31">}}15-30%{{</text>}}* {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}of all that phosphorus is **{{<text size="18.0pt" fg="#A9D18E">}}actually taken up by plants{{</text>}}**.
In fact, 25% of the **{{<text size="36.0pt" bg="lime">}}250{{</text>}}** {{<text size="36.0pt" bg="lime">}}**GIGATONNES**{{</text>}}{{<text size="36.0pt">}}{{</text>}}(a gigatonne is 1 trillion kg --- just so you know) has ended up in
 **{{<text size="14.0pt" fg="#BDD7EE" bg="blue">}}water bodies{{</text>}}** **{{<text size="14.0pt" fg="#BDD7EE">}}{{</text>}}** or **{{<text size="18.0pt" fg="#ED7D31" bg="purple">}}landfill{{</text>}}**.

That's *{{<text size="20.0pt" fg="#FFC000">}}62.5 billion tonnes{{</text>}}* {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}of phosphorus.

The phosphorus content of freshwater GLOBALLY is **{{<text size="20.0pt" fg="#C55A11">}}75% higher{{</text>}}** {{<text size="20.0pt" fg="#C55A11">}}{{</text>}}than it was in pre-industrial levels.

This means that globally, only 29% of agricultural land is phosphorus deficient and 71% is actually in a phosphorus surplus.

CROPS THAT HAVE A SURPLUS *{{<text size="20.0pt" fg="#ED7D31">}}DON"T NEED MORE PHOSPHORUS{{</text>}}*. IT"S VERY STABLE IN SOILS (unlike nitrates) but because people don't know about this they just keep slathering it on like there's
 no tomorrow.

![Kitty's Lizard Spasm GIF - Kitty Cat Lizard - Discover ...](kittys_lizard_spasm_gif_-_kitty_cat_liza.gif)

Now, the worst part is, as we waste ***{{<text size="18.0pt" fg="#70AD47">}}more and more phosphorus{{</text>}}*** {{<text size="18.0pt" fg="#70AD47">}}{{</text>}}it becomes more energy intensive to extract the sources that we still have. This is seriously expensive and with a lot of environmental consequences{{<text size="12.0pt">}}.{{</text>}}
{{<text size="12.0pt">}}{{</text>}}
{{<text size="12.0pt">}}To get one tonne of that sweet, sweet fertiliser you need to have{{</text>}} **{{<text size="18.0pt" fg="#843C0C">}}3 tonnes of phosphate rock{{</text>}}** {{<text size="18.0pt" fg="#843C0C">}}{{</text>}}{{<text size="12.0pt">}}(that's the bit you mine out of the ground), then you need{{</text>}} **{{<text size="22.0pt" fg="yellow" bg="black">}}1.4 tonnes of sulfuric acid{{</text>}}** {{<text size="22.0pt" fg="yellow">}}{{</text>}}{{<text size="12.0pt">}}(nasty stuff) and{{</text>}} **{{<highlight size="22.0pt" fg="#00B0F0" bg="aqua">}}11,000 litres of water{{</highlight>}}** {{<text size="12.0pt">}}.{{</text>}}
{{<text size="12.0pt">}}{{</text>}}
{{<text size="12.0pt">}}It also makes about 5.4 tonnes of{{</text>}} *{{<text size="26.0pt" fg="#ED7D31">}}phosphogypsum{{</text>}}* {{<text size="12.0pt">}}. This material is mostly made up of gypsum (CaSO4 2H2O) which
 is used a lot in the construction industry. YAY. Finally, something good!! Use it for construction!! Useful by-products!!{{</text>}}
{{<text size="12.0pt">}}{{</text>}}
{{<text size="12.0pt">}}Oh wait {{</text>}}
{{<text size="12.0pt">}}{{</text>}}
![More Funny GIFs here discovered by You Must Love Me!](more_funny_gifs_here_discovered_by_you_m.gif)
{{<text size="12.0pt">}}Phosphogypsum tends not to be used but is actually{{</text>}}{{<text size="26.0pt" fg="#5B9BD5">}}stored indefinitely{{</text>}}{{<text size="12.0pt">}}because of its WEAK LEVELS OF{{</text>}} **{{<highlight size="26.0pt" fg="#00B050" bg="yellow">}}RADIOACTIVITY{{</highlight>}}** {{<text size="26.0pt" fg="#00B050">}}{{</text>}}{{<text size="12.0pt">}}due to the presence of naturally occurring uranium and thorium, and their daughter isotopes (such a great term) radium, radon and polonium.{{</text>}}
{{<text size="12.0pt">}}{{</text>}}
{{<text size="12.0pt">}}So not very ideal {{</text>}}
**{{<text size="10.5pt" fg="#202122" bg="white">}}{{</text>}}**
{{<text size="12.0pt">}}And, of course, as we use lower quality phosphate rock we will need more of all those other components and{{</text>}} **{{<text size="20.0pt" fg="#ED7D31">}}more energy{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}{{<text size="12.0pt">}}will be needed,{{</text>}} *{{<text size="20.0pt" fg="#92D050">}}higher costs{{</text>}}* {{<text size="20.0pt" fg="#92D050">}}{{</text>}}{{<text size="12.0pt">}}etc etc etc.{{</text>}}
{{<text size="12.0pt">}}{{</text>}}
![Funny animal gifs - part 258 (10 gifs) | Amazing Creatures](funny_animal_gifs_-_part_258_10_gifs_ama.gif)
*{{<text size="12.0pt">}}This might be my most favourite GIF ever --- dedicated to
@Julia{{</text>}}*
{{<text size="12.0pt">}}{{</text>}}
{{<text size="12.0pt">}}Sorry --- humans suck.{{</text>}}
{{<text size="12.0pt">}}{{</text>}}
{{<text size="12.0pt">}}But we gotta keep learning so in the future we can suck less.{{</text>}}
{{<text size="12.0pt">}}{{</text>}}
{{<text size="12.0pt">}}Love Flora xxx{{</text>}}