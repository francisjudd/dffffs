+++
author = "Flora Judd"
title = "The Marvel of Regeneration"
date = "2021-02-18"
description = "Exploring the fascinating world of animal regeneration capabilities."
tags = ["Regeneration","Animal Biology","Flatworms","Hydra","Skin Repair","Lizards","Starfish","Sea Cucumber","African Spiny Mouse","Biological Mechanisms"]
categories = ["Biology","Regeneration","Animals","Science"]
image = "the_hydra.jpeg"
+++

Dear Fact Friend, welcome to today's *{{<text size="18.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* {{<text size="18.0pt">}}.{{</text>}}
{{<text size="18.0pt">}}{{</text>}}
In a (sort of) continuation of our discussion of multicellularity I thought that today we could have a look at how you can regrow more cells when things goes wrong --- a process known as **{{<text size="20.0pt" bg="fuchsia">}}regeneration{{</text>}}**.

Here we're gonna define regeneration as *{{<highlight size="16.0pt" bg="yellow">}}the ability to recreate lost or damaged tissues and organs, re-establishing their natural morphology and function{{</highlight>}}*.

Arguably most animals have *some* capacity to regenerate and it's only the ***{{<text size="16.0pt">}}really short-lived{{</text>}}*** **{{<text size="16.0pt">}}animals{{</text>}}** {{<text size="16.0pt">}}{{</text>}}that {{<u>}}don't have any wound response{{</u>}}.

Now --- having said that there is a big ol" spectrum of **{{<text size="14.0pt">}}regenerative capacity{{</text>}}**.

Some species have **{{<text size="18.0pt" fg="red">}}whole body regeneration{{</text>}}** {{<text size="18.0pt" fg="red">}}{{</text>}}which is exactly as cool as it sounds. You can essentially chop them, slice them and dice them into as many pieces as you like and each piece will regenerate into a **{{<text size="18.0pt" fg="#ED7D31">}}WHOLE NEW ANIMAL{{</text>}}**.

You can see this in the
 **{{<text size="16.0pt" bg="fuchsia">}}gorgeous flatworms{{</text>}}** {{<text size="16.0pt">}}{{</text>}}we saw yesterday (these were apparently not as popular as I thought they"d be with
 *someone* describing them as "weird bouncy poos").

![Want a Whole New Body? Ask This Flatworm How | Deep Look ...](want_a_whole_new_body_ask_this_flatworm_.gif)

Don't get a **{{<text size="18.0pt">}}HEAD{{</text>}}** {{<text size="18.0pt">}}{{</text>}}of yourself now little guy hehehe yeah so this decapitated flatworm is totally fine --- he will become a whole new worm.

This also happens in some **{{<highlight bg="yellow">}}annelid worms{{</highlight>}}** (this group contains earthworms, ragworms, leeches etc) and in
 **{{<highlight bg="yellow">}}Hydra{{</highlight>}}**.

![The Hydra](the_hydra.jpeg)

No --- **{{<text size="18.0pt">}}not
 *that* kind of Hydra{{</text>}}** {{<text size="18.0pt">}}{{</text>}}(although they have pretty impressive regenerative capacities). ***{{<text size="18.0pt">}}This{{</text>}}*** **{{<text size="18.0pt">}}kind of Hydra{{</text>}}**.

![Cnidaria - Invertebrates](cnidaria_-_invertebrates.jpeg)

These are little **{{<highlight bg="yellow">}}fresh-water invertebrates{{</highlight>}}** and they're very edgy. They have such extensive regenerative powers that actually they *{{<text size="24.0pt">}}don't seem to die of old age ever{{</text>}}*.

In fact they **{{<text size="20.0pt" fg="#00B050">}}don't seem to age at all{{</text>}}** {{<text size="20.0pt" fg="#00B050">}}{{</text>}}which is totally nuts.

Some animals can extensively regenerate a lot of tissues and organs, but not their whole body --- think of your starfish growing back limbs or lizards growing back tails. Or if you're really cool and you're
 a sea cucumber when you get stressed out you
***{{<highlight size="22.0pt" bg="yellow">}}LITERALLY{{</highlight>}}*** {{<highlight size="22.0pt" bg="yellow">}}***EJECT YOUR ENTIRE DIGESTIVE SYSTEM***{{</highlight>}} **{{<text size="22.0pt">}}{{</text>}}** and use it to rapidly **{{<text fg="red" bg="black">}}yeet yourself away from potential danger{{</text>}}**. ****

SO as you can imagine it kind of needs to be able to regenerate that.

![Image result for sea cucumber ejects gut gif](image_result_for_sea_cucumber_ejects_gut.gif)

Mmmmm those guts billowing in the ocean (breeze?) current.

Some have regeneration of only a ***{{<text size="14.0pt" fg="#ED7D31">}}limited number of their tissues{{</text>}}*** **{{<text size="14.0pt" fg="#ED7D31">}}*and organs*{{</text>}}** {{<text size="14.0pt" fg="#ED7D31">}}{{</text>}}
and you tend to see this in insects and crustaceans.

Some only regenerate organs but only really in a sort of "{{<u>}}clean up maintenance and repair{{</u>}} " sort of way. This is what we do (we can only
 *really* **regenerate** our livers) and this is also the level of regeneration seen in the **{{<text size="20.0pt" fg="red">}}African spiny mouse{{</text>}}**. These have spines on their back that sit on some skin that is
 ***very loose*** ( *mmmm that skin is so loose* ).

That allows the mouse to do is essentially ***{{<highlight size="20.0pt" bg="yellow">}}detach from its own skin{{</highlight>}}*** when it is bitten by a predator.

Like what the fuck.

They can lose **{{<highlight size="26.0pt" bg="aqua">}}60% of their skin{{</highlight>}}** {{<text size="26.0pt">}}{{</text>}}and still survive by regenerating the spines and the skin!!

**{{<text size="48.0pt" fg="red">}}60%!!{{</text>}}**

![Image result for spiny mouse](image_result_for_spiny_mouse.jpeg)


Some have **{{<highlight size="18.0pt" bg="yellow">}}no cellular response{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}to damage at all like *C. elegans* which is a nematode worm that looks like this.

![Image result for c elegans](image_result_for_c_elegans.jpeg)

Now regeneration is a bit tricky because regenerating tissues need to know exactly
 ***{{<text fg="red">}}what is missing{{</text>}}*** {{<text fg="red">}}{{</text>}}so that they can replace it. *THEN* you need to ***{{<text size="16.0pt" fg="red">}}functionally integrate{{</text>}}*** {{<text size="16.0pt" fg="red">}}{{</text>}}the new tissues with the existing tissues. And those tissues need to have what's called **{{<text size="24.0pt" bg="fuchsia">}}positional identity{{</text>}}** {{<text size="24.0pt">}}{{</text>}}so they "know" where they should be in relation to the system around them.

Sometimes this goes wrong.

![Image result for lizard with two tails](image_result_for_lizard_with_two_tails.jpeg)

*{{<text size="16.0pt">}}Oops got a bit overexcited growing them tails didn't you?{{</text>}}*

Hope you enjoyed this brief foray into the world of regeneration.

Lots of love,

Flora xx