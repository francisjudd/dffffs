+++
author = "Flora Judd"
title = "Glow in the Deep"
date = "2021-02-09"
description = "Exploring the dazzling world of bioluminescence and its functions in the deep sea."
tags = ["Bioluminescence","Marine Life","Anglerfish","Vampire Squid","Stoplight Loosejaw","Chemical Reactions","Deep Sea","Predator Evasion","Evolutionary Function","Ecological Roles"]
categories = ["Marine Biology","Bioluminescence","Animals","Nature"]
image = "bioluminescent_animals_photo_gallery_smi.jpeg"
+++

{{<highlight size="14.0pt" bg="aqua">}}Tena koutou e taku hoa aroha{{</highlight>}}{{<text size="14.0pt">}}{{</text>}}(that means "greetings to you my dear friend" in Maori --- see not just a biologist but a
 *cunning linguist* as well)

Welcome to today's fun fact. We are going to be looking at the wonderful world of **{{<highlight size="16.0pt" bg="yellow">}}BIOLUMINESCENCE{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}(I warned you).

**{{<highlight size="16.0pt" bg="yellow">}}Bioluminescence{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}is a form of ***{{<text size="20.0pt" bg="lime">}}chemiluminescence{{</text>}}*** {{<text size="20.0pt">}}{{</text>}}where light energy is released through a
 *{{<u size="14.0pt">}}chemical reaction{{</u>}}* {{<text size="14.0pt">}}{{</text>}}and there are quite a few animals that are able to do this.

Now when people think of glowing animals I imagine the first ones they"d think of would be your **{{<text size="14.0pt" fg="red">}}fireflies{{</text>}}** {{<text size="14.0pt" fg="red">}}{{</text>}}and your **{{<text size="16.0pt" fg="red">}}glow worms{{</text>}}**. These ones are actually pretty unique, because very few animals that live on land can use bioluminescence.
 Actually, **{{<text size="20.0pt" fg="#ED7D31">}}80%{{</text>}}** of animals that can are found in the sea.

We mentioned before that there is
 **NO** light at **ALL** in the sea once you go down below **{{<text size="16.0pt" fg="#70AD47">}}1,000m{{</text>}}**. That means being able to make your own is pretty handy because you can use it for all kinds
 of things. You can use it for {{<u>}}camouflage{{</u>}}, you can use it to *catch prey* , to
 **find mates** , to {{<text bg="silver">}}yeet yourself away from predators{{</text>}}.

{{<u size="20.0pt">}}Camouflage{{</u>}}.

Now you wouldn't expect to be able to use **{{<text size="16.0pt">}}light{{</text>}}** {{<text size="16.0pt">}}{{</text>}}to **{{<text size="18.0pt">}}camouflage{{</text>}}** {{<text size="18.0pt">}}{{</text>}}yourself but hear me out.

This is called
 **{{<highlight size="16.0pt" bg="aqua">}}counter-shading{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}or **{{<highlight size="16.0pt" bg="yellow">}}counter-illumination{{</highlight>}}**. Basically by producing light on the underside of you, you prevent a silhouette
 from forming relative to the light that filters down from the surface. Like this:

![Bioluminescent Animals Photo Gallery | Smithsonian Ocean](bioluminescent_animals_photo_gallery_smi.jpeg)


*{{<text size="22.0pt">}}Catching prey{{</text>}}*

**{{<text size="14.0pt">}}Bioluminescence{{</text>}}** {{<text size="14.0pt">}}{{</text>}}is super helpful to catch prey. You might remember the infamous example from the wonderful
 *Finding Nemo* of the anglerfish that uses its lovely little glowing barbel and then **{{<text size="26.0pt" fg="red">}}BAM{{</text>}}**.


![World of Wonder: Explorations in teaching and learning ...](world_of_wonder_explorations_in_teaching.jpeg)

You're lunch.

Also there's a pretty cool fish called a **{{<text size="20.0pt" fg="#C55A11">}}stoplight loosejaw{{</text>}}** , which is a
 type of *{{<text size="14.0pt">}}dragonfish{{</text>}}* {{<text size="14.0pt">}}{{</text>}}(we all aspire to such levels of coolness). They have a pretty sick use of bioluminescence. Firstly though, let's have a little recap of the effect that water has on light.

{{<highlight size="16.0pt" fg="#0070C0" bg="aqua">}}Blue wavelengths{{</highlight>}}{{<text size="16.0pt" fg="#0070C0">}}{{</text>}}of light travel best through water and
{{<u fg="#00B0F0">}}that's why the sea is blue{{</u>}}. All the other wavelengths of light get filtered out. So that means that by the time you get down to those deep parts of the mesopelagic (the twilight zone) anything **{{<text size="14.0pt" fg="red">}}red{{</text>}}** {{<text size="14.0pt" fg="red">}}{{</text>}}or **{{<text size="14.0pt" fg="#ED7D31">}}orangey{{</text>}}** {{<text size="14.0pt" fg="#ED7D31">}}{{</text>}}coloured looks a bit rubbish. That's why so many underwater photographers use red filters, this is an example of a picture of a coral taken without and then with a red filter.

![Buy Magic Filter Auto 50x50mm (Single) Online in Australia ...](buy_magic_filter_auto_50x50mm_single_onl.png)

So long story short --- fish eyes normally don't see
{{<text fg="red">}}red light{{</text>}}because why would you? If {{<text fg="red">}}red light{{</text>}}doesn't reach you in the water, being able to see it is totally useless. BUT this stoplight loosejaw has a bit of a lifehack.

{{<text size="14.0pt" fg="red" bg="black">}}It
 *can* see red wavelengths of light{{</text>}}. Now I know what you're thinking,

 Flora it's a fish that lives 500m below the surface **{{<text size="14.0pt">}}NO RED LIGHT GETS THERE{{</text>}}** {{<text size="14.0pt">}}{{</text>}}being able to see it is **{{<text size="14.0pt">}}POINTLESS{{</text>}}**. You're so-called "science" is all a lie. 

But what if I told you it can **{{<text size="18.0pt" fg="red" bg="black">}}make red light{{</text>}}** {{<text size="18.0pt" fg="red">}}{{</text>}}using bioluminescence?

So YES that means that this fish can essentially hunt with a torch that{{<u size="18.0pt">}}IS TOTALLY INVISIBLE TO ALL OTHER FISH SPECIES.{{</u>}}

And yes it also has that deep-sea-fish thing of looking like something that was pulled out of the
 **sack that was forgotten under the tree of life**.

![image](d774a52e42ce7a57.jpeg)

And this red thing is the light producing organ (called a **{{<text size="18.0pt" fg="yellow" bg="black">}}photophore{{</text>}}** ).

![image](523a5be41d0f708d.png)

What's really weird as well is that the reason we think it can see in red light is that it uses a derivative of **{{<text size="14.0pt" fg="#00B050">}}chlorophyll{{</text>}}** {{<text size="14.0pt" fg="#00B050">}}{{</text>}}to absorb the red wavelengths of light. This sets off a bunch of *{{<text size="14.0pt">}}chemical reactions{{</text>}}* {{<text size="14.0pt">}}{{</text>}}that can stimulate the visual systems that would **{{<text size="16.0pt" fg="#0070C0">}}normally only see blue light{{</text>}}**.

But
 ***no vertebrates*** are known to be able to synthesize chlorophyll derivatives so this is really weird. We think it might be that it gets them from the little crustaceans it eats but we're really not sure.


{{<u size="18.0pt">}}Finding mates{{</u>}}

Not gonna go into a huge amount of detail here because it's pretty similar to the finding prey thing. Some things glow and{{<text size="14.0pt" fg="red">}}that's really sexy{{</text>}}.

Simple as.

(also helps in finding mates when you live in the equivalent of a pitch black room except that room is the size of the UK)


{{<text size="18.0pt" bg="silver">}}Avoiding predators{{</text>}}

Now things get pretty fabulous here. We're going to have a quick look at one of my ALL TIME FAVOURITE species --- the **{{<text size="16.0pt">}}Vampire Squid{{</text>}}**.

(I even dressed up as one when I was 14 an age where I really should have known better).

Who wore it better?

![image](b1746d4edab4c9fe.png)

![natureza | Tumblr](natureza_tumblr.gif)
Me I know.

This guy has remained unchanged for about{{<text size="18.0pt" fg="#FFC000">}}3 million years{{</text>}}.

When he's being hunted by a predator he's got to do something to get away. If you think what a normal squid would do --- it would **{{<text size="16.0pt">}}fire some ink{{</text>}}** {{<text size="16.0pt">}}{{</text>}}at that bastard. Oh shit wait. It's totally dark. Not so useful.

*{{<text size="16.0pt">}}NOT TO WORRY{{</text>}}*.

Instead it will fire a ***{{<text size="20.0pt" fg="yellow" bg="black">}}cloud of bioluminescent mucus{{</text>}}*** {{<text size="20.0pt" fg="yellow">}}{{</text>}}(try that one out next time you sneeze if you really want to freak someone out) at the predator. This confuses the predator (obviously --- who wouldn't be confused having glowing goop fired at them)
 and the **{{<text size="14.0pt">}}squid can escape{{</text>}}**. These guys are very cool though. They have
 **amazing control** over their light-producing photophores and can literally turn them on and off like Christmas lights. This means they can make totally fabulous patterns that
 *baffle predators* or *{{<text fg="#7030A0" bg="fuchsia">}}dazzle prey{{</text>}}*.

![vampire squid on Tumblr](vampire_squid_on_tumblr.gif)

Quite brilliantly, some squid species can even **{{<highlight size="18.0pt" bg="aqua">}}DETACH BIOLUMINESCENT ARMS{{</highlight>}}** to stick to their predators. This distracts the predator for
 long enough for the squid to escape. Again. I"m not surprised. I"d be fucking terrified if someone detached an arm onto me.

This also has the advantage of **{{<text size="16.0pt" fg="#00B0F0">}}attracting secondary predators{{</text>}}** {{<text size="16.0pt" fg="#00B0F0">}}{{</text>}}to come and eat the one that tried to eat the squid. Because you know, it's quite difficult to go into stealth mode when you have *{{<text size="14.0pt">}}A GLOWING ARM STUCK TO YOUR FACE{{</text>}}*.

Now the squid has effectively signed the death warrant of the predator quite challenging to write without an arm though 

**{{<highlight bg="yellow">}}Now how the hell do you make the light{{</highlight>}}** ?

Well that will have to wait for another instalment. Let me know if you want to know --- if not we shall leave the joys of bioluminescence there.

Lots of love and stay glowy,

Flora xx