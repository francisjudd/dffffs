+++
author = "Flora Judd"
title = "DNA's Sneaky Ways"
date = "2021-02-05"
description = "Exploring how bacteria share genes and develop antibiotic resistance."
tags = ["Antibiotic Resistance","Gene Sharing","Bacterial Transformation","Plasmids","Transduction","Conjugation","Genomic Studies","Microbial Threats","Genetic Mechanisms","Health Implications"]
categories = ["Health","Microbiology","Bacteria","Genetics"]
image = "5a06718172783da5.png"
+++

Hellooooo and welcome to another Daily Fun Fact From Flora,

Now everyone gets very hot and bothered about{{<highlight-u size="14.0pt" bg="yellow">}}antibiotic resistant bacteria{{</highlight-u>}}{{<text size="14.0pt">}}{{</text>}}(quite right too --- they're a bit of a nightmare) but people don't really talk much about the actual mechanisms that *{{<text size="14.0pt">}}allow them to come about{{</text>}}*.

Bacteria have most of their genes found in a **{{<text size="16.0pt" fg="#7030A0">}}circular chromosome{{</text>}}** {{<text size="16.0pt" fg="#7030A0">}}{{</text>}}that kind of floats around in the cell. They also have some little ***{{<text size="14.0pt">}}autonomous loops of DNA{{</text>}}*** {{<text size="14.0pt">}}{{</text>}}that float around the cell called **{{<text size="24.0pt" fg="#0070C0">}}plasmids{{</text>}}**. These can carry other **{{<text size="18.0pt" fg="#00B0F0">}}accessory genes{{</text>}}** {{<text size="18.0pt" fg="#00B0F0">}}{{</text>}}(I"m gonna tell you what those are in a mo").

Bacteria are actually quite
 *{{<text size="14.0pt" fg="yellow" bg="navy">}}snazzy{{</text>}}* {{<text size="14.0pt" fg="yellow">}}{{</text>}}and have a {{<u>}}bunch of different ways{{</u>}} of sharing DNA with each other. It's about as close as bacteria get to having **{{<text size="14.0pt" fg="red">}}sex{{</text>}}** {{<text size="14.0pt" fg="red">}}{{</text>}}(if you take sex to be the mixing of different organisms genes). This gene mixing ability is pretty important given that bacteria reproduce through something called **{{<text size="16.0pt" fg="#ED7D31">}}binary fission{{</text>}}**. This means that one mother cell splits into two ***{{<text size="16.0pt">}}identical{{</text>}}*** *{{<text size="16.0pt">}}{{</text>}}* *daughter cells*.

![Binary Fission: Cell Division & Reproduction of Prokaryotes](binary_fission_cell_division_reproductio.gif)

That means that if you don't
 **{{<text fg="#ED7D31">}}mix your genes{{</text>}}** {{<text fg="#ED7D31">}}{{</text>}}around all your individuals are **{{<text fg="#ED7D31">}}gonna be the same{{</text>}}**. As soon as a
{{<u>}}disease{{</u>}} comes along or as soon as you {{<u>}}use up the only food that you all eat{{</u>}} you're a little bit
{{<u>}}fuckarood{{</u>}}.

Now let's have a look at those{{<text size="20.0pt" bg="fuchsia">}}bacterial genomes{{</text>}}.

We're going to use an analogy that I came across in a paper because I think it's totally brilliant. We will compare our bacterial cell to a smart phone.

{{<text size="20.0pt">}}Firstly you have the **{{<text fg="red">}}core genome{{</text>}}**{{</text>}}. This is your *{{<text size="20.0pt" fg="#C00000">}}operating system{{</text>}}*. It is all the genes that are totally **{{<text size="14.0pt">}}crucial for functioning{{</text>}}** {{<text size="14.0pt">}}{{</text>}}--- genes that control replication, metabolism and important components of the cell. These are the essential genes.

{{<text size="20.0pt">}}Now you have the **{{<text fg="#ED7D31">}}accessory genome{{</text>}}**{{</text>}}. These are the different{{<text size="20.0pt" fg="#C55A11">}}" *apps"*{{</text>}}that you can put on your bacteria. These are the genes that you **{{<text size="14.0pt">}}don't *need* to function{{</text>}}** {{<text size="14.0pt">}}{{</text>}}but can be nice to have as well. Antibiotic resistance would be an example here, or the genes that allow you to use different metabolic pathways.

{{<text size="20.0pt">}}Then you have the **{{<text fg="#FFC000">}}gene pool{{</text>}}**{{</text>}}{{<text fg="#FFC000">}}.{{</text>}}This is the " *{{<text size="18.0pt" fg="#BF9000">}}internet{{</text>}}* " and is made up of **{{<text size="14.0pt">}}all the different genes that you could possibly get{{</text>}}**. This means that whilst each bacteria might not have every app on the internet downloaded into its genome, but they're out
 there.

{{<text size="20.0pt">}}Then you have **{{<text fg="#92D050">}}parasitic elements{{</text>}}**{{</text>}}. These are your " *{{<text size="18.0pt" fg="#00B050">}}spam mail{{</text>}}* ".
 This can include toxins or restriction systems that make your life as a bacteria that much harder.

So that's a little insight into the genomes of a bacteria. Now how do they *share them around* ?

{{<text size="16.0pt">}}There are three ways that bacteria like to do this:{{</text>}}
{{<text size="16.0pt">}}{{</text>}}
**{{<text size="24.0pt" bg="fuchsia">}}Transformation{{</text>}}**

This one is a bit rogue. Bacteria genuinely sometimes just come across
 **DNA in their environment** , take a look at it and go{{<text size="16.0pt">}} Yeah --- I"ll have that{{</text>}} and pull it into their cells and incorporate it into their genomes.

Now not
 *all* bacteria can do this. They have to be **competent** (I know --- that's actually the science word) or competence inducible (I like to think of myself as competence inducible).

You can force a bacteria to become competent by exposing it to *{{<text size="14.0pt" fg="yellow" bg="black">}}brief, high voltage electrical pulses{{</text>}}* {{<text size="14.0pt" fg="yellow">}}{{</text>}}but I would not recommend using this as a means of making people in your life more competent.

{{<highlight size="24.0pt" bg="aqua">}}Transduction{{</highlight>}}

This one involves a virus called a **{{<text size="20.0pt" fg="#00B0F0">}}phage{{</text>}}**. What this virus loves to do is infect its bacterial host and **{{<text size="14.0pt">}}shove its DNA into the bacterial genome{{</text>}}**. Because why make your own proteins when you can get someone else to do it for you!?

It's a pretty good life.

But when the virus wants to leave and go infect a different bacterium it cuts its DNA back out of the bacteria. But sometimes
{{<u>}}it's a bit shit{{</u>}} and accidentally takes some of the bacterial DNA as well as the virus DNA.

Oops!

That means the next host not only gets the
 **viral DNA** but it gets a bit of the **hand-me-down DNA** from the last bacterium (which is definitely dead by now btw).

{{<highlight size="24.0pt" bg="yellow">}}Conjugation{{</highlight>}}

This one is the best I think. What one bacteria does is it reaches out with a little extension of its cell towards another bacterium. This is known as its **{{<text size="20.0pt" fg="red">}}sex pilus{{</text>}}**. In this sex pilus it sends over one of those autonomous little plasmids as a lovely present.

![Conjugate GIFs - Find & Share on GIPHY](conjugate_gifs_-_find_share_on_giphy.gif)
*HAPPY CHRISTMAS SUSAN*

So basically, if any of those bits of DNA that get shunted around happen to carry the gene for{{<text size="20.0pt" fg="red" bg="black">}}antibiotic resistance{{</text>}}then
**{{<text size="80.0pt">}}BAM{{</text>}}**.

It's gonna travel through that population and it's gonna do it fast. Especially if you have antibiotics in the environment Eek.

![image](5a06718172783da5.png)

**{{<text size="18.0pt">}}{{</text>}}**
**{{<text size="18.0pt">}}BUT it's not all bad!{{</text>}}** I did want to bring up something else that I think is very cool. This exchange can happen between{{<text size="14.0pt" fg="red">}}bacterial{{</text>}}and{{<text size="14.0pt" fg="red">}}non-bacterial species{{</text>}}.

 What the fuck Flora?! I hear you cry How on earth would that work?! 

Two words: **{{<highlight size="26.0pt" bg="aqua">}}sea{{</highlight>}}** {{<text size="26.0pt">}}*{{<text fg="#0070C0">}}squirts{{</text>}}*{{</text>}}.

Now these are **{{<text size="18.0pt" fg="#ED7D31">}}sessile{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}(this means they don't move and cannot move this is currently too relatable 
*{{<highlight bg="yellow">}}BORIS PLEASE FOR GODS SAKE LET US LEAVE{{</highlight>}}* ****
***ahem*** I"m fine).

![Madang --- Ples Bilong Mi Blog Archive Sea Squirts ...](madang_ples_bilong_mi_blog_archive_sea_s.jpeg)

They are basically **{{<text size="20.0pt" fg="#0070C0">}}filter feeding blobs{{</text>}}**. That's all you really need to know.

But what *is* weird is that they are made up of **{{<text size="20.0pt" fg="#00B050">}}cellulose{{</text>}}**. Now animals can't normally make cellulose. That's usually restricted to **{{<text size="18.0pt" fg="#92D050">}}plants{{</text>}}** {{<text size="18.0pt" fg="#92D050">}}{{</text>}}and you guessed it ---{{<text size="36.0pt" fg="#7030A0">}}bacteria!{{</text>}}

So we think that the genes for making cellulose in these weird little dudes ACTUALLY came from bacteria using one of those three mechanisms I told you about.

Pretty cool eh?

Have a {{<text bg="fuchsia">}}fabulous weekend{{</text>}} and I"ll see you next time.

Love Flora xxx