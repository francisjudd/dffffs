+++
author = "Flora Judd"
title = "The Great Dying"
date = "2021-02-23"
description = "Exploring the catastrophic End-Permian extinction and its lasting effects."
tags = ["End-Permian","Extinction","Siberian Traps","Lystrosaurus","Climate Change","Ocean Acidification","Recovery","Lilliput Effect","Stromatolites","Ecosystem Collapse"]
categories = ["Extinctions","Earth Science","Paleontology","History"]
image = "0d9eb954e1725dd8.png"
+++

WELL HOWDY THERE.

![Howdy Howdy GIFs | Tenor](howdy_howdy_gifs_tenor.gif)

Welcome to another *{{<text size="22.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}*.

We're gonna have another look at one of those *{{<highlight size="26.0pt" fg="#ED7D31" bg="yellow">}}sexy extinctions{{</highlight>}}* {{<text size="26.0pt" fg="#ED7D31">}}{{</text>}}( **{{<text size="14.0pt" fg="red">}}sextinctions{{</text>}}** ? No that definitely sounds like something else )

If you remember this *{{<text size="16.0pt">}}joyous timeline{{</text>}}* {{<text size="16.0pt">}}{{</text>}}from yesterday we are now going to have a look at the middle star --- the **{{<text size="18.0pt" fg="#4472C4">}}End-Permian extinction{{</text>}}** {{<text size="18.0pt" fg="#4472C4">}}{{</text>}}(that little Late Devonian one isn't good enough for us I"m afraid). Sometimes known under the very jolly name of
 **{{<text size="24.0pt" bg="red">}} The Great Dying {{</text>}}**.
Mmmm sounds like **{{<text size="16.0pt">}}fun{{</text>}}**.

![image](0d9eb954e1725dd8.png)

This took place about **{{<highlight size="22.0pt" bg="yellow">}}252 million years ago{{</highlight>}}** and it was a
 *{{<text size="22.0pt">}}BIG ONE{{</text>}}* {{<text size="22.0pt">}}{{</text>}}.

It caused the extinction of **{{<text size="22.0pt" fg="red" bg="maroon">}}over 90% of all species{{</text>}}** {{<text size="22.0pt" fg="red">}}{{</text>}}and recovery of the Earth took about *{{<highlight size="26.0pt" bg="yellow">}}8 or 9 MILLION YEARS{{</highlight>}}*.

Yeah that's quite a while.

Right now do you remember how last time there was an extinction because
 **{{<highlight size="16.0pt" fg="#0070C0" bg="aqua">}}things got really bloody cold{{</highlight>}}** {{<text size="16.0pt" fg="#0070C0">}}{{</text>}}and glaciers locked up so much water that **{{<text size="18.0pt">}}sea levels dropped by{{</text>}}** **{{<text size="48.0pt" fg="#2E75B6">}}100m{{</text>}}** ? Well we have a
 *bit of a contrast* in this one.

The End-Permian event hosted the **{{<text size="22.0pt" fg="#FFC000" bg="red">}}HOTTEST conditions{{</text>}}** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}that this little planet has seen for the last ***{{<text size="16.0pt">}}540 million years{{</text>}}***.

So Flora *{{<text size="20.0pt">}}WHAT THE FUCK HAPPENED TO RENDER THIS BEAUTIFUL EARTH SUCH AN INHOSPITABLE FLAMING MESS{{</text>}}* !?!

{{<u>}}Excellent question{{</u>}}.

Well a lot of people think that the **{{<highlight size="20.0pt" bg="yellow">}}Siberian Traps{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}were responsible. The Siberian Traps are a big ol" ***{{<text size="18.0pt" fg="red">}}igneous province{{</text>}}*** {{<text size="18.0pt" fg="red">}}{{</text>}}that started spewing out lava.

Now when I saw spewing out lava I don't mean this

![Lava GIF - Find & Share on GIPHY](lava_gif_-_find_share_on_giphy.gif)

I mean like this --- all over Russia.

![flowing-lava | Tumblr](flowing-lava_tumblr.gif)

Also isn't that the most *fucking calming* thing you"ve ever seen? If you're stressed --- Google lava GIFs they are bizarrely soothing.

ANWAYS this lava started being spewed out and it *{{<text size="16.0pt">}}just didn't stop{{</text>}}*. There were
**{{<text size="36.0pt" fg="red" bg="black">}}4 million m3{{</text>}}** {{<text size="26.0pt" fg="red">}}{{</text>}}of material ejected by these volcanoes. Now that number doesn't mean anything does it?

Well it's the equivalent amount of lava that you would need to cover
 ***{{<text size="24.0pt" fg="#00B050">}}all of the UK{{</text>}}*** *{{<text size="24.0pt" fg="#00B050">}}{{</text>}}* in a **{{<highlight size="24.0pt" bg="yellow">}}16km thick layer{{</highlight>}}** of
 *{{<text size="20.0pt" fg="red">}}molten-ass rock{{</text>}}*. Now this makes sense when you look at what the Siberian Traps actually look like.

![Science E-Portfolio: Siberian Traps](science_e-portfolio_siberian_traps.jpeg)

Look at all that igneous rock. {{<text fg="#7030A0" bg="fuchsia">}}Geologist's wet dream{{</text>}}{{<text fg="#7030A0">}}{{</text>}}right there.

Now --- hold up a second. Yes volcanoes do release a lot of greenhouse gases BUT
 *{{<text size="14.0pt">}}nowhere near enough{{</text>}}* {{<text size="14.0pt">}}{{</text>}}to cause the increases in CO2 that we see in this time.

Ah yes. Well there is one small complication there. That is the fact that --- not only did the Siberian Traps
{{<text size="18.0pt" fg="red">}}erupt{{</text>}}like crazy, they erupted ***{{<text size="20.0pt" fg="#ED7D31">}}through the petroleum and shale deposits{{</text>}}*** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}that had been built up during the Cambrian and Proterozoic periods.

So you *{{<text size="16.0pt">}}NOT ONLY{{</text>}}* {{<text size="16.0pt">}}{{</text>}}have lava but you have lava *{{<text size="18.0pt" fg="yellow" bg="black">}}BLOWING UP{{</text>}}* {{<text fg="yellow">}}{{</text>}}through **{{<text size="22.0pt">}}LAYERS AND LAYERS{{</text>}}** {{<text size="22.0pt">}}{{</text>}}of highly flammable fuels.

![This is what the 'i' in iPhone actually stands for | JOE.co.uk](this_is_what_the_i_in_iphone_actually_st.gif)

*{{<u size="18.0pt">}}Not really so ideal.{{</u>}}*

This is what released over **{{<highlight size="22.0pt" bg="yellow">}}100,000 gigatons of carbon dioxide{{</highlight>}}** {{<text size="22.0pt">}}{{</text>}}through these massive ***{{<text size="24.0pt" fg="#C00000" bg="silver">}}"kilometre wide blow-outs"{{</text>}}*** {{<text size="24.0pt" fg="#C00000">}}{{</text>}}where huge areas of land would pretty much explode.

This caused ocean surface temperatures to increase by **{{<text size="20.0pt" fg="#ED7D31">}}5{{</text>}}** **{{<text size="20.0pt" fg="#ED7D31">}} {{</text>}}** **{{<text size="20.0pt" fg="#ED7D31">}}C{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}at the equator and over **{{<text size="22.0pt" fg="#ED7D31">}}15{{</text>}}** **{{<text size="22.0pt" fg="#ED7D31">}} {{</text>}}** **{{<text size="22.0pt" fg="#ED7D31">}}C{{</text>}}** {{<text fg="#ED7D31">}}{{</text>}}at higher latitudes. Such hot temperatures and the insane amounts of {{<text size="20.0pt" bg="lime">}}ocean acidification{{</text>}}{{<text size="20.0pt">}}{{</text>}}led to widespread
{{<text size="20.0pt" fg="#B4C7E7" bg="navy">}}ocean anoxia{{</text>}}{{<text size="20.0pt" fg="#B4C7E7">}}{{</text>}}(no oxygen :( oops ) and huge loss of reefs. I mean tbh everything in the marine environment died.

So what happened next?! How did the world recover?!

We marine ecosystems were first replaced by these things called
 **{{<text size="22.0pt" fg="#4472C4">}}stromatolites{{</text>}}** {{<text size="22.0pt" fg="#4472C4">}}{{</text>}}which are basically **{{<text size="22.0pt" fg="#00B0F0">}}microbial mats{{</text>}}** {{<text size="22.0pt" fg="#00B0F0">}}{{</text>}}and a few ***{{<text size="22.0pt" fg="#ED7D31">}}disaster taxa{{</text>}}*** , such as bivalves (the molluscs that have two shells --- so clams, oysters, mussels etc). You can still see stromatolites in places like Shark Bay in Australia and
 they look like this.

![faslanyc: Antarctica: Stromatolites and Rompehielos](faslanyc_antarctica_stromatolites_and_ro.jpeg)

Weird eh?

So for this extinction there were some land animals and surprisingly enough --- some of them did manage to survive. The devastated land was mostly inherited by a group of small herbivores called
 ***{{<text size="22.0pt" fg="red">}}Lystrosaurus{{</text>}}*** , meaning
 **{{<highlight bg="yellow">}}"shovel lizard"{{</highlight>}}** (I love it too), probably because it could burrow.

There were also some small pioneer plants, like a group called
 *Pleuromeia* , which called dense thickets that the *Lystrosaurus* would be able to feed on. This is what things might have looked like post-extinction.

![Space and Earth Science: Ancient Environment - Land ...](space_and_earth_science_ancient_environm.jpeg)

*{{<text size="16.0pt">}}Makes corona look like a doddle doesn't it.{{</text>}}*

Now there's a very cool pattern that you tend to see after extinctions which is that most of the taxa is pretty
 **{{<text size="18.0pt" fg="#70AD47">}}small{{</text>}}**. Makes sense --- if there's less of you it's easier to survive because you need less food and you can shelter more easily. Also you're more likely to have a rapid life cycle that helps your populations
 recover.

This is known as the ***{{<highlight size="24.0pt" bg="aqua">}}Lilliput effect{{</highlight>}}*** {{<text size="24.0pt">}}{{</text>}}named after the island of Lilliput in *Gulliver's Travels* which is inhabited by a
{{<u size="16.0pt">}}miniature race of people{{</u>}}.

But things did take a **{{<text size="20.0pt">}}LONG ASS time{{</text>}}** to recover.

See you on Thursday for the next (and final) big extinction. Of course tomorrow is
 ***{{<text size="22.0pt" fg="#00B050" bg="lime">}}Wednesday's What Was the World Doing Ages and Ages Ago{{</text>}}*** so we will take a break from all this extinction for that. Get excited things are getting sexy again.

Lots of love to you,

Flora xx