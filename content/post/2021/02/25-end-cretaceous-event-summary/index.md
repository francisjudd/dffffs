+++
author = "Flora Judd"
title = "Meteor Madness"
date = "2021-02-25"
description = "Diving into the extinction event that wiped out the dinosaurs."
tags = ["Cretaceous","Dinosaurs","Chicxulub Impact","Extinction Event","Adaptive Radiations","Fern Spike","Species Loss","Paleontological Evidence","Earth History","Mass Extinction"]
categories = ["Extinction Events","Dinosaurs","Earth Science","History"]
image = "the_chicxulub_meteor_impact.jpeg"
+++

Hello and welcome to another
 *{{<text size="22.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* ,

The final instalment of our extinction excitement has come. It's **{{<text size="16.0pt" fg="red">}}66 million years ago{{</text>}}**. This is the **{{<text size="16.0pt">}}sexy{{</text>}}** {{<text size="16.0pt">}}{{</text>}}one --- the ***{{<highlight size="18.0pt" bg="yellow">}}End-Cretaceous Event{{</highlight>}}***.

![image](d6875d347173dbe7.png)

{{<text size="26.0pt" bg="red">}}The dinosaurs are dead{{</text>}}{{<text size="26.0pt">}}.{{</text>}}

{{<text size="36.0pt">}}Get ready for a {{<highlight bg="yellow">}}fuck-off meteor{{</highlight>}}{{</text>}}.

![The Asteroid that Killed the Dinosaurs and Punched a Hole ...](the_asteroid_that_killed_the_dinosaurs_a.gif)


***{{<text size="80.0pt" fg="#ED7D31">}}BOOOOOOOM.{{</text>}}***

Sorry --- what I *meant to say* is that
 *there is a lot of evidence* that the **{{<text size="16.0pt">}}end-Cretaceous mass extinction{{</text>}}** {{<text size="16.0pt">}}{{</text>}}coincided with an{{<highlight size="18.0pt" bg="yellow">}}extra-terrestrial impact{{</highlight>}}.

* **ahem** *

Now this is known as the
 ***{{<text size="22.0pt" fg="red">}}Chicxulub impact{{</text>}}*** {{<text size="22.0pt" fg="red">}}{{</text>}}landed in what is now the Yucatan peninsula in Mexico. The meteor was thought to have been about **{{<text size="24.0pt" fg="#00B0F0">}}15km wide{{</text>}}**.

Yes. It was a ***{{<text size="18.0pt" fg="#0070C0">}}hefty chonker{{</text>}}***.

It formed a crater that was over **{{<highlight size="22.0pt" bg="yellow">}}180km in diameter{{</highlight>}}** {{<text size="22.0pt">}}{{</text>}}and is named after the town Chicxulub which is *{{<text size="14.0pt">}}near the centre{{</text>}}* {{<text size="14.0pt">}}{{</text>}}where the crater landed. To be honest though --- with a crater 180km in diameter it should really be called *{{<text size="20.0pt" bg="fuchsia">}}The Whole of Fucking Mexico Impact{{</text>}}* {{<text size="20.0pt">}}{{</text>}}but apparently no one thought of that.

![The Chicxulub Meteor Impact](the_chicxulub_meteor_impact.jpeg)

Now there was another little problem here. Remember the **{{<text size="18.0pt" fg="red">}}Siberian Traps{{</text>}}** {{<text size="18.0pt" fg="red">}}{{</text>}}that poured out all that lava in the last extinction event? Well I have some new ones for you --- the **{{<text size="20.0pt" fg="red" bg="black">}}Deccan Traps{{</text>}}**. The impact triggered this igneous province to release a cheeky **{{<text size="22.0pt" fg="#ED7D31">}}1 million km3 of lava{{</text>}}**.

YOU KNOW WHAT THAT MEANS

more lava gifs hehe

![Lava Animated Gifs ~ Gifmania](lava_animated_gifs_gifmania.gif)
yum

The impact also ever so slightly *{{<text size="16.0pt">}}burnt through the oil fields{{</text>}}* {{<text size="16.0pt">}}{{</text>}}in the Gulf of Mexico which was 100% not ideal.

This event was so intense that it can be seen in sedimentary rocks with the untrained eye.{{<highlight size="16.0pt" bg="yellow">}}EVERYTHING JUST DIES{{</highlight>}}. These rocks have levels of **{{<text size="18.0pt" fg="#ED7D31">}}iridium{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}(usually found in asteroids) that are about 1,000 times higher than lower rock levels.

This *{{<text size="16.0pt">}}helpful man{{</text>}}* {{<text size="16.0pt">}}{{</text>}}will point out the boundary. It's really difficult to spot so I"m using him as a visual aid.

![K-T boundary clay (Cretaceous-Tertiary boundary, 65 Ma; ro ...](k-t_boundary_clay_cretaceous-tertiary_bo.jpeg)

*Thanks Brian*.

The **{{<highlight size="16.0pt" bg="yellow">}}dust cloud{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}created from this impact would have caused{{<text size="18.0pt" fg="yellow" bg="black">}}many months of near-total darkness{{</text>}}. Quite a lot of rocks would have been vapourised
 upon impact which isn't so great for the ol" lungs.

We think about ***{{<text size="18.0pt" fg="#2E75B6">}}75% of all species went extinct{{</text>}}***.

Problem. **{{<text size="18.0pt" fg="#00B050">}}Most plants died{{</text>}}** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}because they
 *couldn't photosynthesise*.

BUT luckily ***{{<text size="20.0pt" bg="lime">}}ferns{{</text>}}*** {{<text size="20.0pt">}}{{</text>}}that were adapted to live in dark conditions survived!! The pollen records that we have show a fern spike so really ferns were *{{<text size="20.0pt" bg="fuchsia">}}thriving{{</text>}}* *{{<text size="20.0pt">}}{{</text>}}* at this point okay maybe not quite thriving but ***{{<text size="16.0pt">}}surviving{{</text>}}***.

In terms of the land animals, apart from a few cold-blooded species like sea turtles and crocodilians (I"ll let you guess what those are), no **{{<text size="22.0pt" fg="red">}}tetrapods{{</text>}}** {{<text size="22.0pt" fg="red">}}{{</text>}}(four limbed things) survived that ***{{<highlight size="22.0pt" bg="yellow">}}weighed more than 25kg{{</highlight>}}***.

This is another example of the **{{<text size="16.0pt" fg="#ED7D31">}}Lilliput Effect{{</text>}}** {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}we spoke about last time where after an extinction things tend to be quite smoll. The mammals, for example, were all *{{<highlight size="18.0pt" bg="yellow">}}pretty much rat-sized{{</highlight>}}*.

The world that followed was colonised by **{{<text size="20.0pt" fg="#7030A0">}}small birds{{</text>}}** {{<text size="20.0pt" fg="#7030A0">}}{{</text>}}and **{{<text size="20.0pt" fg="#7030A0">}}mammals{{</text>}}**. Now that all those pesky dinosaurs
 were gone it opened up LOADS of niches for us mammals to exploit. There were what we like to call ***{{<text size="22.0pt" fg="#FFC000">}}adaptive radiations{{</text>}}*** , where organisms
 *rapidly* diversify from their ancestral species into new forms.

In terms of **{{<text size="16.0pt" fg="#4472C4">}}fish{{</text>}}** {{<text size="16.0pt" fg="#4472C4">}}{{</text>}}--- things were actually much less bad compared to some other extinctions. ***{{<highlight size="22.0pt" fg="#4472C4" bg="aqua">}}Over 90%{{</highlight>}}*** {{<text size="22.0pt" fg="#4472C4">}}{{</text>}}of bony fish families survived and only ***{{<text size="22.0pt" fg="#9DC3E6" bg="blue">}}7 out of 41{{</text>}}*** {{<text size="22.0pt" fg="#9DC3E6">}}{{</text>}}families of modern sharks, skates and rays disappeared.

Sadly though it was the **{{<text size="20.0pt" fg="#ED7D31">}}end of the dinosaurs{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}as we know them. I mean we still have **{{<text size="16.0pt" fg="red">}}chickens{{</text>}}** {{<text size="16.0pt" fg="red">}}{{</text>}}which are basically living dinosaurs Not quite the same though is it.

A
 ***{{<highlight size="22.0pt" bg="yellow">}}few groups{{</highlight>}}*** {{<text size="22.0pt">}}{{</text>}}of dinosaurs made it that would become modern day reptiles and birds but the vast majority were wiped out. There are some really ***{{<text size="22.0pt" fg="#7030A0" bg="fuchsia">}}hilariously awful{{</text>}}*** {{<text size="22.0pt" fg="#7030A0">}}{{</text>}}artistic renditions of this. Please enjoy.

![Tyrannosaurus Rex Dinosaurs Dying Digital Art by Elena ...](tyrannosaurus_rex_dinosaurs_dying_digita.jpeg)

![Illustration of the K/T Event at the end of the Cretaceous ...](illustration_of_the_kt_event_at_the_end_.jpeg)

Mmmm so realistic.

Or my personal favourite

![Babisu Kourtis and his Amazing Skills. | The Sketchcard ...](babisu_kourtis_and_his_amazing_skills_th.jpeg)

Sorry dinosaur --- **{{<text size="18.0pt" fg="#B4C7E7">}}floaty God man{{</text>}}** {{<text size="18.0pt" fg="#B4C7E7">}}{{</text>}}{{<text size="18.0pt">}}says{{</text>}} ***{{<highlight size="20.0pt" bg="yellow">}}no{{</highlight>}}***.

Hope you enjoyed. Stay tuned for more facts.

Lots of love,

Flora xx