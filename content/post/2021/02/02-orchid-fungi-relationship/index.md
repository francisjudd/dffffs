+++
author = "Flora Judd"
title = "Orchids and Their Fungal Friends"
date = "2021-02-02"
description = "Exploring the bizarre symbiosis between orchids and fungi."
tags = ["Orchids","Fungi","Symbiosis","Germination","Plant Ecology","Nutrient Acquisition","Photosynthesis","Bizarre Plants","Underground Orchids","Ecological Roles"]
categories = ["Plants","Nature","Botany","Symbiosis"]
image = "scaning_electronmicrograph_of_orchid_pel.png"
+++

Hola and welcome to your second fun fact of the week,

Today we are going to be looking at **{{<text size="18.0pt" fg="#7030A0">}}orchids{{</text>}}**.

Orchids are a fabulous and bizarrely diverse group of
 **flowering plants**. The name orchid itself comes from the Ancient Green work
 *** rkhis*** which means{{<text size="16.0pt" fg="#002060">}}testicle{{</text>}}, because of the shape of the twin tubers some of them have. In fact they used to be known as{{<highlight size="24.0pt" bg="yellow">}} bollockwort {{</highlight>}}in Middle English.

They're actually the second largest group of flowering plants with a current species count of **{{<text size="24.0pt" fg="red">}}28,000{{</text>}}** {{<text size="24.0pt" fg="red">}}{{</text>}}and comprise (up to --- figures vary)
{{<text size="20.0pt" bg="lime">}}11% of *ALL SEED PLANTS*{{</text>}}.

Basically there are a **FUCK TON** of orchids out there. And some of them are super crazy looking.

This one is called the
 **{{<text size="16.0pt" fg="#7030A0">}}flying duck orchid{{</text>}}**.

![PlantFiles Pictures: Species Orchid, Caleana, Flying Duck ...](plantfiles_pictures_species_orchid_calea.jpeg)


Perhaps the **{{<text size="20.0pt" fg="#ED7D31">}}monkey face orchid{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}is more your style?

![16 best Unusual Plants images on Pinterest | Rare plants ...](16_best_unusual_plants_images_on_pintere.jpeg)



But I think my all-time favourite has to be the **{{<text size="20.0pt" fg="#E025DE">}}ballerina orchid{{</text>}}**.

![10 Most Beautiful but Strange Flowers - World's Weirdest ...](10_most_beautiful_but_strange_flowers_-_.jpeg)


LOOK HOW COOL THEY ARE!

Tune in right to the end there's a
 *bonus orchid* there if you fancy a look. I really hope your testicles don't look like that though if they do I recommend seeking
{{<u>}}medical attention{{</u>}}.

BUT that's not what we're here to talk about. What we are here to talk about is their weird germination habits.

Orchid seeds are **{{<text size="16.0pt">}}TINY{{</text>}}**. Usually between **{{<text size="20.0pt" bg="lime">}}0.35mm{{</text>}}** {{<text size="20.0pt" bg="lime">}}{{</text>}}and **{{<text size="20.0pt" bg="lime">}}1.5mm{{</text>}}** {{<text size="20.0pt" bg="lime">}}{{</text>}}long.

![Orchid Seeds | thismodernwife](orchid_seeds_thismodernwife.jpeg)

This can cause problems for the little orchid seedling because that means there are basically **{{<u size="14.0pt">}}no food reserves{{</u>}}** {{<text size="14.0pt">}}{{</text>}}for it in there.

That's one of the great advantages of having a seed --- the parent plant packages up some nice snacks (maybe a
{{<text fg="red">}}Fruit Winder{{</text>}}and a {{<text fg="red">}}Frube{{</text>}}
* **other brands are available** *) inside the seed for the seedling to use before it can make any of its own energy. You have to remember that whilst plants are great at making their own food they have to first grow a **{{<text size="14.0pt">}}shoot{{</text>}}** {{<text size="14.0pt">}}{{</text>}}that can reach the surface and the light. But if you look at that tiny seed there's basically ***{{<text size="14.0pt">}}no food{{</text>}}*** {{<text size="14.0pt">}}{{</text>}}in there to do that with.

How on earth is the little orchid going to grow big enough to reach the
{{<highlight bg="aqua">}}surface of the soil{{</highlight>}}, {{<highlight bg="yellow">}}get to the light{{</highlight>}} and {{<text bg="lime">}}start photosynthesising{{</text>}}?!

Well it turns out they have some
 **friends** (ooh friends).

There's a handy little{{<text size="20.0pt" fg="red">}}fungus{{</text>}}on hand to help. The orchid *{{<text size="16.0pt">}}allows itself to be infected{{</text>}}* {{<text size="16.0pt">}}{{</text>}}by the fungus which provides nutrients, mostly in the forms of
 **carbohydrates** (mmmm we all need some good carbohydrates).

The infection occurs after the embryo takes up water and swells, rupturing the seed coat. The embryo produces a few little **{{<text size="16.0pt">}}root hairs{{</text>}}** {{<text size="16.0pt">}}{{</text>}}and this is
 *{{<text size="16.0pt">}}very quickly{{</text>}}* {{<text size="16.0pt">}}{{</text>}}colonised by the fungus. The fungus then makes **{{<highlight size="14.0pt" bg="yellow">}}special coil structures{{</highlight>}}** {{<text size="14.0pt">}}{{</text>}} *inside* the cells of the orchid which allow for a big surface area for nutrients to be exchanged over. These coils are called **{{<u size="20.0pt" fg="red">}}pelotons{{</u>}}** {{<text size="20.0pt" fg="red">}}{{</text>}}and they look like this:

![Scaning electronmicrograph of orchid pelotons](scaning_electronmicrograph_of_orchid_pel.png)

Sometimes the fungus is the *{{<text size="16.0pt">}}only source of nutrition{{</text>}}* the orchid receives during its first years of life. In fact some species of orchid don't photosynthesise
 at all and solely rely on their fungal friends for food. These orchids can live
 **{{<text size="16.0pt">}}underground{{</text>}}** {{<text size="16.0pt">}}{{</text>}}because --- hey who needs light to make food when you have a fungus that can do that all for you?

![Rhizanthella --- Underground Orchid | Orchids of South-west Australia](rhizanthella_underground_orchid_orchids_.jpeg)

Here's an underground orchid. Just chillin.

This is an
 **unusual interaction**. Usually when plants interact with fungi the plant supplies the fungus with **{{<text size="20.0pt" fg="#4472C4">}}carbon{{</text>}}** {{<text size="20.0pt" fg="#4472C4">}}{{</text>}}(it has lots because it photosynthesises) in exchange for **{{<text size="18.0pt" fg="#ED7D31">}}phosphorous{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}and **{{<text size="18.0pt" fg="#ED7D31">}}nitrogen{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}(which plants can struggle to extract from the soil). But
 *here* there seems to be a flow of carbon that goes **both ways**
 *{{<text size="16.0pt">}}as well as{{</text>}}* {{<text size="16.0pt">}}{{</text>}}providing **phosphorous** and
 **nitrogen** --- so the fungus is doing a lot of the heavy lifting. In fact sometimes (around 400 species) there is no flow of carbon from the plant and **{{<text size="20.0pt">}}all the nutrients{{</text>}}** are supplied by the fungus.

It's no wonder then that sometimes the fungi turn against their plants. Occasionally they become parasitic and actually end up
{{<text fg="red">}}killing the orchid{{</text>}}. (gasp). But most of the time the orchid is able to keep them in check and they enjoy a long and fruitful and weirdly intertwined life together.

Lots of love to you,

Flora xxx




Here's the bonus orchid --- the **{{<text size="14.0pt">}}white egret orchid{{</text>}}**

![White Flowers Seeds - White Egret Orchid - Orchid Seeds - Rare Orchid Seeds- Japanese Flower Seeds - Egrow 200pcs](white_flowers_seeds_-_white_egret_orchid.jpeg)

LIVE YOUR LIFE WITH NO EGRETS <3
 just egret orchids