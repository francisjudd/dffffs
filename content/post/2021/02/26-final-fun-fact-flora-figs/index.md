+++
author = "Flora Judd"
title = "Figgy Factoids"
date = "2021-02-26"
description = "Explore the wild world of figs and their wasp buddies."
tags = ["Figs","Pollination","Wasp Relationships","Vegetarian Food","Syconium","Symbiotic Partnerships","Germination","Ecological Roles","Biodiversity","Plant Ecology"]
categories = ["Botany","Food Science","Vegetarianism","Nature"]
image = "the_fig_shake_by_samuel_james_pillar_on_.gif"
+++

Hello and welcome to your final *{{<text size="20.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* ,

Get ready for some figgy fun.

![The Fig Shake by Samuel James Pillar on Dribbble](the_fig_shake_by_samuel_james_pillar_on_.gif)

Now I don't know if you remember when people started talking about whether **{{<highlight size="22.0pt" bg="yellow">}}figs{{</highlight>}}** {{<text size="22.0pt">}}{{</text>}}are suitable for ***{{<text size="18.0pt" fg="#00B050">}}vegetarians/vegans{{</text>}}*** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}or not.

![image](62076c2ccc8ec501.png)
![image](99bb52c21b76f66d.png)

It caused a **{{<text size="20.0pt" fg="#ED7D31">}}bit of a fuss{{</text>}}**. I"m going to tell you what's ***{{<text size="16.0pt">}}actually behind{{</text>}}*** {{<text size="16.0pt">}}{{</text>}}these **{{<text size="16.0pt" fg="#FFC000">}}rambunctious headlines{{</text>}}**.

So figs are all the plants in the genus *{{<highlight size="24.0pt" bg="yellow">}}Ficus{{</highlight>}}* {{<text size="24.0pt">}}{{</text>}}and there are between ***{{<text size="22.0pt" fg="#ED7D31">}}800 and 1,000 species{{</text>}}*** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}of them. That's {{<u>}}a lot{{</u>}} of figs.

Now all species of *Ficus* have a **{{<text size="18.0pt">}}unique reproductive structure{{</text>}}** {{<text size="18.0pt">}}{{</text>}}called the ***{{<highlight size="20.0pt" fg="#0070C0" bg="yellow">}}syconium{{</highlight>}}***. This is a (and I quote from my lecturer) **{{<text size="20.0pt" bg="fuchsia">}} large, fleshy receptacle {{</text>}}** (I don't think I"ve every encountered a more horrifying combination of words) that contains between ***{{<text size="20.0pt" fg="#7030A0">}}50 and 7,000{{</text>}}*** {{<text size="20.0pt" fg="#7030A0">}}{{</text>}}(depending on species) very **{{<highlight size="18.0pt" bg="yellow">}}reduced flowers.{{</highlight>}}**

![image](77e04aa501a6d0cc.jpeg)

These plants are ***{{<text size="20.0pt" fg="#70AD47">}}monoecious{{</text>}}*** {{<text size="20.0pt" fg="#70AD47">}}{{</text>}}--- which means that they have **{{<text size="16.0pt">}}some
{{<text fg="red">}}male flowers{{</text>}}{{</text>}}** {{<text size="16.0pt" fg="red">}}{{</text>}}that produce **{{<text size="18.0pt" fg="#ED7D31">}}pollen{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}and **{{<text size="22.0pt">}}some{{</text>}}** {{<text size="22.0pt">}}**{{<text fg="red">}}female flowers{{</text>}}** {{<text fg="red">}}{{</text>}}{{</text>}}that contain **{{<text size="20.0pt" fg="#ED7D31">}}eggs{{</text>}}**.

But there are *{{<text size="18.0pt">}}two types{{</text>}}* {{<text size="18.0pt">}}{{</text>}}of female flower. One has a ***{{<highlight size="16.0pt" bg="yellow">}}very long style{{</highlight>}}*** {{<text size="16.0pt">}}{{</text>}}(these are the{{<u size="16.0pt">}}true female flowers{{</u>}}) and one has a ***{{<highlight size="20.0pt" bg="yellow">}}very short style{{</highlight>}}*** {{<text size="20.0pt">}}{{</text>}}(these are actually{{<u size="18.0pt">}}neuter flowers{{</u>}}).

The **{{<text size="22.0pt" bg="fuchsia">}}style{{</text>}}** {{<text size="22.0pt">}}{{</text>}}is not the fig's amazing fashion sense but is actually the "stalk" that connects the ***{{<text size="20.0pt" fg="#7030A0">}}stigma{{</text>}}*** {{<text size="20.0pt" fg="#7030A0">}}{{</text>}}(the bit that receives pollen at the top) to the ***{{<text size="20.0pt" fg="#ED7D31">}}ovary{{</text>}}*** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}(where the eggs are found at the bottom).

![image](a8a9e5652779d297.png)

This is how it would look on a{{<text size="16.0pt">}}normal flower{{</text>}}(sadly not a fig flower --- couldn't find a diagram of them).

{{<text size="18.0pt">}}Each species of fig{{</text>}}shares a ***{{<text size="18.0pt" fg="#00B050">}}symbiotic relationship{{</text>}}*** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}with their own ***{{<highlight size="16.0pt" bg="yellow">}}species of fig wasp{{</highlight>}}*** {{<text size="16.0pt">}}{{</text>}}that allows for their **{{<text size="18.0pt" bg="fuchsia">}}pollination{{</text>}}**. This is a pretty weird process and it goes a little bit like
 this.

There is an *{{<text size="18.0pt">}}"entrance"{{</text>}}* {{<text size="18.0pt">}}{{</text>}}to the syconium called the ***{{<highlight size="20.0pt" bg="yellow">}}ostiole{{</highlight>}}***. Now it's a pretty
 *tight squeeze* so the female wasp really has to **{{<text size="18.0pt">}}force her way{{</text>}}** {{<text size="18.0pt">}}{{</text>}}in and will often lose her *{{<text size="20.0pt" fg="#ED7D31">}}wings{{</text>}}* , *{{<text size="20.0pt" fg="#ED7D31">}}antennae{{</text>}}* {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}and even *{{<text size="20.0pt" fg="#ED7D31">}}legs{{</text>}}* {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}in the process.

![image](ff9949c551d767c0.jpeg)

See she's really forcing her way in I think that ant might just be there for **{{<text size="16.0pt">}}moral support{{</text>}}** ?

{{<text size="18.0pt">}}And then it gets *really weird*{{</text>}}.

She ***{{<highlight size="18.0pt" bg="yellow">}}PURPOSEFULLY pollinates{{</highlight>}}*** {{<text size="18.0pt">}}{{</text>}} **{{<text size="14.0pt">}}female{{</text>}}** {{<text size="14.0pt">}}{{</text>}}and **{{<text size="16.0pt">}}neuter{{</text>}}** {{<text size="16.0pt">}}{{</text>}}flowers.

This is odd. Most insect pollinators tend to ***{{<text size="20.0pt" fg="#ED7D31">}}pollinate by accident{{</text>}}***. Think of bees bumbling around looking for nectar, getting covered in pollen in the process and just kind of mixing it around. That
 is *{{<text size="16.0pt">}}not what happens here{{</text>}}*.

The female has ***{{<text size="18.0pt" fg="#7030A0">}}specialised pouches{{</text>}}*** {{<text size="18.0pt" fg="#7030A0">}}{{</text>}}on the **{{<text size="16.0pt">}}side of her thorax{{</text>}}** filled with pollen. When she's inside she **{{<text size="28.0pt">}}deliberately{{</text>}}** {{<text size="28.0pt">}}{{</text>}}takes this pollen *{{<highlight size="20.0pt" bg="yellow">}}out of the special pouches{{</highlight>}}* {{<text size="20.0pt">}}{{</text>}}and applies it to the stigmas of the flowers.

She then ***{{<text size="16.0pt" fg="#ED7D31">}}deposits eggs{{</text>}}*** {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}into the flowers. The style is too long in the **{{<text size="16.0pt">}}female flowers{{</text>}}** {{<text size="16.0pt">}}{{</text>}}for the egg to fit so she *{{<text size="14.0pt">}}can only deposit the eggs{{</text>}}* {{<text size="14.0pt">}}*into the neuter* *flowers*{{</text>}}.

Having laid her eggs,
 **{{<text size="16.0pt" fg="red" bg="black">}}she dies{{</text>}}**.

The ***{{<highlight size="18.0pt" bg="yellow">}}eggs then hatch{{</highlight>}}*** {{<text size="18.0pt">}}{{</text>}}out into male and female wasps. But what's weird is that the ***{{<text size="22.0pt" fg="#ED7D31">}}males never leave the fig{{</text>}}*** *.*
**
**{{<text size="14.0pt">}}They don't have wings{{</text>}}**.

*{{<text size="18.0pt">}}They will never see the outside world.{{</text>}}*

They **{{<highlight size="18.0pt" bg="yellow">}}mate with as many females as they can{{</highlight>}}** , then the females **{{<text size="16.0pt" fg="#70AD47">}}scoop up a bunch of pollen{{</text>}}** (to put in those handy pouches) then the males **{{<text size="36.0pt" fg="#7030A0">}}dig the females a hole{{</text>}}** to leave the **{{<text size="18.0pt" fg="#ED7D31">}}syconium{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}out of (so they don't have squeeze out and lose their legs and antennae n shit) and the female wasps then fly away to find a new syconium.

*{{<text size="16.0pt">}}HOW WEIRD IS THAT.{{</text>}}*

The males are totally stuck.

But this system is ***{{<text size="20.0pt" fg="#ED7D31">}}open to exploitation{{</text>}}***.

There are **{{<highlight size="18.0pt" bg="yellow">}}parasitic wasps{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}that like to{{<u size="22.0pt">}}fuck shit up{{</u>}}.

These have incredibly long --- essentially **{{<text size="18.0pt">}}egg-laying syringes{{</text>}}** {{<text size="18.0pt">}}{{</text>}}called **{{<text size="22.0pt" fg="#ED7D31">}}ovipositors{{</text>}}** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}that they can use to{{<highlight size="16.0pt" bg="yellow">}}inject their eggs{{</highlight>}}{{<text size="16.0pt">}}{{</text>}}into the syconium. This means they don't have to force their way in like the poor lady fig wasp who lost her legs and antennae.

When I say syringe *I"m not kidding* --- I mean look at these guys.

![[TMP] "The Wasp With a Metal-Reinforced Needle on Its ...](tmp_the_wasp_with_a_metal-reinforced_nee.gif)

Then the parasitic wasps can then hatch and *{{<text size="16.0pt" fg="#ED7D31">}}eat the other grubs inside{{</text>}}*.

But don't worry! Figs contain an enzyme called **{{<highlight size="20.0pt" bg="yellow">}}ficin{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}that ***{{<text size="18.0pt" fg="#7030A0">}}breaks down those exoskeletons{{</text>}}*** {{<text size="18.0pt" fg="#7030A0">}}{{</text>}}so you really don't need to worry. Also --- the fig wasps are **{{<text size="20.0pt" fg="#70AD47">}}1.5mm long{{</text>}}** so I think that even if there was one in there that hadn't been digested --- I"m not sure you"d notice.

**{{<text size="20.0pt">}}SO ENJOY YOUR FIGS.{{</text>}}**

But no --- they are technically not vegetarian.
**{{<text size="18.0pt">}}BUT{{</text>}}** {{<text size="18.0pt">}}{{</text>}}a lot of commercially grown figs are
 ***{{<highlight size="20.0pt" bg="yellow">}}self-pollinating{{</highlight>}}*** {{<text size="20.0pt">}}{{</text>}}so there are no wasps at all.

Go FIGure.

Heheh.

Love Flora xxx