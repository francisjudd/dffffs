+++
author = "Flora Judd"
title = "Ordovician Extinction Unveiled"
date = "2021-02-22"
description = "Exploring the dramatic End-Ordovician extinction and its implications."
tags = ["Extinction","Ordovician","Reef Diversity","Marine Species","Climate Change","Fossil Record","Brachiopods","Disaster Taxa","Earth Science","Survival Strategies"]
categories = ["Paleontology","Earth History","Biology","Extinction Events"]
image = "0d9eb954e1725dd8.png"
+++

Hello and welcome to a new week of AMAZING excitement *{{<text size="20.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}*.

I thought that this week we could have a look at some of the
 ***{{<highlight size="22.0pt" bg="yellow">}}"Big Five" extinctions{{</highlight>}}*** {{<text size="22.0pt">}}{{</text>}}that the Earth has experienced throughout its history. People tend to know about the
 **{{<text size="20.0pt" fg="#ED7D31">}}big ol" meteor{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}that wiped out the dinosaurs but some of the less high profile ones are really important too.

Now one of the big questions is --- ***{{<text size="16.0pt">}}how the hell do you know if an extinction has occurred?{{</text>}}*** The fossil record is patchy at the best of times so how can you know that
 ***{{<highlight size="16.0pt" bg="yellow">}}loads of things went extinct{{</highlight>}}*** {{<text size="16.0pt">}}{{</text>}}rather than ***{{<highlight size="18.0pt" bg="yellow">}}loads of things didn't get preserved{{</highlight>}}***.

The best proxy that we have for this is using changes in **{{<highlight size="16.0pt" fg="#0070C0" bg="aqua">}}reef diversity{{</highlight>}}** , where you look at the rise and fall of reef-building organisms. These
 **{{<text size="18.0pt" fg="red">}}"reef gaps"{{</text>}}** {{<text size="18.0pt" fg="red">}}{{</text>}}are often due to **{{<text size="16.0pt">}}dramatic changes in temperature{{</text>}}** {{<text size="16.0pt">}}{{</text>}}or **{{<text size="16.0pt">}}ocean acidification{{</text>}}** {{<text size="16.0pt">}}{{</text>}}from {{<u size="18.0pt">}}rising CO2 levels{{</u>}}{{<text size="18.0pt">}}{{</text>}}--- which often have global implications (oops --- sounds familiar).

This is still very challenging and very prone to error because the final member of any species is
 *{{<text size="12.0pt">}}almost certainly not in the fossil record{{</text>}}*. It makes sense. The odds that
 **the final dinosaur that ever lived** just *{{<text size="16.0pt" fg="red">}}happened to get preserved{{</text>}}* {{<text size="16.0pt" fg="red">}}{{</text>}}is tiny, meaning that the species almost certainly **lived a bit longer** than your
 **{{<text size="18.0pt">}}oldest specimen{{</text>}}**.

So this is one of our best ideas of how extinction rates have changed since the Cambrian. Now there are
 **{{<text size="16.0pt" fg="red">}}five peaks{{</text>}}** {{<text size="16.0pt" fg="red">}}{{</text>}}(the stars) and three that stand out **{{<text size="16.0pt">}}a LOT{{</text>}}**.

![image](0d9eb954e1725dd8.png)

OKAY --- let's take a look at that first {{<text size="28.0pt" bg="fuchsia">}}MAHOOSIVE spike{{</text>}}{{<text size="28.0pt">}}{{</text>}}known as the End-Ordovician event (sexy I know). So what happened here?

Firstly, let's have a look at what was going on during the Ordovician.

![Burgess Shale and Cambrian Explosion - Hugh Ross - YouTube](burgess_shale_and_cambrian_explosion_-_h.jpeg)

So we"d just had the {{<highlight size="24.0pt" fg="#ED7D31" bg="yellow">}}Cambrian explosion{{</highlight>}}{{<text size="24.0pt" fg="#ED7D31">}}{{</text>}}--- which is this massive eruption of life and a huge increase in number of species that happened around
 **{{<text size="20.0pt">}}540-520 million years ago{{</text>}}**.

There is a lot of debate as to *why* this explosion happened --- was there some
 **{{<text size="16.0pt" fg="#4472C4">}}genetic innovation{{</text>}}** {{<text size="16.0pt" fg="#4472C4">}}{{</text>}}that gave animals the ability to evolve in all these crazy ways? Was there some sort of
 **{{<text size="20.0pt" fg="#7030A0">}}external stimulus{{</text>}}** , like a change in climate or calcium availability (what all their lovely shells need)? Was it to do with the evolution of
 **{{<text size="18.0pt" fg="#00B050">}}predator-prey relationships{{</text>}}** ? i.e. did animals learn to eat each other and so evolved all these crazy forms?

![image](8cb6f5bbc0c383f6.gif)

Some even wonder ***{{<text size="18.0pt">}}whether{{</text>}}*** {{<text size="18.0pt">}}{{</text>}} ***{{<highlight size="24.0pt" bg="yellow">}}it's real at all{{</highlight>}}*** {{<text size="24.0pt">}}{{</text>}}or just the result of a **{{<text size="20.0pt" fg="#00B0F0">}}patchy fossil record{{</text>}}**. Maybe we aren't seeing the origin of life but just the
 ***{{<text size="24.0pt" fg="red">}}origin of fossilise-able animals{{</text>}}***.

But whether it was an "explosion" not it still marks a BIG CHANGE in the ecology of life on earth.

![Quarry Hill Nature Center | Ordovician Minnesota Mural on ...](quarry_hill_nature_center_ordovician_min.jpeg)

So we have all these lovely new (ish) species and then **{{<text size="16.0pt">}}BOOM{{</text>}}** {{<text size="16.0pt">}}{{</text>}}something goes wrong. What?

Well it seems to actually have happened in **{{<highlight size="20.0pt" fg="#0070C0" bg="aqua">}}two waves{{</highlight>}}** {{<text fg="#0070C0">}}{{</text>}}that occurred about ***{{<text size="16.0pt">}}a million years apart{{</text>}}***. One was associated with a
{{<text size="18.0pt" fg="#0070C0">}}big period of cooling{{</text>}}and then there was a
{{<text size="18.0pt" fg="#ED7D31">}}period of warming{{</text>}}that caused the second (you just can't win can you!).

We know that about **{{<text size="20.0pt">}}443 million years ago{{</text>}}** {{<text size="20.0pt">}}{{</text>}}CO2 levels *{{<highlight size="16.0pt" bg="yellow">}}dropped hugely{{</highlight>}}*. We are not 100% sure
 *why* but we think it might be due to the rise of **{{<text size="20.0pt" fg="#00B050">}}early land plants{{</text>}}** {{<text size="20.0pt" fg="#00B050">}}{{</text>}}using up lots of that sweet, sweet carbon dioxide for photosynthesis or through the
 ***{{<text size="18.0pt" fg="#C00000">}}weathering of lava flows{{</text>}}***.

This decrease in CO2 was so intense that it led to a **{{<text size="72.0pt" fg="#00B0F0">}}100m{{</text>}}** **{{<text size="72.0pt">}}{{</text>}}**
**{{<text size="72.0pt">}}{{</text>}}** **{{<text size="26.0pt" fg="#0070C0">}}fall in sea level{{</text>}}**. Given that at this point in evolutionary time,
 *{{<text size="18.0pt">}}almost ALL ORGANISMS WERE IN THE OCEAN{{</text>}}* , you can see how this is not really ideal.

![image](3d4aff836698fd89.png)
*{{<highlight bg="yellow">}}This is a reconstruction of Late Ordovician global geography (southern hemisphere), showing the south polar ice cap{{</highlight>}}*

That meant we lost the **{{<text size="14.0pt">}}first ever primitive coral reefs{{</text>}}** {{<text size="14.0pt">}}{{</text>}}that the world had seen (cri) and we lost a lot of other **{{<text size="14.0pt">}}marine fauna{{</text>}}**. In fact
 ***{{<text size="26.0pt" fg="red">}}86% of marine species{{</text>}}*** {{<text size="26.0pt" fg="red">}}{{</text>}}were wiped out during this extinction.

When this glacial period did end, about a million years later,
 **{{<text size="16.0pt" fg="#00B0F0">}}sea levels rose very quickly{{</text>}}** {{<text size="16.0pt" fg="#00B0F0">}}{{</text>}}and this caused oxygen levels in the oceans to *{{<text size="22.0pt">}}DROP{{</text>}}* (it like it's hot). And that wiped out a whole bunch of species again.

What you get at the end of these events are oceans dominated by a small number of what we like to call
 **{{<text size="22.0pt" fg="#7030A0" bg="fuchsia">}}disaster taxa{{</text>}}**. These are the ones that are so hardcore they could survive all the shit storms that the Earth tried to throw at them.

One of these is the rather nice **brachiopod** , *{{<text size="18.0pt" fg="#7030A0">}}Hirnantia{{</text>}}*.

![The Hirnantian (Late Ordovician) Brachiopod Fauna of the ...](the_hirnantian_late_ordovician_brachiopo.jpeg)

![PLATE WITH VERY RARE HIRNANTIA FAUNA.Brachiopod Hirnantia ...](plate_with_very_rare_hirnantia_faunabrac.jpeg)

These were then followed by other species that **{{<text size="18.0pt">}}started to recover{{</text>}}** {{<text size="18.0pt">}}{{</text>}}and those that could persist in these **{{<text size="18.0pt" bg="fuchsia">}}very low oxygen conditions{{</text>}}**.

*{{<text size="16.0pt">}}I"ll see you in 200 million years for the next one.{{</text>}}*

Lots of love,

Flora xxx