+++
author = "Flora Judd"
title = "Vocal Learning Unveiled"
date = "2021-02-04"
description = "Exploring the fascinating world of vocal learning in animals."
tags = ["Vocal Learning","Communication","Animal Behavior","Mating Signals","Bats","Dolphins","Songbirds","Echolocating","Evolution of Language","Species Interaction"]
categories = ["Animals","Biology","Science","Communication Studies"]
image = "dolphin_communication_animals_and_nature.jpeg"
+++

Helloooo and welcome to another
 *Daily Fun Fact From Flora* ,

Today we're gonna have a little look at **{{<text size="16.0pt" fg="red">}}vocal learning{{</text>}}**.

The ability for animals to {{<u>}}learn vocalisations{{</u>}} from each other can be seen in a **few different species** , but not as many as you think. Now, the exact number of species really depends on how you define vocal learning but here we're gonna say that if an animals can:

imitate the sounds of **{{<text fg="#00B0F0">}}another of its own species{{</text>}}**imitate the sounds of a **{{<text fg="#0070C0">}}different species{{</text>}}**imitate a totally **{{<text fg="#002060">}}artificial sound{{</text>}}**

then it's a *{{<text size="16.0pt">}}vocal learner.{{</text>}}*

Obviously we humans can do this and the only other species that we think can are **{{<highlight size="14.0pt" bg="yellow">}}bats{{</highlight>}}** , **{{<highlight size="14.0pt" bg="yellow">}}cetaceans{{</highlight>}}** {{<highlight size="14.0pt" bg="yellow">}}{{</highlight>}}(that's your lovely whales and dolphins),
 **{{<highlight size="14.0pt" bg="yellow">}}elephants{{</highlight>}}** , **{{<highlight size="14.0pt" bg="yellow">}}songbirds{{</highlight>}}** , **{{<highlight size="14.0pt" bg="yellow">}}parrots{{</highlight>}}** {{<highlight size="14.0pt" bg="yellow">}}{{</highlight>}}and **{{<highlight size="14.0pt" bg="yellow">}}pinnipeds{{</highlight>}}** {{<highlight size="14.0pt" bg="yellow">}}{{</highlight>}}(that's seals and sea lions).

*{{<text size="20.0pt">}}That's it.{{</text>}}*

Of course other species can
{{<u size="14.0pt">}}communicate{{</u>}}{{<text size="14.0pt">}}{{</text>}}with each other in complicated ways and
{{<u size="14.0pt">}}transfer information{{</u>}}{{<text size="14.0pt">}}{{</text>}}to each other, but they can't *{{<text size="16.0pt">}}learn{{</text>}}* {{<text size="16.0pt">}}novel vocalisations{{</text>}}. We're pretty unique in that respect. If you
 think of a dog, yes you can teach it what the word "sit" means, but it can't say it back to you and expect you to do the same. Hence --- not a vocal learner.

![Teach Kids Proper Pet Care | Parenting](teach_kids_proper_pet_care_parenting.jpeg)

Good luck pal --- you're gonna be there for a while.

Now humans and songbirds are the best studied examples and they use a pretty fan technique of learning. What we do is we
 **{{<text fg="#7030A0">}}listen to the{{</text>}}** **{{<text size="14.0pt" fg="#7030A0">}}incoming sound{{</text>}}** {{<text size="14.0pt">}}{{</text>}}and we make an **{{<text size="16.0pt" fg="#DA28C9">}}acoustic model{{</text>}}** {{<text size="16.0pt">}}{{</text>}}in our head that basically forms a template that we can try and match with our mouths.

It's not a perfect system straight away and it takes time. That's why babies
 *babble* and songbirds make noises that are known as " *subsongs"* that are their babbling equivalent which I think is
 *{{<highlight-u bg="yellow">}}totally fucking adorable{{</highlight-u>}}*.

Now this all requires some pretty **{{<text size="18.0pt" fg="#ED7D31">}}intense{{</text>}}** **{{<text size="18.0pt">}}neural machinery{{</text>}}** {{<text size="18.0pt">}}(big brain flex){{</text>}}which needs a lot of energy and so
 there need to be some *pretty big advantages* to having this vocal learning ability to make it worth maintaining.

This might sound stupid given how important learning and communication is in our lives today. Imagine the whole of human society trying to go about its business *{{<text size="14.0pt" fg="red">}}without verbal language{{</text>}}* {{<text size="14.0pt" fg="red">}}{{</text>}}--- it would result in a shit show that would put the pandemic to shame.

BUT that is an advantage that comes about when
 ***everyone* is a vocal learner**. That pushes for evolution to ***{{<text size="16.0pt">}}maintain{{</text>}}*** our ability to learn vocalisations, but what drove it in the first place? What advantages
 do you get from vocal learning when it's just you who knows how to do it?

{{<highlight size="14.0pt" bg="yellow">}}Thank God you asked.{{</highlight>}}{{<text size="14.0pt">}}{{</text>}}

Boy have I got some hypotheses for you.

{{<text size="20.0pt">}}The
{{<text fg="red" bg="black">}}Sexy Song{{</text>}} Hypothesis (I know biologists are great){{</text>}}

Both vocal learners and non-learners use vocalisations to **{{<text size="14.0pt" fg="#DA28C9">}}attract mates{{</text>}}** {{<text fg="#DA28C9">}}{{</text>}}(met my boyfriend in choir just saying). These vary a lot in
 **complexity.**

I mean frogs just kind of shout really loudly --- the equivalent of
*{{<text size="18.0pt" fg="#C00000" bg="fuchsia">}} SEXY MAN OVER HERE LOOKING FOR SEXY LADY FUN TIMES {{</text>}}*

Whilst humpback whales construct **{{<text size="16.0pt">}}elaborate songs{{</text>}}** {{<text size="16.0pt">}}{{</text>}}that ***{{<text size="16.0pt">}}change throughout the singing season{{</text>}}***. In fact you can see that whale song types tend to move from East to West in the southern
 Pacific even though the whales themselves don't relocate and we have NO IDEA WHY. I mean look at this sexy graphic. Each colour represents a different song type and the populations are listed from East to West along the top.

![ChartDescription automatically generated](chartdescription_automatically_generated.jpeg)

SORRY --- getting distracted by whales. Anyways the Sexy Song Hypothesis states that vocal learners have a
 **{{<highlight bg="yellow">}}key advantage{{</highlight>}}** over non-learners and that is that they can{{<highlight size="14.0pt" bg="aqua">}}vary their
 **syntax** and their **frequency**{{</highlight>}}. Birds produce sound using a **{{<text size="20.0pt" fg="#0070C0">}}syrinx{{</text>}}** {{<text size="20.0pt" fg="#0070C0">}}{{</text>}}(their version of the larynx that we have) and canaries have two of them that can produce sound
 *independently* !!

They basically have *{{<u size="16.0pt">}}two voices{{</u>}}*. Absolute
{{<u>}}fuckery{{</u>}}.

They can use these syrinxes syrinces ? Voice boxes. To produce a **{{<text size="16.0pt">}}huge range of frequency modulations{{</text>}}** {{<text size="16.0pt">}}{{</text>}}that are genuinely referred to as "{{<u size="16.0pt" fg="red">}}sexy syllables"{{</u>}}{{<text size="16.0pt" fg="red">}}{{</text>}}or "{{<u size="16.0pt" fg="red">}}sexy songs"{{</u>}}{{<text size="16.0pt" fg="red">}}{{</text>}}because they are thought to{{<text size="16.0pt" fg="#DA28C9">}}stimulate oestrogen production{{</text>}}in females.

![This Canary Bird is amazing funny - YouTube](this_canary_bird_is_amazing_funny_-_yout.jpeg)
Mmm doesn't that just get ya going 

{{<text size="20.0pt" fg="yellow" bg="blue">}}Get More Snax{{</text>}}{{<text size="20.0pt" fg="yellow">}}{{</text>}}{{<text size="20.0pt">}}Hypothesis ( okay maybe I made that one up){{</text>}}

BUT it's still really cool. What bats can do is they can
 **change the frequency of their echolocation** **signals** when they need to.

 Why would they need to Flora? 

Thank God you asked.

Basically, sometimes bats encounter
 **other vocalisations** at the *same frequency* they're using to echolocate which leads to **{{<text size="18.0pt" fg="#ED7D31">}}sonar jamming{{</text>}}**. Now some of you who have been around here for a while will recognise that term, because we once chatted about a moth that does just that. The Tiger
 moth makes its own high frequency sounds to totally baffle the bat so it has no idea where it's echoes are coming from or where the tasty moth is hiding.

So
 **AMAZINGLY** some bats can *{{<highlight bg="yellow">}}change the frequency{{</highlight>}}* of their echolocation signals in order to avoid that "jammy" response.

What's also really fucking cool is that bats can shift the frequency of their echolocation calls so that the ***{{<u size="18.0pt" fg="red">}}Doppler-shifted echo{{</u>}}*** that comes back is at a favourable frequency. WHAT EVEN!? (the Doppler shift is the one that means car engines or sirens sound higher
 in pitch when they're approaching than when they're receding kind of like NEEEEEEOOOOOOWWWWWW).

{{<text size="20.0pt">}}Individual Recognition Hypothesis{{</text>}}

What you can use vocal learning for is associating certain individuals with recognisable songs/sounds that mean you can differentiate them from other individuals in your population.
 This is super important in{{<highlight size="16.0pt" bg="yellow">}}social groups{{</highlight>}}.

But this one is actually pretty unique. Humans and bottlenose dolphins are the only species that use **{{<text size="18.0pt" fg="#4472C4">}}names{{</text>}}** {{<text size="18.0pt" fg="#4472C4">}}{{</text>}}as a means of identification. Dolphins respond when other individuals use that dolphins own signature "whistle" but they don't respond to other ones.

Slightly heartbreakingly, dolphins that are kept in captivity will still remember the signature whistles of their pod members for up to{{<text size="18.0pt" fg="#8FAADC" bg="blue">}}20 years{{</text>}}. :( cri

![Dolphin communication | Animals and Nature lessons | DK ...](dolphin_communication_animals_and_nature.jpeg)


*{{<highlight size="20.0pt" bg="aqua">}}HOW COOL IS THAT?!{{</highlight>}}* *{{<text size="20.0pt">}}{{</text>}}*

Dolphins are also able to use this skill to associate **{{<text size="14.0pt">}}new signals{{</text>}}** {{<text size="14.0pt">}}{{</text>}}to **{{<text size="14.0pt">}}unknown objects{{</text>}}** , presumably allowing for some seriously high level communication.

That's *{{<u size="14.0pt">}}totally mind blowing{{</u>}}*.

You can introduce a **{{<text size="16.0pt" fg="#70AD47">}}completely alien object{{</text>}}** {{<text size="16.0pt" fg="#70AD47">}}{{</text>}}into a dolphin pen and it will come up with its own "word" for it. If you then play that "word" back to the dolphin it understands what you're talking about.


I HOPE YOU ENJOYED THAT.

A *very* brief introduction to the magical world of vocal learning.

Lots of love,

Flora