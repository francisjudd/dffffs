+++
author = "Flora Judd"
title = "Bizarre Barreleye"
date = "2021-02-01"
description = "Explore the incredible adaptations of the barreleye fish in deep waters."
tags = ["Barreleye Fish","Transparent Cranium","Bioluminescence","Tubular Eyes","Deep Sea Adaptations","Evolutionary Traits","Marine Biology","Ecosystem Dynamics","Predator-Prey Relationships","Vision"]
categories = ["Animals","Marine Life","Biology","Deep Sea"]
image = "the_barreleye_fish_ronnies_blog.gif"
+++

Helloooo,

Welcome to another fresh week of fun facts.

Today we're gonna start the week off with a fantastically bizarre looking animals --- the ***{{<text size="16.0pt" fg="#0070C0">}}barreleyes{{</text>}}***.

The **barreleyes** (sometimes also known as **{{<text size="28.0pt" fg="#C00000">}}spook fish{{</text>}}** {{<text size="28.0pt" fg="#C00000">}}{{</text>}}cos ya know, why not) are small deep-sea fish and they have one
 *very weird* *and very notable* feature 

A **{{<highlight size="16.0pt" bg="yellow">}}transparent cranium{{</highlight>}}**.

You might be thinking that when I say transparent I mean it in a slightly disappointing sort of
 * ooh-I-guess-if-you-look-at-it-just-right-it's-a-little-bit-see-through * sort of way.

OH *{{<u size="22.0pt" fg="red">}}HELL NO{{</u>}}*.

This is what I mean.

![The Barreleye Fish | Ronnie's Blog](the_barreleye_fish_ronnies_blog.gif)


Those weird **{{<text size="16.0pt" fg="#00B050">}}ginormous green things{{</text>}}** {{<text size="16.0pt" fg="#00B050">}}{{</text>}}in its head? Yeah --- those are the{{<text size="16.0pt" fg="#00B050">}}barrel shaped eyes{{</text>}}.

This particularly sexy individual is a species called the
 **{{<highlight bg="aqua">}}Pacific barreleye{{</highlight>}}**.

Now --- if you're a fish living in the deep sea (as I often dream I am) you live in a weird world where light doesn't really reach further than about **{{<highlight size="16.0pt" bg="yellow">}}200m{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}and this is where plants stop being able to grow because they can't photosynthesise. It's not a hard and fast rule --- the depth does depend on the clarity of the water, but 200m is a good rule of thumb.

And things are **{{<text size="18.0pt" fg="white" bg="black">}}totally pitch black{{</text>}}** {{<text size="18.0pt" fg="white">}}{{</text>}}by the time you get to 1,000m. Now the fantastic thing about this is that it means that lots of species have evolved
{{<text bg="lime">}}bioluminescence{{</text>}} (THE ABILITY TO GLOW) to be able to communicate, signal, hunt and mate with each other but that's a whole different email.

So for now we keep it simple. Where it's deep and dark some fish have evolved the ability to emit their own light.
 **Bioluminescence** is most common around **200m-1,000m**which is actually known as the **{{<text size="18.0pt">}}mesopelagic{{</text>}}** {{<text size="18.0pt">}}{{</text>}}or **{{<text size="20.0pt" fg="yellow" bg="blue">}}the twilight zone{{</text>}}** 
 I think we know which one sounds more romantic. Imagine the franchises 

![image](17d89c4f44d8efb5.png)

*Stephenie Meyer I will be awaiting your call.*

This poses some interesting challenges for the species that can survive in a range that goes
 **a bit above 200m** (where you get some good ol" sunlight) **and a bit below 200m** (where you get some sexy bioluminescence).

You need to be able to see both **{{<u size="16.0pt">}}diffuse light{{</u>}}** {{<text size="16.0pt">}}{{</text>}}that comes from the surface but also
 **{{<u size="16.0pt">}}point light sources{{</u>}}** {{<text size="16.0pt">}}{{</text>}}created by glowing fish. The ability to see extended light sources and point light sources uses slightly different visual machinery so some species have evolved some rather novel solutions.

Enter the barreleyes.

Lots of deep sea fish have developed very special **{{<text size="18.0pt" fg="#548235">}}tubular eyes{{</text>}}** {{<text size="18.0pt" fg="#548235">}}{{</text>}}(those are the crazy green blobs) which have the typical fishy
 **spherical lens** (our lenses are quite a bit flatter) andelevates it at the top of a tube (or barrel if you will).

![Vision: Through air, water and snow](vision_through_air_water_and_snow.jpeg)


So this is a typical fish eye but the barreleye also has its spherical lens.

Interestingly, some **{{<text fg="#002060">}}nocturnal birds{{</text>}}** {{<text fg="#002060">}}{{</text>}}also have **{{<text size="16.0pt">}}tubular eyes{{</text>}}** {{<text size="16.0pt">}}{{</text>}}--- just goes to show that evolution finds the best way however she chooses.

Now, "normaltubular eyes" (as far as there is such a thing) allow you to spot
 **{{<u>}}predators{{</u>}}** and **{{<u>}}prey{{</u>}}** that are *silhouetted* against the light coming from above. BUT they have **{{<text size="18.0pt" fg="#ED7D31">}}high sensitivity{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}and **{{<text size="18.0pt" fg="#ED7D31">}}high resolution{{</text>}}** {{<text size="18.0pt">}}{{</text>}}over a relatively **{{<u size="22.0pt" fg="red">}}narrow field{{</u>}}** {{<text fg="red">}}{{</text>}}so they're really bad at spotting the bioluminescence that comes in laterally (i.e. light not from above).

So this problem has been solved in the barreleye. What it has done is
 ***ROTATE*** its tubular eyes ***{{<u size="18.0pt">}}INSIDE{{</u>}}*** {{<text size="18.0pt">}}{{</text>}}the **{{<text size="28.0pt" fg="#FFD966" bg="fuchsia">}}transparent dome of its fluid filled head{{</text>}}**.

Yes of course.

That makes total sense.

Hope you enjoyed your foray into the deep sea on this first Monday of February.

Love Flora xxx