+++
author = "Flora Judd"
title = "Origins of Life"
date = "2021-02-03"
description = "Exploring cell theory and the origins of life on Earth."
tags = ["Cell Theory","LUCA","Prebiotic Soup","Natural Selection","RNA","Life Origins","Molecules","Replication","Membranes","Evolutionary Biology"]
categories = ["Science","Biology","Origins of Life","Cell Biology"]
image = "life_free_full-text_a_self-assembled_agg.png"
+++

Helloooo,

I swear to God one of these days I"m gonna put the wrong number of Fs in the subject. Every email I have to check them about 4 times.

ANYWAYS.

Let's get a good fact going.

I thought today we could take a little look at ***{{<text size="24.0pt" fg="#00B050">}}The Beginning Of Life{{</text>}}*** {{<text size="24.0pt" fg="#00B050">}}{{</text>}}(ahhhhh). It sounded serious enough to be capitalised.

Now there's a thing we like to use in biology called **{{<u size="14.0pt">}}Cell Theory{{</u>}}** {{<text size="14.0pt">}}{{</text>}}and it has **three key components** :

{{<text size="18.0pt">}}All organisms are composed of {{<highlight-u bg="yellow">}}one or more{{</highlight-u>}} cells{{</text>}}{{<text size="14.0pt">}}{{</text>}}

{{<text size="18.0pt">}}Cells are the {{<highlight-u bg="yellow">}}basic unit of life{{</highlight-u>}}{{</text>}}

{{<text size="18.0pt">}}All cells arise from {{<highlight-u bg="yellow">}}pre-existing cells{{</highlight-u>}}{{</text>}}

Wait a second. Hold the (cell) phone. Look at that last one.

![Cell Phone Pun - Science - Tapestry | TeePublic UK](cell_phone_pun_-_science_-_tapestry_teep.png)

{{<text size="22.0pt">}}3.{{</text>}}{{<text size="18.0pt">}}All cells arise from
 *{{<highlight-u fg="red" bg="yellow">}}pre-existing cells{{</highlight-u>}}*{{</text>}}

But what about the *first ever cell* ? There must have been one somewhere. At some point we had lovely
 **LUCA** (our Last Universal Common Ancestor) who was the parent of every single organism that has since come to live on this
{{<u>}}luscious planet{{</u>}}.

We reckon that the first ever living "thing" originated about *{{<text size="18.0pt" bg="fuchsia">}}4 billion years ago{{</text>}}*. For context, the Earth is estimated to be about *{{<highlight size="20.0pt" bg="yellow">}}4.54 billion years old{{</highlight>}}* yep doesn't really help does it. How about this. If you took all of Earth's history and condensed it into a single **{{<text size="18.0pt">}}calendar year{{</text>}}** {{<text size="18.0pt">}}{{</text>}}(Clare you have yet to build me a special room for this) --- life would begin on the{{<highlight size="20.0pt" bg="aqua">}}25th of February{{</highlight>}}.

*Homo sapiens* appears at **{{<text size="18.0pt">}}23:36{{</text>}}** {{<text size="18.0pt">}}{{</text>}}on the **{{<text size="18.0pt" fg="red">}}31st of December{{</text>}}**. And we think we're so important.

Anyways, we have a rough idea of when but the biggest questions are
 **HOW** and **WHERE** it happened.

Did life originate on Earth or did it come from an
 ***extra-terrestrial*** source? That's not as far-fetched as it might first sound --- we have found meteorites that contain complex organic molecules like amino acids and nucleotides (which make up DNA).

Did life come about ***just once*** ? Maybe there isn't just one LUCA?!

It's all pretty speculative, but this is our best idea and it's called the **{{<highlight size="16.0pt" bg="yellow">}}prebiotic soup hypothesis{{</highlight>}}**.

I know prebiotic soup sounds like some horrific concoction that you"d find in a caf run by
 **Deliciously Ella** , but actually what we mean here is oceans that were rich in **{{<text fg="red">}}simple organic molecules{{</text>}}**.

![image](6a6c936008ce5160.png)

These can quite easily form from **{{<text size="14.0pt">}}atmospheric gases{{</text>}}** {{<text size="14.0pt">}}{{</text>}}(like your **{{<text fg="#548235">}}ammonia{{</text>}}** ,
 **{{<text fg="#ED7D31">}}methane{{</text>}}** {{<text fg="#ED7D31">}}{{</text>}}
and **{{<text fg="#2E75B6">}}water{{</text>}}** {{<text fg="#2E75B6">}}**vapour**{{</text>}}) that are hit by UV light and lightning (there were A LOT of storms on early Earth --- she was quite an angry and inhospitable environment she got better once the volcanoes settled down a bit).

![Geodynamics | Pre-plate-tectonics on early Earth: How to ...](geodynamics_pre-plate-tectonics_on_early.jpeg)
*She was annngry made a good soup though.*

Once you have these **{{<text size="14.0pt">}}simple organic molecules{{</text>}}** {{<text size="14.0pt">}}{{</text>}}they can start *combining* with each other to form some slightly more **{{<text size="14.0pt">}}complex{{</text>}}** {{<text size="14.0pt">}}{{</text>}}ones. This gives us *{{<text size="14.0pt" fg="#0070C0">}}amino acids{{</text>}}* {{<text size="14.0pt" fg="#0070C0">}}{{</text>}}(these ones are the building blocks of proteins), *{{<text size="14.0pt" fg="#7030A0">}}nucleotides{{</text>}}* {{<text size="14.0pt" fg="#7030A0">}}{{</text>}}(the ones that make up DNA) and *{{<text size="16.0pt" fg="#E025DE">}}sugars{{</text>}}*.

These more complex molecules then started to attach to each other to form chains of **polymers**. We think that by sheer chance one of these polymers was formed and it was able to{{<highlight-u size="14.0pt" bg="yellow">}}catalyse its own replication{{</highlight-u>}}. As soon as you can increase the speed of your own production you are going to
 ***DOMINATE*** in a soup that's full of passively floaty floating molecules.

Then, if (through random changes --- or mutations) this process became faster, then
 *those* molecules would do even better.

In this way you have the very beginning of **{{<text size="16.0pt" fg="red">}}natural selection{{</text>}}** {{<text size="16.0pt" fg="red">}}{{</text>}}taking place.
*{{<text size="14.0pt">}}Whatever can make itself the fastest will become dominant in the environment{{</text>}}*.
That's essentially **natural selection in a nutshell** (sorry Darwin).

 But Flora --- I hear you cry --- these are just autocatalytic entities floating around in soup! 

Good point. We're missing something very important here.

*{{<text fg="#E025DE">}}Membranes{{</text>}}* , bitch.

Membranes are fantastic because they can **spontaneously self-assemble** (IKEA please take note). They are made up of little molecules called **{{<u size="18.0pt" fg="red">}}amphiphiles{{</u>}}**. This is because they have two very different ends --- a tail that
 *HATES WATER* (it's **{{<text fg="#ED7D31">}}hydrophobic{{</text>}}** {{<text fg="#ED7D31">}}{{</text>}}af) and a head that *LOVES WATER* ( **{{<text fg="#00B0F0">}}hydrophilic{{</text>}}** {{<text fg="#00B0F0">}}{{</text>}}--- now that's relatable).

This means that in a very watery environment they are happiest when they look like this:

![Life | Free Full-Text | A Self-Assembled Aggregate ...](life_free_full-text_a_self-assembled_agg.png)


All the **hydrophobic** ends face inwards so they protect each other and all the
 **hydrophilic** ends face outwards to get their faces in that {{<text fg="#E025DE">}}sweet, sweet H2O{{</text>}}. This is what *all* of the membranes in *all* of our cells look like well they look a bit less like a terrifying number in a
 *sperm cell musical* , but you get the gist.

Now this whole process does actually happen when you replicate it experimentally.

If you put **nucleotides** and
 **lipids** (these are a key part of those little sperm) together and **{{<text fg="#ED7D31">}}heat{{</text>}}** {{<text fg="#ED7D31">}}**them**{{</text>}}and **{{<text fg="#ED7D31">}}concentrate them{{</text>}}** {{<text fg="#ED7D31">}}{{</text>}}for long enough you get **{{<text size="16.0pt" fg="red">}}vesicles{{</text>}}** {{<text size="16.0pt" fg="red">}}{{</text>}}(like the one in the picture above) that contain molecules of **{{<highlight size="16.0pt" bg="yellow">}}RNA{{</highlight>}}**. This is why we think that those first self-replicating polymers were made up of RNA. Now, RNA is
 *like* DNA but uses a different type of sugar called a ribose --- hence the R in RNA --- and we think that life started out using this instead of DNA.

And BOOM.

There was life.

Well --- what we define as life. In order to count as life a molecule needs to be able to:

**Store information** (RNA can do this in the same way DNA can)**Adapt/mutate** (again --- yep RNA can do that)**Catalyse reactions** (yes we saw how it can catalyse its own production)**Replicate itself** (oh yep that one's good too)**Sense the environment** (this is a bit more complex and involves something called *{{<text size="16.0pt" fg="red">}}riboswitches{{</text>}}* {{<text size="16.0pt" fg="red">}}{{</text>}}but just trust me that yes it can happen!)

And well. *{{<highlight size="16.0pt" bg="yellow">}}That's life.{{</highlight>}}* {{<text size="16.0pt">}}{{</text>}}

We did it. Let's make a cup of tea.

I know this one was a little heavy on the science so thanks for sticking it out if you did!

Lots of love,

Flora