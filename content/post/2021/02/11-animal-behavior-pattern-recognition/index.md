+++
author = "Flora Judd"
title = "Animal Behavior Unpacked"
date = "2021-02-11"
description = "Exploring how patterns influence animal behavior through sign stimuli."
tags = ["Sign Stimuli","Animal Behavior","Pattern Recognition","Cuckoo Adaptation","Behavioral Mechanisms","Evolution","Development","Human Behavior","Field Studies","Behavioral Ecology"]
categories = ["Animal Behavior","Biology","Evolutionary Science","Ethology"]
image = "living_on_earth_birdnote_solving_the_mys.png"
+++

Hi there --- welcome to another *{{<text size="18.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}*.

Today we are going to look at **{{<highlight size="16.0pt" bg="yellow">}}animal behaviour{{</highlight>}}**.

Now whenever scientists see an animal using a given behaviour there are four **{{<text size="14.0pt" fg="#ED7D31">}}questions
 to be asked{{</text>}}**.

Us biologists like to divide those into two categories.

We have the **{{<text size="22.0pt" fg="red">}}ULTIMATE{{</text>}}** {{<text size="22.0pt" fg="red">}}{{</text>}}or *{{<text size="20.0pt" fg="#00B0F0">}}EVOLUTIONARY{{</text>}}* {{<text size="20.0pt" fg="#00B0F0">}}{{</text>}}questions.
**{{<text size="18.0pt" fg="#ED7D31">}}Adaptation{{</text>}}** = How does this behaviour help the animal?
 How does it make the animal more able to survive and reproduce?**{{<text size="18.0pt" fg="#FFD966">}}Phylogeny{{</text>}}** {{<text size="18.0pt" fg="#FFD966">}}{{</text>}}(fancy
 word for evolutionary history) = What did the behaviour evolve *from* ? i.e. how did the ancestors behave?

And then we have our **{{<text size="22.0pt" fg="red">}}PROXIMATE{{</text>}}** {{<text size="22.0pt" fg="red">}}{{</text>}}or *{{<text size="20.0pt" fg="#00B0F0">}}CAUSAL{{</text>}}* {{<text size="20.0pt" fg="#00B0F0">}}{{</text>}}questions
**{{<text size="18.0pt" fg="#92D050">}}Mechanism{{</text>}}** {{<text size="18.0pt" fg="#92D050">}}{{</text>}}=
 What is the mechanism behind the behaviour? How does the animal decide to do this behaviour rather than a different one?**{{<text size="18.0pt" fg="#00B050">}}Development{{</text>}}** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}=
 How does the behaviour develop in that individual? Is it learned? Is it innate? Do genes play a role?

Now we are going to have a look at **{{<text fg="#92D050">}}question 3{{</text>}}** ---
 the *mechanism* (a bit random I know, but who needs sensible order nowadays).
Now a lot of the time mechanisms behind animal behaviour come down to **{{<highlight size="18.0pt" bg="yellow">}}pattern
 recognition{{</highlight>}}**. Animals are able to **{{<text size="14.0pt" fg="red">}}recognise simple patterns{{</text>}}** {{<text size="14.0pt" fg="red">}}{{</text>}}in
 their environment and can use these to *drive their behaviour*.

The simplest form this takes is something called a **{{<text size="18.0pt" fg="yellow" bg="red">}}sign stimulus{{</text>}}**.
 Here, the animal responds to just a **small part** of the **huge array of stimuli** in front of it and pretty much just ignore the rest. This might sound a bit crap but it *can* be super effective.

Let's take a look at herring gulls. The adults have this big ol" red spot on their beaks.

![Living on Earth: BirdNote: Solving the Mystery of the ...](living_on_earth_birdnote_solving_the_mys.png)

They also have some pretty funky looking{{<u fg="red">}}eye makeup{{</u>}}{{<text fg="red">}}{{</text>}}(or
 a really bad case of **{{<text fg="red">}}conjunctivitis{{</text>}}** ).

Now whilst this red spot may *{{<text size="16.0pt">}}look like{{</text>}}* {{<text size="16.0pt">}}{{</text>}}it's
 there for the aesthetics, it actually acts as a **{{<text size="18.0pt" fg="#ED7D31">}}sign stimulus{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}for
 the little baby chicks. The chicks will **{{<text size="14.0pt">}}peck{{</text>}}** {{<text size="14.0pt">}}{{</text>}}at
 this red spot to get their parents to vom up some yummy food for them. So in many ways what the chicks see is this 

![image](1cf32408bb20c905.png)

What is interesting here is that chicks seem to ***{{<highlight size="18.0pt" bg="yellow">}}only respond to this
 spot{{</highlight>}}***. They won't peck heads that don't have the spot and they will even peck crude cardboard models that do have the spot.

Now this sort of stimulus can be really effective but in order for it to work there are **{{<highlight bg="yellow">}}three criteria{{</highlight>}}** it has to fulfil:
{{<text size="10.0pt">}} {{</text>}} *{{<text size="18.0pt">}}The signal has to be **unique**{{</text>}}*
i.e. it can't be found anywhere else in the environment, if there are other red spots in the environment this one won't work
{{<text size="10.0pt">}} {{</text>}} *{{<text size="18.0pt">}}The response has to be made **quickly**{{</text>}}*
Hanging around here is no good. It needs to be almost instantaneous.
{{<text size="10.0pt">}} {{</text>}} *{{<text size="18.0pt">}}The young has to be able to do it **rapidly after birth**{{</text>}}*
There's no opportunity for learning here. If the chick doesn't know that that spot means lunch from the get-go then it's not gonna survive

BUT this is a system that is{{<highlight size="18.0pt" bg="yellow">}}vulnerable to mistakes{{</highlight>}}.

A lot of birds don't use red spots, but instead the parent birds are stimulated to feed their young when they see **{{<text size="18.0pt" fg="#ED7D31">}}big
 open gaping mouths{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}like this.

![Baby Birds GIFs | Tenor](baby_birds_gifs_tenor.gif)

Now there is a bit of a problem here, because other species can **{{<text size="18.0pt" fg="red">}}exploit that instinct{{</text>}}**.
 Take the cuckoo for example. This poor little reed warbler is trying to feed this MASSIVE cuckoo that she thinks is her child not so ideal 

![Reed warbler attempts to feed huge cuckoo chick she thinks ...](reed_warbler_attempts_to_feed_huge_cucko.jpeg)

(this also happens to be a very accurate representation of what the Clark household will look like when Charlie comes back from Leeds)

What is even more bizarre (and this might be one of my all-time favourite photos) is that sometimes it's a *{{<highlight size="20.0pt" bg="yellow">}}TOTALLY
 DIFFERENT SPECIES{{</highlight>}}* {{<text size="20.0pt">}}{{</text>}}that exploits this sign stimulus.

![cidimage007.png@01D7008A.8D255580](cidimage007png01d7008a8d255580.png)

Now this poor little female cardinal recently lost her babies so has instead taken to **{{<text size="16.0pt" fg="#0070C0">}}feeding
 the fish{{</text>}}** {{<text size="16.0pt" fg="#0070C0">}}{{</text>}}in this **{{<text size="16.0pt">}}garden
 pond{{</text>}}** {{<text size="16.0pt">}}{{</text>}}instead. She is still responding to that big old **{{<text size="18.0pt" fg="#ED7D31">}}gaping
 mouth stimulus{{</text>}}** , but now it is the mouth of a fish instead of a baby bird oops.

Sometimes these stimuli are a bit *too* simple.

But what's weird is sometimes animals respond really strongly to stimuli that are *{{<text size="16.0pt">}}waaaaay
 stronger{{</text>}}* {{<text size="16.0pt">}}{{</text>}}than anything they would **{{<text size="18.0pt" fg="#00B050">}}normally
 encounter in the natural world{{</text>}}**.

If we go back to our herring gulls --- you can see that actually what they prefer most of all is not a parent with a red spot (A), or the beak with a red spot (B), or the
 whole parent without the red spot (C), but actually **{{<text size="14.0pt" fg="white" bg="red">}}a red stick with white stripes{{</text>}}** on
 it (D).

*{{<text size="14.0pt">}}THEY GO NUTS FOR IT.{{</text>}}*

![A picture containing text, envelopeDescription automatically generated](a_picture_containing_text_envelopedescri.png)

What these poor bastards will also do --- is if you give them the choice between their own eggs and an ostrich egg guess which one they pick. Maybe they're just totally
 convinced that they"ve laid this **{{<text size="72.0pt" fg="#00B0F0">}}massive egg{{</text>}}** {{<text size="72.0pt" fg="#00B0F0">}}{{</text>}}and
 they're so chuffed about it they abandon all other eggs? Who on earth knows.

![A picture containing grass, outdoor, mammal, hayDescription automatically generated](a_picture_containing_grass_outdoor_mamma.png)

CHICK out that bad boy hehehe **{{<text size="16.0pt">}}egg{{</text>}}** cellent 

**{{<text size="14.0pt">}}Humans{{</text>}}** {{<text size="14.0pt">}}{{</text>}}are
 even vulnerable to this sort of stimulus, believe it or not. There's an infamous study where you leave out an honesty box for people to pay for a communal supply of milk. You then either put a **{{<highlight size="16.0pt" bg="yellow">}}picture
 of eyes{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}above the box or a **{{<highlight size="16.0pt" bg="yellow">}}picture
 of flowers{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}and the results are pretty striking.

They swapped the pictures each week and you can see how the amount people paid ***{{<text size="16.0pt">}}zig-zags
 across the graph{{</text>}}***. It increases **{{<highlight size="16.0pt" bg="yellow">}}A LOT{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}when
 there are eyes and goes **{{<highlight size="16.0pt" bg="yellow">}}back down{{</highlight>}}** where there are flowers.

Those last eyes were clearly the{{<u fg="#ED7D31">}}freakiest and most intimidating{{</u>}}{{<text fg="#ED7D31">}}{{</text>}}cos
 LOOK just how much money people were willing to pay for their milk when some random old dude was watching them.

![cidimage010.png@01D7008A.8D255580](cidimage010png01d7008a8d255580.png)


Which explains why you often see these signs around and about above bike stations.

![Allsigns International Ltd - Cycle Thieves](allsigns_international_ltd_-_cycle_thiev.jpeg)

Trying to tap into your **{{<highlight size="20.0pt" bg="aqua">}}unconscious social human brain{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}that
 forces you to act like a **better person** when you think other people are watching.

Hope you enjoyed this foray into the weirdnesses of animal behaviour.

See you next time!

Love Flora xxx