+++
author = "Flora Judd"
title = "Oxygen's Origins"
date = "2021-02-10"
description = "Exploring the evolution of photosynthesis and its impact on life."
tags = ["Photosynthesis","Cyanobacteria","Chloroplasts","Endosymbiosis","Oxygen Production","Dinoflagellates","Life Origins","Evolutionary Impact","Biological Mechanisms","Earth History"]
categories = ["Science","History","Biology","Evolution"]
image = "one_planet_but_many_different_earths_man.jpeg"
+++

Howdy there,

Welcome to another *{{<text size="16.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}*.

Today we are going to continue along our calendar year of the Earth's history. If you remember last time --- we were at the beginning of life on the **{{<text size="16.0pt">}}25th of February{{</text>}}**. Well now let's zoom ahead to the **{{<text size="20.0pt" fg="#ED7D31">}}28th March{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}when *{{<text size="18.0pt" fg="#E2F0D9" bg="green">}}photosynthesis{{</text>}}* {{<text size="18.0pt" fg="#E2F0D9">}}{{</text>}}first appeared on Earth.

This is equivalent to about
 **{{<text size="14.0pt" fg="red">}}2.7 billion years ago{{</text>}}**.

Photosynthesis first appeared in the **{{<text size="16.0pt" fg="#00B0F0">}}sea{{</text>}}**. Remember,
{{<highlight-u bg="yellow">}}all life begins in the sea{{</highlight-u>}}. Nothing lives on land until **November** of our calendar year (kind of mental really ***{{<text size="20.0pt" fg="#0070C0">}}sharks are older than trees{{</text>}}*** think about that !

The first things that could photosynthesise were the great, great, great grandparents of what we now call **{{<text size="18.0pt" fg="#FFC000" bg="gray">}}cyanobacteria{{</text>}}**.

These guys probably started off using **{{<text size="16.0pt" fg="#ED7D31">}}hydrogen sulfide{{</text>}}** {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}} **instead of water** to act as an{{<text size="18.0pt" fg="red">}}electron donor{{</text>}}for the chemical reactions of photosynthesis. This is known as **{{<text size="16.0pt" fg="#92D050" bg="blue">}}anoxic photosynthesis{{</text>}}** , because it doesn't produce oxygen at the end.

The equation is *pretty much the same as normal photosynthesis* but you just replace the
 **O** of **H2O** with an **S** to make **hydrogen sulfide** which is
 **H2S**. That means at the end instead of getting nice **{{<text size="14.0pt" fg="#92D050">}}oxygen gas{{</text>}}** {{<text size="14.0pt" fg="#92D050">}}{{</text>}}--- you get **{{<text size="14.0pt" fg="#BF9000">}}sulphur{{</text>}}**.

Not so ideal for life.

But then the sulfide levels started to go down as it all got used up and so these early little photosynthesisers **{{<text size="16.0pt" fg="#8FAADC">}}started to use water instead{{</text>}}**. That produced oxygen.

Now we can't get all excited just yet. It took a ***{{<text size="16.0pt" fg="red">}}LONG ASS TIME{{</text>}}*** {{<text size="16.0pt" fg="red">}}{{</text>}}for the oxygen to accumulate in the atmosphere. This is because as soon as any oxygen was created it reacted spontaneously with the **{{<text size="14.0pt">}}iron minerals{{</text>}}** {{<text size="14.0pt">}}{{</text>}}that were out in the ocean.

This forms **{{<text size="16.0pt" fg="#C00000">}}iron oxide{{</text>}}** {{<text size="16.0pt" fg="#C00000">}}{{</text>}}which is not so good at dissolving in water, so it settles down to the bottom of the ocean. We can see this change in the fossil record of these sedimentary rocks.

![One Planet, But Many Different Earths --- Many Worlds](one_planet_but_many_different_earths_man.jpeg)

These are some sexy banded iron formations at
{{<highlight bg="yellow">}}Karijini National Park in Western Australia{{</highlight>}}. The layers of reddish iron are from oceans poor in oxygen and rich in dissolved iron.

So it was only once ***{{<text size="16.0pt">}}all the iron{{</text>}}*** **{{<text size="16.0pt">}}was oxidised{{</text>}}** {{<text size="16.0pt">}}{{</text>}}that oxygen could start to accumulate in the atmosphere. Once there was enough oxygen in the atmosphere we start to see the **{{<text size="20.0pt" fg="#7030A0">}}ozone layer{{</text>}}** {{<text fg="#7030A0">}}{{</text>}}beginning to develop --- which was obviously incredibly important for life.

As we discussed yesterday --- UV rays can do baaad things to your body (and any body on land --- plant, animals and fungi included) so this lovely protective layer was one of the key reasons that life was able
 to evolve.

But wait a second. We made a big ol" jump there.
 *{{<highlight bg="yellow">}}How do you just "learn" how to photosynthesise{{</highlight>}}*. How do you make that jump?

In order to photosynthesise oxygenically you need some good ol" **{{<text size="16.0pt" fg="#E2F0D9" bg="green">}}chloroplasts{{</text>}}**. These are the little organelles that
 you find inside the cells photosynthesising organisms and they're what makes plants green. Or rather, the **{{<text size="16.0pt" fg="#00B050">}}chlorophyll{{</text>}}** {{<text size="16.0pt" fg="#00B050">}}{{</text>}} *inside* the **{{<text size="18.0pt" fg="#00B050">}}chloroplasts{{</text>}}** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}is what makes them green.

![image](4466689c9476c743.jpeg)
These are some chloroplasts inside the cells of a moss --- aren't they adorable.

So that leaves us with the question as to *{{<text size="14.0pt">}}where on Earth did these chloroplasts come from{{</text>}}* ?

This is actually a **very cool story.**

So what we think happened is that chloroplasts are actually little{{<text size="14.0pt" bg="lime">}}primitive cyanobacterial cells{{</text>}}{{<text size="14.0pt">}}{{</text>}}that were swallowed by other, bigger cells and literally ***{{<text size="24.0pt" fg="red">}}enslaved{{</text>}}*** {{<text size="24.0pt" fg="red">}}{{</text>}}to do all of the food making.

PRETTY COOL RIGHT?

This is called **{{<text size="20.0pt" fg="#E2F0D9" bg="black">}}endosymbiosis{{</text>}}** {{<text size="20.0pt" fg="#E2F0D9">}}{{</text>}}(endo = inside). That means that the
 **{{<text size="14.0pt">}}little cyanobacterial cell{{</text>}}** can use the
{{<highlight bg="yellow">}}energy from sunlight{{</highlight>}}
to {{<highlight bg="aqua">}}split water molecules{{</highlight>}} and use that to drive the
{{<text bg="lime">}}conversion of CO2 into carbohydrates and oxygen{{</text>}}. The big cell then uses those
 **delicious carbohydrates** to power all of its cellular processes.

It's not total slavery though, the big host cell provided some of the more **{{<text size="16.0pt">}}tricky-to-make molecules{{</text>}}** {{<text size="16.0pt">}}{{</text>}}that the cyanobacterial cell needed.


![image](cdbe5de36af7c437.png)


The reason we think this is the case is that the **{{<text size="16.0pt" bg="lime">}}photosynthesis{{</text>}}** {{<text size="16.0pt" bg="lime">}}**mechanisms**{{</text>}}{{<text size="16.0pt">}}{{</text>}}used by cyanobacteria
 **today** are ***{{<text size="18.0pt">}}really{{</text>}}*** **{{<text size="18.0pt">}}similar{{</text>}}** {{<text size="18.0pt">}}{{</text>}}to the way that{{<text size="18.0pt" fg="#00B050">}}chloroplasts{{</text>}}work. ALSO, chloroplasts reproduce **asexually** , ***{{<text size="16.0pt">}}independently{{</text>}}*** **{{<text size="16.0pt">}}*of their host cell*{{</text>}}** {{<text size="16.0pt">}}{{</text>}}--- which is kind of mad.

Even though it is an organelle within the cell it's autonomous and will replicate itself whenever
 *{{<text bg="fuchsia">}}it wants to{{</text>}}* {{<text bg="fuchsia">}}{{</text>}}and will *{{<text bg="fuchsia">}}not be controlled by no cell{{</text>}}*.

**{{<highlight size="14.0pt" bg="yellow">}}Strong confident independent chloroplasts.{{</highlight>}}** **{{<text size="14.0pt">}}{{</text>}}**

But what is even crazier is that we think this only happened
 *once* in all of evolutionary history.

***{{<text size="16.0pt">}}ONCE. THAT"S IT.{{</text>}}***

That means that *{{<text size="16.0pt">}}every single plant on this planet{{</text>}}* {{<text size="16.0pt">}}{{</text>}}of ours can trace its ancestry back to
 *{{<text size="16.0pt" fg="red">}}that one cell that swallowed another cell{{</text>}}*.

There is a potential problem here. What if that chloroplast somehow breaks
 *out* of the cell again!? Then the host cell is left without anything to make it lots of lovely carbohydrates.

Well there's a life hack to be had here. What you see in the history of these chloroplasts is that **{{<text size="16.0pt">}}genes{{</text>}}** {{<text size="16.0pt">}}{{</text>}}get **{{<text size="18.0pt">}}shunted over into the
{{<text fg="red">}}host cell nucleus{{</text>}}{{</text>}}** {{<text size="18.0pt" fg="red">}}{{</text>}}from the chloroplast. In fact over **{{<text size="20.0pt" fg="#ED7D31">}}90%{{</text>}}** of the **{{<highlight size="20.0pt" bg="yellow">}}~5,000 of proteins{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}that a chloroplast needs to function can
 *{{<text size="20.0pt">}}only be made by the host cell{{</text>}}*. The chloroplast knows how to make about **{{<text size="18.0pt">}}250{{</text>}}** {{<text size="18.0pt">}}{{</text>}}and that's it. That means that the chloroplast is now totally dependent on the host.

But what is even cooler is that this **{{<text size="16.0pt">}}happened again{{</text>}}** {{<text size="16.0pt">}}{{</text>}}in what is called a **{{<highlight size="18.0pt" bg="yellow">}}secondary endosymbiosis{{</highlight>}}**. That means that another,
 *even bigger cell* came along and swallowed a **{{<text size="18.0pt">}}whole plant cell{{</text>}}**.

![image](7c429b02ca46720f.png)

Now, you're not going to believe this but we think it might have then happened **{{<text size="24.0pt">}}again{{</text>}}** {{<text size="24.0pt">}}{{</text>}}in a **{{<highlight size="18.0pt" bg="yellow">}}tertiary endosymbiosis{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}(it's nuts). So *THAT* big daddy host cell got swallowed by a whole
{{<text bg="lime">}}DIFFERENT CELL{{</text>}} (I know it's starting to sound like
 *There Was An Old Lady Who Swallowed A Fly* but I promise this is real science) who again, kept the handy chloroplasts but got rid of most of the other genes from the other plants. Now I don't have a diagram long enough for this but I think you get the
 picture.

This produced some plants that we see today called **{{<text size="16.0pt" fg="#548235">}}dinoflagellates{{</text>}}** {{<text size="16.0pt" fg="#548235">}}{{</text>}}(incredible name) and the reason I bring them up is because (link to yesterday's fact) use bioluminescence!

This is a whale swimming through water full of bioluminescent dinoflagellates:

![dinoflagellates | Tumblr](dinoflagellates_tumblr.gif)

Have a good one and stay fabulous. Try not to engulf and enslave anyone around you!

Love Flora xxx