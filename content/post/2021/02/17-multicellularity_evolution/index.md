+++
author = "Flora Judd"
title = "The Multicellularity Revolution"
date = "2021-02-17"
description = "Discover how multicellularity emerged and its significance in shaping life."
tags = ["Multicellularity","Cell Evolution","Choanoflagellates","Cellular Functions","Self-Cannibalism","Species Development","Evolutionary Biology","Significance to Life","Genetic Changes","Scientific Insights"]
categories = ["Evolution","Biology","Science","Origins of Life"]
image = "one_gene_may_drive_leap_from_single_cell.jpeg"
+++

Helloooo and welcome to the best part of your day --- that's right it's
 *{{<text size="20.0pt" fg="#FFF2CC" bg="purple">}}DAILY FUN FACT TIME{{</text>}}*.

![Happy Dance GIFs - Find & Share on GIPHY](happy_dance_gifs_-_find_share_on_giphy.gif)
**
Today we are continuing on our
***{{<text size="22.0pt" fg="#00B050" bg="lime">}}Wednesday's What Was the World Doing Ages and Ages Ago{{</text>}}*** {{<text size="22.0pt" fg="#00B050">}}{{</text>}}series.

**It's gonna go global any minute now.**

So we last saw the {{<text size="14.0pt" fg="#00B050">}}evolution of photosynthesis{{</text>}}on the **{{<text size="14.0pt">}}28th March{{</text>}}**. That was pretty sick. Things are only gonna get better from here guys.

Well actually we have a **{{<text size="14.0pt">}}biiiiit of haitus{{</text>}}** {{<text size="14.0pt">}}{{</text>}}before the next exciting phase. We're now on the **{{<highlight size="20.0pt" bg="yellow">}}16th of August{{</highlight>}}** and 

***{{<text size="26.0pt" fg="red">}}MULTICELLULARITY HAS JUST EVOLVED{{</text>}}***.

Okay I know it doesn't *sound* super exciting but I **PROMISE** you it is actually very, very cool.

Firstly let's define what we mean. **{{<text size="16.0pt" bg="fuchsia">}}True multicellularity{{</text>}}** {{<text size="16.0pt">}}{{</text>}}means that you don't just have ***{{<text size="14.0pt">}}many{{</text>}}*** **{{<text size="14.0pt">}}cells{{</text>}}** you have to have
 ***{{<text size="16.0pt">}}many types{{</text>}}*** **{{<text size="16.0pt">}}of cells{{</text>}}** {{<text size="16.0pt">}}{{</text>}}that can ***{{<text size="16.0pt">}}do different things{{</text>}}***.

Some species can clump together but if the cells are {{<u>}}all the same{{</u>}} type they're
 **{{<highlight size="16.0pt" bg="aqua">}}colonial{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}NOT **{{<text size="16.0pt" bg="fuchsia">}}multicellular{{</text>}}**.

Now, we don't know how many times it happened but we think it did happen on lots of different occasions,
 *{{<u>}}totally independently of each other{{</u>}}*. Pretty cool, eh?

*{{<highlight size="14.0pt" bg="yellow">}}So how does this happen?{{</highlight>}}* *{{<text size="14.0pt">}}{{</text>}}*

Well the key distinction to make here is that it's not about cells
 ***coming together*** but rather about cells ***not coming apart***. And why would you want to do that? Why do you want more cells?

![One gene may drive leap from single cell to multicellular ...](one_gene_may_drive_leap_from_single_cell.jpeg)

{{<text size="16.0pt">}}WELL{{</text>}}.

{{<text size="36.0pt">}}You get to be{{</text>}}{{<text size="90.0pt">}}bigger{{</text>}}{{<text size="18.0pt">}}{{</text>}}
(but of course we all know size doesn't matter)

{{<highlight bg="yellow">}}If each cell can swim then you can combine your swimming power and swim farther!{{</highlight>}}
I mean is that *really* going to make a big difference if you're still a very small clump of cells? Not so sure.

{{<highlight bg="yellow">}}It might be harder to be eaten!{{</highlight>}}
But by what? There are no multicellular organisms around yet maybe a really scary ever so slightly larger cell?

{{<highlight bg="yellow">}}Might be able to catch more food!{{</highlight>}}
If there are more of you --- you can combine your food-catching efforts and get more.

*{{<text size="14.0pt" fg="red">}}WAIT A SECOND{{</text>}}*. All of these very precarious reasons sound like ones that would push you towards being a
 **{{<highlight size="16.0pt" bg="aqua">}}colonial organism{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}not a multicellular one hmmm 

So --- I guess --- really the question we want to ask is *why do you want to have different types of cells* ?

**{{<highlight size="18.0pt" bg="yellow">}}Splitting cellular functions{{</highlight>}}**

This means you can have some cells that are
{{<u>}}"body"{{</u>}} cells and some that are {{<u>}}"sex"{{</u>}} cells. This means you can put **special measures** in place to **{{<text size="14.0pt">}}protect your "sex" cells from mutating{{</text>}}**. That's important for protecting the *{{<text size="16.0pt" fg="red">}}genetic integrity of your offspring{{</text>}}*. But the "body" ones don't need as much care and can mutate a bit without it causing major problems.

**{{<highlight size="20.0pt" bg="yellow">}}The flagellar constraint hypothesis{{</highlight>}}**

The flagellum is what cells use to swim --- think of the
 **tail** on a sperm cell. That's a flagellum. They're very handy.

![swiming-bacteria GIFs Search | Find, Make & Share Gfycat GIFs](swiming-bacteria_gifs_search_find_make_s.gif)

BUT one problem is that the **{{<text size="16.0pt" fg="#ED7D31">}}cellular machinery{{</text>}}** that the flagellum needs to work is
 *also* needed ***{{<text size="16.0pt" fg="#C55A11">}}when the cell divides{{</text>}}***. And there's only one set. Hmmm Problem.

This means the cell is swimming around
 **happy as Larry** , eating food, generally just *{{<text size="14.0pt">}}thriving{{</text>}}*. But then it needs to divide it has to retract its flagellum to divide.

This means that **{{<text size="12.0pt" fg="#0070C0">}}some cells can do the swimming{{</text>}}** {{<text size="12.0pt" fg="#0070C0">}}{{</text>}}and **{{<text size="12.0pt" fg="#7030A0">}}some can do the dividing{{</text>}}** {{<text size="12.0pt" fg="#7030A0">}}{{</text>}}so you **{{<text size="14.0pt" fg="#00B050">}}don't stop swimming and start sinking{{</text>}}** {{<text size="14.0pt" fg="#00B050">}}{{</text>}}whenever you need to divide!

**{{<highlight size="20.0pt" bg="yellow">}}Self-cannibalism hypothesis{{</highlight>}}**

This is the idea that if
 **food becomes scarce** , it's useful having specialised food storage cells that can be used instead. This means that instead of
 ***all your cells dying*** , ***only a few*** will.

***{{<text size="16.0pt" fg="red">}}Flatworms still do this!{{</text>}}*** {{<text size="16.0pt" fg="red">}}{{</text>}}If you feed it, it gets bigger ( ***{{<text size="48.0pt" fg="#ED7D31">}}growth{{</text>}}*** ) and if you don't feed it, it gets smaller ( ***{{<text fg="#ED7D31">}}degrowth{{</text>}}*** ).

Also if you're ever having a bad day look at some flatworms. Look how **{{<highlight size="14.0pt" fg="#ED7D31" bg="yellow">}}bouncy{{</highlight>}}** {{<text size="14.0pt" fg="#ED7D31">}}{{</text>}}they are!!

![These Flatworms Can Regrow A Body From A Fragment. How Do ...](these_flatworms_can_regrow_a_body_from_a.gif)


Now these are the **best hypotheses we have** , but we *haven't* yet found any animals that look like what we think early animals would look like. So that means that if multicellularity started how we think it did, life has gotten a
 **{{<text size="18.0pt">}}WHOLE LOT more complicated{{</text>}}** {{<text size="18.0pt">}}{{</text>}}since then.

![Tell Me Why Do You Have to Go and Make Things So ...](tell_me_why_do_you_have_to_go_and_make_t.png)

{{<text size="20.0pt">}}Yes Avril --- ***{{<text fg="red">}}why{{</text>}}*** ?{{</text>}}

The closest non-animal relative we have is a little single-celled protist called
 ***{{<text size="24.0pt" fg="#7030A0">}}choanoflagellates{{</text>}}***.

![5 facts you should know about Choanoflagellates ...](5_facts_you_should_know_about_choanoflag.jpeg)

See --- they do like to **{{<text size="16.0pt" fg="#00B0F0">}}stick together{{</text>}}** {{<text size="16.0pt" fg="#00B0F0">}}{{</text>}}(they form structures where they're heads all stick together and they wave their little flagella around). And they are also very diverse.
{{<highlight size="20.0pt" bg="aqua">}}Loricate choanoflagellates{{</highlight>}}{{<text size="20.0pt">}}{{</text>}}secrete a ***{{<text size="18.0pt" fg="#0070C0">}}little glass basket{{</text>}}*** {{<text size="18.0pt" fg="#0070C0">}}{{</text>}}around themselves that looks like this:

![Detail of the anterior part of the lorica of Stephanoeca ...](detail_of_the_anterior_part_of_the_loric.jpeg)


*{{<text size="18.0pt" bg="fuchsia">}}ISN"T THAT COOL!?{{</text>}}* *{{<text size="18.0pt">}}{{</text>}}*

Hope you enjoyed this week's ***{{<text size="22.0pt" fg="#00B050" bg="lime">}}Wednesday's What Was the World Doing Ages and Ages Ago{{</text>}}***

Lots of love,

Flora xxx