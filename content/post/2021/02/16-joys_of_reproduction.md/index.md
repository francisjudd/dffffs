+++
author = "Flora Judd"
title = "The Mystery of Sex"
date = "2021-02-16"
description = "Exploring the evolutionary enigma of sexual reproduction and its advantages."
tags = ["Sex","Evolution","Asexual Reproduction","Gene Combination","Bdelloid Rotifers","Pathogen Resistance","Red Queen Dynamics","Genetic Diversity","Mating Strategies","Evolutionary Advantages"]
categories = ["Biology","Sexual Reproduction","Evolution","Science"]
image = "duck_penis_controversy_nsf_is_right_to_f.jpeg"
+++

Hello and welcome to another
 *{{<text size="18.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}*

Today we are going to have a look at *{{<text size="22.0pt" fg="red" bg="black">}}sex{{</text>}}* {{<text size="22.0pt" fg="red">}}{{</text>}}(baby let's talk about you and me, let's talk about all the good things * **ahem** *)

*Calm down now.* Things are going to remain ***{{<text size="22.0pt" fg="#00B050">}}strictly biologica{{</text>}}*** ***{{<text size="20.0pt" fg="#00B050">}}l{{</text>}}*** **{{<text size="20.0pt" fg="#00B050">}}{{</text>}}** and ***{{<text size="22.0pt" fg="#0070C0">}}strictly theoretical{{</text>}}***.

Mhmmm. Lookin at **you**. Keep your mind{{<text size="18.0pt">}}out of the gutter{{</text>}}. We can save the **{{<text fg="#C00000">}}nitty-gritty{{</text>}}** {{<text fg="#C00000">}}{{</text>}}for another day (look forward to the corkscrew shaped penis of ducks).

![Duck penis controversy: NSF is right to fund basic ...](duck_penis_controversy_nsf_is_right_to_f.jpeg)

(yeah no kidding I had a lecture on them)

BUT HERE we are ***{{<text size="20.0pt" fg="#ED7D31">}}not looking at funny-shaped penises{{</text>}}*** , we are looking at sex defined as ***{{<highlight bg="yellow">}}biparental
 mating or the bringing together of two distinct gene sets{{</highlight>}}***.

Now --- sex is considered to be one of evolution's biggest mysteries. One of the main reasons it is so confusing is because of the **{{<u size="22.0pt" fg="#00B0F0">}}two-fold cost of males{{</u>}}**.

**{{<text size="16.0pt" bg="lime">}} Flora, what in
 *GOD"S NAME* are you on about? {{</text>}}** **{{<text size="16.0pt">}}{{</text>}}**

Thank you for asking so nicely.

Well if you think about it,
 **{{<text size="16.0pt">}}males don't actually produce offspring{{</text>}}** , they *{{<text size="18.0pt">}}just fertilise eggs{{</text>}}*.

That means that an ***{{<text size="18.0pt" fg="#ED7D31">}}asexual{{</text>}}*** **{{<text size="18.0pt" fg="#ED7D31">}}population{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}that stops producing males is in **{{<text size="22.0pt">}}BUSINESS{{</text>}}** {{<text size="22.0pt">}}{{</text>}}because *{{<highlight size="24.0pt" bg="yellow">}}all individuals can make more individuals{{</highlight>}}*.

That means (in a world where everyone only has two offspring) an asexual female will produce
 **twice as many daughters** and ***four times* as many granddaughters** as a sexual female.

![Asexual Reproduction GIFs - Get the best GIF on GIPHY](asexual_reproduction_gifs_-_get_the_best.gif)

This means that if one of the individuals in a population
 *randomly mutates* and becomes asexual they will be at a serious reproductive advantage and so the population will look like this:

![DiagramDescription automatically generated](diagramdescription_automatically_generat.png)

There are **{{<text size="16.0pt" fg="red">}}other costs as well{{</text>}}** {{<text size="16.0pt" fg="red">}}{{</text>}}--- these include **{{<highlight size="18.0pt" bg="yellow">}}finding a mate{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}which requires *time* and *energy* (took me twenty fuckin years), you are at **{{<text size="18.0pt" bg="lime">}}risk of STIs{{</text>}}** (fingers crossed on that one), you only get to **{{<highlight size="18.0pt" bg="aqua">}}transmit half of your genes{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}to your offspring and during sexual reproduction **{{<text size="18.0pt" bg="fuchsia">}}a lot of genes get swapped and shunted around{{</text>}}** {{<text size="18.0pt">}}{{</text>}}so if you have a ***{{<text size="16.0pt">}}really banging genome{{</text>}}*** {{<text size="16.0pt">}}{{</text>}}(which you probably do because you lived long enough to reproduce) that's going to be a real shame and your offspring{{<u size="14.0pt" fg="red">}}might be totally shit{{</u>}}.

But sex is ***{{<text size="20.0pt" fg="#ED7D31">}}nearly universal{{</text>}}*** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}in plants, animals and fungi. So there have got to be some upsides right!? Otherwise why is there{{<highlight-u bg="yellow">}}so much sex{{</highlight-u>}}
about?

Well, okay, whilst I said that sex might break up your really good gene combination, it could also **{{<text size="16.0pt">}}break up a bad one{{</text>}}**. OR it can prevent ***{{<highlight size="18.0pt" fg="red" bg="yellow">}}competition between two differentially advantages alleles{{</highlight>}}***.

WOAH FLORA. English please.

Basically, you have your genes and those come in lots of different types (I wanted to say flavours but I think that might be misleading --- your genome doesn't taste of anything sorry) and these different types
 are called **{{<text size="16.0pt" fg="red" bg="silver">}}alleles{{</text>}}**. For example, there is an allele for blue eyes and one for brown eyes so depending on which "type" of gene
 you have (which allele you have) you will express different traits (here --- eye colour).

So imagine you have ***two asexually reproducing organisms***. One of them is
{{<u>}}really good at finding food in its habitat.{{</u>}} One is {{<u>}}really good at avoiding being eaten{{</u>}}. If you don't have any way of those genomes
{{<highlight bg="yellow">}}combining{{</highlight>}}, {{<highlight bg="yellow">}}mixing{{</highlight>}}and {{<highlight bg="yellow">}}reshuffling{{</highlight>}}--- there's going to be a **{{<text size="18.0pt">}}competition{{</text>}}** {{<text size="18.0pt">}}{{</text>}}between the ***{{<text size="16.0pt" fg="#ED7D31">}}good-finders{{</text>}}*** {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}and the ***{{<text size="16.0pt" fg="#00B050">}}good-avoiders{{</text>}}***.

If there are *loads* of predators about the good-avoiders will win and if there's
 *hardly any food* about the good-finders will win, but what you **REALLY WANT** is a way of mashing them together so you can be both!

Then it's no longer a competition between these two alleles that are both super useful and instead allows for a **{{<text size="14.0pt">}}super-organism{{</text>}}** {{<text size="14.0pt">}}{{</text>}}with *{{<highlight size="16.0pt" bg="aqua">}}both these fantastic genes{{</highlight>}}*.

I hope that made sense.

Also sex means that you can make
 **brand new gene combinations**. If you have a pathogen that really likes to infect your species, it is going to be very specialised to the most common form. If you can be **{{<highlight size="16.0pt" fg="#7030A0" bg="yellow">}}new{{</highlight>}}** {{<highlight size="16.0pt" fg="#7030A0" bg="yellow">}}{{</highlight>}}and **{{<highlight size="16.0pt" fg="#7030A0" bg="yellow">}}novel{{</highlight>}}** {{<highlight size="16.0pt" fg="#7030A0" bg="yellow">}}{{</highlight>}}and **{{<highlight size="16.0pt" fg="#7030A0" bg="yellow">}}funky{{</highlight>}}** {{<highlight size="16.0pt" fg="#7030A0" bg="yellow">}}{{</highlight>}}and **{{<highlight size="16.0pt" fg="#7030A0" bg="yellow">}}fresh{{</highlight>}}** {{<highlight size="16.0pt" fg="#7030A0" bg="yellow">}}{{</highlight>}}then the pathogen is going to have a harder time infecting you.

Of course the enemy can then catch up. If you are super rare and the pathogen can't figure out how to infect you --- you are going to do ***{{<text size="14.0pt">}}amazingly well!{{</text>}}*** You can have sex with everyone and make fuckloads of babies and then oops suddenly your genes *{{<text size="16.0pt" bg="fuchsia">}}aren't so rare anymore{{</text>}}*. So you are now common and the pathogen evolves to be more like you.

These are known as Red Queen dynamics from a fabulous Alice in Wonderland quote:

**{{<text size="16.0pt" fg="red" bg="black">}} Now, *here* , you see, it takes all the running you can do, to keep in the same place. {{</text>}}** **{{<text size="16.0pt" fg="red">}}{{</text>}}**

![image](7c9116ed760aabee.jpeg)

These species are **{{<highlight bg="yellow">}}desperately evolving{{</highlight>}}** to stay ahead of their pathogens but nothing actually
 *changes*. There's no *trajectory* here. They aren't being pushed by evolution in a
 **{{<highlight bg="yellow">}}certain direction{{</highlight>}}** for long enough --- they're just trying to be a bit different from whatever came before.

This opens up a question as to **{{<text size="14.0pt">}}how asexual species can persist{{</text>}}** ? Some species don't
 *have* to reproduce asexually but they will if they need to. Sharks can do this --- females can fertilise their own eggs without any males. Like this zebra shark.

![Sharks Habitat and Distribution - Animal Facts and Information](sharks_habitat_and_distribution_-_animal.jpeg)

But what about species without ***{{<highlight size="22.0pt" bg="yellow">}}any sexual lifestyle stages{{</highlight>}}*** ??? These are known in the scientific literature as **{{<text size="20.0pt" fg="red">}}ancient asexual scandals{{</text>}}** {{<text size="20.0pt" fg="red">}}{{</text>}}(genuinely).

One example of this is a group of organisms called the *{{<text size="18.0pt" bg="fuchsia">}}Bdelloid rotifers{{</text>}}*. These are **{{<text size="8.0pt">}}microscopic{{</text>}}** , **{{<text size="14.0pt" fg="#ED7D31">}}soft-bodied{{</text>}}** ,{{<highlight size="14.0pt" bg="aqua">}}filter-feeding aquatic{{</highlight>}}{{<text size="14.0pt">}}{{</text>}}invertebrates and we have never,{{<text size="16.0pt">}}ever{{</text>}},{{<text size="28.0pt">}}ever{{</text>}}not once **{{<text size="90.0pt">}}ever{{</text>}}** {{<text size="90.0pt">}}{{</text>}}found a male.

![rotifer | Tumblr](rotifer_tumblr.gif)

There have been around for
 **{{<highlight size="22.0pt" bg="yellow">}}FIFTY MILLION YEARS{{</highlight>}}** {{<text size="22.0pt">}}{{</text>}}and there are over ***{{<text size="16.0pt">}}450 different species{{</text>}}*** and ***{{<text size="20.0pt" fg="red">}}NO MALES AT ALL{{</text>}}***. This is the largest animal group with no males and yet they are
 **incredibly successful**. You can find them in almost **{{<highlight size="14.0pt" fg="#0070C0" bg="aqua">}}every freshwater habitat{{</highlight>}}** {{<text size="14.0pt" fg="#0070C0">}}{{</text>}}in the world.

And it's not that they don't have pathogens --- they have **{{<text size="18.0pt" bg="red">}}extremely virulent fungi{{</text>}}** that can wipe out{{<highlight size="16.0pt" bg="yellow">}}whole populations of rotifers{{</highlight>}}{{<text size="16.0pt">}}{{</text>}}at once. So how have they not been wiped out?!

Well it seems that these rotifers have a trick up their sleeves. They are able to ***{{<u size="18.0pt">}}completely desiccate themselves{{</u>}}*** for
 **months** or **years**. While they are in this totally dry state that can be picked up and **{{<highlight size="16.0pt" bg="aqua">}}dispersed by the wind{{</highlight>}}**. The pathogenic fungus LOVES its water and so cannot come with them as they disperse. Then they rehydrate and recover
 as soon as water comes back.

So instead of making new genotypes through **{{<text size="20.0pt">}}sex{{</text>}}** {{<text size="20.0pt">}}{{</text>}}--- they make new genotypes through ***{{<highlight size="20.0pt" bg="yellow">}}MIGRATION{{</highlight>}}***.

***{{<text size="16.0pt" fg="#7030A0" bg="fuchsia">}}ISN"T THAT SNAZZY.{{</text>}}*** ***{{<text size="16.0pt" fg="#7030A0">}}{{</text>}}***

Hope you enjoyed this foray into the joys of sex!

Lots of love,

Flora xxx