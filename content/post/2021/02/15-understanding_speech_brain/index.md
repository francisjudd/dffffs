+++
author = "Flora Judd"
title = "Brain and Speech"
date = "2021-02-15"
description = "Explore how speech is processed in the brain."
tags = ["Broca's Area","Wernicke's Area","Aphasia","Language Processing","Brain Anatomy","Speech Production","Comprehension","Communication Disorders","Stroke Recovery","Music Therapy"]
categories = ["Neuroscience","Speech","Health","Education"]
image = "26fb3db8369478cc.png"
+++

Hello and welcome to today's fact,

We are going to have a look at how we speak. We spoke a liiiiiittle bit about the fact that humans learn vocalisations by listening, making an auditory template and then trying to copy that template.

But let's now zoom in on the brain and see what's happening once you actually know how to speak.

There are two key zones in the brain when it comes to speech and these are your **{{<text size="20.0pt" bg="lime">}}Broca's area{{</text>}}** {{<text size="20.0pt">}}{{</text>}}(named after the French anatomist and anthropologist
{{<text fg="#00B050">}}Pierre Paul Broca{{</text>}}) and the **{{<highlight size="20.0pt" bg="aqua">}}Wernicke's area{{</highlight>}}** (named after the German physician
{{<text fg="#00B0F0">}}Carl Wernicke{{</text>}}).

Here they are!

![image](26fb3db8369478cc.png)


Mmm brains.

These two bits of your brain are both very important to speech, but actually do slightly different things. Your Broca's area, found in your *{{<text size="18.0pt" fg="red">}}frontal lobe{{</text>}}* , is more in charge of
 **speech production** , but you Wernicke's area, found in your *{{<text size="20.0pt" fg="red">}}cerebral cortex{{</text>}}* , is more in charge of
 **comprehension of written and spoken language**. These are both found on the
 **{{<highlight size="22.0pt" bg="yellow">}}LEFT{{</highlight>}}** {{<text size="22.0pt">}}{{</text>}}side of the brain.

These parts of your brain are obviously in a lot of communication with each other a lot of the time, you know to do all that language processing you need to be able to{{<text size="16.0pt">}}talk to your cats{{</text>}}about your day (one day they will reply).

Broca's idea was that information is **{{<text size="18.0pt">}}received by your ears{{</text>}}** {{<text size="18.0pt">}}{{</text>}}and *{{<text size="16.0pt">}}then transmitted to the Wernicke's area{{</text>}}* where{{<highlight size="16.0pt" bg="yellow">}}tones are converted into concepts or objects{{</highlight>}}.

THEN the information from the Wernicke's area is transferred along a
 **{{<highlight bg="yellow">}}snazzy{{</highlight>}}** bundle of nerves called the *{{<text size="28.0pt" fg="#7030A0" bg="fuchsia">}}arcuate fasciculus{{</text>}}* {{<text size="28.0pt" fg="#7030A0">}}{{</text>}}(say that to anyone you"ll sound REALLY smart) to the Broca's area. From the Broca's area the information travels to your **{{<text size="20.0pt">}}motor cortex{{</text>}}** {{<text size="20.0pt">}}{{</text>}}which controls **{{<text size="16.0pt">}}muscles{{</text>}}** {{<text size="16.0pt">}}{{</text>}}in your *{{<highlight size="14.0pt" bg="yellow">}}mouth and tongue{{</highlight>}}*. Then you can speak!

![Brain injury awareness - how TBI affects communication](brain_injury_awareness_-_how_tbi_affects.jpeg)


So in essence the Wernicke's area is where the{{<text size="22.0pt">}}"{{</text>}} **{{<text size="16.0pt">}}theoretical"{{</text>}}** {{<text size="16.0pt">}}{{</text>}}bit is processed (i.e. what the words actually mean) and the Broca's area is the{{<text size="22.0pt">}}"{{</text>}} **{{<text size="20.0pt">}}active"{{</text>}}** {{<text size="20.0pt">}}{{</text>}}bit is processed (i.e. actually physically speaking words).

*{{<highlight size="14.0pt" bg="yellow">}}A lot of stutterers struggle with underactivity of the Broca's area!{{</highlight>}}* *{{<text size="14.0pt">}}{{</text>}}*

A really fascinating, if very unfortunate, way we can look at how different parts of the brain function --- is by looking at people who have had different regions of their{{<text size="14.0pt" fg="#7030A0">}}brain damaged{{</text>}}for different reasons.

One of the most common examples here is **{{<text size="14.0pt">}}stroke victims{{</text>}}**. When you have a stroke, a **{{<text size="16.0pt" fg="red">}}blood vessel{{</text>}}** {{<text size="16.0pt" fg="red">}}{{</text>}}that leads to your brain gets{{<text size="16.0pt" fg="yellow" bg="black">}}blocked{{</text>}}, usually by a blood clot, and that means that the brain
 tissue that is normally served by that blood vessel now *{{<text size="16.0pt">}}can't get any oxygen{{</text>}}* {{<text size="16.0pt">}}(yoiks){{</text>}}. This means the tissue is going to be oxygen starved and if the blood clot isn't removed or unblocked quickly, the tissue will start to die.

![Doctor's Warn: COVID-19 May Cause Deadly Blood Clots ...](doctors_warn_covid-19_may_cause_deadly_b.gif)


*{{<text size="16.0pt" bg="red">}}TOTALLY NOT IDEAL.{{</text>}}* *{{<text size="16.0pt">}}{{</text>}}*

(side note --- if the blood vessel is one that leads to cardiac tissue then you get a heart attack instead of a stroke)

Now when these parts of the brain that are in charge of speech get deprived of oxygen and start to die it often results in a condition called **{{<text size="20.0pt" bg="lime">}}aphasia{{</text>}}** and there are lots of different types. This affects how you speak, how you understand what people are saying to you and your reading
 and writing skills. Sadly, most of the time you don't just get one --- you get a whole mixed bunch.

But what is kind of amazing is that there is such a **{{<highlight size="18.0pt" bg="yellow">}}strong correlation{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}between symptoms and damage in certain brain areas that most of the time you can roughly figure out which part of the brain has been damaged *{{<text size="16.0pt">}}based on the symptoms of the person in front of you{{</text>}}*. Isn't that nuts?!

Let's start with the Broca's area. Broca's area is associated with a type of aphasia called **{{<text size="18.0pt" fg="#ED7D31">}}expressive aphasia{{</text>}}** which means you know what you want to say but you can't get it out. People with this type of aphasia can usually understand words and simple
 sentences but can't put together fluent speech. This is because the "active" part is struggling.

Damage to the Wernicke's area tends to cause **{{<text size="22.0pt" fg="#ED7D31">}}receptive aphasia{{</text>}}** which means that speech stays pretty normal-sounding but there is a biiiiiiig impairment of language comprehension. This is really weird because
 the sentences are still spoken totally normally but the actual words make no sense. This is sometimes known as ***{{<text size="18.0pt" fg="#00B050">}}"word salad"{{</text>}}*** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}(worst kind of salad ever). This is because all of the assignment of the
 *values* of the words is done in the Wernicke's area so when that part goes down all you get is a jumble of words.

If you click the photo below, Byron will show you what that sounds like.

<a href="https://www.youtube.com/watch?v=3oef68YabD0">![image](fb28a206e467f5db.png)</a>
{{< youtube 3oef68YabD0 >}}


**{{<highlight size="20.0pt" bg="yellow">}}Conduction aphasia{{</highlight>}}** **{{<text size="20.0pt">}}{{</text>}}** is seen where people find it really difficult to repeat what has been said to them. This occurs when this bundle of nerves (the ***{{<text size="16.0pt" fg="#7030A0" bg="fuchsia">}}arcuate fasciculus{{</text>}}*** ) is damaged, meaning that there is a delay between hearing sounds and comprehending them which
 makes them really struggle with these repetition tasks.

For example:

**Clinician** : Now, I want you to say some words after me. Say "boy".
*Aphasic* : Boy.
**Clinician** : Home.
*Aphasic* : Home.
**Clinician** : Seventy-nine.
*Aphasic* : Ninety-seven. No sevinty-sine siventy-nice .
**Clinician** : Let's try another one. Say "refrigerator".
*Aphasic* : Frigilator no? how about frerigilator

This totally contrasts with the next one which is called **{{<highlight size="20.0pt" bg="yellow">}}mixed transcortical aphasia{{</highlight>}}**. This one is super weird. Damage to the brain effectively
 *isolates* the arcuate fasciculas and the Broca's and Wernicke's areas from the rest of the brain. So they're all connected no problemo but they are *{{<text size="18.0pt" fg="#4472C4">}}totally cut off from anything else{{</text>}}*. This means they struggle a lot to produce their own language or to
 *understand* what is being said to them but they can copy long complex sentences. It's pretty miserable though, so it's lucky it's rare, because these people usually won't speak (unless spoken to), can't name objects or read or write.

However, **{{<highlight size="20.0pt" bg="yellow">}}global aphasia{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}is definitely the most grim where all language abilities are severely affected and patients are essentially unable to produce, repeat or understand words.

BUT there are very **{{<text size="22.0pt" fg="#70AD47">}}cool speech therapies{{</text>}}** {{<text size="22.0pt" fg="#70AD47">}}{{</text>}}that can use ***{{<text size="16.0pt" bg="lime">}}music{{</text>}}*** {{<text size="16.0pt">}}{{</text>}}(which activates the RIGHT hand side of your brain) to try and coax the brain into producing speech by using the association of rhythms and words from well known songs.

People with global aphasia have been shown to be able to **{{<text size="22.0pt">}}sing full songs{{</text>}}** {{<text size="22.0pt">}}{{</text>}}. People who are TOTALLY MUTE otherwise literally singing WHOLE ENTIRE SONGS. LIKE WHAT!?

And they are thought to be utilising this right hand **{{<text size="22.0pt" bg="fuchsia">}}"singing centre"{{</text>}}** {{<text size="22.0pt">}}{{</text>}}instead of the **{{<highlight size="20.0pt" bg="yellow">}}speech centres{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}we just looked at.

Hope you enjoyed today's daily fact!

Love Flora xxx