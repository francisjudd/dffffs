+++
author = "Flora Judd"
title = "The Dance of Selection"
date = "2021-02-24"
description = "Exploring the intriguing role of sexual selection in evolution."
tags = ["Sexual Selection","Reproductive Success","Courtship Behaviors","Exaggerated Traits","Good Genes Hypothesis","Evolutionary Mechanisms","Natural Selection","Stalk-eyed Flies","Long-tailed Widowbirds","Animal Behavior"]
categories = ["Evolution","Biology","Sexual Selection","Behavioral Ecology"]
image = "photographer_of_the_year_2020_weekly_sel.jpeg"
+++

Hello and welcome to your
 *{{<text size="24.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* ,

Today is your favourite day of the week --- it's ***{{<text size="22.0pt" fg="#00B050" bg="lime">}}Wednesday's What Was the World Doing Ages and Ages Ago{{</text>}}***

So let's have a recap of what we"ve seen so far this calendar year.

We saw ***{{<highlight size="20.0pt" bg="yellow">}}life begin{{</highlight>}}*** {{<text size="20.0pt">}}{{</text>}}on the **{{<text size="26.0pt" fg="#4472C4">}}25th February{{</text>}}**.
*{{<text size="22.0pt" bg="lime">}}Photosynthesis started{{</text>}}* {{<text size="22.0pt">}}{{</text>}}on the **{{<text size="22.0pt" fg="#4472C4">}}28th March{{</text>}}**.
***{{<text size="22.0pt" bg="fuchsia">}}Multicellular organisms{{</text>}}*** {{<text size="22.0pt">}}{{</text>}}appeared on the **{{<text size="22.0pt" fg="#4472C4">}}16th August{{</text>}}**.

NOW we have the **{{<text size="26.0pt" fg="red" bg="maroon">}}evolution of sexual reproduction{{</text>}}** {{<text size="26.0pt" fg="red">}}{{</text>}}on the **{{<text size="22.0pt" fg="#0070C0">}}17th September{{</text>}}**.

Phwoar this year is just *flying* by.

We"ve already discussed the
 **{{<text size="16.0pt">}}evolutionary confusion{{</text>}}** {{<text size="16.0pt">}}{{</text>}}surrounding **{{<text size="18.0pt" bg="red">}}sexual reproduction{{</text>}}** {{<text size="18.0pt">}}{{</text>}}and why it might have evolved (I jumped the gun a bit there oops) so I"m going to take you on a slightly different exploration of sexual reproduction. And that --- is ***{{<text size="22.0pt" fg="#C00000">}}the role of sexual selection{{</text>}}***.

Now sexual selection is a
 ***{{<text size="16.0pt">}}fundamental component{{</text>}}*** {{<text size="16.0pt">}}{{</text>}}of **{{<text size="16.0pt" fg="#00B050">}}sexual reproduction{{</text>}}**.

If you're gonna mingle ya genes --- you gotta make sure they're a
 **good "un**.

This is often seen in the form of **{{<text size="18.0pt" fg="#ED7D31">}}exaggerated displays{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}and **{{<text size="18.0pt" fg="#ED7D31">}}courtship behaviours{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}in males. But these are bit odd, because they seem to be ***{{<highlight size="22.0pt" bg="yellow">}}totally inconsistent{{</highlight>}}*** **{{<text size="22.0pt">}}with
{{<text fg="#00B050">}}natural selection{{</text>}}{{</text>}}**.

![superb bird of paradise on Tumblr](superb_bird_of_paradise_on_tumblr.gif)

*{{<text bg="fuchsia">}}COME DANCE WITH ME DEBORAH{{</text>}}* **
Why would you want all these traits that are actually *{{<text size="16.0pt">}}very costly{{</text>}}* {{<text size="16.0pt">}}{{</text>}}and *{{<text size="18.0pt">}}difficult to maintain{{</text>}}*. I mean think of the **{{<highlight size="18.0pt" bg="aqua">}}peacock{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}--- lugging all those feathers around is going to make it difficult to find food and run away from predators.

In a slightly more niche example, **{{<text size="20.0pt" fg="#4472C4">}}male tungara frogs{{</text>}}** {{<text size="20.0pt" fg="#4472C4">}}{{</text>}}attract their lovely lady friends using
{{<highlight size="20.0pt" bg="yellow">}}"chuck"{{</highlight>}}{{<text size="20.0pt">}}{{</text>}}calls. These calls are actually heard by
 ***{{<text size="18.0pt" fg="#ED7D31">}}fringe-eared bats{{</text>}}*** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}which use them to find males that they can then eat. Hmmm. Sexy --- yes. But not so good when trying not to get eaten.

Plus they look real funny.

![Google+](google.gif)

I know you're now *{{<text size="16.0pt">}}dying to know{{</text>}}* {{<text size="16.0pt">}}{{</text>}}what this "chuck" sounds like now but I"m afraid this is the most accurate representation I have:

![image](5708b36d254f6d84.png)
*(yes that is Chuck Norris)*

So ***{{<highlight size="20.0pt" bg="yellow">}}why{{</highlight>}}*** {{<text size="20.0pt">}}{{</text>}}do males have these really costly traits? Well --- sexual selection operates on the ***{{<text size="20.0pt" fg="#ED7D31">}}variation in individual reproductive success{{</text>}}***. The{{<u size="14.0pt">}}more variation{{</u>}}{{<text size="14.0pt">}}{{</text>}}there is, the
{{<u size="16.0pt">}}more intense{{</u>}}{{<text size="16.0pt">}}{{</text>}}the selection is on the **{{<text size="16.0pt">}}traits{{</text>}}** {{<text size="16.0pt">}}{{</text>}}that can be used to **{{<text size="18.0pt" fg="#00B050">}}measure that reproductive success{{</text>}}**.

This leads to the evolution of traits that **{{<highlight size="18.0pt" bg="yellow">}}signal{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}some sort of
 ***{{<text size="24.0pt" fg="red">}}competitive advantage{{</text>}}*** {{<text size="24.0pt" fg="red">}}{{</text>}}in reproduction --- even if they have these big costs.

Now I know what you're thinking. Flora --- a shouting frog isn't going to tell you how "good" a mate it is.

*{{<text size="16.0pt">}}Except that it kind of is.{{</text>}}*

There's an argument called the ***{{<text size="22.0pt" bg="fuchsia">}}"good genes" hypothesis{{</text>}}*** {{<text size="22.0pt">}}{{</text>}}that basically says ---
 *because* these traits are so costly to produce, **{{<text size="16.0pt" fg="#ED7D31">}}only the good males{{</text>}}** {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}will be able to live with them *{{<highlight size="18.0pt" bg="yellow">}}without dying or being eaten{{</highlight>}}*.

Because, in any environment, there is a ***{{<text size="14.0pt">}}limited number of resources{{</text>}}*** {{<text size="14.0pt">}}{{</text>}}there is a trade-off in males between their **{{<text size="16.0pt">}}investment in these snazzy, sexy traits{{</text>}}** {{<text size="16.0pt">}}{{</text>}}and important things like{{<highlight size="18.0pt" bg="yellow">}}moving{{</highlight>}}and{{<highlight size="18.0pt" bg="yellow">}}eating{{</highlight>}}and{{<highlight size="18.0pt" bg="yellow">}}breathing{{</highlight>}}.

We can see this is ***{{<text size="22.0pt" fg="#ED7D31">}}stalk-eyed flies{{</text>}}***. Their eyes are found at the end of ***{{<highlight size="18.0pt" bg="yellow">}}elongated lobes{{</highlight>}}*** ***{{<text size="18.0pt">}}{{</text>}}*** and females just *{{<text size="22.0pt" bg="fuchsia">}}love{{</text>}}* {{<text size="22.0pt">}}{{</text>}}a male with a wider eye span. They start with
 **normal looking eyes** and when they reach maturity the males will **{{<u size="48.0pt">}}inflate their lobes{{</u>}}** {{<text size="48.0pt">}}{{</text>}}--- forcing their eyes outward kind of like this:

![Stalk Eyed Fly GIFs - Find & Share on GIPHY](stalk_eyed_fly_gifs_-_find_share_on_giph.gif)

***{{<text size="20.0pt">}}I KNOW ISN"T THAT NUTS.{{</text>}}***

In experiments, when you feed males a **{{<text size="16.0pt" fg="#ED7D31">}}poor diet{{</text>}}** {{<text fg="#ED7D31">}}{{</text>}}their eye-spans are **{{<text size="16.0pt" fg="#ED7D31">}}much less{{</text>}}** {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}than males that are fed **{{<text size="16.0pt" fg="#ED7D31">}}nutrient-rich diets{{</text>}}**. Therefore, in a poor-quality environment females can use the eye-span as a proxy for ***{{<highlight size="22.0pt" bg="yellow">}}how good the male is at surviving and finding food{{</highlight>}}*** {{<text size="22.0pt">}}{{</text>}}in the habitat. The males that are really strongly affected by that harsh environment can be easily identified and ignored.

Now these will only work if they are **{{<text size="20.0pt" fg="#70AD47">}}honest signals{{</text>}}**. A stalk-eyed fly cannot control its eye-span --- only it's nutritional intake can.

If we go back to the example of the frogs, only **{{<text size="18.0pt">}}larger males{{</text>}}** {{<text size="18.0pt">}}{{</text>}}can make the deep "chucks" that the females love so much. That's how they know a male is a good "un. If males could make those deep "chucks" without being larger ***{{<text size="20.0pt" fg="#ED7D31">}}the system would break down{{</text>}}***. Females would catch on that the call was no longer a good indicator of
 male fitness and would stop listening.

Sometimes these traits aren't about nutrition but are about ***{{<text size="20.0pt" fg="#4472C4">}}immuno-competence{{</text>}}*** {{<text size="20.0pt" fg="#4472C4">}}{{</text>}}instead. Testosterone ***{{<text size="16.0pt">}}determines the expression of male sexual traits{{</text>}}*** {{<text size="16.0pt">}}{{</text>}}in a lot of species, but{{<highlight-u size="18.0pt" bg="yellow">}}also handicaps their immune systems{{</highlight-u>}}. Therefore, the ones that express the condition
 most strongly need to be in ***{{<text size="16.0pt" bg="fuchsia">}}tip top shape{{</text>}}*** {{<text size="16.0pt">}}{{</text>}}(genetically speaking) to allow for that.

Sometimes these traits are even preferred when they are artificially made more extreme. We can see this in ***{{<highlight size="18.0pt" bg="yellow">}}long-tailed widow birds{{</highlight>}}***. These bad boys are a strongly sexually dimorphic
 species --- meaning that the males and females look REALLY different. Males have a **{{<text size="18.0pt" fg="#4472C4">}}really elongated tail{{</text>}}** {{<text size="18.0pt" fg="#4472C4">}}{{</text>}}that they display to females during courtship.

![Photographer of the Year 2020 Weekly Selection: Week 6 ...](photographer_of_the_year_2020_weekly_sel.jpeg)

Mmmmm dat tail is LONG.

So what some researchers did is they investigated whether females like males with longer tails. To test this they used four different treatments:

Tail is **{{<text size="18.0pt">}}clipped{{</text>}}**Tail is **{{<text size="18.0pt">}}elongated{{</text>}}***{{<text size="18.0pt">}}Control 1{{</text>}}* {{<text size="18.0pt">}}{{</text>}}(males had tails clipped and put back at the same length)*{{<text size="18.0pt">}}Control 2{{</text>}}* (males were handled without altering the tail)

They then **{{<text size="16.0pt" fg="#7030A0">}}measured female preference{{</text>}}** {{<text size="16.0pt" fg="#7030A0">}}{{</text>}}by seeing how many nests females built in the territories of these different males. Long story short --- ***{{<highlight size="24.0pt" bg="aqua">}}females went NUTS{{</highlight>}}*** {{<text size="24.0pt">}}{{</text>}}for the males with **{{<text size="20.0pt" fg="#ED7D31">}}super-extra-long tails{{</text>}}**.

![Flashcards - EXAM 4 - Sexual Dimorphism Exaggerated ...](flashcards_-_exam_4_-_sexual_dimorphism_.jpeg)

Pretty nuts. So basically evolution is likely to *{{<text size="22.0pt" fg="#ED7D31">}}keep pushing those tails longer{{</text>}}* {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}} ***{{<u size="48.0pt" fg="#ED7D31">}}and longer{{</u>}}*** {{<text size="48.0pt" fg="#ED7D31">}}{{</text>}}because the females thing it's sexy.

Hope you enjoyed this little foray into some more sexy topics.

Lots of love,

Flora

{{<text bg="red">}}PS --- be as awesome as this guy xx{{</text>}}

![Long-tailed Widowbird, display flight | Long-tailed widow ...](long-tailed_widowbird_display_flight_lon.jpeg)