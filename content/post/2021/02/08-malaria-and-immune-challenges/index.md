+++
author = "Flora Judd"
title = "Malaria Unmasked"
date = "2021-02-08"
description = "Exploring the cunning strategies of malaria against the immune system."
tags = ["Malaria","Immune System","Plasmodium falciparum","Parasite","Red Blood Cells","Cerebral Malaria","Transmission Strategies","Infection Mechanisms","Health Risks","Epidemiology"]
categories = ["Health","Immunology","Infectious Diseases","Tropical Medicine"]
image = "free_picture_malaria_parasites_undergo_a.jpeg"
+++

Greetings loved ones, let's take a journey (Katy Perry, 2010)

Today we are going to have a little look at a very sneaky little pathogen called *{{<text size="18.0pt" fg="red">}}Plasmodium falciparum{{</text>}}* {{<text size="18.0pt" fg="red">}}{{</text>}}or as you are more likely to know it ---
 **{{<highlight size="22.0pt" bg="yellow">}}malaria{{</highlight>}}**.

![Free picture: malaria, parasites, undergo, asexual ...](free_picture_malaria_parasites_undergo_a.jpeg)

Whilst some of those look more like puffer fish --- they are in fact **{{<text size="16.0pt">}}cells infected with malaria{{</text>}}**.

Now malaria is a very interesting parasite because it does some very *{{<highlight size="14.0pt" bg="aqua">}}snazzy things{{</highlight>}}* {{<text size="14.0pt">}}{{</text>}}to try and get out of being found out by your
 **immune system**.

So first things first --- the parasite has a whole bunch of **{{<text size="16.0pt">}}different life cycle stages{{</text>}}**. This starts off inside the **{{<text size="14.0pt" fg="red">}}mosquito{{</text>}}** {{<text size="14.0pt" fg="red">}}{{</text>}}(interestingly the mosquito's immune system also isn't so pleased the malaria is there but no one talks about that so much poor mosquitoes )

*yeah nah sympathy is difficult.*

To cut a long story short --- the parasite forms a **{{<highlight size="16.0pt" bg="yellow">}}cyst{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}inside the belly of the mosquito which releases little **{{<text size="20.0pt" fg="#ED7D31">}}motile zygotes{{</text>}}** (zygote is the fancy name for what you get when an
 **egg cell** meets a **sperm cell** ) called *{{<text size="26.0pt" fg="#7030A0" bg="fuchsia">}}sporozoites{{</text>}}*. These migrate to the **{{<text size="18.0pt" fg="#5B9BD5">}}salivary glands{{</text>}}** {{<text size="18.0pt" fg="#5B9BD5">}}{{</text>}}of the mosquito ready to be injected.

Now the mosquito bites and injects these little *{{<text size="20.0pt" fg="#7030A0" bg="fuchsia">}}sporozoites{{</text>}}* {{<text size="20.0pt" fg="#7030A0">}}{{</text>}}into a human bean. This immediately sets off the alarm bells of your immune system which starts freaking out and it's all

![monsters inc on Tumblr](monsters_inc_on_tumblr.gif)

But these little *{{<text size="22.0pt" fg="#7030A0" bg="fuchsia">}}sporozoites{{</text>}}* {{<text size="22.0pt" fg="#7030A0">}}{{</text>}}do not hang around to wait for your immune system to kick in. They head STRAIGHT for your **{{<text size="16.0pt">}}liver{{</text>}}** {{<text size="16.0pt">}}{{</text>}}and can get there in *{{<text size="18.0pt">}}minutes{{</text>}}*.

It is{{<u size="18.0pt">}}RAPID{{</u>}}.

Now what they do is very
 **{{<text size="18.0pt">}}cunning{{</text>}}**.

![It's plan time! | Blue Cloud Triathlon](its_plan_time_blue_cloud_triathlon.png)

They yeet themselves *{{<text size="22.0pt" fg="#ED7D31">}}inside{{</text>}}* {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}the cells of your liver (called **{{<text size="16.0pt" bg="red">}}hepatocytes{{</text>}}** ) and by becoming **{{<highlight size="18.0pt" bg="yellow">}}intracellular{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}they can't be found by most of the components of your immune system. We think there might be some ***{{<text size="20.0pt" fg="#FFC000" bg="black">}}natural killer cells{{</text>}}*** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}(they're one of your immune cell types) in your body that can track the malaria down but it's not fully understood yet.

Now once the parasite is in the liver it starts going through **{{<text size="18.0pt">}}asexual replication{{</text>}}** {{<text size="18.0pt">}}{{</text>}}to make a big ol" bundle of malaria called a *{{<highlight size="24.0pt" fg="#4472C4" bg="aqua">}}schizont{{</highlight>}}* {{<text size="24.0pt" fg="#4472C4">}}{{</text>}}(excellent Scrabble word).

This takes about **{{<text size="16.0pt">}}two weeks{{</text>}}** {{<text size="16.0pt">}}{{</text>}}which is just about how long it takes your body to prepare lots of nice **{{<highlight size="20.0pt" bg="yellow">}}antibodies{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}that can ATTACK the *{{<text size="22.0pt" fg="#7030A0" bg="fuchsia">}}sporozoites{{</text>}}*. Remember that the **{{<text size="14.0pt">}}specific responses{{</text>}}** {{<text size="14.0pt">}}{{</text>}}of your immune system take a bit longer than the more general defence ones (the **{{<text size="18.0pt">}}innate responses{{</text>}}** ) that are always drifting around in your bod.

SO these parasites all exit the liver at once in **{{<text size="14.0pt">}}one big rush{{</text>}}**. Now I bet you're thinking.

 *Good bloody timing bod* --- just came up with all those fantastic antibodies. That malaria parasite won't know what's hit it 

Ummm not so much. Whilst the parasite was chilling in that *{{<highlight size="18.0pt" fg="#0070C0" bg="aqua">}}schizont{{</highlight>}}* {{<text size="18.0pt" fg="#0070C0">}}{{</text>}}it matured into something called a *{{<text size="24.0pt" fg="#385723" bg="lime">}}merozoite{{</text>}}* {{<text size="24.0pt" fg="#385723">}}{{</text>}}which needs *{{<text size="18.0pt">}}TOTALLY DIFFERENT ANTIBODIES{{</text>}}*.

So *none of the antibodies* that your bod has spent the last two weeks making actually work.

{{<highlight bg="yellow">}}Crumbs{{</highlight>}}.

So instead the merozoites move into your{{<text size="16.0pt" bg="red">}}red blood cells{{</text>}}{{<text size="16.0pt">}}{{</text>}}to do their next life cycle stage. Now this is problematic, because your red blood cells are the only cells in your body that can't show other cells in your body that there is a pathogen inside.

All the other cells in your body have little **{{<text size="18.0pt">}}protein complexes{{</text>}}** {{<text size="18.0pt">}}{{</text>}}on the outside that can be used as little advertising signs saying {{<highlight size="16.0pt" bg="aqua">}}PATHOGEN IN HERE --- PLS SEND HELP{{</highlight>}} . Unfortunately
 red blood cells do not have this.

But the reason they don't is because your red blood cells get cleaned up a lot. That's the job of your ***{{<text size="22.0pt">}}spleen{{</text>}}*** {{<text size="22.0pt">}}{{</text>}}(the totally pointless organ that everyone forgets isn't actually so pointless!!). So yay! We have defeated malaria because all those grotty infected red blood cells will get
{{<u>}}swept along to the spleen{{</u>}} and will be {{<u>}}cleaned out{{</u>}}.

Mmmmmm not so much.

The parasite is smart *again* and instead puts some delicious{{<u size="18.0pt" bg="silver">}}membrane proteins{{</u>}}{{<text size="18.0pt">}}{{</text>}}on the outside of the red blood cells that make them **{{<text size="16.0pt">}}really sticky{{</text>}}**. They stick to each other, they stick to the walls of the blood vessels, they stick to
 *EVERYTHING*.

So this means the blood cells ***{{<text size="20.0pt" fg="red">}}can't get to the spleen{{</text>}}*** {{<text size="20.0pt" fg="red">}}{{</text>}}and the parasite can happily sit there doing its thang.

This is a big problem, because one of this parasite's favourite places to be is in the **brain**. When it starts causing these blockages you can get **{{<text size="16.0pt">}}oxygen starvation{{</text>}}** in the brain tissue. And that's called **{{<text size="20.0pt" fg="red">}}cerebral malaria{{</text>}}**.

Not nice. *Not nice at all*.

BUT there's a good thing about these{{<u size="16.0pt" bg="silver">}}membrane proteins{{</u>}}. Your immune system can use them instead of the protein complexes used by other cells. These membrane proteins are essentially
 their own little advertising sign saying {{<highlight size="16.0pt" bg="aqua">}}PATHOGEN IN HERE --- PLS SEND HELP{{</highlight>}} . Yayy!

**{{<text size="14.0pt">}}Antibodies can bind to those proteins{{</text>}}** {{<text size="14.0pt">}}{{</text>}}and pack them off the spleen to be cleaned. What a win.

Hmmmmmmm not quite.

You see these sneaky little parasites actually have 50-60 different types of membrane protein that they can use 

50-60 varieties did you say?...

![image](8333e2e5cf317e52.png)

If anyone would like to join my new conspiracy theory group that Heinz is secretly spreading malaria in tins of baked beans let me know and I"ll add you to our WhatsApp group chat.

Anyways --- they can change this protein every **{{<text size="18.0pt" fg="red">}}7-10 days{{</text>}}** so just as your body makes a new antibody **{{<text size="90.0pt">}}BAM{{</text>}}** {{<text size="90.0pt">}}{{</text>}}
the parasite changes.
 *{{<highlight size="16.0pt" bg="yellow">}}This is not ideal{{</highlight>}}*. And it means that whilst your immune system can control the number of parasites in the body, total elimination is extremely
 difficult 

Sorry there's not really a happy ending on this one If
 *Cheryl Cole* survived it --- it can't be that bad?

Hope you enjoyed that ***dramatic*** foray into the world of
 *Plasmodium falciparum*.

See you next time.

Flora xxx