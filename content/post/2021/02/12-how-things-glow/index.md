+++
author = "Flora Judd"
title = "The Glow Factor"
date = "2021-02-12"
description = "Discover the magic of bioluminescence and how enzymes create light."
tags = ["Bioluminescence","Enzymes","Luciferase","Chemical Reactions","Light Production","Antioxidants","Evolution","Symbiotic Relationships","Biological Mechanisms","Chemical Ecology"]
categories = ["Biology","Science","Education","Chemical Biology"]
image = "image_result_for_bioluminescence_gif.gif"
+++

Helloooo you wonderful bean,

Are you ready to find out just
 *how things glow* !?

I knew you were.

Excellent. Let's get cracking.

So **{{<highlight size="16.0pt" bg="yellow">}}bioluminescence{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}is the result of a *{{<text size="16.0pt" fg="red">}}chemical reaction{{</text>}}*. Now this chemical reaction{{<highlight-u size="18.0pt" bg="aqua">}}so high in energy{{</highlight-u>}}{{<text size="18.0pt">}}{{</text>}}that it produces a **{{<text size="18.0pt">}}photon{{</text>}}** {{<text size="18.0pt">}}{{</text>}}i.e. light.

Now to take you back to the depths of any biology you once studied we're gonna talk about **{{<text size="16.0pt" fg="#7030A0" bg="fuchsia">}}enzymes{{</text>}}**.

**{{<text size="14.0pt" fg="#7030A0" bg="fuchsia">}}Enzymes{{</text>}}** {{<text size="14.0pt" fg="#7030A0">}}{{</text>}}are *{{<text size="16.0pt" fg="#7030A0">}}proteins{{</text>}}* {{<text size="16.0pt" fg="#7030A0">}}{{</text>}}in your body that act as little **{{<text size="22.0pt" fg="#00B0F0">}}catalysts{{</text>}}** , speeding up all of the chemical reactions that take place in your body.

Just to put in perspective
 *{{<text size="16.0pt">}}how important{{</text>}}* {{<text size="16.0pt">}}{{</text>}}these little guys are, there's a reaction in your body that produces **{{<text size="16.0pt" fg="red">}}haemoglobin{{</text>}}** {{<text size="16.0pt" fg="red">}}{{</text>}}(the lovely molecule we use to transport oxygen around the body) and without enzymes it would take about **{{<text size="36.0pt" fg="#ED7D31">}}2.3 billion years{{</text>}}**.

Yeah --- that's about *{{<highlight bg="yellow">}}half of the age of the Earth{{</highlight>}}*.
 **Enzymes** = **muy** **importante**.

The other fab thing about enzymes is that they are **{{<text size="14.0pt">}}super specific{{</text>}}**. They can only catalyse *{{<text size="14.0pt">}}one{{</text>}}* {{<text size="14.0pt">}}{{</text>}}(or maybe at a push two) *{{<text size="14.0pt">}}reactions{{</text>}}*. This is why they are often talked about in the context of the **{{<text size="18.0pt" fg="red">}}lock and key model{{</text>}}**.

The {{<text bg="fuchsia">}}enzyme{{</text>}}is the {{<text bg="fuchsia">}}lock{{</text>}}
and can only be used by a very specific {{<highlight bg="aqua">}}substrate{{</highlight>}}molecule ({{<highlight bg="aqua">}}key{{</highlight>}}). The substrate locks into a special part of the enzyme and forms the
{{<highlight bg="yellow">}}enzyme-substrate complex{{</highlight>}}. The enzyme *does its magic* and the *substrate products* yeet themselves out of there.

If that didn't make any sense --- take a look at this **{{<text size="14.0pt" fg="red">}}handy GIF{{</text>}}** :

![Image result for enzymes gif](image_result_for_enzymes_gif.gif)


Now in this example the enzyme is called **{{<text size="20.0pt" fg="red" bg="black">}}luciferase{{</text>}}** {{<text size="18.0pt" fg="red">}}{{</text>}}and the substrate is called **{{<text size="20.0pt" fg="red" bg="black">}}luciferin{{</text>}}** {{<text fg="red">}}{{</text>}}(pretty easy to remember eh?) from the Latin "{{<highlight bg="yellow">}}lucifer"{{</highlight>}}meaning "{{<highlight bg="yellow">}}light{{</highlight>}}-{{<highlight bg="yellow">}}bearer"{{</highlight>}}.

This works because when these bad boys (enzyme and substrate) come together, the molecule at the end is **{{<text size="18.0pt" fg="#00B0F0">}}so high in energy{{</text>}}** that it can release that **{{<highlight size="16.0pt" bg="yellow">}}photon{{</highlight>}}**.


![Image result for luciferin gif](image_result_for_luciferin_gif.jpeg)

Now this can either be done by the{{<text size="16.0pt" fg="#ED7D31">}}organism itself{{</text>}} *or* it can be done by{{<text size="16.0pt" fg="#ED7D31">}}friendly symbiotic bacteria{{</text>}}. In exchange for their light-producing abilities the bacteria get food and a safe place
 to live --- {{<text bg="fuchsia">}}EVERYBODY WINS{{</text>}}.

But there is a big question here.

*{{<text size="20.0pt">}}How do you evolve the ability to glow?{{</text>}}*
**
![Image result for bioluminescence gif](image_result_for_bioluminescence_gif.gif)
**
Like what? How does that happen?

Something had to do it for the first time once. And we think its evolved over **{{<text size="18.0pt" fg="#0070C0">}}40 different times{{</text>}}** {{<text size="18.0pt" fg="#0070C0">}}{{</text>}}throughout evolutionary history. That's a lot. In fact some species (like the lovely anglerfish) **{{<text size="24.0pt">}}have evolved it twice{{</text>}}**. They have not one but{{<highlight size="18.0pt" bg="yellow">}}TWO
 *different light emitting systems*{{</highlight>}}.

Their lure that sticks out over its head is produced by some lovely **{{<text size="14.0pt" fg="#ED7D31">}}symbiotic bacteria{{</text>}}** {{<text size="14.0pt" fg="#ED7D31">}}{{</text>}}and it has a chin barbel that uses some **{{<text size="16.0pt" fg="#70AD47">}}slightly funky chemistry{{</text>}}** {{<text size="16.0pt" fg="#70AD47">}}{{</text>}}we don't fully understand.

{{<text size="18.0pt">}}SO HOW!?{{</text>}}

Fear not. I have the answers or at least what smart people think the answers are 

Now --- **{{<text size="16.0pt">}}luciferins{{</text>}}** {{<text size="16.0pt">}}{{</text>}}are actually really good **{{<text size="18.0pt" fg="#7030A0" bg="fuchsia">}}antioxidants{{</text>}}**. That term might ring a bell. People talk about them
 a lot, but usually in the context of {{<text fg="#0070C0">}}blueberries{{</text>}}
and {{<text fg="#92D050">}}health supplements{{</text>}}instead of in weird deep sea glowing fish weirdos.

Basically what antioxidants do is they neutralise
 **unstable molecules** called *{{<highlight size="18.0pt" bg="yellow">}}free radicals{{</highlight>}}*. These molecules are super unstable
 because they have an{{<u size="16.0pt" fg="#ED7D31">}}unpaired electron{{</u>}}{{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}that wants to react with anything it bumps into.

You might have heard about these free radicals in the context of **{{<text size="14.0pt">}}UV light{{</text>}}**. Rays of UV light can produce **{{<text size="14.0pt">}}free radicals{{</text>}}** {{<text size="14.0pt">}}{{</text>}}inside your body. That's why it's so important to wear your
 **sun cream** kids --- if those free radicals find a nice skin cell to wreak havoc in --- that's
 *skin cancer*.

BUT.

If you live really, really deep in the sea the *{{<text size="16.0pt">}}UV light won't reach you{{</text>}}* so you don't really need antioxidants maybe that means that if you live in the dark it's more
 useful to use your luciferin to **{{<text size="14.0pt">}}create light{{</text>}}** {{<text size="14.0pt">}}*instead* *of using it as an antioxidant*{{</text>}}.

**{{<text size="20.0pt">}}ISN"T THAT SO COOL?!{{</text>}}**

Hope you enjoyed. Don't forget to stay fabulous and party like you're a comb jelly.

![Image result for bioluminescence gif](image_result_for_bioluminescence_gif.gif)

Lots of love,

Flora xxxx