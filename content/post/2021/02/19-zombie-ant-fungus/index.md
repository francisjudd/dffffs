+++
author = "Flora Judd"
title = "Zombie Ant Fungus"
date = "2021-02-19"
description = "Exploring the bizarre life cycle of the zombie ant fungus."
tags = ["Zombie Ant Fungus","Entomopathogenic Fungus","Camponotus Leonardi","Spore Dispersal","Fungal Species","Tropical Forests","Coercive Behavior","Fruiting Body Formation","Parasite-Host Interactions","Ecological Impact"]
categories = ["Nature","Fungi","Wildlife","Parasitology"]
image = "ophiocordyceps_unilateralis_the_zombie_a.jpeg"
+++

Hello and welcome to your final *{{<text size="18.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* of the week.

Today we are looking at a very cool fungus. It is known as the
 **{{<text size="18.0pt" fg="red">}}ZOMBIE ANT FUNGUS{{</text>}}**.

Get ready to party.

![Dancing Ant | 6legs2many](dancing_ant_6legs2many.gif)

I know. And yes, it is as cool as it sounds.

Now **{{<text size="18.0pt" fg="#00B050">}}fungi{{</text>}}** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}are a *pretty weird* bunch of organisms and include the **{{<highlight size="16.0pt" bg="yellow">}}yeasts{{</highlight>}}** ,
 **{{<highlight size="16.0pt" bg="yellow">}}molds{{</highlight>}}** {{<highlight size="16.0pt" bg="yellow">}}{{</highlight>}}and **{{<highlight size="16.0pt" bg="yellow">}}mushrooms{{</highlight>}}**. They are actually
{{<u size="18.0pt">}}more closely related to animals than they are to plants{{</u>}}{{<text size="18.0pt">}}{{</text>}}--- they ***{{<text size="18.0pt" fg="#ED7D31">}}don't have chloroplasts{{</text>}}*** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}and they are **{{<text size="18.0pt" fg="#4472C4">}}heterotrophic{{</text>}}** {{<text size="18.0pt" fg="#4472C4">}}{{</text>}}(meaning they can't make their own food like autotrophs --- aka plants and algae --- but have to eat other things instead).

Now we"ve ***{{<highlight size="22.0pt" bg="yellow">}}only classified 120,000{{</highlight>}}*** {{<text size="22.0pt">}}{{</text>}}(lol "only") but we estimate that there are about
{{<text size="26.0pt" bg="red">}}3.8 MILLION FUNGAL SPECIES OUT THERE{{</text>}}.

Like what the *{{<text size="12.0pt">}}diddly{{</text>}}* -fuck.

SO anyways --- I know you're dying to know what on earth a zombie ant fungus is.

Well it is a species of ***{{<text size="22.0pt" fg="#00B050">}}entomopathogenic{{</text>}}*** {{<text size="22.0pt" fg="red">}}{{</text>}}fungus. That basically means it can **{{<text size="14.0pt" bg="lime">}}act as a parasite{{</text>}}** {{<text size="14.0pt">}}{{</text>}}of {{<u size="18.0pt" bg="lime">}}insects{{</u>}}{{<text size="18.0pt">}}{{</text>}}that severely **{{<text size="20.0pt" bg="lime">}}disables{{</text>}}** {{<text size="20.0pt" bg="lime">}}**or** **kills**{{</text>}}them. Oopsies.

It lives in tropical forests and its favourite ant to infect is the species
 *Camponotus leonardi* this little guy.

![image](366d683ecfe1f4b9.png)

*{{<text size="16.0pt">}}NOT THAT KIND OF LEONARDI.{{</text>}}*

![image](5b978e489cf716da.png)

That's better.

Now --- what it does is it attaches its **{{<highlight size="16.0pt" bg="yellow">}}spores{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}to the **{{<highlight size="20.0pt" bg="yellow">}}exoskeletons{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}of these ants as they make their way along the forest floor. Once they latch on they use a whole bunch of
 **{{<text size="22.0pt" fg="#7030A0">}}digestive enzymes{{</text>}}** {{<text size="22.0pt" fg="#7030A0">}}{{</text>}}and ***{{<text size="20.0pt" fg="#4472C4">}}mechanical pressure{{</text>}}*** {{<text size="20.0pt" fg="#4472C4">}}{{</text>}}to {{<u size="20.0pt">}}FORCE THEIR WAY IN{{</u>}}.

Once the fungus is inside the ant it can **{{<highlight size="22.0pt" bg="yellow">}}travel to the brain{{</highlight>}}** {{<text size="22.0pt">}}{{</text>}}and is able to ***{{<text size="20.0pt" fg="red">}}take over the central nervous system{{</text>}}*** {{<text size="20.0pt" fg="red">}}{{</text>}}(what kind of sci-fi crap is this) of the ant through the release of various compounds. If you want to be really fancy --- the two best contenders we have for those brain-altering chemicals are
 **{{<text size="22.0pt" bg="fuchsia">}}sphingosine{{</text>}}** {{<text size="22.0pt" bg="fuchsia">}}{{</text>}}and **{{<text size="22.0pt" bg="fuchsia">}}guanidinobutyric acid{{</text>}}** {{<text size="22.0pt" bg="fuchsia">}}{{</text>}}(GBA).

Now these ants like to dwell at the tops of trees. But the fungus doesn't like that. So it o begins to
 **{{<text size="20.0pt" fg="#ED7D31">}}alter the behaviour{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}of the ant. This results in ***{{<highlight size="20.0pt" bg="yellow">}}full-body convulsions{{</highlight>}}*** {{<text size="20.0pt">}}{{</text>}}that cause the ant to fall from the tree canopy to the forest floor.

AS IF THAT ISN"T ENOUGH --- the fungus decides that *actually it doesn't like the forest floor* 
 *so much* and so it **{{<text size="18.0pt">}}forces the ant to climb the stem of a plant{{</text>}}** and
 **{{<text size="16.0pt" fg="#92D050">}}grab onto a leaf vein{{</text>}}** {{<text size="16.0pt" fg="#92D050">}}{{</text>}}that is usually about ***{{<text size="18.0pt" fg="#00B050">}}25cm{{</text>}}*** off the floor.

Now scientists have done studies and found that 25cm is actually the
 **{{<highlight size="20.0pt" bg="yellow">}}optimal height{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}for spore dispersal in these forested environments so that is **{{<text size="18.0pt" bg="lime">}}one damn smart fungus{{</text>}}**.

The fungus then has a great time as it *{{<text size="16.0pt">}}slowly eats the ant from the inside{{</text>}}*. Ew. Not nice way to go. But you"d think the ant would fall when it died. NOT SO MUCH.

The fungus forces the ant to **{{<text size="22.0pt" fg="#ED7D31">}}bite onto that leaf
 *SO HARD*{{</text>}}** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}that it
 *{{<text size="18.0pt">}}remains attached to the plant{{</text>}}* {{<text size="18.0pt">}}{{</text>}}even {{<u size="14.0pt">}}when it dies{{</u>}}. The fungus even releases
 **{{<highlight size="18.0pt" bg="yellow">}}antimicrobials{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}to make sure that the ant is not invaded by other microorganisms.

The fungus then grows its **{{<text size="22.0pt" fg="red">}}fruiting body{{</text>}}** {{<text size="22.0pt" fg="red">}}{{</text>}}(the bit that makes spores --- the part you eat of a mushroom) **{{<highlight size="22.0pt" bg="yellow">}}OUT OF THE ANT"S HEAD{{</highlight>}}** and then the spores can be released into the forest.

!['Planet Earth' Got Zombie Cordyceps Fungus Wrong, Study ...](planet_earth_got_zombie_cordyceps_fungus.gif)

![Ophiocordyceps unilateralis: The Zombie Ant Fungus ...](ophiocordyceps_unilateralis_the_zombie_a.jpeg)


*{{<text size="24.0pt">}}WHAT THE FUCK.{{</text>}}*

*Damn nature --- you scary.*

Hope you enjoyed your fact of the day and aren't too traumatised :)))))))

Lots of love,

Flora