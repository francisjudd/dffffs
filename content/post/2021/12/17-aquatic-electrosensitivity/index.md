+++
author = "Flora Judd"
title = "Electric Sense in Skates"
date = "2021-12-17"
description = "Discover how skates detect predators even as embryos."
tags = ["Electric Fields","Clearnose Skate","Predator Awareness","Aquatic Vertebrates","Embryonic Development","Electrosense","Survival Mechanisms","Marine Biology","Sensory Ecology","Adaptations"]
categories = ["Marine Biology","Aquatic Life","Sensory Biology","Research"]
image = "clearnose_skate_-_alchetron_the_free_soc.jpeg"
+++

Hello and welcome to a
 *{{<text size="18.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,
You know what's really cool? Lots of aquatic vertebrates can detect electric fields. They can **{{<highlight size="24.0pt" bg="yellow">}}FEEL ELECTRICITY{{</highlight>}}**. And we're not just talking fish here --- we're talking platypus and frogs too.

This is usually for the purpose of detecting stuff --- whether that's prey, predators or your fellow pals.

![Quantum&#39; sensor mimics shark&#39;s ability to detect tiny electric fields](quantum39_sensor_mimics_shark39s_ability.jpeg)


There is a LOT of awesome stuff in the science of this but given that I"m supposed to be doing revision I"m gonna keep it brief.

Let's talk about **{{<text size="20.0pt" fg="#00B0F0">}}detecting predators{{</text>}}**. This is the *{{<text size="18.0pt" fg="#0070C0">}}clearnose skate{{</text>}}*. He's a pretty cool dude.

![Clearnose skate - Alchetron, The Free Social Encyclopedia](clearnose_skate_-_alchetron_the_free_soc.jpeg)
( *seriously look how clear that nose is tho* )

He can detect changes in the
 **{{<text size="24.0pt" fg="#FFC000">}}electric field{{</text>}}** around him and actually these guys are *{{<text size="22.0pt" fg="#70AD47">}}most sensitive{{</text>}}* {{<text size="22.0pt" fg="#70AD47">}}{{</text>}}to the frequencies of electricity that are produced by **{{<text size="26.0pt" fg="#F4B183">}}predators of their eggs{{</text>}}**.

What is even cooler is that the ability to detect electric fields (the electrosense) starts kicking in while these skates are still egg-encapsulated embryos. So these little dudes can detect when a predator
 is nearby **{{<text size="24.0pt" fg="#FFF2CC" bg="fuchsia">}}EVEN WHEN THEY ARE STILL INSIDE THEIR EGG CASES{{</text>}}** {{<text fg="#FFF2CC" bg="fuchsia">}}.{{</text>}}{{<text fg="#FFF2CC">}}{{</text>}}

![Little skate embryo in egg case - YouTube](little_skate_embryo_in_egg_case_-_youtub.jpeg)

That is **totally mad**.
But I know what you're thinking. Damn useless to be able to detect a predator if you're **{{<text size="20.0pt" fg="#A9D18E">}}inside an egg{{</text>}}**. There's not a lot you can do to escape a predator if you are literally ***{{<text size="24.0pt" fg="#FFC000">}}trapped inside a bag{{</text>}}***.

Oh my dear this is where it gets **{{<highlight size="22.0pt" bg="aqua">}}cool{{</highlight>}}**.

When the baby embryo skates detect a predator they **{{<text size="28.0pt" fg="#BDD7EE" bg="navy">}}STOP BREATHING{{</text>}}** {{<text size="28.0pt" fg="#BDD7EE">}}{{</text>}}so that the predator is much less likely to find it.

![Clearnose skate - Alchetron, The Free Social Encyclopedia](clearnose_skate_-_alchetron_the_free_soc.jpeg)

***{{<text size="26.0pt" bg="fuchsia">}}WHHAAAAAT.{{</text>}}*** ***{{<text size="26.0pt">}}{{</text>}}***

That means that the *{{<text size="20.0pt" fg="#C55A11">}}tiny electric pulses{{</text>}}* {{<text size="20.0pt" fg="#C55A11">}}{{</text>}}produced by its own heartbeat stop so that the **{{<text size="24.0pt" fg="#FFC000">}}other electrosensing animals{{</text>}}** {{<text size="24.0pt" fg="#FFC000">}}{{</text>}}are gonna have a tough old time trying to find it.

Isn't that insane?!?!

Lots of love,

Flora xx

![Funny Animal GIFs To Make Your Laugh (16 GIFs) --- Page 2 of ...](funny_animal_gifs_to_make_your_laugh_16_.gif)