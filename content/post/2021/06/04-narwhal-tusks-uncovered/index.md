+++
author = "Flora Judd"
title = "The Truth About Narwhal Tusks"
date = "2021-06-04"
description = "Exploring the mysteries behind narwhal tusks and their surprising purpose."
tags = ["Narwhals","Tusks","Sexual Selection","Marine Life","Arctic Adaptations","Evolutionary Functions","Sensory Organs","Mysterious Traits","Reproductive Strategies","Animal Behavior"]
categories = ["Marine Biology","Wildlife","Nature","Cetology"]
image = "narwhal_facts_and_photos.jpeg"
+++

Hello and welcome to another
 *{{<text size="18.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

Hasn't it been a little while!? Never fear. This one is a good "un.
Today we have a little look at **{{<text size="22.0pt" fg="#00B0F0">}}narwhal tusks{{</text>}}**.

For a long time scientists have just been hella baffled by what these ***{{<text size="22.0pt" fg="#00EEE8">}}ginormous javelins{{</text>}}*** {{<text size="22.0pt" fg="#00EEE8">}}{{</text>}}sticking out of the faces of these whales are for. This is partly because narwhals spend a lot of their lives hidden underneath the Arctic ice.

![Narwhals GIFs - Find & Share on GIPHY](narwhals_gifs_-_find_share_on_giphy.gif)

So what is that tusk? It's actually an **{{<text size="18.0pt" fg="#FF5FF8">}}elongated upper left canine{{</text>}}**. And when I say elongated
{{<text bg="fuchsia">}}I"m not kidding{{</text>}}. It's usually about ***{{<text size="18.0pt" fg="#FAC5F0">}}8 feet long{{</text>}}***.{{<text size="10.5pt">}}{{</text>}}

People thought that maybe it was an *{{<text size="18.0pt" fg="#92D050">}}environmental sensor{{</text>}}* , that it was used for **{{<text size="18.0pt" fg="#A1F8DF">}}opening breathing holes{{</text>}}** in sea ice, as a **{{<text size="18.0pt" fg="#ED7D31" bg="black">}}weapon{{</text>}}** {{<text size="18.0pt" fg="red">}}{{</text>}}or that it was used for ***{{<text size="24.0pt" fg="#C00000">}}hunting{{</text>}}*** , but the weird thing is that{{<highlight-u size="28.0pt" bg="yellow">}}not all individuals have one{{</highlight-u>}}.

Females typically don't have them, so the tusks cannot perform some aspect of narwhal-life that is **{{<text size="16.0pt" fg="#ED7D31">}}totally essential{{</text>}}** , because females actually typically live
 *longer* than males.

![Rare Audio of Narwhal Buzzes, Clicks and Whistles Captured ...](rare_audio_of_narwhal_buzzes_clicks_and_.jpeg)

A lot of studies have drawn different conclusions. One drone recorded narwhals using it to ***{{<text size="18.0pt" fg="#FFC000">}}strike and stun{{</text>}}*** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}small Arctic cod, before **{{<text size="22.0pt" fg="#EB9000">}}eating{{</text>}}** {{<text size="22.0pt" fg="#EB9000">}}{{</text>}}them.

(you can see the footage here and it's badass)

<a href="https://youtu.be/j_5jIBHcmpQ">![image](9f4c8f7c999ad572.png)</a>
{{< youtube j_5jIBHcmpQ >}}

But again, this cannot be their ***{{<text size="18.0pt" fg="#FF5A2B">}}primary{{</text>}}*** **{{<text size="18.0pt" fg="#FF5A2B">}}function{{</text>}}** , otherwise individuals that didn't have them would fare so much less well than
 their tusked-compatriots.

***{{<text size="16.0pt" fg="#00EEE8">}}THIS ONE THAT WASHED UP ON A BEACH EVEN HAD TWO{{</text>}}***
![image](968aa8af82202da5.png)

The tusks have also been analysed and shown to be **{{<highlight size="18.0pt" bg="yellow">}}highly innervated sensory organs{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}with *{{<text size="16.0pt" fg="#FFC000">}}millions of nerve endings in them{{</text>}}*. So they could be used to **{{<text size="24.0pt" fg="#EB9000">}}detect certain stimuli{{</text>}}** {{<text size="24.0pt" fg="#ED7D31">}}{{</text>}}happening in the ocean around them. Some have also postulated (excellent word) that males may ***{{<text size="18.0pt" fg="#FF5A2B">}}rub their tusks together{{</text>}}*** {{<text size="18.0pt">}}{{</text>}}to communicate certain traits of the water they have travelled through.


![Narwhal | Marine Wiki | Fandom](narwhal_marine_wiki_fandom.jpeg)
*{{<text fg="#00F9F1" bg="blue">}}Mmmm yes. Water. Thanks Barry.{{</text>}}* *{{<text fg="#00F9F1">}}{{</text>}}*

But none of these explanations
 *quite* cut the mustard.

The leading evidence now suggests that actually they are an example of **{{<text size="22.0pt" bg="fuchsia">}}runaway sexual selection{{</text>}}**. Like the feathers of a peacock, or the crazy antlers of a stag --- evolution has just got a bit overexcited.

![Deer Genetics NZ stag Prometheus snagged a national top price of $95,000.](deer_genetics_nz_stag_prometheus_snagged.jpeg)
*I mean look at it --- it's just dumb.*

You can understand how this might happen. A narwhal with a longer tusk is likely to be in **{{<text size="16.0pt" fg="#92D050">}}better condition{{</text>}}** , after all it's grown this *{{<text size="18.0pt" fg="#00B050">}}mighty fine-looking tusk{{</text>}}* {{<text size="18.0pt" fg="#00B050">}}{{</text>}}and still survived with it sticking out of its face, and so is probably going to have better babies. ***{{<text size="14.0pt" fg="#00A122">}}You should have sex with that narwhal{{</text>}}***.

That just leads to a **{{<text size="20.0pt" fg="#FFC000">}}relentless cycle{{</text>}}**. Those females breed with long-tusked males, producing daughters that
 find long tusks really sexy and sons that have really long tusks. You can see how it can run away a bit.

![Narwhal, facts and photos](narwhal_facts_and_photos.jpeg)

We know this because the tusks **{{<text size="22.0pt" fg="#FAC5F0">}}vary in size{{</text>}}** {{<text size="22.0pt">}}{{</text>}}way more than you would expect for a functional trait. Sexual traits are very sensitive to *{{<text size="20.0pt" fg="#D86CF7">}}nutrient{{</text>}}* {{<text size="20.0pt">}}{{</text>}}and *{{<text size="20.0pt" fg="#D86CF7">}}body{{</text>}}* {{<text size="20.0pt">}}*{{<text fg="#D86CF7">}}condition{{</text>}}*{{</text>}}, meaning that only the biggest and strongest individuals can afford to produce such mahoosive traits. The tusks can vary from **{{<text size="22.0pt" fg="#9D5FDF">}}1.5 feet{{</text>}}** {{<text size="22.0pt">}}{{</text>}}to **{{<text size="22.0pt" fg="#9D5FDF">}}8.2 feet{{</text>}}** {{<text size="22.0pt">}}{{</text>}}long, whereas the fluke (the tail) only varies between about 1.5 feet to 3 feet long.

Hope you enjoyed a little foray into the lives of our **{{<highlight size="20.0pt" bg="aqua">}}unicornly cousins{{</highlight>}}**.

![Narwhal GIF by Nat Geo Wild - Find & Share on GIPHY](narwhal_gif_by_nat_geo_wild_-_find_share.gif)
(seriously though look how weird they are)

Love Flora xxx

And here's a GIF of an animal falling over

![Love That Max : Special needs motherhood, pretty much ...](love_that_max_special_needs_motherhood_p.gif)