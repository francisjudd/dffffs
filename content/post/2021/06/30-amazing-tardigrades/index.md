+++
author = "Flora Judd"
title = "Incredible Tardigrades"
date = "2021-06-30"
description = "Discover the astonishing resilience and adaptability of tardigrades."
tags = ["Tardigrades","Resilience","Extreme Conditions","Metabolism","IDPs","Species Diversity","Adaptation","Vitrification","Evolution","Survival Strategies"]
categories = ["Nature","Science","Biology","Extreme Biology"]
image = "tardigrades_the_species_that_will_live_t.jpeg"
+++

Hello and welcome to another *{{<text size="20.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

Today we are going to have a look at some very cool little beasts called
 **{{<highlight size="26.0pt" bg="yellow">}}tardigrades{{</highlight>}}** (for you Chris). Also known as
 **{{<highlight size="20.0pt" bg="aqua">}}water bears{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}or **{{<text size="18.0pt" bg="lime">}}moss piglets{{</text>}}** {{<text size="18.0pt">}}{{</text>}}and they look like this:

![Tardigrades --- indestructible animals | DinoAnimals.com](tardigrades_indestructible_animals_dinoa.jpeg)

These bad boys are found in a part of the evolutionary tree called the
 ***{{<text size="24.0pt" fg="#FFC000">}}Ecdysozoa{{</text>}}*** {{<text size="24.0pt" fg="#FFC000">}}{{</text>}}(try saying that 5 times without stopping). This is a group that includes two large phyla which are your
 **{{<text size="20.0pt" fg="#ED7D31">}}arthropods{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}(invertebrates that have an exoskeleton) **{{<text size="22.0pt" fg="#C55A11">}}nematodes{{</text>}}** {{<text size="22.0pt" fg="#C55A11">}}{{</text>}}(roundworms). They also include a lot of smaller ones like *{{<text size="14.0pt" fg="#00B0F0">}}penis worms{{</text>}}* {{<text size="14.0pt" fg="#00B0F0">}}{{</text>}}(yes they are real and yes you should Google them) and these lovely tardigrades.

![image](b14ac1d2fcbcd11e.png)

There are about **{{<text size="20.0pt" fg="red">}}1,300 different species{{</text>}}** {{<text size="20.0pt" fg="red">}}{{</text>}}that we know about and the earliest ones that we know about are from about
 **{{<text size="20.0pt" bg="red">}}145 million years ago{{</text>}}** , trapped in amber. These ones look pretty much identical to the ones we see know so were probably around a whole lot before that. They split off from their most
 recent relatives over *{{<text size="18.0pt" fg="#C00000">}}500 million years ago{{</text>}}*. Just saying.

![Tardigrades: The species that will live to the end of the ...](tardigrades_the_species_that_will_live_t.jpeg)

They have evolved these little **{{<text size="16.0pt" fg="#70AD47">}}stumpy, non-jointed legs{{</text>}}** {{<text size="16.0pt" fg="#70AD47">}}{{</text>}}that they use to aid their movement and live mostly in moss. They are incredibly abundant and have been found
 *{{<text size="36.0pt" bg="fuchsia">}}everywhere in Earth's biosphere{{</text>}}* from mountains to the deep sea to mud volcanoes to thermal vents and even all the way to the Antarctic.

![Scientists Identify Gene That Protects Tardigrades From ...](scientists_identify_gene_that_protects_t.gif)

You can also find them in **{{<text size="16.0pt" fg="#0070C0">}}your garden{{</text>}}** {{<text size="16.0pt" fg="#0070C0">}}{{</text>}}(they really like moss --- especially moss on roofs) so if you have a pot of water to put some dry moss into and a magnifying glass you can probably find some yourself.

They are one of (if not) the most ***{{<text size="18.0pt" fg="yellow" bg="black">}}resilient animals{{</text>}}*** {{<text size="18.0pt" fg="yellow">}}{{</text>}}that we know about and they can survive exposure to a huge range of extreme conditions.

They survive temperatures near to **{{<highlight size="20.0pt" bg="aqua">}}absolute zero{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}(a bloomin -272 degrees C) and temperatures up to **{{<highlight size="22.0pt" bg="yellow">}}150 degrees C{{</highlight>}}** {{<text size="22.0pt">}}{{</text>}}for several minutes and pressures that are about ***{{<text size="24.0pt" fg="#ED7D31">}}six times greater{{</text>}}*** {{<text size="24.0pt" fg="#ED7D31">}}{{</text>}}than those found in the deepest ocean trenches. They are totally chilled with being shot with **{{<text size="24.0pt" bg="red">}}ionizing radiation{{</text>}}** {{<text size="24.0pt">}}{{</text>}}at doses hundreds of
 times higher than the lethal dose for a human.

They have survived ***{{<text size="26.0pt" fg="#DAE3F3" bg="navy">}}exposure to outer space{{</text>}}*** {{<text size="26.0pt" fg="#DAE3F3">}}{{</text>}}for God's sake --- these guys don't fuck around.

![7 Incredible Things You Should Know About The Tardigrades](7_incredible_things_you_should_know_abou.jpeg)

They aren't considered to be extremophiles because they don't
 *thrive* in these conditions, but they are able to *endure* them.

But how do they do it?

They can **{{<text size="24.0pt" fg="#00B050">}}suspend their metabolisms{{</text>}}**.

*{{<text size="12.0pt" fg="#7030A0">}}HOW COOL IS THAT?!{{</text>}}*

In this state, their metabolism reaches less than **{{<text size="24.0pt" fg="#E7E6E6" bg="purple">}}0.01% of its normal rate{{</text>}}** {{<text size="24.0pt" fg="#E7E6E6">}}{{</text>}}and their water content can decrease to ***{{<text size="22.0pt" fg="#E7E6E6" bg="teal">}}only 1%{{</text>}}*** {{<text size="22.0pt" fg="#E7E6E6">}}{{</text>}}of what it would be normally. In this state they are called tuns.

They can go without food or water **{{<highlight size="26.0pt" bg="yellow">}}FOR THIRTY YEARS{{</highlight>}}** , only to later rehydrate, forage and reproduce.

![Digestion - Tardigrades](digestion_-_tardigrades.png)

So how do they stay desiccated for so long? Why can't we all do that?

Well, we think that it's to do with ***{{<text size="20.0pt" fg="red">}}intrinsically disordered proteins{{</text>}}*** {{<text size="20.0pt" fg="red">}}{{</text>}}(IDPs) which they make a lot more of when they are being dried out. In fact, three of them have been found that are only seen in tardigrades and are therefore called
 **{{<text size="24.0pt" fg="#C55A11">}}tardigrade specific proteins{{</text>}}** {{<text size="24.0pt" fg="#C55A11">}}{{</text>}}(TDPs). Us biologists are really imaginative.

![Tardigrade - Wikipedia](tardigrade_-_wikipedia.jpeg)

We think these TDPs work by **{{<text size="20.0pt" fg="#00B0F0">}}maintaining the structure of all the cell membranes{{</text>}}** {{<text size="20.0pt" fg="#00B0F0">}}{{</text>}}and therefore preventing them from becoming damaged when the tardigrade starts to rehydrate.

What is also super cool is that these TDPs are **{{<highlight size="20.0pt" bg="aqua">}}super hydrophilic{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}and so are actually involved in what's called a ***{{<text size="24.0pt" fg="#0070C0">}}vitrification mechanism{{</text>}}*** {{<text size="24.0pt" fg="#0070C0">}}{{</text>}}where a glass-like matrix forms inside the cell to protect the contents during desiccation.

ISN"T THAT SO COOL!?

Hope you enjoyed.

All my nerdy love,

Flora

Here's your animal falling over GIF

![Funny animal gifs - part 202 (10 gifs) | Amazing Creatures](funny_animal_gifs_-_part_202_10_gifs_ama.gif)