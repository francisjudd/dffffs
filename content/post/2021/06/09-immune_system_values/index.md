+++
author = "Flora Judd"
title = "Immune System Wonders"
date = "2021-06-09"
description = "Exploring the amazing capabilities of our immune system."
tags = ["Immune System","Lymphocytes","Antigens","Clonal Selection","Pathogen Defense","Stem Cells","Infections","Health","Antibody Formation","Biological Mechanisms"]
categories = ["Health","Science","Biology","Immunology"]
image = "immune_system_gif_-_find_on_gifer.gif"
+++

Hello and welcome to a *{{<text size="20.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

Today we are going to have a quick look at what I think is a really badass aspect of your
{{<highlight size="20.0pt" bg="yellow">}}immune system{{</highlight>}}{{<text size="20.0pt">}}{{</text>}}that you probably don't know about (sorry to all the humans who actually study this (@MJ) --- I"m probably going to butcher it).

Now your body is able to recognise and respond to a *{{<text size="18.0pt" fg="#ED7D31">}}virtually limitless{{</text>}}* number of different **{{<text size="16.0pt" fg="#FFC000">}}infectious agents{{</text>}}**. How on earth does it do that? How does it actually recognise the things that come into your body that
 ***{{<text size="22.0pt" fg="#FF8623">}}shouldn't be there{{</text>}}*** *?*

![Immune system GIF - Find on GIFER](immune_system_gif_-_find_on_gifer.gif)

I imagine it's not something you think about very often (if ever) because instinctively it feels like it should be easy --- if it doesn't look like something I"ve made then you should
 **{{<text size="22.0pt" fg="#C00000">}}kill it{{</text>}}**. But in order to kill it, it has to be
 *{{<text size="20.0pt" fg="red">}}recognised{{</text>}}* first.

Then in the 1950s a classy bloke called **{{<text size="18.0pt" fg="#5B9BD5">}}Sir Macfarlane Burnet{{</text>}}** {{<text size="18.0pt" fg="#5B9BD5">}}{{</text>}}(oof to be called such a wonderful name) came up with a model, called *{{<text size="24.0pt" fg="#BDD7EE">}}the clonal selection theory of antibody formation{{</text>}}* ,
 that explained this wonderful phenomenenomenenon.

![Sir Frank Macfarlane Burnet becomes director | Walter and ...](sir_frank_macfarlane_burnet_becomes_dire.jpeg)

Basically what happens if you have some lovely stem cells called
 **{{<text size="26.0pt" fg="red" bg="black">}}hematopoietic stem cells{{</text>}}** **{{<text size="26.0pt" fg="red">}}{{</text>}}** that produce *{{<text size="16.0pt" fg="#FFC000">}}immature lymphocytes{{</text>}}* , which are a type of white blood cell.

The hematopoietic stem cell makes these by **{{<text size="18.0pt" fg="#92D050">}}rearranging its genes{{</text>}}** and
 *{{<text size="20.0pt" fg="#00B050">}}differentiating{{</text>}}* {{<text size="20.0pt" fg="#00B050">}}{{</text>}}so that it produces lots of *{{<text size="26.0pt" fg="#A9D18E">}}different{{</text>}}* immature lymphocytes. The reason that they are different is because they all have
 different **{{<text size="22.0pt" fg="#00F295">}}receptors{{</text>}}** {{<text size="22.0pt">}}{{</text>}}on their surfaces, known as {{<u size="20.0pt" bg="lime">}}antigen receptors{{</u>}}, that can respond to different molecules.

![Researchers Discover New Role of Immune Cells | TrendinTech](researchers_discover_new_role_of_immune_.jpeg)

Any of the cells that have antigen receptors that respond ***{{<text size="24.0pt" fg="#ED7D31">}}to your own tissues{{</text>}}*** {{<text size="24.0pt" fg="#ED7D31">}}{{</text>}}are immediately destroyed. The last thing that you want is immune cells that are trying to attack
{{<u size="18.0pt" fg="#00B0F0">}}your own body{{</u>}}.

The rest of them mature and become **{{<text size="20.0pt" fg="#FFC000">}}inactive lymphocytes{{</text>}}**.

The bit that I find totally mad though is that *most of these will* ***{{<highlight size="24.0pt" bg="yellow">}}NEVER ENCOUNTER A MATCHING ANTIGEN{{</highlight>}}*** *.*

![image](62604cfa7e1b4c84.gif)

{{<text fg="#00B050">}}EVER{{</text>}}.

Your body produces **{{<text size="18.0pt" fg="#FF31F0">}}so many immune cells{{</text>}}** {{<text size="18.0pt">}}{{</text>}}that can respond to *{{<text size="26.0pt" fg="#CB7DF4">}}such a massive variety of compounds{{</text>}}* that most of them will never be needed.

***{{<text size="22.0pt" bg="fuchsia">}}ISN"T THAT MAD.{{</text>}}*** ***{{<text size="22.0pt">}}{{</text>}}***

But what is so awesome is that as soon as there is an antigen or a pathogen that enters your body --- you almost certainly have an immune cell that's
 **{{<highlight size="18.0pt" bg="aqua">}}waiting to be activated{{</highlight>}}**. This is why you
 *feel ill* for a while before your immune system kicks in. There are so few of each of those
 *{{<text size="20.0pt" fg="#00F295">}}inactive lymphocytes{{</text>}}* {{<text size="20.0pt" fg="#00F295">}}{{</text>}}that it takes your body a little while to make enough of them to allow you to actually get rid of the infection.

![Meilleurs GIFs Antibodies | Gfycat](meilleurs_gifs_antibodies_gfycat.gif)

*{{<text size="14.0pt">}}Pretty cool though, eh?{{</text>}}*

Hope you enjoyed.

Love as always,

Flora

Here's the GIF

![Animals GIF - Find & Share on GIPHY](animals_gif_-_find_share_on_giphy.gif)