+++
author = "Flora Judd"
title = "Agrobacterium Unveiled"
date = "2021-05-15"
description = "Discover the fascinating ways Agrobacterium tumefaciens manipulates plant cells."
tags = ["Agrobacterium tumefaciens","Ti Plasmid","Genetic Engineering","Auxin","Plant Cells","Opines","Microbial Interactions","Bacterial Pathogens","Biotechnology","Plant Manipulation"]
categories = ["Bacteria","Plant Biology","Genetic Engineering","Microbiology"]
image = "galls_-_agrobacterium_tumefaciens.jpeg"
+++

Hello and welcome to another *{{<text size="22.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* {{<text fg="#7030A0">}}{{</text>}}

Now today we are going to have a little look at a very cool species of bacterium called
 ***{{<highlight size="24.0pt" bg="yellow">}}Agrobacterium tumefaciens{{</highlight>}}*** {{<text size="24.0pt">}}{{</text>}}which looks like this when attaching itself to a carrot cell.

![image](543355a1a7bc96ed.png)

This little guy is
 *pretty cool* and is actually a **{{<text size="18.0pt" fg="#ED7D31">}}tumour-producing parasite{{</text>}}**. You might have seen some of the *{{<text size="16.0pt" fg="#92D050">}}weird lumps{{</text>}}* {{<text size="16.0pt" fg="#92D050">}}{{</text>}}it produces on plants.

![Galls - Agrobacterium tumefaciens](galls_-_agrobacterium_tumefaciens.jpeg)

The way that it does this is super cool. It has a little **{{<text size="18.0pt" fg="#4472C4">}}loop of DNA{{</text>}}** , called the **{{<text size="26.0pt" fg="#FF58F0">}}Ti plasmid{{</text>}}** , which can essentially insert a section of itself *{{<text size="16.0pt" fg="#FFC000">}}into the genome{{</text>}}* of the **{{<text size="16.0pt" fg="#00B050">}}plant host{{</text>}}**. It uses a syringe-like mechanism called a **{{<text size="24.0pt" fg="#F4B183">}}Type IV Secretion System{{</text>}}** to inject
 its DNA into the host.

Now there are a **{{<highlight size="14.0pt" bg="yellow">}}couple of genes{{</highlight>}}** {{<text size="14.0pt">}}{{</text>}}that are whacked in.

Some are hormones, and more specifically **{{<text size="18.0pt" fg="#00B0F0">}}growth hormones{{</text>}}** , that lead to the ***{{<text size="22.0pt" fg="#FFC000">}}rapid formation{{</text>}}*** {{<text size="22.0pt" fg="#FFC000">}}{{</text>}}of these tumours. One of those hormones is called **{{<text size="26.0pt" fg="#9DC3E6">}}auxin{{</text>}}** , which plants do make normally
 for themselves for growing, but this very rapid and concentrated production causes tumour formation.

![Biology Plant GIF - Find & Share on GIPHY](biology_plant_gif_-_find_share_on_giphy.gif)

And what's pretty badass is that the DNA inserted uses a way of making auxin that the plant **{{<text size="18.0pt" fg="#ED7D31">}}doesn't normally use{{</text>}}**. This means that the plant has no way of *{{<text size="18.0pt" fg="#4472C4">}}regulating the auxin production{{</text>}}*. Normally plants are good at making sure they don't grow tumours but this
 is a totally different pathway so they just can't regulate it.

**{{<text size="20.0pt" fg="#70AD47">}}SUPER COOL{{</text>}}**.

![Have you got the gall? | Micro Photonics](have_you_got_the_gall_micro_photonics.jpeg)

The other badass thing that this bacterium does is that it inserts genes that code for **{{<text size="20.0pt" fg="red">}}enzymes{{</text>}}**.

These enzymes are able to make delicious *{{<text size="16.0pt" fg="#FFC000">}}nitrogen-filled compounds{{</text>}}* {{<text size="16.0pt" fg="#FFC000">}}{{</text>}}called **{{<text size="22.0pt" fg="#A9D18E">}}opines{{</text>}}**.
 These molecules{{<text size="18.0pt" fg="#FF58F0">}}CANNOT BE METABOLISED BY PLANTS{{</text>}}. So that bacterium has just tricked it into making a food source
 that ***{{<highlight-u size="18.0pt" bg="aqua">}}ONLY IT CAN EAT.{{</highlight-u>}}***

They essentially ***{{<text size="16.0pt" fg="#92D050">}}reprogram the metabolic activity{{</text>}}*** *{{<text size="16.0pt" fg="#92D050">}}{{</text>}}* of the plant to make a compound that is ***{{<text size="18.0pt" fg="#00B050">}}totally useless to the plant itself{{</text>}}*** {{<text size="18.0pt" fg="#00B050">}}{{</text>}}and ***{{<text size="18.0pt" fg="#00F6B1">}}totally awesome for the bacterium{{</text>}}***.

Pretty epic if you ask me.

The other thing that's very cool is we actually like to swap out the DNA that's inside this bacterium to allow us to **{{<text size="20.0pt" fg="#ED7D31">}}genetically engineer stuff{{</text>}}**. You take the DNA coding for auxin and opines out and put it in *{{<text size="18.0pt" fg="#FFC000">}}literally whatever you want{{</text>}}* {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}and you have your own{{<text size="22.0pt" fg="#FF7D00">}}personal genetic engineer{{</text>}}{{<text size="22.0pt">}}{{</text>}}(in the form of *A. tumefaciens* ) that does all the hard work for you.

**{{<highlight size="20.0pt" bg="yellow">}}HOW NEAT IS THAT.{{</highlight>}}** **{{<text size="20.0pt">}}{{</text>}}**

We love the science.

![Science GIFs - Find & Share on GIPHY](science_gifs_-_find_share_on_giphy.gif)

Love Flora xx