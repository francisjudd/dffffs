+++
author = "Flora Judd"
title = "Ground-Breaking Discoveries"
date = "2021-05-12"
description = "Explore hilarious misconceptions about nature and science's evolution."
tags = ["Lemmings","Barnacle Geese","Misconceptions","Natural History","Oxford Museum","Scientific Discoveries","Flora","Fauna","Historical Perspective","Humor in Science"]
categories = ["Science","Nature","History","Humor"]
image = "barnacle_goose_-_wikipedia.jpeg"
+++

Hello and welcome to your *{{<text size="18.0pt" fg="#7030A0">}}Frequent Fun Fact From Flora{{</text>}}*

I have been galivanting around the **{{<text size="18.0pt" fg="#ED7D31">}}Natural History Museum{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}(the Oxford one) this week (even though it's still closed to the public --- just biology tings) and have found out some
 *{{<text size="18.0pt" fg="#70AD47">}}glorious{{</text>}}* {{<text size="18.0pt" fg="#70AD47">}}{{</text>}}and *{{<text size="18.0pt" fg="#00B0F0">}}hilarious{{</text>}}* {{<text size="18.0pt" fg="#00B0F0">}}{{</text>}}things about their collections.

We have a lot of basic science knowledge --- all of us --- even if ya don't THINK ya do. Ya really do. Or at least better than these guys.

I"m gonna run you past a couple of my favourite **{{<text size="36.0pt" fg="#FFC000">}}GROUND BREAKING{{</text>}}** {{<text size="36.0pt" fg="#FFC000">}}{{</text>}}scientific discoveries.

***{{<text size="22.0pt">}}# 1 ---{{</text>}}*** ***{{<text size="24.0pt">}}Lemmings are actually rodents and don't just
{{<u fg="#A9D18E">}}appear out of thin air{{</u>}}{{</text>}}*** ***{{<text size="22.0pt">}}{{</text>}}***

![image](5827f2e7bcfcec1f.jpeg)

This was figured out by a guy called **{{<highlight size="18.0pt" bg="yellow">}}Ole Worm{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}(I know) and it was originally because in Norway, lemmings would suddenly appear in huge numbers every few years. Their populations would then totally crash and no one would see them for another couple of years before they came back again in droves.

So in the 1530s, a geography called Zeigler of Strasbourg proposed that they
 **{{<text size="22.0pt" fg="#00B0F0">}}fell out of the sky{{</text>}}** {{<text size="22.0pt" fg="#00B0F0">}}{{</text>}}during *{{<text size="20.0pt" fg="#0070C0">}}stormy weather{{</text>}}* {{<text size="20.0pt" fg="#0070C0">}}{{</text>}}and died suddenly when grass grew in spring.

Ole Worm originally accepted the idea that they fell out of the sky but he said it's
 *{{<text size="36.0pt" fg="#ED7D31">}}obviously{{</text>}}* {{<text size="36.0pt" fg="#ED7D31">}}{{</text>}}because they're being brought over by the **{{<text size="24.0pt" fg="#70AD47">}}wind{{</text>}}** {{<text size="24.0pt" fg="#70AD47">}}{{</text>}}instead of spontaneously creation.
![File:Portrait of Ole Worm, "Fasti Danici", 1626 Wellcome ...](fileportrait_of_ole_worm_fasti_danici_16.jpeg)

He then decided to **{{<text size="22.0pt" fg="#5B9BD5">}}actually dissect one{{</text>}}** {{<text size="22.0pt" fg="#5B9BD5">}}{{</text>}}and realised that it looked pretty similar to most other rodents.

Mystery solved.

Thanks Worm.


***{{<text size="24.0pt">}}# 2 --- Barnacles grow on trees and also
{{<u fg="#FF61E9">}}sprout geese{{</u>}}{{</text>}}***

This one is my favourite. Have you ever heard of **{{<text size="22.0pt" bg="fuchsia">}}barnacle geese{{</text>}}** ? They look like this.

![Barnacle goose - Wikipedia](barnacle_goose_-_wikipedia.jpeg)

Ever wondered why they are called barnacle geese? Well, people used to think that
 **{{<text size="22.0pt" fg="#92D050">}}barnacles grew on trees{{</text>}}** {{<text size="22.0pt" fg="#92D050">}}{{</text>}}that then *{{<text size="36.0pt" fg="#FFD966" bg="blue">}}sprouted baby geese{{</text>}}*.


![image](5a00dd60cd9e4a2d.png)

{{<text size="20.0pt" fg="#00B0F0">}}John Gerard{{</text>}}, an English botanist, backed it as a concept and in his
 *{{<text size="24.0pt" fg="#92D050">}}Generall Historie of Plantes{{</text>}}* , published in 1597, in which he wrote that
 **{{<text size="20.0pt" fg="#A96BF1">}}mussel-shaped shells{{</text>}}** {{<text size="20.0pt">}}{{</text>}}would grow until they split open, revealing {{<highlight bg="yellow">}}"the legs of the Bird hanging out...til at length it is all come forth."{{</highlight>}}

The bird would hang by its bill until it was fully matured THEN it would drop into the sea "{{<text size="16.0pt" fg="#ED7D31">}}where it gathereth feathers, and groweth to a foule, bigger than a Mallard, and lesser than a Goose{{</text>}}."

![image](ed8ca44b004b563a.jpeg)

BUT if the goose hits land, apparently, it will **{{<text size="24.0pt" fg="red">}}perish{{</text>}}**.

I"m not sure who exactly debunked this one, but it shouldn't have been too tricky to do.

Hope you enjoyed this foray into (not) science.

Love Flora xxx