+++
author = "Flora Judd"
title = "Bizarre Platypus Insights"
date = "2021-05-20"
description = "Delve into the unique characteristics of the extraordinary platypus."
tags = ["Platypus","Monotremes","Echidna","Puggle","Ecological Niche","Biofluorescence","Nocturnal Behavior","Egg-laying","Adaptations","Unique Biology"]
categories = ["Animals","Nature","Wildlife","Mammalogy"]
image = "national_icon_the_platypus_declared_a_th.jpeg"
+++

Hello and welcome to another
 *{{<text size="20.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}* ,

The **{{<highlight size="26.0pt" bg="yellow">}}platypus{{</highlight>}}** {{<text size="26.0pt">}}{{</text>}}is a delightful and bizarre creature. They are part of a group of mammals called the ***{{<text size="20.0pt" fg="#ED7D31">}}monotremes{{</text>}}*** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}(named because they, like birds and reptiles, have a one-hole-serves-all kind of situation called a **{{<text size="18.0pt" fg="#FFC000">}}cloaca{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}--- all the excreting and mating happens in one place).

![True Facts About The Platypus](true_facts_about_the_platypus.gif)


![National icon, the platypus, declared a threatened species ...](national_icon_the_platypus_declared_a_th.jpeg)

The monotremes are made up of only five living species --- the platypus and four species of a wonderful animal called an **{{<text size="22.0pt" bg="fuchsia">}}echidna{{</text>}}** {{<text size="22.0pt">}}{{</text>}}which looks like this.

![Echidna GIFs - Find & Share on GIPHY](echidna_gifs_-_find_share_on_giphy.gif)

Firstly and most importantly though. A baby platypus is called are you ready a

***{{<text size="90.0pt" fg="#AE70F3">}}PUGGLE{{</text>}}***.

Fantastic isn't it.

Even more fantastic when you find out that they look like this.

![15 Adorable Photos Of "Puggles" (Baby Platypuses ...](15_adorable_photos_of_puggles_baby_platy.jpeg)

Anyways, platypuses are found only in Tasmania and eastern Australia. They are nocturnal and usually reach a maximum size of about half a metre.

Now they look very weird but are actually *{{<text size="18.0pt" fg="#92D050">}}fabulously well-adapted{{</text>}}* {{<text size="18.0pt" fg="#92D050">}}{{</text>}}to their ecological niche. They have **{{<text size="18.0pt" fg="#8FAADC">}}webbed feet{{</text>}}** , kind of{{<u size="20.0pt" fg="#00B0F0">}}flat sprawling bodies{{</u>}}{{<text size="20.0pt" fg="#00B0F0">}}{{</text>}}and ***{{<text size="24.0pt" fg="#0070C0">}}waterproof fur{{</text>}}*** {{<text size="24.0pt" fg="#0070C0">}}{{</text>}}which is perfect for their semi-aquatic lifestyle.

Their claws make them very efficient diggers --- they dig **{{<text size="20.0pt" fg="#00B050">}}tunnels{{</text>}}** {{<text size="20.0pt" fg="#00B050">}}{{</text>}}that are about
 *{{<text size="20.0pt" fg="#FFC000">}}5m long{{</text>}}* {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}in riverbanks which is where they live.

Their epic duck bills are ***{{<highlight size="22.0pt" bg="yellow">}}BAD ASS{{</highlight>}}*** {{<text size="22.0pt">}}{{</text>}}and actually have **{{<text size="28.0pt" fg="yellow" bg="black">}}electroreceptive sensors{{</text>}}** {{<text size="28.0pt" fg="yellow">}}{{</text>}}in them that allow them to detect the teeny tiny electrical impulses created by the muscle contractions of their prey. This is ideal because they have to hunt with their{{<text size="18.0pt" fg="#FFC000">}}eyes{{</text>}},{{<text size="18.0pt" fg="#FFC000">}}ears{{</text>}}and{{<text size="18.0pt" fg="#FFC000">}}noses{{</text>}}shut.
![Tinfoil Hat ESP](tinfoil_hat_esp.jpeg)
(I don't know what this diagram is or what it means but it looks sciencey so that's nice)

But what IS that bill? I hear you cry.

Well it's actually made of **{{<text size="20.0pt" bg="fuchsia">}}hardened gum tissue{{</text>}}** {{<text size="20.0pt">}}{{</text>}}(I KNOW --- MAD) and this means that actually they *{{<text size="20.0pt" fg="#70AD47">}}don't have teeth{{</text>}}*. Instead they use sheer force to mash the food in their mouths into a swallowable pulp.

![Platypus bills are : NatureIsFuckingLit](platypus_bills_are_natureisfuckinglit.jpeg)

And if that doesn't work they will **{{<text size="24.0pt" fg="#FFC000">}}pop some stones in{{</text>}}** their mouth the help the whole process along.

Once they"ve munched that yummy food it goes straight from the oesophagus to the intestine. These guys, and their echidna cousins, are the only mammals that **{{<text size="24.0pt" fg="#ED7D31">}}don't have stomachs{{</text>}}**.

It turns out stomachs only (lol --- only) evolved *{{<text size="22.0pt" fg="#92D050">}}450 million years ago{{</text>}}* {{<text size="22.0pt" fg="#92D050">}}{{</text>}}and this was well before platypuses came along. So we don't really understand **{{<text size="18.0pt" fg="#00B0F0">}}why they have lost their stomachs{{</text>}}**.

Some people think it might be because they ***{{<text size="18.0pt" fg="#FF7EFF">}}eat a lot of shellfish{{</text>}}*** {{<text size="18.0pt">}}{{</text>}}and so the calcium carbonate from the shells would neutralise the stomach acid --- making the organ kinda redundant.

![8 Facts Revealed by Genetic Analysis of the Platypus ...](8_facts_revealed_by_genetic_analysis_of_.jpeg)

Another **{{<text size="20.0pt" fg="red">}}EXCELLENT{{</text>}}** {{<text size="20.0pt" fg="red">}}{{</text>}}and *{{<text size="20.0pt" bg="fuchsia">}}EXTREMELY RELATABLE{{</text>}}* {{<text size="20.0pt">}}{{</text>}}piece of platypus biology is that they **{{<text size="24.0pt" fg="#FFC000">}}sleep for 14 hours a day{{</text>}}** , including 8 hours of **{{<text size="22.0pt" fg="#A9D18E">}}REM{{</text>}}** {{<text size="22.0pt" fg="#A9D18E">}}{{</text>}}({{<text size="18.0pt" fg="#A9D18E">}}Rapid Eye Movement{{</text>}})
 sleep. This is more than *{{<text size="22.0pt" fg="#ED7D31">}}any other animal{{</text>}}*.

And my favourite and final weird platypus fact --- they **{{<text size="22.0pt" fg="#61F8FF" bg="black">}}glow in the dark{{</text>}}**.

This is totally bizarre. Last year some researchers at the Field Museum of Natural History in Chicago had been trying to figure out the **{{<text size="22.0pt" fg="yellow" bg="gray">}}mechanisms of biofluorescence{{</text>}}** {{<text size="22.0pt" fg="yellow">}}{{</text>}}in some species in their collection. They accidentally shone some of their UV light on a platypus and its fur emitted a **{{<highlight size="18.0pt" bg="aqua">}}"blue-{{</highlight>}}** **{{<text size="18.0pt" bg="lime">}}green{{</text>}}** **{{<highlight size="18.0pt" bg="aqua">}}glow"{{</highlight>}}**.

![Scientists Find Platypuses Glow Under Black Light](scientists_find_platypuses_glow_under_bl.jpeg)

This has scientists **{{<text size="20.0pt" fg="#FFC000">}}totally baffled{{</text>}}**.

It's usually used as a means of ***{{<text size="24.0pt" fg="#FF7EFF">}}communication{{</text>}}*** {{<text size="24.0pt" fg="#FF7EFF">}}{{</text>}}or ***{{<text size="24.0pt" fg="#AE70F3">}}camouflage{{</text>}}*** , but this makes no sense
 in a nocturnal species. They are most active when there is very little UV light about 

All very mysterious.

{{<highlight size="16.0pt" bg="yellow">}}AND LET"S NOT FORGET THE FACT THAT THEY LAY EGGS{{</highlight>}}.

A mother digs a 30m long burrow, seals herself in and lays one or two eggs. And then instead of breastfeeding she just kind of **{{<text size="20.0pt" fg="#9DC3E6">}}oozes milk from her stomach{{</text>}}** {{<text size="20.0pt" fg="#9DC3E6">}}{{</text>}}that the babies drink.

Also please appreciate --- this might be one of the most amazing GIFs I"ve ever seen.

![Platypus GIFs | Tenor](platypus_gifs_tenor.gif)

**{{<text size="18.0pt" bg="fuchsia">}}LOOK HOW TINY IT IS.{{</text>}}** **{{<text size="18.0pt">}}{{</text>}}**

Hope you enjoyed this exploration of a weird and wonderful mammal!

Love Flora xxx