+++
author = "Flora Judd"
title = "Jumping Marvels"
date = "2021-05-21"
description = "Discover the adorable Siberian flying squirrel and its gravity-defying glides."
tags = ["Siberian Flying Squirrel","Gliding","Animal Behavior","Wildlife","Patagia","Gravity-Defying","Mammalian Adaptations","Unique Biology","Ecosystem Role","Cute Animals"]
categories = ["Animals","Nature","Wildlife","Mammalogy"]
image = "siberian_flying_squirrel_caught_mid-body.jpeg"
+++

Hello and welcome to another
 *{{<text size="18.0pt" fg="#7030A0">}}Fun Fact From Flora{{</text>}}*

Today I am going to tell you about the existence of what might be the **{{<text size="18.0pt" fg="#ED7D31">}}cutest animal you"ve ever seen{{</text>}}**.

This is the ** *{{<text size="16.0pt" bg="fuchsia">}}Siberian flying squirrel{{</text>}}*.

![Siberian flying squirrel caught mid-body-slam in rare ...](siberian_flying_squirrel_caught_mid-body.jpeg)

ISN"T IT DELIGHTFUL

![despicable me unicorn gif | WiffleGif](despicable_me_unicorn_gif_wifflegif.gif)

Now these guys are slightly misnamed because they don't actually
 *fly* but rather **{{<text size="22.0pt" fg="#FFC000">}}jump{{</text>}}** {{<text size="22.0pt" fg="#FFC000">}}{{</text>}}from high places (aka trees) and glide. They do this using **{{<text size="24.0pt" fg="#70AD47">}}specialised skin membranes{{</text>}}** between their forelimbs and hindlimbs that are called patagia.

Like this.

![Siberian flying Squirrel Photo by Masatsugu Ohashi # ...](siberian_flying_squirrel_photo_by_masats.jpeg)

ISN"T HE MAJESTIC.

They can soar for up to *{{<highlight size="22.0pt" bg="yellow">}}50 METRES{{</highlight>}}*.

Pretty epic for an animal that only reaches a maximum length of **{{<text size="24.0pt" fg="#FF7EFF">}}23cm{{</text>}}**.

![Siberian flying squirrel. | Japanese dwarf flying squirrel ...](siberian_flying_squirrel_japanese_dwarf_.jpeg)

Also look at how *{{<text size="20.0pt" fg="#AE70F3">}}preposterous{{</text>}}* {{<text size="20.0pt" fg="#AE70F3">}}{{</text>}}they are.

I mean come on.

Lots of love,

Flora