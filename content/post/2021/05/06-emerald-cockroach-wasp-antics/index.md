+++
author = "Flora Judd"
title = "Wasp Wonders"
date = "2021-05-06"
description = "Explore the incredible life of the emerald cockroach wasp and her ingenious methods."
tags = ["Emerald Cockroach Wasp","Parasitoids","Neurotoxins","Cockroach Manipulation","Hymenoptera","Insect Behavior","Host Manipulation","Ecological Role","Parasitic Relationships","Evolutionary Biology"]
categories = ["Entomology","Nature","Wasps","Insects"]
image = "emerald_cockroach_wasp_this_is_a_huge_wa.jpeg"
+++

Hello and welcome to your *{{<text size="18.0pt" fg="#7030A0">}}Frequent Fun Fact From Flora{{</text>}}*

Today we are going back to the bug world. This might happen a couple of times because I spent an **{{<text size="18.0pt" fg="#9DC3E6">}}AWESOME{{</text>}}** {{<text size="18.0pt" fg="#9DC3E6">}}{{</text>}}couple of hours yesterday chatting with a man who specialises in the *{{<text size="16.0pt" fg="#ED7D31">}}Hymenoptera{{</text>}}* {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}--- ya bees, wasps and ants.

And you know how I feel about ants.

His name is ***{{<text size="16.0pt" fg="#92D050">}}Jonathan Green{{</text>}}*** {{<text size="16.0pt" fg="#92D050">}}{{</text>}}(sadly not the award winning author) and he is a true nerd legend. But if he did write books I imagine they"d look like this.

![image](4926cedadc37f637.png)![image](12a8116d1d9f9865.png)

(it's funny because{{<highlight size="16.0pt" bg="yellow">}}paper wasps{{</highlight>}}{{<text size="16.0pt">}}{{</text>}}are actually a subgroup of vespid wasps)

![image](5a2cfb5ff19b2e8b.png)

ANYWAYS --- wasps are very cool and I think we have sadly overlooked them throughout time.

*{{<text size="20.0pt" fg="#FFC000">}}TODAY THIS ENDS.{{</text>}}*

We are going to specifically have a look at the **{{<text size="22.0pt" bg="lime">}}emerald{{</text>}}** **{{<highlight size="22.0pt" bg="aqua">}}cockroach{{</highlight>}}** **{{<text size="22.0pt" bg="lime">}}wasp{{</text>}}** and she really is a beauty.

![Emerald cockroach wasp | This is a huge wasp about 2cm in ...](emerald_cockroach_wasp_this_is_a_huge_wa.jpeg)

Gorgeous ain't she?

Well she is very aesthetically pleasing, but not the nicest because she is a **{{<text size="24.0pt" fg="yellow" bg="black">}}parasitoid{{</text>}}** {{<text size="24.0pt" fg="yellow" bg="black">}}**wasp**{{</text>}}. This means that she lays her egg *{{<text size="18.0pt" fg="#ED7D31">}}inside a host{{</text>}}* {{<text size="18.0pt" fg="#ED7D31">}}*insect*{{</text>}}whilst it is *{{<text size="26.0pt" fg="#92D050">}}still alive{{</text>}}* {{<text size="26.0pt" fg="#92D050">}}{{</text>}}and then the larvae hatches and eats the host from *{{<text size="36.0pt" fg="#00B0F0">}}the inside out{{</text>}}*.

Not ideal.

But at least she does it in a **{{<text size="20.0pt" fg="#FFC000">}}VERY COOL WAY.{{</text>}}**

Now if you are going to lay your eggs in another insect it is very handy for you if that insect{{<text size="24.0pt" fg="#00B050">}}stays alive{{</text>}}. That way the insect doesn't start to **{{<text size="18.0pt" fg="#0070C0">}}decompose{{</text>}}** {{<text size="18.0pt" fg="#0070C0">}}{{</text>}}(putting your lovely egg at risk of microbial attack) and it keeps a nice *{{<text size="20.0pt" fg="#ED7D31">}}constant environment{{</text>}}* in which your egg can live.

Some insects do this by
{{<highlight size="16.0pt" bg="yellow">}}paralyzing{{</highlight>}}{{<text size="16.0pt">}}{{</text>}}their hosts. That is all very well but one of the big problems is that you then need to find a way of **{{<text size="24.0pt" fg="#4472C4">}}hiding your host{{</text>}}**. If ya leave it out and about something else is gonna eat it.

Well there are options. You can try and **{{<text size="22.0pt" bg="fuchsia">}}carry the host{{</text>}}** back to your nest. But this wasp lays its eggs in ***{{<text size="22.0pt" fg="#7030A0">}}cockroaches{{</text>}}*** {{<text size="22.0pt" fg="#7030A0">}}{{</text>}}and they are{{<text size="20.0pt" fg="#4472C4">}}quite hefty{{</text>}}{{<text size="20.0pt" fg="red">}}{{</text>}}so that's not a great option. In fact they are **{{<text size="36.0pt" fg="#00B0F0">}}six times larger{{</text>}}** {{<text size="36.0pt" fg="#00B0F0">}}{{</text>}}than the wasp.

So what she does is pretty
 ***{{<highlight size="18.0pt" bg="aqua">}}ingenious{{</highlight>}}***. She stalks her cockroach host and then stings it in the thorax to temporarily paralyze it.

![Watch a Wasp Turn a Cockroach into a Zombie](watch_a_wasp_turn_a_cockroach_into_a_zom.gif)

AND THIS IS WHERE THE BADASSERY OCCURS.

She uses ***{{<text size="22.0pt" fg="#ED7D31">}}specialised sensory organs{{</text>}}*** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}on the tip of her stinger to locate a very
 **{{<text size="24.0pt" fg="#8BD95A">}}specific ganglion{{</text>}}** {{<text size="24.0pt" fg="#70AD47">}}{{</text>}}within the cockroach brain that controls its **{{<text size="28.0pt" fg="#FFC000">}}escape reflex{{</text>}}**. She then injects a neurotoxic substance which makes the cockroach become really sluggish and stops showing its normal escape responses. It becomes ***{{<text size="20.0pt" fg="#FF62DD">}}totally docile{{</text>}}***.

The wasp then takes the cockroach by its antennae and **{{<text size="28.0pt" fg="#00EBAA">}}WALKS IT BACK TO HER NEST LIKE A BAD ASS{{</text>}}**.

![19 Nightmare Real-World Creatures That Will Give You ...](19_nightmare_real-world_creatures_that_w.gif)

She then lays her eggs by it and leaves the living, totally stupid and unwilling-to-escape cockroach in her nest and her **{{<text size="18.0pt" fg="#00B0F0">}}eggs hatch{{</text>}}** {{<text size="18.0pt" fg="#00B0F0">}}{{</text>}}and *{{<text size="48.0pt" fg="#8BD95A">}}eat it from the inside{{</text>}}*.

![An emerald cockroach wasp emerging from an American ...](an_emerald_cockroach_wasp_emerging_from_.png)


I KNOW.

SO COOL.

Hope you enjoyed.

Lots of love Flora xxx