+++
author = "Flora Judd"
title = "Monkey Vision Unpacked"
date = "2021-01-27"
description = "Exploring the unique vision capabilities of New World monkeys and their implications."
tags = ["Vision","Monkeys","Trichromatic Vision","Dichromatic Vision","Color Perception","New World Monkeys","Evolutionary Adaptations","Gummivores","Behavioral Ecology","Primate Research"]
categories = ["Science","Animals","Primates","Vision Studies"]
image = "new_world_monkeys_-_worldatlascom.jpeg"
+++

Hello there!

Let's do some science.

Today we're going to have a little look at **vision**. Specifically, we're going to have a look at vision in the
 *New World monkeys*. This group contains the five families of primates that can be found in the tropical parts of
 **Mexico** , **Central America** and **South America**. They're closely related to the Old World monkeys (think more like baboons and macaques) and the apes. This group includes the
{{<text size="18.0pt" bg="fuchsia">}}pygmy marmoset{{</text>}}, also known as the finger monkey (you"ll see why), which is the smallest monkey in the world (reaching only
 *{{<text size="20.0pt">}}14 to 16cm{{</text>}}* ) and quite possibly the {{<u>}}{{<highlight size="14.0pt" bg="yellow">}}cutest thing ever{{</highlight>}}{{</u>}}.

Now I know we had sea otters yesterday and those were **very cute** but seriously have a look at this ridiculous animal.

![Pygmy Marmoset Facts, Baby, Habitat, Diet, Adaptations ...](pygmy_marmoset_facts_baby_habitat_diet_a.jpeg)


And when they grow up they get a bit more sexy like this one.

![pygmy-marmoset-5](pygmy-marmoset-5.jpeg)

Mmmmm those eyyeeeesss.

Also what is quite cool is that they feed on the **gum** and
 **sap** of trees so are legitimately known as **{{<text size="16.0pt" fg="#00B050">}}gummivores{{</text>}}**. Also they can leap
{{<highlight-u bg="yellow">}}FIVE METRES{{</highlight-u>}} from tree to tree like what I can't even do that and I am so much longer than 15cm. ANYWAY --- sorry this fact is
 **not** about finger monkeys --- it's about **VISION**.

We humans have a sort of vision called **{{<text size="16.0pt" fg="red">}}tri{{</text>}}** **{{<text size="16.0pt" fg="#00B0F0">}}chrom{{</text>}}** **{{<text size="16.0pt" fg="#00B050">}}atic{{</text>}}** **{{<text size="16.0pt">}}vision{{</text>}}**. This is because we have thr{{<u>}}ee types of cone cells{{</u>}} in our eyes which gives us three different independent channels for receiving colour information.

Most other mammals have **{{<text size="16.0pt" fg="red">}}dichr{{</text>}}** **{{<text size="16.0pt" fg="#00B0F0">}}omatic{{</text>}}** **{{<text size="16.0pt">}}vision{{</text>}}** {{<text size="16.0pt">}}{{</text>}}which essentially means they have some degree of colour perception but it's not very good. Usually it allows you to distinguish colour in a single dimension . Now what I mean by that is being able to see blues/violets OR yellows/greens/reds OR shorter
 wavelengths/longer wavelengths of light.

Most aquatic species are a **bit shit** and have **{{<text size="16.0pt" fg="red">}}monochromatic{{</text>}}** **{{<text size="16.0pt">}}vision{{</text>}}**.

So humans, and primates as well, are pretty unusual in our colour perceiving abilities and it's basically because a gene duplicated itself. This gene was for an
 **opsin** (remember opsins --- the ones that we find in octopus skin?) and essentially this random mutation gave us colour vision.
 *Pretty snazzy*.

Now what's weird about *this* group of monkeys is that almost all species (except for a couple of genera --- one of howler monkeys and one of owl monkeys) have what we call
 **{{<text size="18.0pt" fg="#ED7D31">}}polymorphic trichromacy{{</text>}}**.

 WOAH! I did not sign up for this what the hell does that mean, Flora? 

Glad you asked.

Basically, if you're a male monkey with only {{<u>}}one X chromosome{{</u>}} you have
 **{{<text size="16.0pt" fg="red">}}dichr{{</text>}}** **{{<text size="16.0pt" fg="#00B0F0">}}omatic{{</text>}}** **{{<text size="16.0pt">}}vision{{</text>}}** {{<text size="16.0pt">}}.{{</text>}}

If you're a female with two copies of the X chromosome that are the
{{<text fg="red">}}same{{</text>}}(so you are **homozygous** ) you have **{{<text size="16.0pt" fg="red">}}dichr{{</text>}}** **{{<text size="16.0pt" fg="#00B0F0">}}omatic{{</text>}}** **{{<text size="16.0pt">}}vision{{</text>}}**.

If you are a female with two *different* copies of the X chromosome (so you are
 **heterozygous** ) you have **{{<text size="16.0pt" fg="red">}}tri{{</text>}}** **{{<text size="16.0pt" fg="#00B0F0">}}chrom{{</text>}}** **{{<text size="16.0pt" fg="#00B050">}}atic{{</text>}}** **{{<text size="16.0pt">}}vision{{</text>}}** and you see all the beautiful colours of the rainbow.

Isn't that INSANE?!

So that means that in these species --- all males see in shitty colour, some of the females see in shitty colour but some of the females see the full technicolour dream coat??

Some researchers mocked up these images of what this actually looks like from the monkey's perspective.

![image](2b857df72b73ceba.png)

**a =** shows human vision
**b** **=** shows trichromatic lemur vision
**c =** shows dichromatic lemur vision

So while the difference isn't huge it is quite important. Can you see a kind of red-like hue of the fruit in image
 **b** ? That shows you how ripe it is. You can't see this at all in **c**. Kind of a disadvantage/

*{{<text size="18.0pt">}}Evolution --- wtf????{{</text>}}*

There are some theories as to why this is able to maintain itself in these monkey populations. Some say that whilst trichromatic vision allows you to see the
 **{{<text fg="#E64BE9">}}ripeness of fruits{{</text>}}** , dichromatic vision might be better in
 **{{<highlight bg="yellow">}}edge{{</highlight>}}** {{<highlight bg="yellow">}}**detection**{{</highlight>}} and **{{<highlight bg="yellow">}}breaking camouflage{{</highlight>}}** when looking for
 *cryptic insects*. So maybe the trichromats eat more fruits and the dichromats eat more insects.

Some researchers even think that, because these monkeys live in social groups, they are actually at an advantage by having individuals with different specialties. If the dichromats are good at spotting camouflaged things they can see
 **predators** more easily and warn the group. In return the trichromats will be better at spotting
 **patches of nice ripe fruit** from further away and so the whole group can go for a nice forage.

So maybe it DOES make sense after all.

Still kind of weird though.

![New World Monkeys - WorldAtlas.com](new_world_monkeys_-_worldatlascom.jpeg)


Love to you,

Flora :))))