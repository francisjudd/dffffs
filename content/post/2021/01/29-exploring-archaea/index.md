+++
author = "Flora Judd"
title = "Discovering Archaea"
date = "2021-01-29"
description = "Uncover the fascinating world of Archaea and their unique adaptations."
tags = ["Archaea","Microorganisms","Extreme Environments","Lokiarchaeota","DNA Evolution","PCR Techniques","Biomass","Genomic Insights","Environmental Adaptations","Extremophiles"]
categories = ["Science","Microbiology","Nature","Extremophiles"]
image = "haloquadratum_walsbyi_microbestiary.jpeg"
+++

**{{<text size="12.0pt">}}Hellooooooo,{{</text>}}**

Today we're gonna have a look at a funky little section of the tree of life. This is the
 **{{<text size="18.0pt" fg="#00B0F0">}}Archaea{{</text>}}**. It might get a bit science, but you can definitely handle it.

Archaea are a type of **microorganism** , they're **{{<text size="12.0pt">}}single-celled{{</text>}}** {{<text size="12.0pt">}}{{</text>}}but their cells **{{<text size="14.0pt" fg="red">}}don't have a nucleus{{</text>}}**. This meant that when we first found them we got very overexcited, thought they were bacteria and gave them the name
 **{{<highlight size="16.0pt" bg="yellow">}}archaebacteria{{</highlight>}}**. Most of them look kind of like bacteria as well Apart from this one called
 *Haloquadratum walsbyi*. These have very flat and almost perfectly square cells

![Haloquadratum walsbyi | MicroBestiary](haloquadratum_walsbyi_microbestiary.jpeg)
Aren't they cool looking?

So yeah these guys are totally not bacteria.

We think that they are the **{{<text size="18.0pt" fg="#44546A">}}oldest organisms on Earth{{</text>}}** {{<text size="18.0pt" fg="#44546A">}}{{</text>}}(which is a pretty cool flex).

We usually find them in pretty {{<text fg="#ED7D31">}}extreme{{</text>}} environments. They can survive in temperatures up to
{{<text size="28.0pt" fg="red">}}122 degrees C{{</text>}}like you"d find in geysers, hot springs or hydrothermal vents.

![image](455435a9c593d384.jpeg)
This is a very cool hot spring from Yellowstone National Park where some of those archaea would be
 **thriving** (isn't it so pretty).

BUT they can *also* tolerate **{{<text size="18.0pt" fg="#4472C4">}}very cold habitats{{</text>}}** {{<text size="18.0pt" fg="#4472C4">}}{{</text>}}and areas that are very *{{<text size="16.0pt" fg="#FFC000">}}salty{{</text>}}* ,
 **{{<text size="16.0pt" fg="#C00000">}}acidic{{</text>}}** {{<text size="16.0pt" fg="#C00000">}}{{</text>}}or {{<u size="18.0pt" fg="#548235">}}alkaline{{</u>}}. They are pretty bad ass. There's one called
 *Picrophilus torridus* that can literally live in **acid** with a ***{{<text size="20.0pt">}}pH of 0{{</text>}}***. That is the equivalent of living in
 **1.2 *molar* sulfuric acid**.

There are also some that live in mild conditions like swamps, marshes, the ocean and on your skin, in your mouth and in your intestines. In fact there's one species that makes up 1 in 10 of all the microorganisms in your gut. Just one species!

Don't be alarmed by this!! They do us some good favours. They, along with the bacteria living in your gut, break down the
 **{{<text fg="#70AD47">}}cellulose{{</text>}}** {{<text fg="#70AD47">}}{{</text>}}
that is in the plant matter you (hopefully) eat because that's not something you can do yourself.

Now they are nigh on *{{<text fg="#ED7D31">}}impossible{{</text>}}* {{<text fg="#ED7D31">}}{{</text>}}to cultivate in a lab. A lot of the time that's because recreating their very niche optimal habitats is pretty difficult to do. Because of this we don't really know much about them, but they are actually super abundant, especially in the ocean where
 they are thought to make up **{{<text size="20.0pt" fg="red">}}40% of the microbial biomass.{{</text>}}** That's A LOT for something we don't know much about.

What's really weird about them is that they are actually more related to eukaryotes (that's our ancestral line) than they are to bacteria.

![BEACON Researchers at Work: Testing Phylogenetic Inference ...](beacon_researchers_at_work_testing_phylo.jpeg)
*This is a little tree of life. The point where all the branches meet is where LUCA lives --- our Last Universal Common Ancestor*.

Now we think the tree of life looks like this because when we went down to have a look at some hydrothermal vents we found
 ***{{<text fg="red">}}Lokiarchaeota{{</text>}}*** {{<text fg="red">}}{{</text>}}in the sediments nearby. Named because the vent was called **{{<text size="16.0pt">}}Loki's castle{{</text>}}**. Then someone had some fun and started naming all the different archaea after Norse gods. You have the ***{{<text fg="red">}}Thorarchaeota{{</text>}}*** {{<text fg="red">}}{{</text>}}(named after Thor), ***{{<text fg="red">}}Odinarchaeota{{</text>}}*** {{<text fg="red">}}{{</text>}}(named after Odin) and ***{{<text fg="red">}}Heimdallarchaeota{{</text>}}*** {{<text fg="red">}}{{</text>}}(named after Heimdallr). So in the end the researchers put all of those species together in a superphylum and called it
 **{{<text size="18.0pt" fg="#C00000">}}Asgard{{</text>}}**.

![A disputed origin for Eukaryotes | News | Astrobiology](a_disputed_origin_for_eukaryotes_news_as.jpeg)
 bit shit for a castle really 

Well when we sequenced the genomes of the ***{{<text fg="red">}}Lokiarchaeota{{</text>}}*** {{<text fg="red">}}{{</text>}}and we found genes that encoded proteins that are really similar to eukaryotic proteins. It gets a bit sciencey when you start looking at what sort of things make us more similar to archaea than archaea are to bacteria but let's have a little look.

Like eukaryotes they have lots of types of an enzyme called {{<highlight bg="yellow">}}RNA polymerase{{</highlight>}} (very important in DNA processing), we have the same amino acid that initiates protein synthesis (called
{{<highlight bg="yellow">}}methionine{{</highlight>}} --- known as the "start codon") and they also wrap their DNA up around little proteins called
{{<highlight bg="yellow">}}histones{{</highlight>}}. Bacteria don't do any of that.

But there's still so much we don't understand. When we first sequenced the genome of
 *Lokiarchaeota* we found about 5,000 protein coding genes and had no idea what the fuck about
 **{{<text size="20.0pt" fg="#7030A0">}}32%{{</text>}}** of them did. They didn't correspond to
 ***{{<text size="16.0pt">}}any known proteins{{</text>}}***.

*WHAT*.

![This false-color image shows a cell of thermophilic methanogenic archaea. Image credit: University of California Museum of Paleontology.](this_false-color_image_shows_a_cell_of_t.jpeg)
**
*WHAT DO ALL YOUR PROTEINS DO LOKI???*
**
*...* he's a picture Flora ** he can't answer 

Anyways - we really want to find out more about these proteins. They could have some very handy uses --- some already have proved themselves to be incredibly useful.

You might have heard of **{{<text size="12.0pt" fg="#4472C4">}}PCR{{</text>}}** {{<text size="12.0pt" fg="#4472C4">}}{{</text>}}(or {{<text fg="#4472C4">}}polymerase chain reaction{{</text>}}), because it's what we use to get those looooovely 24 hour coronavirus tests. That process relies on enzymes that we nicked from archaea. We needed enzymes that could process DNA at
 **{{<u>}}high temperatures{{</u>}}** BUT all of our enzymes **{{<highlight size="16.0pt" bg="yellow">}}denature{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}(or basically deform so they don't work anymore) above about **40-50 degrees** C.

We took one of those archaea that lives in **{{<text fg="#ED7D31">}}SUPER HOT WATER{{</text>}}** {{<text fg="#ED7D31">}}{{</text>}}and used their enzymes instead. BAM. Nailed it.

Archaea could also be the next source of ***{{<text size="16.0pt" fg="#70AD47">}}antibiotics{{</text>}}***. The antibiotics we have always used have almost always come
 *from bacteria* (weird I know) so some ability to resist them is built into the bacteria's genetics. Because the antibacterial compounds from archaea, called
 **{{<text size="16.0pt" fg="#C00000">}}archaeocins{{</text>}}** , have different structures to those bacterial ones, they may have totally new ways of working. That would be
 *{{<highlight bg="yellow">}}REALLY helpful{{</highlight>}}*.

Hope you enjoyed this little look at a very weird branch of our family tree.

Love Flora xxx