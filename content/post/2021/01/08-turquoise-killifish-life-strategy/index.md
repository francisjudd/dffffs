+++
author = "Flora Judd"
title = "Life in the Fast Lane"
date = "2021-01-08"
description = "Exploring the unique adaptations of turquoise killifish in ephemeral pools."
tags = ["Turquoise Killifish","Ephemeral Pools","Life Strategy","Short Lifespan","Zimbabwe","Eggs","Desert Adaptations","Reproductive Behavior","Survivorship","Freshwater Ecosystems"]
categories = ["Science","Wildlife","Fish","Adaptations"]
image = "turquoise_killifish_life_expectancy.jpeg"
+++

Hi b, welcome to this fun science fact of the day.

Today's topic is {{<text fg="#00C69D">}}turquoise killifish{{</text>}}{{<text fg="#00B0F0">}}{{</text>}}(ahhhhhhhhh). Now these guys are very cool indeed. They live in freshwater ephemeral pools in the Zimbabwean savannah --- which is pretty tricky to do as a fish. It's not really ideal living in water
 if that water is going to dry up.

Here he is:
![TURQUOISE KILLIFISH LIFE EXPECTANCY](turquoise_killifish_life_expectancy.jpeg)


**{{<text fg="red">}}So how does he do this!?{{</text>}}** --- I hear you cry. Well, let me tell you. These bad boys employ the critical life strategy of
***{{<text size="24.0pt">}}Live fast. Die young.{{</text>}}***
(much like myself as I"m sure you agree)

So they live in these very shallow and temporary pools in the desert. When the eggs hatch they reach sexual maturity in
 **{{<u fg="#FF831E">}}two weeks{{</u>}}**. WHAT!? TWO WEEKS?! That's the fastest of
 ***any vertebrate*** and adults only live for 3-9 months which also happens to be the shortest life span of any known vertebrate (cri for them). When they're mature, females lay between 20 and 120 eggs
 ***every day*** (sounds like a phat lot of effort to me) and the males will come and fertilise them until the pool their in literally dries up and they die.

But, Flora, what about the little eggs that they just produced such a preposterous number of?

Excellent question. Those eggs are able to persist in the dry, dry desert and just wait until the next rainy season comes along the following and makes a puddle for them. When that happens they rapidly rehydrate
 and BAM they're good to go again.

Pretty cool, eh?

xxxx