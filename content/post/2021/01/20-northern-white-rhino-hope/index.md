+++
author = "Flora Judd"
title = "Rhino Revival Hope"
date = "2021-01-20"
description = "Exploring the hopeful journey of saving the northern white rhino."
tags = ["Northern White Rhino","Embryos","Conservation Genetics","Species Revival","Southern White Rhino","Fertility Research","Conservation Efforts","Wildlife Protection","Endangered Species","Genetics"]
categories = ["Conservation","Wildlife","Rhinos","Endangered Species"]
image = "9105b07feb7b4f97.jpeg"
+++

Hello and welcome to your *Daily Fun Fact From Flora*.

Today's fact is either very hopeful or very sad depending on your perspective. If you have any choice in the matter I advise you to take the first approach.

NOW --- as you may know we very sadly lost the last male northern white rhino (named
 **Sudan** ) in March of 2018. The only white rhinos that we have left now are females and they are called
 **Najin** and she has a daughter called **Fatu**. They both live in the Ol Pejeta Conservancy in Kenya (omg did you know I went on
{{<text fg="#7030A0">}}gap yah{{</text>}}to Kenya!?).

![image](9105b07feb7b4f97.jpeg)

This is them j chillin. (Fatu on the left and Najin on the right --- just so you know when you meet them obvs don't want you calling them the wrong names)

***{{<text size="14.0pt">}}BUT THERE MAY BE HOPE YET.{{</text>}}***

Before Sudan died we nicked some of his sperm (can't be jealous of whoever had the job of wanking off a rhino but you know we all gotta make ends meet) and have also taken 10 eggs from these lovely gals.

These were combined using a very snazzy method called **{{<text size="14.0pt" fg="red">}}intracytoplasm sperm injection{{</text>}}** {{<text size="14.0pt" fg="red">}}{{</text>}}(oof say that four times).

We now have **{{<text size="26.0pt">}}TWO VIABLE EMBRYOS{{</text>}}**.

*{{<u size="18.0pt">}}That's it{{</u>}}* {{<text size="18.0pt">}}.{{</text>}}

That's all we have of the legacy of the northern white rhino.

Those two little bad boys (or girls --- gender unknown) will be injected into Najin and Fatu in (hopefully) the next few months but the researchers aren't totally sure when it can happen because there's kind
 of a little pandemic going on that's sorting of ruining everything for everyone. Don't know if you"ve heard about it. Their aiming for insemination
 **before 2022**.

But don't fret!! They back themselves. We are now more sure than ever that it will work says Thomas Hildebrandt, one of the key researchers on the project.

Of course we then have to wait for the baby rhino to be born which will take another
{{<highlight-u bg="yellow">}}SIXTEEN TO EIGHTEEN MONTHS{{</highlight-u>}}. Holy guac that is a long-ass gestation period. I also don't want the job of forcibly inseminating a female rhino with the tiny remaining fragments of a nearly-extinct
 species but luckily that *ain't gon be me*.

BUT if they do work it could be the saving grace of this species! I know that to take a species down to such small numbers does funky things to their genetics (lots of inbreeding means lots of chances for
 slightly dodgy deleterious genes to accumulate) but it has happened before. The Mauritius kestrel, for example, got down to four individuals in 1974, but by 2014 there were about 400 of them.

![Mauritius Kestrel Bird Facts and Information](mauritius_kestrel_bird_facts_and_informa.jpeg)

Aren't they
 **sexy birds** ? If they can do it --- the rhinos can too.

ALSO if this doesn't work there are still more plans to save the northern white rhino by making hybrid embryos with its sister sub-species --- the **{{<text size="14.0pt">}}southern white rhino{{</text>}}**. (ooooh ahhhh) And even plans of taking cells from northern white rhinos, converting them into stem
 cells, making sperm and eggs and then implanting the resulting embryos into southern white rhinos. That would be some sci-fi level shit and the technology definitely
 *does not* exist yet but we love that the scientists are still trying to think of some bright ideas. Rhi-know you just want to learn more so I"ll pop the link to the paper at the bottom

 

I know you don't actually want to read primary literature I just wanted to make the pun.

Lots of love and stay sane,

Flora

<a href="https://www.sciencedaily.com/releases/2020/01/200115120614.htm">https://www.sciencedaily.com/releases/2020/01/200115120614.htm</a>