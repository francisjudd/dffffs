+++
author = "Flora Judd"
title = "Otterly Adorable Recovery"
date = "2021-01-26"
description = "Discover the remarkable rebound of sea otters and their critical role in kelp forest ecosystems."
tags = ["Sea Otters","Kelp Forests","Keystone Species","Ecosystem","Marine Life","Conservation Efforts","Urchin Barrens","Biodiversity","Endangered Species","Marine Ecosystems"]
categories = ["Wildlife","Conservation","Marine Biology","Biodiversity"]
image = "the_rebound_of_the_sea_otter_-_alaska_ma.jpeg"
+++

Helloooo,


Today's fact is going to be fucking adorable. We are looking at sea otters.

![The Rebound of the Sea Otter - Alaska Magazine](the_rebound_of_the_sea_otter_-_alaska_ma.jpeg)

Now this story starts of a little bit sad but you gotta stay with me here. Our story begins in the 18th century in Russia when people decided that these gorgeously fluffy
 animals would make some really fantastic clothes and so started hunting them.

By 1911 it is estimated that there were only{{<text size="16.0pt" bg="red">}}2,000{{</text>}}{{<text size="16.0pt">}}{{</text>}}of these little guys left in the most extreme parts of their habitat. They normally live in kelp forests. Kelp are actually algae instead of being true plants so they
 **don't have a vascular system** (they don't have tubes inside to carry water or molecules around) and they also
 **don't have true roots**. Instead they have a holdfast that keeps them anchored to the floor. They are *{{<text size="16.0pt">}}hugely productive{{</text>}}* {{<text size="16.0pt">}}{{</text>}}and can grow up to **{{<text size="20.0pt">}}30cm a DAY{{</text>}}** {{<text size="20.0pt">}}{{</text>}}(yoiks).

Now these ecosystems are
 **{{<highlight bg="yellow">}}HUGELY DIVERSE{{</highlight>}}** and one single kelp plant can be a home for **{{<text size="18.0pt" fg="red">}}30-70{{</text>}}** {{<text size="18.0pt" fg="red">}}{{</text>}}different species. That's a lot a critters. These include algae, crabs, brittle stars, crabs --- you name it.

But you see there was a bit of a problem when these otters started to get fished. They normally snack on sea urchins ( **spikey bastards** ) and so when they were taken out of the
 ecosystem the urchins went MENTAL. Urchins eat kelp so this was a bit of a problem. But the urchins don't just snack on a leaf or two --- their favourite bit to eat is the holdfast. So as soon as they eat some --- the whole kelp gets detached and drifts away.
 Uh oh. *That's bad news*.

They formed these things call urchin barrens that look kind of like this:
![image](e12c50eacd46c78f.png)


Not so good. Suddenly all those species living on and in the kelp are kind of
 **fucked** as well.

And this is a really difficult thing to undo, because if you have that many sea urchins as soon as any kelp starts to grow it gets munched.

Enter sea otters.

We finally stopped hunting the poor little beans and this gave their numbers a chance to recover. The sea otters were having the times of their lives.
 *SO MANY SNACKS TO EAT* so their populations came back pretty rapidly. Once that happened they could keep the urchins in check, the kelp was able to re-establish and so all of those other species could start coming back as well. All around Alaska there
 was significant recovery of kelp forests as the sea otters expanded their ranges again.

![From the Chill of Canadian Waters to the Hypnotic Kelp ...](from_the_chill_of_canadian_waters_to_the.jpeg)


This shows us that otters are what is known as a **{{<text size="16.0pt" fg="red">}}keystone species{{</text>}}**. This means that they have implications on their ecosystem waaaay beyond just the simple dynamics
 you might expect.

Plus also they are just so adorable.

***{{<text size="20.0pt" fg="#7030A0">}}THEY HOLD HANDS FOR GODS SAKES.{{</text>}}***

![animal facts --- yourhappyplaceblog](animal_facts_yourhappyplaceblog.jpeg)


**{{<highlight size="18.0pt" bg="yellow">}}See you next time!{{</highlight>}}** **{{<text size="18.0pt">}}{{</text>}}**

![Cute Otters GIFs - Get the best GIF on GIPHY](cute_otters_gifs_-_get_the_best_gif_on_g.gif)

Love Flora xx