+++
author = "Flora Judd"
title = "Diversity in Faces"
date = "2021-01-13"
description = "Exploring the uniqueness of human facial features and our ability to recognize them."
tags = ["Facial Diversity","Human Faces","Recognition","Negative Frequency Dependent Selection","Variation","Anatomical Constraints","Social Hierarchies","Natural Selection","Evolutionary Biology","Cognitive Science"]
categories = ["Science","Biology","Evolution","Psychology"]
image = "diversity_dance_act_-_britains_got_talen.jpeg"
+++

Hello and welcome to today's **FUN** fact.

Today we're looking at human faces. Not yours specifically --- don't worry --- but more faces in general. The theory behind faces if you will.

![image](044bc27b5d35c5f8.gif)


Look at all these humans. There is so much DIVERSITY.

![Diversity (Dance Act) - Britains Got Talent 2009 HIGH ...](diversity_dance_act_-_britains_got_talen.jpeg)

No not THAT kind of Diversity (but shout out to the Britain's Got Talent dance troupe of 2009).

Now I don't know if you"ve ever lovingly stared into the faces of hundreds of penguins but their faces all LOOK THE SAME not that I"ve done that that would be weird. We used think that we couldn't spot the
 differences in the facial features of other species because we weren't used to it. Our little brains weren't practiced at finding patterns in their facial features but ACTUALLY it turns out we're pretty unique in how much our faces vary.

Across ethnic groups ***and*** sexes --- there is consistently
{{<text fg="red">}}more variation{{</text>}}in our facial features than there are in other parts of our bodies.

There are also fewer correlations between the different features in our faces compared to the rest of our body (i.e. if you have longer arms you probably have a longer torso but if you have a long mouth you
 won't necessarily have any corresponding features in your face). This suggests that there are fewer
 **anatomical constraints** on facial features.

In fact --- what's weird --- is that facial features seem to provide an advantage when their more rare. This makes sense --- we want to be identifiable as individuals. You don't want some angry man thinking you're
 the woman that stole his favourite wheelie bin and chasing you down the street with a rolled up newspaper. You want him to be able to tell you apart from his next door neighbour who did steal his bin. Having a big diversity in faces allows for a visual assignment
 of identity --- the angry man knows you're you and not his crazy neighbour.

The genes that we have that are associated with our facial features show a form of natural selection called
 **negative frequency dependent selection**. {{<highlight bg="yellow">}}BIG SCIENCE ENERGY{{</highlight>}} I know but it's actually a simple concept. The rarer you are --- the better you are. But this has problems. Because you're rare you have an advantage. So you have loads of babies who all carry that same advantage and those babies have
 loads of babies that carry it too etc etc etc Oops. Suddenly it's not so rare and the advantage is gone so someone else who
 *is* rare gets to have the advantage now.

But not only do we show a lot of
 **diversity** in our faces --- we are also really good at **remembering** faces. You know the feeling of looking at someone and just KNOWING you"ve seen them before and it turns out you haven't seen since they stole your Lego bricks in nursery school (Finlay
 Watts I"m looking at you). That's been evolutionarily whacked into your brain because if you're a social animal it's super important to know who is who in your group. The ability to recognise other individuals is super important in animals that show
 **social hierarchies**. If you have a scuffle with a dominant member of the group it pays to remember who they are so you don't make the same mistake next time and you avoid the scuffle.

This one is a bit less POW WOW cool, but I was worried I was including too many invertebrates in my facts so I hope you enjoyed the diversity. If this was not enough diversity for you, please enjoy the link
 to the Britain's Got Talent final of 2009 below.

Love Flora

{{< youtube PtwVfJqBfms >}} (seriously it's amazing watch it)