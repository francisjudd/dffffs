+++
author = "Flora Judd"
title = "The wild sex lives of bees"
date = "2021-01-12"
description = "An installment of fabulously bizarre animal genetics"
tags = ["Bees"]
categories = ["Insects", "Sex", "Genetics"]
image = "bee.jpg"
+++

As I’m sure you know bees live in colonies that are run by a Queen. This is a tad unrelated but do excuse me --- I think it’s very cool! She is much larger than the others and even though she is genetically just the same as her workers she is fed royal jelly during her development. This is why she grows bigger than the others (almost twice as big in fact) and it’s also the reason behind her lengthy lifespan. Workers live to about 5-6 weeks old whereas Queens can live for up to 5 years! Isn’t it crazy how nature and nurture play out?
 
Anyways! The really interesting thing about bees though is the fact that the Queen is the only reproducing female in the hive. She will lay lots and lots of eggs that are fertilised by males (who are known, perhaps unkindly, as drones) and those fertilised eggs are then cared for by subordinate females (known as workers). But this poses an interesting evolutionary question --- why on earth would you spend all of your time and energy caring for someone else’s offspring without having any of your own? That goes against everything we think we know about evolution. The goal is to pass on your genes to the next generation so why on earth would natural selection allow for the formation of these sterile workers who never reproduce?
 
This is where genetics comes in.
 
Bees have a very interesting genetic system known as haplodiploidy. It sounds a bit odd doesn’t it --- a species that is both haploid and diploid? How does that work?
 
Well instead of having sex chromosomes like we do, bees use a system where females are diploid (with two copies of all genes) whilst males are haploid (with only half of the genetic material that the females have). So that means that if an egg is laid that does not get fertilised it simply develops into a male. If it does get fertilised it develops into a female. Also, bizarrely enough, this means that male bees cannot have sons, they can only have daughters or grandsons. To make a male all you need is an egg so the males are not needed --- no genetic contribution required thank you sir!
 
It’s this, rather neat, genetic backdrop that allows for eusociality to evolve (eusociality being the term for having one Queen who does all the reproducing whilst her offspring are cared for by sterile female workers). This is because if one Queen mates with a male many times she will produce lots of daughters. But what is interesting here is that these daughters experience a phenomenon known as super-sister relatedness. Now this is a bit complicated. This is because they actually share 75% of their genes with their sisters, compared to the normal 50% that you would find in human sisters. In humans you are 50% related to your siblings with 25% of that relatedness coming from your mum and 25% coming from your dad.
 
This is not the case in bees because the male bees only have half a set of genes! So those are the only genes he can pass on to his daughters. The male genetic contribution is exactly the same in every daughter. The half of their genes from their dad is the same in every worker (50%) and then they get 25% from their mothers.
 
So you have these super related sisters that share 75% of their genes with each other. If any of those workers were to have their own offspring they would pass on 50% of their genes... wait a minute... that means they are more related to their sisters than they are to their own children. Woah! Suddenly it makes total sense --- you actually pass on more of your genetic material by looking after your sisters than you would if you produced your own eggs.
 
Of course, this only works if the Queen bee remains monogamous, which (unsurprisingly) is not the case in hives today. However, it is thought that monogamy would have been (BEEn... haha) essential for this system to evolve. Once it had evolved, perhaps there was a bit more room for manoeuvre.
 
Having said that though, who knows how eusociality evolved in naked mole rats (the only mammal we know about that has one reproducing Queen and sterile workers)? A topic for a different post!
 
I hope some of that made sense. It’s pretty complex stuff, so I fear a blog might not be the easiest platform to explain it over, but I just think that bees are truly incredible. Have you heard of the waggle dance? It is one of my favourite biological phenomena perhaps ever. If you don’t know about it I cannot recommend a quick Google highly enough.

