+++
author = "Flora Judd"
title = "Snail Love Darts"
date = "2021-01-28"
description = "Exploring the unusual mating rituals of hermaphrodite snails."
tags = ["Hermaphrodite","Snails","Mating Behavior","Love Dart","Sexual Strategies","Biology","Hormonal Effects","Reproduction","Evolutionary Mechanisms","Unique Reproductive Structures"]
categories = ["Nature","Biology","Relationships","Reproductive Strategies"]
image = "love_dart_-_wikipedia.jpeg"
+++

Helloooo and good morning b,

I think this is the only one you're missing now.

I love you dearly dearly and your replies were simply joyous to read.

Welcome to your *{{<text fg="red">}}Daily Fun Fact From Flora{{</text>}}*.
 Today we're learning about hermaphrodite sex (I know. We're all very excited).

If you're a snail (a scenario I know you"ve dreamed of often) you can stab your hermaphrodite lover with one of these:

![Love dart - Wikipedia](love_dart_-_wikipedia.jpeg)

This freaky looking harpoon is actually a **love dart** (or a **gypsobelum** for the slightly less romantic name) and it's a sharp, often calcareous, dart that snails/slugs
 will fire at each other. But this dart does not carry any sperm or eggs. It is fired at the other individual *before* mating takes place. These darts can get pretty big as well (up to a fifth of the length of the foot) so what the hell do they actually
 do?

WELL --- thank god you asked.

Before copulation, each of the two snails/slugs will attempt to "shoot" one or more darts into the other. There is no organ to receive this dart, they are literally stabbing
 each other like you would a real dart. Important to note there's no flying through the air --- it's a contact shot. But anyways, what the dart *actually* does is induce the other snail/slug to accept more sperm than it normally would by introducing a hormone-like
 substance into their body. So THEN they can mate and when they do the snail donating the sperm will fertilise more eggs.

HOW WEIRD IS THAT!?

Tune in tomorrow for another fact.

Love Flora xxx