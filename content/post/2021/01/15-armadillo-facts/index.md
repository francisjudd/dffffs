+++
author = "Flora Judd"
title = "Weird Armadillo Wonders"
date = "2021-01-15"
description = "Explore the fascinating biology and history of armadillos and their ancient relatives."
tags = ["Armadillos","Evolution","Glyptodonts","Biology","Delayed Implantation","Mammalian Adaptations","Ecological Niche","Unique Characteristics","Conservation Status","Ancient Relatives"]
categories = ["Animals","Biology","Mammals","Nature"]
image = "extinct_glyptodonts_really_were_gigantic.jpeg"
+++

Helloooo,

Welcome to today's *Daily Fun Fact From Flora*.

Today we are going to be having a look at a very cool group of mammals, the Cingulata, or as you probably know them --- the armadillos. These are a group of (and this is just fabulous)
 **armoured** (LITERAL ARMOUR WHY IS OUR SKIN SO FEEBLE I WANT ARMOUR), placental mammals. In fact armadillo actually is a Spanish word that means little armoured one . Isn't that just fab.

First weird things about these guys is that they have single-colour vision and they have some of the lowest metabolic rates of all of the placental mammals. They also lack a functioning
 **{{<text fg="#00B050">}}pineal gland{{</text>}}** {{<text fg="#00B050">}}{{</text>}}which is what produces the melatonin that we need for establishing regular sleep patterns (regular sleep patterns --- what are those?) in circadian cycles.

Okay so if we are going to talk about armadillos we should head back about 20 million years ago so we can have a look at some of their CRAZY relatives. Firstly let's have a look at **{{<text size="16.0pt">}}glyptodonts{{</text>}}** {{<text size="16.0pt">}}{{</text>}}which had a real-life{{<text size="18.0pt">}}MACE{{</text>}}at the ends of their tails that they could use to bash each other over the head with --- as is helpfully demonstrated in this artist's rendering below:

![Extinct glyptodonts really were gigantic armadillos ...](extinct_glyptodonts_really_were_gigantic.jpeg)


Their body armour is pretty tortoise-like, made of bony deposits on their skin called osteoderms ( **osteo** = bone and
 **derm** = skin so literal **boneskin** ) which might also explain why they could weigh up to{{<text size="16.0pt" fg="#00B0F0">}}2,000kg{{</text>}}{{<text fg="#00B0F0">}}.{{</text>}}Sadly these guys went extinct at the end of the last ice age about 10,000 years ago alongside giant ground
 sloths (yes literal sloths that were 6m tall and weighed 4 tons --- that's a whole different fun fact but here's a picture to enjoy anyways):

![image](70d12700cff540d1.jpeg)


But anyway, back to armadillos. My favourite weird biological fact about them is that they give birth to **{{<text size="16.0pt">}}four {{<text fg="#7030A0">}}monozygotic{{</text>}}offspring{{</text>}}** {{<text size="16.0pt">}}{{</text>}}--- and what that means is that they have{{<text size="48.0pt">}}identical quadruplets{{</text>}}EVERY SINGLE TIME THEY MATE. How crazy is that. This happens when one egg is fertilised which splits in half and each of those halves split again and
 *then* they implant into the uterus of the mother armadillo.

What is interesting as well is that these bad boys also use **{{<text size="14.0pt">}}delayed implantation{{</text>}}** {{<text size="14.0pt">}}{{</text>}}which essentially allows the mother to choose when her embryos implant into her uterus so that she can give birth in favourable environmental conditions. Pretty cool eh!?

Also look how adorable they are when they're little pups.

![Earth Teach Me: March 2014](earth_teach_me_march_2014.jpeg)


I couldn't find a collective noun for them but would anyone be willing to sign a petition to have an
 **{{<text fg="#92D050">}}armada{{</text>}}of armadillos** ?

Let me know.

All the love,

Flora