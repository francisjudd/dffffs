+++
author = "Flora Judd"
title = "Ants and Parenting Hacks"
date = "2021-01-12"
description = "Discover how *Protomognathus americanus* ants master parenting through deception."
tags = ["Ants","Parenting Strategies","Slave-making Ants","Protomognathus americanus","Temnothorax","Deception","Social Behavior","Colonial Living","Evolution","Survival Tactics"]
categories = ["Wildlife","Insects","Nature","Entomology"]
image = "50d70f36fabfaf82.jpeg"
+++

Hello and WELCOME to today's
 ***{{<text size="18.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}***.

Hope you're in for a ride --- this one is a good"un.

I imagine you don't need me to tell you that --- all over the animal kingdom --- parenting is difficult. So much time, so much energy, so many resources I mean DEAR GOD WILL THEY EVER STOP EATING?!

It is something of a struggle.

So, what if I told you that there was a solution? A solution figured out by a rather fantastic species of ant known as
 *Protomognathus americanus* (also known as *Temnothorax americanus* ). It turns out that these bad boys are a species of slave-maker ant and actually have
 *no idea* how to rear their own young. They are totally clueless.

They have no idea how to raise offspring and so instead just **{{<text size="14.0pt">}}USE OTHER ANTS{{</text>}}** **as** **{{<text size="22.0pt" fg="#ED7D31">}}SLAVE LABOUR!!{{</text>}}**
**{{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}**

![image](50d70f36fabfaf82.jpeg)

A small *P. americanus* 
colony might feature a queen, between 2-5 workers and 30-60 slaves.

To kick off one of these colonies a young
 *P. americanus* queen will usurp the nest of another ant species by driving away or **{{<text size="14.0pt">}}killing{{</text>}}** {{<text size="14.0pt">}}{{</text>}}the resident queen and sometimes killing
 **{{<text size="18.0pt">}}all of the adult workers{{</text>}}** {{<text size="18.0pt">}}{{</text>}}as well.

I know what you're thinking --- why would the queen do that? Why would she kill all the workers? She will just be hella lonely.

BUT THIS IS WHERE THE GENIUS LIES.

She sits and waits for all the carefully cared-for pupae to hatch and when they do these freshly hatched ants have no idea that the queen isn't of their own species. So they just work for her instead. They
 become imprinted on her chemical odour and so will think that that nest is their own and that that species is their own. **GENIUS**.

But now you must be thinking --- Ah Flora but then all of those little workers will die and then the Queen will once more be lonely af. 

This is where it gets even juicier.

These ants will nick more slaves from other (closely related) ant species in the
 *Temnothorax* genus in a process known as{{<text size="16.0pt">}}"{{</text>}}{{<u size="14.0pt">}}slave raiding"{{</u>}}{{<text size="14.0pt">}}{{</text>}}(I promise I"m not making this up). The raids take place in two steps. Firstly a scout will go and find a potential host nest and when it does it returns to its nest to recruit its nest-mates and they will go and seize
 a brood to bring back. This is actually done by a whole bunch of ant species and some will capture up to **{{<highlight size="16.0pt" bg="yellow">}}14,000 pupae{{</highlight>}}** {{<text size="16.0pt">}}{{</text>}}in a single season!

As far as I can tell --- the slave-making ants are able to reproduce and then just rely on these other ant species to do all the parenting. And thus the cycle continues.

Maybe rethink your parenting strategies?

Love you loads,
F xxx