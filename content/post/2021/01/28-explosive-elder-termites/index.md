+++
author = "Flora Judd"
title = "Explosive Elder Termites"
date = "2021-01-28"
description = "Unveiling the remarkable defense mechanism of elder termites."
tags = ["Termites","Defense Mechanism","Neocapritermes taracua","Elderly Workers","Crystal Structures","Explosive Behavior","Eusocial Insects","Toxic Backpacks","Social Behavior","Altruism"]
categories = ["Insects","Entomology","Nature","Biology"]
image = "c41a3103327b3f52.png"
+++

Hello! Hope you are thriving and ready for more *{{<u size="18.0pt">}}TERMITES{{</u>}}*.

You know you were just direly in need of more cool critters. Well don't you worry cos I GOT YOU.

This one is a tropical termite called
 *Neocapritermes taracua* and you find most of them on decaying wood which is their favourite thing to eat. Now, these bad bois use a slightly different strategy to us humans in terms of warfare.

{{<text size="20.0pt">}}We send our{{</text>}} **{{<text size="24.0pt" fg="#4472C4">}}young men{{</text>}}** {{<text size="24.0pt" fg="#4472C4">}}{{</text>}}{{<text size="20.0pt">}}to war, termites send{{</text>}} **{{<text size="26.0pt" fg="#DDB6D7">}}old ladies{{</text>}}** {{<text size="20.0pt">}}.{{</text>}}

This was first spotted when researchers noticed that some of the termite workers had a **{{<text size="16.0pt">}}pair of dark blue, elongated spots{{</text>}}** {{<text size="16.0pt">}}{{</text>}}at the spot between their *thorax* and their
 *abdomen*.

Some workers didn't have them, some had small ones, and some had very well developed ones.

When they looked a little bit closer they saw that these blue spots were actually blue
{{<u>}}copper-based{{</u>}} **{{<text size="16.0pt" fg="#00B0F0">}}crystal-like structures{{</text>}}** {{<text size="16.0pt" fg="#00B0F0">}}{{</text>}}that were stored inside **pouches**. They're made by
 **{{<highlight bg="yellow">}}crystal glands{{</highlight>}}** that sit on their abdomensand are genuinely referred to in scientific literature as **"** ***{{<text size="16.0pt">}}toxic backpacks"{{</text>}}***.

These backpacks sit right next to salivary glands.

![image](c41a3103327b3f52.png)

Sorry the picture isn't so good (you"d think there would be higher demand for photos of old lady termites) but in
 **A** you can just about see the blue dots on the termite labelled bw (blue worker). The white worker (ww) is a young un so hasn't grown her crystals yet. The one with the giant orange head is a soldier (s).

Scientists saw that
 **older workers** carried the **largest** and ***most toxic* backpacks**. Also as termites get older their mandibles get much less sharp. This is because they can't moult their mandibles so the more munching they do, the duller the mandibles become.
 This makes them less good at foraging and maintaining the nest than some of the younger workers.

This means, when the nest is under{{<text size="14.0pt" fg="red">}}attack{{</text>}}, the old lady termites are sent off to
 **defend the colony** (whooooo you go Deborah!). When the enemy invaders bite on to that juicy thorax, it causes these big ol" pouches to **{{<text size="20.0pt">}}burst{{</text>}}** and the crystals mix with the
 **secretions** made by the **{{<highlight bg="yellow">}}salivary glands{{</highlight>}}**.
This makes a{{<text size="20.0pt" fg="#00B0F0">}}toxic blue liquid{{</text>}}that{{<text size="90.0pt" fg="#ED7D31">}}explodes{{</text>}}
out of the termite and covers the attacker.

![image](9ca2dd0696cbdca7.png)

**B** shows what the worker looks like after she's exploded (the scientists grabbed her with tweezers) and
 **C** shows what the worker looks like when her crystals have been removed (and put next to her).

Of course this does also kill the termite, (cri) but this is not at all unusual. Self-destructive behaviour is pretty common among eusocial insects, because the workers don't reproduce.
 This opens them up to all sorts of "altruistic" behaviours that benefit the colony as a whole rather than the individual.

In fact, **{{<highlight size="16.0pt" bg="yellow">}}defensive suicidal rupturing{{</highlight>}}** (yes that is a real scientific term) has evolved independently
 in a whole bunch of different termite and ant species. This tells us that it's a pretty good strategy as evolution goes.

ALSO you gotta remember that this is kind of a brilliant system. As you get old and a bit shit and unable to care for the colony --- you can be
 ***BAD ASS*** instead and explode blue goop over your enemies and kill them.

So getting old isn't all bad.

Lots of love to you,

Flora xxx