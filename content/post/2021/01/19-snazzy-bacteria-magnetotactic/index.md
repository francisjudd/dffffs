+++
author = "Flora Judd"
title = "Magnetotactic Marvels"
date = "2021-01-19"
description = "Discover the unique world of magnetotactic bacteria and their magnetic orientation."
tags = ["Magnetotactic Bacteria","Magnetosomes","Orientation Mechanisms","Microorganisms","Magnetite","Greigite","Magnetic Field Detection","Flagella","Bacterial Behavior","Environmental Adaptations"]
categories = ["Bacteria","Science","Microbiology","Ecology"]
image = "ed440f965f82c1c9.png"
+++

Hello and welcome to another fun fact,

This one is going to be brief and beautiful. I"m going to tell you about some very
{{<text fg="#FFD966" bg="black">}}snazzy bacteria{{</text>}}.

These bacteria are magnetotactic --- which means that they can orient themselves in line with the
{{<highlight-u bg="yellow">}}earth's magnetic field{{</highlight-u>}}.

**THAT"S PRETTY DARN COOL.**

So how do they do it?

They have these little structures inside them called **{{<text size="14.0pt" fg="#70AD47">}}magnetosomes{{</text>}}** {{<text size="14.0pt" fg="#70AD47">}}{{</text>}}and these are little iron-based magnetic crystals that are wrapped up in a nice little membrane to keep them safe inside the bacterial cell. For the super keen beans among you (Chris I"m looking at you) these crystals are
 usually made up of **{{<text size="18.0pt" fg="#ED7D31">}}magnetite{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}(Fe3O4) or **{{<text size="20.0pt" fg="#ED7D31">}}greigite{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}(Fe3S4).

THEN what happens is super cool. These magnetosomes are held together in long chains that run the length of the body of the bacteria and are all held in place by special scaffolding proteins. This is what you can see in the pictures below:

![image](ed440f965f82c1c9.png)
![Life on Mars?! | The Martian Chronicles](life_on_mars_the_martian_chronicles.jpeg)


As these chains form they line up all the magnetic forces of the magnetosomes (technically their
 **{{<text fg="red">}}magmentic moments{{</text>}}** {{<text fg="red">}}{{</text>}}
are in line) and essentially allows the bacteria to act magnetically.

These bacteria can therefore orient themselves in the direction of the earth's magnetic field lines. Kind of like you glued a little worm to the needle of a compass 

But I bet you're thinking Flora that's super dumb --- if I glued a worm to the needle of a compass it would have to spend the rest of its life moving in one plane of direction 

 

Yeah that's exactly what happens. These little bacteria can use their **{{<text size="20.0pt" fg="red">}}flagella{{</text>}}** {{<text size="20.0pt" fg="red">}}{{</text>}}(their little whip-like tails --- you can see them in the second picture) to move forwards or backwards but they
 *{{<highlight bg="yellow">}}can't change direction{{</highlight>}}*. This means they passively orient themselves to be either north-seeking or south-seeking (the magnetic poles that is --- not the geographical ones) and then BAM that's it.
 Forwards and backwards are the only options.

Weird eh?

Let's hope they keep moving forward. We could all use a bit of that.

Love you buckets,

Flora