+++
author = "Flora Judd"
title = "Carnivorous Sea Sponges"
date = "2021-01-22"
description = "Discover the fascinating world of newly found carnivorous sea sponges."
tags = ["Carnivorous Sponges","Marine Animals","Filter Feeding","Deep Sea Discovery","Toxic Compounds","Pharmaceuticals","Ecosystems","Research","Species Diversity","Crustaceans"]
categories = ["Marine Biology","Ocean Life","Biodiversity","Research"]
image = "main_article_image.jpeg"
+++

Hello and welcome to another extremely exciting fact.

As of THIS MONTH we have discovered three new species of
 **CARNIVOROUS SEA SPONGES** (*quaking*) and they are called *Nullarbora heptaxia, Abyssocladia oxyasters* and *Lycopodina hystrix.* So we're gonna appreciate some sponges and how cool they are.

Okay so sponges are some of the oldest bods about and have existed for pretty much, in some form or other, for the last **{{<text size="18.0pt" fg="#C55A11">}}500 million years{{</text>}}** {{<text size="18.0pt" fg="#C55A11">}}{{</text>}}(and you thought you were old). One of the reasons they"ve been around this long is because their biologically pretty simple. They are normally multicellular and made up of holey tissues that allows water to flow through.
 Their cells can then remove the oxygen and nutrients from this water, allowing them to **{{<highlight size="16.0pt" bg="yellow">}}filter feed{{</highlight>}}** **{{<highlight bg="yellow">}}.{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}

![Filter Feed Spongebob GIF - Find & Share on GIPHY](filter_feed_spongebob_gif_-_find_share_o.gif)
(spongebob is 100% biologically accurate --- fact)

What's pretty cool is that sponges metabolise a lot of **{{<text size="16.0pt" fg="red">}}toxic chemical compounds{{</text>}}** {{<text size="16.0pt" fg="red">}}{{</text>}}put into the seawater by other animals, plants and microbes. But instead of just getting rid of those toxins they keep a lot of them for their own personal use and so are often
 ***seriously toxic***. This makes sense --- if you're stuck on a rock and you can't move it probably helps to be really fucking toxic so that stuff is less likely to eat you.

This is one of the reasons why they are so often targeted by the **{{<highlight-u size="16.0pt" bg="yellow">}}pharmaceutical industry{{</highlight-u>}}** {{<text size="16.0pt">}}{{</text>}}(if you didn't know). For example, dysinosin A is a compound that was discovered from the sponge
 *Citronia sp* , from the Great Barrier Reef and it inhibits some of our
 **{{<text size="14.0pt" fg="red">}}blood{{</text>}}** **{{<text size="14.0pt">}}clotting enzymes{{</text>}}** (they're called thrombin and Factor VIIa). That means that they could be used for treating human cardiovascular
 diseases like **stroke** and **thrombosis**. PRETTY COOL EH?

But in the deep sea --- about 3,000m deep --- things get a bit funky (as they so often do) I mean look at this goblin shark that has a mouth that just COMES OUT OF
 *FUCKING NOWHERE* LIKE WHAT THE ***FUCK***.

![Goblin Shark GIFs - Get the best GIF on GIPHY](goblin_shark_gifs_-_get_the_best_gif_on_.gif)

Anyways.

Some carnivorous sponges do still use this water filtering system, but some have abandoned it completely and actually nick little
 **crustaceans** and other prey out of the water using{{<text size="18.0pt" fg="#7030A0">}}filaments{{</text>}}or{{<text size="18.0pt" fg="#7030A0">}}hooks{{</text>}}. They also look really much prettier than you would expect from a sponge. I mean look at this one that looks like a sassy
 flower.

![SAM S2599 MOD 3](sam_s2599_mod_3.jpeg)

Or this one that looks like those shitty earrings you used to be able to buy from Claire's accessories.

![main article image](main_article_image.jpeg)

![Rubber Spike Ball Earrings | Ice Jewelry](rubber_spike_ball_earrings_ice_jewelry.jpeg)

The resemblance is uncanny no?

Hope you enjoyed!

Lots of love,

Flora