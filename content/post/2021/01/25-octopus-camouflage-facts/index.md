+++
author = "Flora Judd"
title = "Octopus Camouflage Wonders"
date = "2021-01-25"
description = "Explore the extraordinary camouflage abilities of octopuses and their unique vision."
tags = ["Octopus","Camouflage","Chromatophores","Color Perception","Vision","Opsins","Marine Life","Cephalopod Behavior","Adaptations","Predator Evasion"]
categories = ["Animals","Marine Life","Cephalopods","Biology"]
image = "octopus_camouflage_gif_woahdude.gif"
+++

Hello and welcome to a fresh week of fantastical facticles from your favourite fabulous fanatic.

So today we're going to be having a little look at what is, in my opinion, one of the coolest animals on this tiny little planet of ours.

The ***{{<u size="24.0pt" fg="#7030A0">}}OCTOPUS{{</u>}}***.

Octopus do a lot of really amazing
{{<text fg="yellow" bg="gray">}}camouflaging{{</text>}}{{<text fg="yellow">}}{{</text>}}and can mimic colours, patterns and textures pretty much perfectly to match their surroundings.

![Octopus Camouflage [GIF] : woahdude](octopus_camouflage_gif_woahdude.gif)

LIKE WHAT ***DID YOU EVEN SEE HIM THERE*** !?

The way that they can do this is they have these special cells in their skin called **{{<text size="22.0pt" fg="red">}}chromatophores{{</text>}}**. There are{{<text size="14.0pt">}}THOUSANDS{{</text>}}
of these just below the surface of the skin. They each contain a little
 **{{<highlight bg="yellow">}}sac of pigment{{</highlight>}}** and that sac is surrounded by a
 **{{<highlight bg="yellow">}}ring of muscle{{</highlight>}}**. This muscle can contract and relax when instructed to by the nerves that link it to the brain, making the colour more or less visible.

They also use these amazing colour patterns to signal to
 **other octopuses** or to **predators** (more DON"T EAT ME PLEASE type stuff) or to
 **mimic other species** or to do God knows what.

Like look at this one what the actual fuck is he doing.

![Mimic Octopus GIFs | Tenor](mimic_octopus_gifs_tenor.gif)

Now this whole camouflage became something of a conundrum when a lot of studies started to conclude that octopuses were actually **{{<text size="16.0pt" fg="#ED7D31">}}colour blind{{</text>}}**.

This whole debate started in the 1900s with some really dodgy experiments involving coloured lights concluding that they
 *were* colour blind and some REALLY dodgy ones concluding that they *weren't* (like one where this bloke Alfred K hn claimed that if he kept hitting an octopus with a stick after flashing coloured light at it they learned to treat the lights as warnings
 and so swam away --- hmm not very ***ETHICAL*** are we Alfred).

BUT FINALLY when researchers were able to
 **dissect** the eyes of more and more species, things got a little clearer. They could only find{{<text size="14.0pt" fg="red">}}one type of light-detecting protein{{</text>}}and it didn't look ANYTHING like the cone cells that we have for colour vision. By the 1980s this protein, and only this protein, was found in almost
 **all cephalopod species** (that's the taxonomical group that octopuses are in). This was pretty strong evidence for the fact they could only see in black and white.

*SO HOW ON EARTH DO THEY MATCH THEIR ENVIRONMENT SO PERFECTLY?*

Well this is where it gets real weird.

They seem to have molecules called **{{<text size="14.0pt" fg="red">}}opsins{{</text>}}** {{<text size="14.0pt" fg="red">}}{{</text>}}in their skin. As you might have already gleaned (what a fabulous word) from their names --- these molecules are sight molecules. They are light-detecting proteins and they are from the same family
 of molecules that *we have* to detect light in our eyes.

Hmmmm.

That's pretty
{{<text fg="#7030A0">}}funky{{</text>}}.

What gets weirder is that when you shine light on those opsins, the wavelength that they absorb best is that of
{{<highlight bg="aqua">}}blue light{{</highlight>}} (480 nanometres for you snazzy science folk). You know what else absorbs that wavelength best?{{<text size="20.0pt" fg="#00B0F0">}}Chromatophores{{</text>}}. When researchers shone white light on little cuttings of skin from different octopuses they found
 that the chromatophores expanded ***{{<text size="14.0pt">}}SUPER RAPIDLY{{</text>}}*** (turning the sample a nice yellowy colour) and then stayed expanded
 and pulsated rhythmically (a phrase that makes me deeply uncomfortable for some unknown reason). In contrast, in the red light there were slow little rhythmic contractions but{{<text fg="red">}}no chromatophore expansion.{{</text>}}

Here's a video of the research which I very much recommend watching 

<a href="https://www.youtube.com/watch?v=AXGYrCNIJNE&feature=emb_title">![image](d7020d7186fc365e.png)</a>
{{< youtube AXGYrCNIJNE >}}

So could it be that they are somehow able to **{{<text size="16.0pt">}}see with their skin?!{{</text>}}** This is not actually as mad as it might sound, given that most of their neural processing actually happens in their arms instead of in their brains.

But there is one other hypothesis for octopuses might see colour that doesn't have a huge amount of
 *evidential basis* (ugh science and all your evidence) but I think is SO COOL. So basically there is an effect called **{{<highlight size="14.0pt" fg="#00B050" bg="yellow">}}chromatic aberration{{</highlight>}}** where different colours of light come into focus
 at **different** **distances** from a lens. Kind of like this:

![Chromatic aberration - Wikipedia](chromatic_aberration_-_wikipedia.png)

This happens in cameras and sometimes causes what is known in photography as "purple fringing".

![Long Hot Purple Fringe Wig](long_hot_purple_fringe_wig.jpeg)

Nope sorry not like that kind of fringe like this kind of fringe.

![image](8c2cd700445b9950.jpeg)

See how it all goes a bit purple around the horse's head? That's
 *chromatic aberration*.

Now this also happens a little bit in human eyes, but it happens most in pupils that are **{{<text size="14.0pt">}}off-centre{{</text>}}** like in an octopus... This means that
 **MAYBE** the octopuses can distinguish colour *{{<highlight bg="yellow">}}without the need for any colour receptors in their eyes{{</highlight>}}* by bringing different colours in and out of focus all the time. This would basically
 allow them to ***{{<text size="20.0pt">}}choose{{</text>}}*** {{<text size="20.0pt">}}{{</text>}}the colour vision that they were seeing.

HOW COOL.

Now it is important to note that whilst this is "physically true" (like this process is happening in octopuses) whether they
 *actually* use it to see different colours at will is totally unknown. Just a hypothesis.
 **BUT GOD WOULDN"T THAT BE SO COOL**.

Hope you enjoyed some funky octopus facts.

Love Flora xx