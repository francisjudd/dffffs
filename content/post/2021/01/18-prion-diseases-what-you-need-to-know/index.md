+++
author = "Flora Judd"
title = "Understanding Prion Diseases"
date = "2021-01-18"
description = "Exploring the terrifying world of prions and their impact on health."
tags = ["Prions","Kuru","Cannibalism","Neurodegenerative Diseases","Brain Damage","Mad Cow Disease","Creutzfeldt-Jakob Disease","Protein Misfolding","Transmission Risks","Public Health"]
categories = ["Health","Science","Disease Awareness","Neuroscience"]
image = "10_of_the_strangest_medical_conditions_-.jpeg"
+++

Hello and welcome to another week filled with fun and exciting facts.

Today we are going to be looking at a pretty terrifying class of diseases that are called by misfolded proteins called **{{<text size="16.0pt">}}prions{{</text>}}**.

The word prion comes from the word
 **{{<u>}}protein{{</u>}}** and **{{<u>}}infection{{</u>}}** mixed together in a slightly backwards way (although I can see why they decided not to opt for proins that would be a really horrible word).

They were properly discovered in 1957 by two guys called Daniel Carleton
 **Gajdusek** , a virologist, and Vincent **Zigas** , a medical doctor. The disease they were looking at is called
 **kuru** and it was rampant in tribes of Papua New Guinea at the time. These guys noticed that it was mostly women and children who were being killed by this disease and the tribes themselves thought that the disease was being caused by
 **sorcery** or **witchcraft**. They names the disease **{{<text size="18.0pt">}}negi-nagi{{</text>}}** {{<text size="18.0pt">}}{{</text>}}which means "foolish person" because of the disease's symptoms. These include an early period (the ambulant stage --- so named because victims can still walk) where they experience shakes, loss of coordination and difficulty
 pronouncing words. There is then a sedentary stage (where the individual is incapable of walking unaided) that is followed by severe tremors, severe emotional instability, depression, yet uncontrolled and sporadic bouts of laughter. The final and terminal
 stage involves individuals being unable to sit without support, they cannot swallow (known as
 **dysphagia** ) and they lose the ability or will to speak.

![10 of the Strangest Medical Conditions - Rarest Known Diseases](10_of_the_strangest_medical_conditions_-.jpeg)

*It's pretty nasty.*

And with that **hot mess** 
going on of course you would conclude that this disease was being caused by ghosts --- which is what the tribes believed. Other researchers had previously concluded that kuru was mere a **psychosomatic episode** resulting from sorcery practices of the tribal people. Medicine has come a long way since then.

What Gajdusek and Zigas noticed was that the instances of kuru were associated with practices of
 **{{<highlight bg="yellow">}}funerary cannibalism{{</highlight>}}** --- where deceased family members were cooked and eaten because it was though that this would help to free their spirits. It was women and children who ate the brain and this
 is where the infectious prions were most concentrated. It was difficult to clock this initially, because what is perhaps unusual about prion diseases is that they can lie latent for between **{{<text size="16.0pt" fg="red">}}10 and 50 years{{</text>}}** before even a single symptom is seen.

So these guys figured out that something in the consumption of dead relatives was causing transmission of this disease. Mad cow disease and Creutzfeld-Jacob disease are other examples of prion-caused diseases.
 What **{{<highlight size="18.0pt" bg="yellow">}}prions{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}actually are, are proteins that have become folded wrong
 *and* are able to transmit their weirdly folded shape onto the other normal proteins.{{<text fg="#ED7D31">}}Basically --- the chaotic energy just spreads and spreads.{{</text>}}

Prion diseases tend to cause their trouble in the
 **brain** and once they make their way there they basically become **rampaging contagious pathogens** that recruit any other proteins they come into contact with, grouping together into clumps that damage cells and eventually cause the brain itself to
 break down.

Not ideal.

![image](910c7bee63e67128.jpeg)

^^ You can see where it's caused the little "holes" which causes the deterioration and formation of sponge-like tissue in the brain. Mmmm spongey brain.

![Photomicrograph of mouse neurons showing red stained inclusions identified as scrapies prion protein.](photomicrograph_of_mouse_neurons_showing.jpeg)

^^ And here you can see them marked in red on these lovely looking neural cells from a mouse. You can see how they're aggregating (they form the same aggregations --- known as amyloids --- that you get when you
 have Alzheimer's disease) and causing trouble in these cells. This is from a mouse infected with a disease called
 **scrapie**.

What is so **WEIRD** though is that these are
 **PROTEINS** for goodness sake. They're not alive. They don't contain any of the same things that pathogens normally do (no nucleic acids for example --- so no DNA or RNA with which to make copies of themselves). Not only that, but they can survive being **{{<text size="16.0pt">}}boiled{{</text>}}** , treated with **{{<text size="18.0pt">}}disinfectants{{</text>}}** {{<text size="18.0pt">}}{{</text>}}and can still infect other brains after
 **{{<text size="16.0pt">}}years{{</text>}}** {{<text size="16.0pt">}}{{</text>}}of **existing on a surface** (say --- maybe a scalpel). And how ON EARTH do you target a pathogen that's made of the same stuff you are?! You can't! So whilst you can manage the symptoms these diseases are totally untreatable.

So I guess my advice is try not to eat any mammal brains and you should be okay?

Lots of love and hope you're a bit less scared of corona now you know some of the REAL stuff out there that can infect your brain and turn it to mush.

Flora :))))