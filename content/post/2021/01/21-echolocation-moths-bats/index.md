+++
author = "Flora Judd"
title = "Echolocation Evolution"
date = "2021-01-21"
description = "Explore the epic battle of adaptation between bats and moths."
tags = ["Echolocation","Bats","Moths","Evolutionary Adaptations","Counterpredation","Sound Sensitivity","Tymbal Mechanism","Communication Considerations","Sensory Biology","Adaptive Evolution"]
categories = ["Evolution","Animal Behavior","Biodiversity","Neurology"]
image = "1123406259531b3e.png"
+++

Hello!

The beady eyed among you will notice that this one is{{<highlight size="14.0pt" bg="yellow">}}number 11{{</highlight>}}. I"m afraid you haven't all received 11 of these joyous emails, but that's how many I"ve done (the first few were only seen by Clare
 --- soz) and I thought it would be fun to keep track.

So here goes --- numero 11.

Today we are going to be looking at
 **echolocation** --- a very cool evolutionary adaptation. And no --- shockingly enough --- I"m not talking about whales.

*{{<u size="24.0pt">}}gasp{{</u>}}*

I know. I actually talk about things that aren't whales sometimes occasionally 

Anyhoo.

**{{<text size="14.0pt" fg="#00B050">}}ECHOLOCATION{{</text>}}** {{<text size="14.0pt" fg="#00B050">}}{{</text>}}has evolved in a whole bunch of different species (more than you"d think) including bats, shrews, toothed whales ( ),
 oilbirds and swiftlets. Now in terms of evolution what is so cool about echolocation is that it takes auditory sensing from being a
 **passive sense** (where you're just absorbing what's happening around you all sponge like) and turns it into an
 **active sense** where you are actually calling out into your habitat and reading what bounces back.

*{{<text size="20.0pt">}}Pretty {{<u>}}EPIC{{</u>}} if you ask me.{{</text>}}*

Now we all know about bats. They do the
 **cl** ic **kety** clic **ke** ty **b** ee **pity** b **e** epi **n** g noises and when the magical echoes bounce back they have found their yummy moth, snaffled it up and BAM its gone.

But actually echolocation can be a bit challenging because you can only use
 *very high frequency sounds* (we can't hear them that's for sure) at about
 **{{<text fg="red">}}50kHz{{</text>}}**. This means the wavelength is tiny at 0.007m (or 7mm). This is because the echo that bounces back is
 **strongest** when the wavelength is **smaller than the target**. That makes sense given that a lot of what bats eat is very small.

But the moths are starting to fight back.

The **thorax** (like the chesty bit) of a moth is pretty bulky so it's very reflective of noise. That makes it an obvious target of a bat. Therefore, some moths have developed
{{<text fg="red">}}thoracic scales{{</text>}}that act as sound absorbers. These scales are really hair-like and are actually really crazily similar in structure to the kinds of fibrous materials we use in sound insulation.

This is a snazzy diagram showing you what these scales look like:

![image](6e63f59c6efac44d.png)

Mmmm so hairy.

You do not see these scales in diurnal butterflies.

And that's not all!

Lots of moths have evolved
 **{{<text fg="orange">}}ultrasound-sensitive ears{{</text>}}** (isn't that cool?!)
that mean they can hear the echo-locating calls of bats and yeet themselves out of the way. If the calls from the bat are far away the moth flies away from the noise but if the call is very close the moths start to fly very erratically
 to try and avoid being caught. This is a **counterpredation** strategy.

SO --- what has happened now is that some of the bats are calling **{{<text size="14.0pt" fg="#8FAADC">}}extra quietly{{</text>}}** {{<text size="14.0pt" fg="#8FAADC">}}{{</text>}}so that the moths can't hear them. This is a bit shit for the bats because it reduces the range of your echolocation, but if you only want to eat
 **eared moths** (yes that is a thing) then you gotta do what you gotta do. This is essentially a
 *counter* - **counterpredation** strategy.

BUT THE MOTHS FIGHT BACK AGAIN.

(such drama I know)

Tiger moths do a super cool thing. Not only can these bad bois hear the bats but they have also evolved a
{{<highlight-u bg="yellow">}}sound-producing structure{{</highlight-u>}} (it's called a
 **{{<text fg="red">}}tymbal{{</text>}}** {{<text fg="red">}}{{</text>}}which for some reason brings me a lot of joy because I imagine it as a
 **{{<text fg="red">}}t{{</text>}}** iny c **{{<text fg="red">}}ymbal{{</text>}}** but that is definitely not scientifically or etymologically accurate). What this does is it produces sounds at the same frequency that bats do and this has a couple of uses.

![image](1e6f4d5c1e78f905.png)
This is a **tymbal** sorry it doesn't look like a tiny cymbal.


{{<text size="22.0pt">}}#1{{</text>}}(the challenge --- demand satisfaction --- if they apologise no need for further action sorry not sorry) Is to use it like the sound equivalent of being a **{{<text size="14.0pt" fg="#ED7D31">}}b{{</text>}}** **{{<text size="14.0pt" fg="#7030A0">}}r{{</text>}}** **{{<text size="14.0pt" fg="#ED7D31">}}i{{</text>}}** **{{<text size="14.0pt" fg="#7030A0">}}g{{</text>}}** **{{<text size="14.0pt" fg="#ED7D31">}}h{{</text>}}** **{{<text size="14.0pt" fg="#7030A0">}}t{{</text>}}** **{{<text size="14.0pt" fg="#ED7D31">}}l{{</text>}}** **{{<text size="14.0pt" fg="#7030A0">}}y{{</text>}}** **{{<text size="14.0pt" fg="#ED7D31">}}c{{</text>}}** **{{<text size="14.0pt" fg="#7030A0">}}o{{</text>}}** **{{<text size="14.0pt" fg="#ED7D31">}}l{{</text>}}** **{{<text size="14.0pt" fg="#7030A0">}}o{{</text>}}** **{{<text size="14.0pt" fg="#ED7D31">}}u{{</text>}}** **{{<text size="14.0pt" fg="#7030A0">}}r{{</text>}}** **{{<text size="14.0pt" fg="#ED7D31">}}e{{</text>}}** **{{<text size="14.0pt" fg="#7030A0">}}d{{</text>}}** {{<text size="14.0pt" fg="#ED7D31">}}{{</text>}}poisonous bug. A lot of these tiger moth species are toxic, so this acts as a sort of
{{<highlight bg="aqua">}}DON"T EAT ME I TASTE GROSS{{</highlight>}} signal like this poison dart frog.

![Poison Dart Frog Facts 24 Hours Of Culture](poison_dart_frog_facts_24_hours_of_cultu.jpeg)

{{<text size="22.0pt">}}#2{{</text>}}Is to use it to elicit a **startle response** in your predator. Basically wait til the bat is really close and do the equivalent of just shouting AHHHHHHHHHHHHHH in its face to freak it out and then run away (this is how I tend to deal with
 my problems and/or predators)

{{<text size="22.0pt">}}#3{{</text>}}Is my favourite and that is to use it for **{{<text size="16.0pt" fg="#A644D6">}}sonar jamming{{</text>}}** which is done by one species of tiger moth called
 *Bertholdia trigona*. Sadly this does not involve an epic jam sesh between the moth and the bat where they forget their differences and join forces over a mutual love of rock and roll. Actually this is where the moth produces such weird and erratic high
 frequency signals that the bat's echolocation is totally fucked and so it has no idea which what where who why --- MOTH?

Click on the picture below to see this in action --- it is ***SO AWESOME***. If you have the sound on you can hear the sort of "cleaner" high pitched clicks of the bat and then at about 0:07 there's a different sound that comes in that's a bit more scratchy and that's the
 moth I"ll let you watch and see what happens next.

<a href="https://www.youtube.com/watch?v=vrZ2hNZsCuE">![image](1123406259531b3e.png)</a>
{{< youtube vrZ2hNZsCuE >}}

Hope I haven't gone too batty by the next one.

Lots of love to you,

Flora