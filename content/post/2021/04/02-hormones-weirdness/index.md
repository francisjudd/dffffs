+++
author = "Flora Judd"
title = "Hormonal Oddities"
date = "2021-04-02"
description = "Exploring the bizarre effects of hormones on animal behavior."
tags = ["Hormones","Animal Behavior","Androgens","Guinea Pigs","Dogs","Aggression","Sexual Behavior","Experimental Biology","Hormonal Treatments","Behavioral Ecology"]
categories = ["Science","Biology","Animal Behavior","Endocrinology"]
image = "a37e1bd2065d40ea.gif"
+++

Hello you wonderful people and welcome to your *{{<text size="18.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* ,

Today we're keeping things
 **{{<text size="22.0pt" bg="fuchsia">}}brief{{</text>}}** {{<text size="22.0pt">}}{{</text>}}and *{{<text size="20.0pt" fg="#FFC000">}}weird{{</text>}}*.

![11 Cutest Animal GIFs EVER. #9 is my favorite. - Mogul | Panda funny, Cuteanimals, Super cute animals](11_cutest_animal_gifs_ever_9_is_my_favor.gif)

We're talking about ***{{<text size="20.0pt" fg="#92D050">}}hormones{{</text>}}***.

Hormones are pretty cool. They are **{{<text size="16.0pt" bg="fuchsia">}}chemical messengers{{</text>}}** {{<text size="16.0pt">}}{{</text>}}that are secreted into the bloodstream that act as *{{<text size="18.0pt" fg="#2E75B6">}}signals{{</text>}}* {{<text size="18.0pt" fg="#2E75B6">}}{{</text>}}to other cells or tissues.

But{{<text size="18.0pt" fg="#8FAADC">}}how do we
 *know*{{</text>}}what effects certain hormones have on animals? How do we figure out what chemicals actually do what? There's a lot of stuff happening in a body --- especially the more ***{{<text size="18.0pt" fg="#FFC000">}}complicated{{</text>}}*** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}ones like ours.

Well first of all you need to establish a **{{<text size="22.0pt" fg="#0070C0">}}causal link{{</text>}}** {{<text size="22.0pt" fg="#0070C0">}}{{</text>}}(none of that correlation bullshit thank you) between the *{{<text size="16.0pt" fg="#ED7D31">}}presence of the hormone{{</text>}}* {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}and whatever{{<highlight size="18.0pt" bg="yellow">}}behaviour{{</highlight>}}{{<text size="18.0pt">}}{{</text>}}it is that you think it might cause.

![Correlation vs Causation - MStranslate](correlation_vs_causation_-_mstranslate.png)
*(my favourite correlation)*
Well any **{{<text size="20.0pt" fg="#92D050">}}hormonally-dependent behaviour{{</text>}}** {{<text size="20.0pt" fg="#92D050">}}{{</text>}}should {{<u>}}stop{{</u>}} if you either *{{<text size="18.0pt" fg="#385723">}}remove the hormone{{</text>}}* {{<text size="18.0pt" fg="#385723">}}{{</text>}}or ***{{<text size="16.0pt" fg="#E2F0D9" bg="green">}}block its signalling pathway{{</text>}}***.

If you then put the hormone
 *{{<text size="16.0pt" fg="#ED7D31">}}back in{{</text>}}* {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}you should see the behaviour come back. Not only that, but the ***{{<text size="18.0pt" fg="#FFC000">}}amount{{</text>}}*** **{{<text size="18.0pt" fg="#FFC000">}}of hormone{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}you put back in should have an effect --- i.e. if you put{{<u size="14.0pt">}}a lot{{</u>}}{{<text size="14.0pt">}}{{</text>}}of the hormone in you would expect a
{{<u size="16.0pt">}}high probability{{</u>}}{{<text size="16.0pt">}}{{</text>}}of seeing your behaviour.

So what scientists like to do is **{{<text size="18.0pt" fg="#4472C4">}}cut out the gland{{</text>}}** {{<text size="18.0pt" fg="#4472C4">}}{{</text>}}that they think is emitting the hormone (or a *{{<text size="14.0pt" fg="#00B0F0">}}precursor{{</text>}}* {{<text size="14.0pt" fg="#00B0F0">}}of the hormone{{</text>}}) and **{{<text size="18.0pt" fg="#8FAADC">}}inject different doses{{</text>}}** {{<text size="18.0pt" fg="#8FAADC">}}{{</text>}}of the hormone into the animal and see
 *{{<text size="16.0pt" fg="#203864">}}what the fuck happens{{</text>}}*.

![Science Experiment GIFs - Get the best GIF on GIPHY](science_experiment_gifs_-_get_the_best_g.gif)

This has led to some ***{{<text size="16.0pt" fg="#92D050">}}very weird experiments{{</text>}}*** {{<text size="16.0pt" fg="#92D050">}}{{</text>}}involving **{{<text size="18.0pt" bg="fuchsia">}}sex hormones{{</text>}}**.

**{{<text size="22.0pt" fg="#FFC000">}}Androgens{{</text>}}** {{<text size="22.0pt" fg="#FFC000">}}{{</text>}}are *{{<text size="14.0pt" fg="#ED7D31">}}male sex hormones{{</text>}}* {{<text size="14.0pt" fg="#ED7D31">}}{{</text>}}and if you inject them into female **{{<highlight size="20.0pt" bg="yellow">}}guinea pigs{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}while they are still in utero --- things come out a bit odd.

Not only do they **{{<text size="16.0pt" fg="#00B0F0">}}permanently suppress all female sexual behaviours{{</text>}}** {{<text size="16.0pt" fg="#00B0F0">}}{{</text>}}but they will also actually start performing ***{{<text size="18.0pt" fg="#0070C0">}}male sexual behaviours{{</text>}}*** {{<text size="18.0pt" fg="#0070C0">}}{{</text>}}even though they have no way of fertilising a female.

If you do this in **{{<highlight size="22.0pt" bg="yellow">}}dogs{{</highlight>}}** {{<text size="22.0pt">}}{{</text>}}they also become **{{<text size="16.0pt" bg="red">}}masculinised{{</text>}}** {{<text size="16.0pt">}}{{</text>}}in their *{{<text size="18.0pt" fg="#ED7D31">}}bodies{{</text>}}* {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}(their morphology) and in their **{{<text size="20.0pt" fg="#ED7D31">}}behaviour{{</text>}}**.

They become ***{{<text size="18.0pt" fg="#4472C4">}}much more aggressive{{</text>}}*** {{<text size="18.0pt" fg="#4472C4">}}{{</text>}}and **{{<text size="20.0pt" fg="#FFC000">}}socially dominant{{</text>}}** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}(whoop feminisim ish?).

Meaning that won't adhere to the *{{<text size="16.0pt" fg="#92D050">}}normal social dominance hierarchies{{</text>}}* {{<text size="16.0pt" fg="#92D050">}}{{</text>}}of small, stable, social groups of dogs.

Oh and they also develop a
 **{{<text size="20.0pt" bg="fuchsia">}}pseudo penis{{</text>}}**.

Now you might thing Flora --- it's literally a female.
 *It's not going to look like a real penis*. 

![image](a37e1bd2065d40ea.gif)



![DiagramDescription automatically generated](diagramdescription_automatically_generat.png)

***{{<highlight size="24.0pt" bg="yellow">}}BAM{{</highlight>}}*** ***{{<text size="24.0pt">}}{{</text>}}***

Again, they cannot fertilise females (because they themselves are still females) but they *{{<text size="20.0pt" fg="#FBE5D6" bg="purple">}}HAVE A LITERAL PENIS{{</text>}}*.

Hormones are **weird**.

Don't go injecting androgens into your unborn children please.

Love Flora xxx