+++
author = "Flora Judd"
title = "Rabbits and Evolution"
date = "2021-04-30"
description = "Exploring the intriguing evolution of rabbits in Australia against Myxoma virus."
tags = ["Myxoma Virus","Rabbits","Evolutionary Impact","Bacterial Interactions","Population Dynamics","Ecological Impact","Biocontrol Strategies","Species Evolution","Genetic Changes","Wildlife Management"]
categories = ["Evolution","Wildlife","Biocontrol","Ecology"]
image = "darwins_rabbit_is_revealing_how_the_anim.jpeg"
+++

Hello and welcome to your
 *{{<text size="18.0pt" fg="#7030A0">}}Frequent Fun Fact From Flora{{</text>}}* (perhaps more accurate than daily whoops)

TODAY we are discussing a very interesting example of **{{<highlight size="20.0pt" bg="yellow">}}virus-host evolution{{</highlight>}}**.

We set the scene by beginning our story in 1859. *{{<text size="16.0pt" fg="#92D050">}}A Tale of Two Cities{{</text>}}* {{<text size="16.0pt" fg="#92D050">}}{{</text>}}has just been published by Charles Dickens. The chimes of **{{<text size="20.0pt" fg="#FFC000">}}Big Ben{{</text>}}** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}are heard for the first time. The ***{{<text size="20.0pt" fg="#ED7D31">}}Pig and Potato War{{</text>}}*** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}begins (that's a real thing you can look it up).

Well there was another thing that happened and that was that a bloke called **{{<text size="20.0pt" fg="#8FAADC">}}Thomas Austin{{</text>}}** {{<text size="20.0pt" fg="#2F5597">}}{{</text>}}brought *{{<text size="20.0pt" fg="#F4B183">}}24 wild rabbits{{</text>}}* {{<text size="20.0pt" fg="#F4B183">}}{{</text>}}over from England to his house in Southern Victoria, Australia.

![Thomas Austin Net Worth 2018: What is this NFL football ...](thomas_austin_net_worth_2018_what_is_thi.jpeg)

He brought them over for some good ol" sporting shooting fun. But little did he realise that they would breed like well {{<text size="20.0pt" fg="#ED7D31">}}rabbits{{</text>}}.

By **{{<text size="36.0pt" fg="#00B0F0">}}1866{{</text>}}** {{<text size="36.0pt" fg="#00B0F0">}}{{</text>}}(that's only{{<text size="24.0pt" fg="#FFC000">}}SEVEN YEARS LATER{{</text>}}) there were a recorded ***{{<highlight size="72.0pt" bg="yellow">}}14,253{{</highlight>}}*** rabbits that were shot for *{{<text size="20.0pt" fg="#70AD47">}}SPORT ALONE{{</text>}}* {{<text size="20.0pt" fg="#70AD47">}}{{</text>}}on Austin's property.

![Darwin's rabbit is revealing how the animals became immune to myxomatosis |Natural History Museum](darwins_rabbit_is_revealing_how_the_anim.jpeg)

I shit you not --- this was the *{{<text size="18.0pt" fg="#ED7D31">}}fastest spread{{</text>}}* of **{{<text size="24.0pt" fg="#92D050">}}ANY COLONISING MAMMAL{{</text>}}** {{<text size="24.0pt" fg="#92D050">}}{{</text>}}IN THE **{{<text size="72.0pt" fg="#FFC000">}}WORLD{{</text>}}**.

So in 1950, **{{<text size="20.0pt" fg="#FFD966" bg="black">}}myxoma virus{{</text>}}** {{<text fg="#FFD966">}}{{</text>}}was released into Australian rabbits as a
 ***{{<text size="22.0pt">}}biocontrol method{{</text>}}*** {{<text size="22.0pt">}}{{</text>}}to try and reduce the population of these crazy, crazy rabbits.

![image](ba504a3b4464d206.jpeg)
Awww look at them releasing their
 *{{<text bg="red">}}deadly diseased rabbits{{</text>}}*.

This was actually hugely successful and the{{<text size="22.0pt" fg="#B4C7E7" bg="navy">}}population decreased
 *dramatically*{{</text>}}.

Now the myxoma virus can be classified into **{{<text size="18.0pt" fg="#00B0F0">}}five grades{{</text>}}** , with *{{<text size="18.0pt" fg="#0070C0">}}grade one{{</text>}}* {{<text size="18.0pt" fg="#0070C0">}}{{</text>}}being the **{{<highlight size="18.0pt" bg="aqua">}}most deadly{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}and *{{<text size="18.0pt" fg="#0070C0">}}grade five{{</text>}}* {{<text size="18.0pt" fg="#0070C0">}}{{</text>}}being **{{<highlight size="20.0pt" bg="aqua">}}the least deadly{{</highlight>}}**.

The original strain had nearly ***{{<text size="22.0pt" fg="#FFC000">}}100% mortality{{</text>}}*** , but the problem with that is that it actually made it
 *more difficult* for the virus to spread because it was *so good at killing rabbits*.

That meant that when a strain evolved that killed ***{{<text size="24.0pt" fg="#ED7D31">}}70-90%{{</text>}}*** of rabbits it rapidly replaced the first. And then that one was replaced by strains with ***{{<text size="22.0pt" fg="red">}}less than 50%{{</text>}}*** {{<text size="22.0pt" fg="red">}}{{</text>}}mortality. This gave the rabbits more time to mingle and catch the virus off of each other before they died.

![dotroom Hip hip hooray](dotroom_hip_hip_hooray.gif)

*{{<text size="20.0pt" bg="fuchsia">}}Yay for the rabbits{{</text>}}*.
 **Not so yay** for the Australians.

And the rabbits also started to evolve resistance to the viruses. One strain that killed **{{<text size="22.0pt" fg="#92D050">}}90% of rabbits{{</text>}}** {{<text size="22.0pt" fg="#92D050">}}{{</text>}}caught in one location, killed only
 **{{<text size="22.0pt" fg="#FFC000">}}26% of rabbits{{</text>}}** {{<text size="22.0pt" fg="#FFC000">}}{{</text>}}seven years later.

That's really *{{<text size="18.0pt">}}not that many years{{</text>}}*.

Interestingly, this was happening in **{{<text size="22.0pt" fg="#00B0F0">}}totally isolated rabbit populations{{</text>}}** {{<text size="22.0pt" fg="#00B0F0">}}{{</text>}}around Australia. One team of researchers sequenced about *{{<text size="22.0pt" fg="#4472C4">}}20,000 genes{{</text>}}* {{<text size="22.0pt" fg="#4472C4">}}{{</text>}}to get to the bottom of this and found that each population of rabbits had gone through some really ***{{<text size="20.0pt" fg="#7030A0">}}similar genetic changes{{</text>}}***.

![Genetics Firm 23andMe Partners in Parkinson's Research ...](genetics_firm_23andme_partners_in_parkin.gif)

But what is ***{{<text size="20.0pt" fg="#FFC000">}}extra interesting{{</text>}}*** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}is that this wasn't a change in one gene that had a big effect on everything else --- this came from **{{<text size="22.0pt" fg="#ED7D31">}}lots of little changes{{</text>}}** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}in many genes AND these genes are all associated with immunity.

BUT they all happened in
 **{{<highlight size="24.0pt" bg="yellow">}}totally different rabbit populations{{</highlight>}}** which I think is nuts.{{<text size="18.0pt" fg="#92D050">}}Evolution{{</text>}}--- she finds the best ways to solve problems.

This meant that ***{{<text size="18.0pt" fg="red">}}ANOTHER VIRUS{{</text>}}*** {{<text size="18.0pt" fg="red">}}{{</text>}}had to be introduced in the 1990s. Can't catch a break if you're a rabbit I"ll tell you that much.

Hope you enjoyed.

Lots of love Flora xxx

![#scretious | MIT Admissions](scretious_mit_admissions.gif)