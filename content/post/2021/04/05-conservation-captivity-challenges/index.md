+++
author = "Flora Judd"
title = "Captivity's Impact"
date = "2021-04-05"
description = "Exploring the hidden effects of captivity on animal behavior and microbiomes."
tags = ["Captivity","Microbiome","Pandas","Animal Behavior","Gastrointestinal Health","Antibiotic Resistance","Wild Diet","Bamboo","Conservation Efforts","Animal Welfare"]
categories = ["Conservation","Animal Behavior","Science","Ethology"]
image = "worlds_oldest_giant_panda_in_captivity_d.jpeg"
+++

Hello and welcome to your *{{<text size="20.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* ,

Today we are going to have a little look at conservation and some of the problems that arise when we keep animals in captivity for long periods of time.

Some of them are pretty predictable --- species ***{{<text size="14.0pt" fg="#92D050">}}forgetting how to hunt{{</text>}}*** , species not knowing their
{{<highlight size="20.0pt" fg="#2F5597" bg="aqua">}}migratory paths{{</highlight>}}, species becoming *{{<text size="18.0pt" fg="#ED7D31">}}too used to human beings{{</text>}}* , species exhibiting
 **{{<text size="20.0pt" fg="#FFC000">}}altered or distressed behaviours{{</text>}}** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}etc

![This Is Exactly What's Wrong With Raising Baby Animals In ...](this_is_exactly_whats_wrong_with_raising.gif)

Some are less predictable --- like how being in captivity affects their
 **{{<text size="20.0pt" bg="fuchsia">}}microbiome{{</text>}}**.

The microbiome includes the **{{<text size="20.0pt" fg="#ED7D31">}}microbiota{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}(all the living microorganisms) and the **{{<text size="22.0pt" fg="#00B050">}}environment{{</text>}}** {{<text size="22.0pt" fg="#00B050">}}{{</text>}}they are living in. Changing the diets of animals going into captivity changes this.

There is something that my lecturers referred to as the "{{<highlight size="22.0pt" bg="yellow">}}Gastrointestinal Enigma of Pandas{{</highlight>}}" (not quite as magical as it sounds) which is that they have a waaay
 **{{<text size="20.0pt" fg="#FFC000">}}shorter gastrointestinal tract{{</text>}}** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}that you would expect from a herbivore.

![World's oldest giant panda in captivity dies in Hong Kong](worlds_oldest_giant_panda_in_captivity_d.jpeg)

In fact --- even though they *{{<text size="20.0pt" fg="#92D050">}}only eat bamboo{{</text>}}* , their microbiome is more like that of a
 ***{{<text size="20.0pt" fg="#C00000">}}carnivore{{</text>}}***.

Scientists wanted to know what the effect of being in captivity was having on pandas and how it was altering their microbiome. This of course varies according to
 **{{<text size="20.0pt" fg="#9DC3E6">}}what they have been fed{{</text>}}** {{<text size="20.0pt" fg="#9DC3E6">}}{{</text>}}and any *{{<text size="18.0pt" fg="#0070C0">}}antibiotics they"ve been given{{</text>}}*.

And sadly captive pandas were actually found to have a ***{{<text size="18.0pt" bg="lime">}}reduced ability to degrade cellulose{{</text>}}*** , a key structural components of plant matter. We can't digest
 it either, but also rely on bacteria in our gut to do it for us.

This may mean that captive pandas will not be able to go back to a
 **{{<text size="18.0pt" fg="#92D050">}}wild diet{{</text>}}** {{<text size="18.0pt" fg="#92D050">}}{{</text>}}of eating *{{<text size="18.0pt" fg="#00B050">}}solely bamboo{{</text>}}*.

![Panda Bamboo GIF - Panda Bamboo Eating - Discover & Share GIFs](panda_bamboo_gif_-_panda_bamboo_eating_-.gif)

Not only that, but genes for **{{<text size="20.0pt" fg="#FFC000">}}antibiotic resistance{{</text>}}** ,
 **{{<text size="20.0pt" fg="#7030A0">}}virulence{{</text>}}** {{<text size="20.0pt" fg="#7030A0">}}{{</text>}}(i.e. causing illness instead of being a mutualistic bacterium) and **{{<text size="20.0pt" fg="#ED7D31">}}heavy metal tolerance{{</text>}}** {{<text size="20.0pt">}}{{</text>}}(genes that **Charlie** , **Lauren** and **I** have had for years) were much higher in the
 **{{<text size="16.0pt" fg="#A9D18E">}}microbiome of captive pandas{{</text>}}**.

So there is a bit of concern about how these genes would spread if the pandas were
 ***{{<text size="16.0pt" fg="#ED7D31">}}released back into the wild{{</text>}}***.

What a useless animal. At least they're cute.

![Baby pandas doing what baby pandas do best - Meme Guy](baby_pandas_doing_what_baby_pandas_do_be.gif)

Lots of love,

Flora xx

*{{<highlight size="16.0pt" bg="yellow">}}DON"T TAKE MY BALL{{</highlight>}}* *{{<text size="16.0pt">}}{{</text>}}*
![I love my ball this much | Animal gifs, Cute animals, Cute ...](i_love_my_ball_this_much_animal_gifs_cut.gif)