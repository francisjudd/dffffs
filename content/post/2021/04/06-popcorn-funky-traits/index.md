+++
author = "Flora Judd"
title = "Popcorn's Funky Traits"
date = "2021-04-06"
description = "Exploring the quirky evolution of popcorn through selective breeding."
tags = ["Popcorn","Maize","Selectivity","Traits Evolution","Kernel Characteristics","Snacks","Agricultural Practices","Culinary Uses","Cultural History","Food Science"]
categories = ["Food Science","Agriculture","History","Culinary Arts"]
image = "51c01c3082865ddf.png"
+++

Hello and welcome to your shortest *{{<text size="14.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* {{<text size="14.0pt" fg="#7030A0">}}{{</text>}}yet,

Popcorn is an example of weird humans selecting *{{<text size="20.0pt" fg="#4472C4">}}funky traits{{</text>}}* in the food we want to eat.

We selected for a *{{<text size="20.0pt" fg="#FFC000">}}toughened pericarp{{</text>}}* {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}(that's the outer coating) of maize and that led to the evolution of **{{<highlight size="22.0pt" bg="yellow">}}"popcorn"{{</highlight>}}** {{<text size="22.0pt">}}{{</text>}}varieties.

![image](51c01c3082865ddf.png)

The outer casing is so hard that when the
 **kernel is heated** , the force of ***{{<text size="24.0pt" fg="#FFC000" bg="red">}}explosion{{</text>}}*** {{<text size="24.0pt" fg="#FFC000">}}{{</text>}}is so much greater that the corn "pops".

![Popcorn Popping GIF - Popcorn Popping Eating - Discover ...](popcorn_popping_gif_-_popcorn_popping_ea.gif)

Snack facts **{{<text size="22.0pt" fg="#ED7D31">}}Snackts{{</text>}}** ?

Lots of love,

Flora