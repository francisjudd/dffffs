+++
author = "Flora Judd"
title = "Prairie Dog Engineering"
date = "2021-04-08"
description = "Exploring prairie dog burrow designs and their evolutionary advantages."
tags = ["Prairie Dogs","Burrow Engineering","Adaptive Behavior","Natural Selection","Airflow Management","Animal Architecture","Bernoulli Effect","Ventilation Strategies","Survival Tactics","Ecosystem Impact"]
categories = ["Biology","Animal Behavior","Natural Selection","Ecology"]
image = "asymmetric_burrow_openings_create_passiv.jpeg"
+++

Hello and welcome to your *{{<text size="22.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}*

In biology we love to wonder
 ***{{<text size="18.0pt" fg="#ED7D31">}}why animals do shit{{</text>}}***.

How is an animal's behaviour **{{<text size="20.0pt" fg="#9DC3E6">}}adaptive{{</text>}}** ? What
 **{{<text size="16.0pt" fg="#70AD47">}}fitness advantage{{</text>}}** {{<text size="16.0pt" fg="#70AD47">}}{{</text>}}is it giving them?

Well one way that you can figure that out is to ask one of your
 *{{<text size="18.0pt" fg="#FFC000">}}engineer friends{{</text>}}* {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}(perhaps a Lucas or a Francis) to **{{<text size="18.0pt" fg="#ED7D31">}}design{{</text>}}** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}an animal body or behaviour so that is {{<text size="22.0pt" fg="#00B0F0">}}optimises a certain criterion{{</text>}}.

*(also stock images of engineers are hilarious please enjoy)*
![Happy engineer Stock Photo Vitaly.R #25550977](happy_engineer_stock_photo_vitalyr_25550.jpeg)![Stressed Construction Engineer Stock Image I3665994 at ...](stressed_construction_engineer_stock_ima.jpeg)
![Engineer Eating While Work Stock Photo - Download Image ...](engineer_eating_while_work_stock_photo_-.jpeg)![Young computer engineer stock photo. Image of disk, human ...](young_computer_engineer_stock_photo_imag.jpeg)


You might want to know the body that *{{<text size="20.0pt" fg="#9DC3E6">}}moves fastest through water{{</text>}}* {{<text size="20.0pt" fg="#9DC3E6">}}{{</text>}}or {{<highlight size="18.0pt" bg="yellow">}}obtains the most amount of energy{{</highlight>}}.

You then **{{<text size="18.0pt" fg="#ED7D31">}}compare{{</text>}}** that to the
 *real* animal and how closely matched they are. If the match is close then you can basically see that
 ***{{<text size="20.0pt" fg="#92D050">}}natural selection{{</text>}}*** {{<text size="20.0pt" fg="#92D050">}}{{</text>}}has "designed" the animals to match that criterion.

We are going to have a quick look at **{{<text size="24.0pt" bg="fuchsia">}}prairie dogs{{</text>}}**.

![Steve Prairie Dog GIF - Steve PrairieDog - Discover ...](steve_prairie_dog_gif_-_steve_prairiedog.gif)

Now they live in *{{<text size="20.0pt" fg="#BF9000">}}burrows{{</text>}}* {{<text size="20.0pt" fg="#BF9000">}}{{</text>}}that have two "exits" to the surface. But what they do that is a bit odd is that they build
 **{{<text size="20.0pt" fg="#A9D18E">}}different shaped mounds{{</text>}}** {{<text size="20.0pt" fg="#A9D18E">}}{{</text>}}at each end. One end has two *{{<text size="20.0pt" fg="#8FAADC">}}tall cone shaped{{</text>}}* {{<text size="20.0pt" fg="#8FAADC">}}{{</text>}}mounds end and the other has two *{{<text size="20.0pt" fg="#2E75B6">}}smaller rounded mounds{{</text>}}*.

![Prairie Dog Looking Out From Its Burrow Photograph by Tony ...](prairie_dog_looking_out_from_its_burrow_.jpeg)

Now the **{{<highlight size="20.0pt" bg="yellow">}}adaptive hypothesis{{</highlight>}}** {{<text size="20.0pt">}}{{</text>}}for why this would be is that the mounds are somehow allowing for *{{<text size="22.0pt" fg="#DAE3F3" bg="blue">}}airflow{{</text>}}* {{<text size="22.0pt" fg="#DAE3F3">}}{{</text>}}through their burrows. Without airflow these guys would **{{<text size="20.0pt">}}suffocate{{</text>}}** {{<text size="20.0pt">}}{{</text>}}so it is pretty important that they have means of allowing for **{{<text size="22.0pt" fg="#9DC3E6">}}good ventilation{{</text>}}**.

![Asymmetric burrow openings create passive ventilation ...](asymmetric_burrow_openings_create_passiv.jpeg)

So this group of researchers got an engineer to optimise the air flow through a tunnel and
 *{{<text size="16.0pt">}}guess what{{</text>}}*.

The model showed a burrow with a {{<highlight size="22.0pt" bg="aqua">}}higher, cone-shaped mounds at one end{{</highlight>}}, because this burrow will have air flowing through it even with
 *{{<text size="24.0pt" fg="#00B0F0">}}no surface wind{{</text>}}*.

![A picture containing watch, antennaDescription automatically generated](a_picture_containing_watch_antennadescri.png)

This is something known as the **{{<text size="22.0pt" fg="#FFC000">}}Bernoulli effect{{</text>}}**.

And I just think it's pretty nuts that these guys have been doing this for
 **{{<text size="20.0pt" fg="#ED7D31">}}millennia{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}and we figured it out a couple of ***{{<text size="20.0pt" fg="#F4B183">}}hundred years ago{{</text>}}***.

*{{<text size="18.0pt" fg="#A9D18E">}}Nature is damn smart{{</text>}}*.

Lots of love,

Flora

( *this is for you Clare* )
![Calgary-bound - Ampersand Inc.](calgary-bound_-_ampersand_inc.gif)