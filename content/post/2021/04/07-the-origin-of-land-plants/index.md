+++
author = "Flora Judd"
title = "The Rise of Land Plants"
date = "2021-04-07"
description = "Explore the evolutionary journey of land plants and their adaptations."
tags = ["Land Plants","Evolutionary Adaptations","Photosynthesis","UV Radiation","Desiccation Resistance","Gravity Effects","Rhynie Chert","Vascular Systems","Plant Ecology","Biodiversity"]
categories = ["Botany","Evolution","Nature","Plant Science"]
image = "the_rhynie_chert_flora.jpeg"
+++

Hello and welcome to your ***{{<text size="16.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}*** **

to ***{{<text size="22.0pt" fg="#00B050" bg="lime">}}Wednesday's What Was the World Doing Ages and Ages Ago{{</text>}}***

So let's see where we are in our calendar year 

We saw ***{{<highlight size="20.0pt" bg="yellow">}}life begin{{</highlight>}}*** {{<text size="20.0pt">}}{{</text>}}on the **{{<text size="26.0pt" fg="#4472C4">}}25th February{{</text>}}**.

*{{<text size="22.0pt" bg="lime">}}Photosynthesis started{{</text>}}* {{<text size="22.0pt">}}{{</text>}}on the **{{<text size="22.0pt" fg="#4472C4">}}28thMarch{{</text>}}**.

***{{<text size="22.0pt" bg="fuchsia">}}Multicellular organisms{{</text>}}*** {{<text size="22.0pt">}}{{</text>}}appeared on the **{{<text size="22.0pt" fg="#4472C4">}}16th August{{</text>}}**.

**{{<text size="26.0pt" fg="red" bg="maroon">}}Sexual reproduction{{</text>}}** {{<text size="26.0pt" fg="red">}}{{</text>}}came about on the **{{<text size="22.0pt" fg="#0070C0">}}17thSeptember{{</text>}}**.

{{<text size="24.0pt" bg="olive">}}Fungi{{</text>}}{{<text size="24.0pt">}}{{</text>}}starting being funky on the **{{<text size="22.0pt" fg="#0070C0">}}15thNovember{{</text>}}**.

**{{<highlight-u size="28.0pt" fg="#0070C0" bg="aqua">}}Fish{{</highlight-u>}}** {{<text size="28.0pt">}}{{</text>}}came onto the scene on the **{{<text size="22.0pt" fg="#0070C0">}}20thNovember{{</text>}}**.

And today we see the origin of
 ***{{<text size="24.0pt" fg="#A9D18E" bg="green">}}land plants{{</text>}}*** {{<text size="24.0pt" fg="#A9D18E">}}{{</text>}}on the **{{<text size="22.0pt" fg="#0070C0">}}22ndNovember{{</text>}}**.


Rather amazingly we can see
 ***{{<text size="20.0pt" fg="#FFC000">}}whole complex terrestrial ecosystems{{</text>}}*** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}evolving in some **{{<text size="16.0pt" bg="red">}}volcanic systems{{</text>}}** {{<text size="16.0pt">}}{{</text>}} *{{<text size="18.0pt" fg="#ED7D31">}}407 million years ago{{</text>}}*. This is because the volcanoes produce waters very rich in silica explosively and periodically. This essentially preserves the **{{<text size="20.0pt" fg="#C55A11">}}whole ecosystem{{</text>}}** {{<text size="20.0pt" fg="#C55A11">}}{{</text>}}in situ.

![The Rhynie Chert Flora](the_rhynie_chert_flora.jpeg)
*This is what they look like*

These Rhynie chert plants were
{{<text size="22.0pt" fg="#4472C4">}}leafless{{</text>}}and had *{{<text size="18.0pt" fg="#0070C0">}}naked shoots{{</text>}}* (gasp). They did have a vascular system (but not all land plants do!!) --- remember those good ol" xylem and phloem? They had **{{<text size="20.0pt" fg="#8FAADC">}}no true roots{{</text>}}**.

INSTEAD they had{{<text size="18.0pt" fg="#00B050">}}horizontal growths{{</text>}}with little, shallow, filamentous things called
 ***{{<highlight size="20.0pt" bg="yellow">}}rhizoids{{</highlight>}}***.

![Rhynie-Chert-Flora](rhynie-chert-flora.jpeg)

Now there were **{{<text size="20.0pt" fg="#A9D18E">}}many challenges{{</text>}}** for these poor
 little land plants to overcome.

**{{<highlight size="22.0pt" bg="yellow">}}UV radiation{{</highlight>}}**

If you live **{{<text size="16.0pt" fg="#00B0F0">}}underwater{{</text>}}** {{<text size="16.0pt" fg="#00B0F0">}}{{</text>}}you are largely protected from UV radiation. If you live on land --- not so much. And we all know that ***{{<text size="20.0pt" fg="#0070C0">}}too much UV is bad{{</text>}}*** {{<text size="20.0pt" fg="#0070C0">}}{{</text>}}for you and has the potential to do all kinds of DNA and protein damage. Not ideal.

 SO what did they do?! I hear you cry.

They evolved the ability to turn certain ***{{<text size="18.0pt" fg="#ED7D31">}}primary metabolites{{</text>}}*** {{<text size="18.0pt" fg="#ED7D31">}}{{</text>}}(your amino acids and sugars) into ***{{<text size="20.0pt" fg="#FFC000">}}secondary products{{</text>}}*** {{<text size="20.0pt" fg="#FFC000">}}{{</text>}}(your alkaloids and aromatic compounds) that had{{<u size="18.0pt" fg="red">}}UV protective qualities{{</u>}}. Accumulating these in the leaves made sure that they stayed safe.

Better than suncream I"ll tell you that for sure.

![Is this plant INTELLIGENT? Tropical fern found to 'learn ...](is_this_plant_intelligent_tropical_fern_.gif)
( *maybe just hide from the sun?* )

{{<highlight size="20.0pt" bg="aqua">}}Desiccation and evaporation{{</highlight>}}

*LAND IS REALLY DRY.*

![Goofy Goober GIF - Dry Driedout Spongebob - Discover ...](goofy_goober_gif_-_dry_driedout_spongebo.gif)

If you are plant that has lived your whole life underwater that is going to be a bit of a shock. The structures that you need to let CO2 in and out will also let water out... and that
 water is going to leave *{{<text size="18.0pt" fg="#A9D18E">}}A WHOLE LOT FASTER{{</text>}}* {{<text size="18.0pt" fg="#A9D18E">}}{{</text>}}than the CO2 is coming in.

This led to some good **{{<highlight size="18.0pt" fg="#0070C0" bg="aqua">}}waterproofing traits{{</highlight>}}** {{<text size="18.0pt" fg="#0070C0">}}{{</text>}}like the ***{{<text size="24.0pt" fg="#9DC3E6">}}cuticle{{</text>}}***. This is a{{<text size="16.0pt">}}waxy layer{{</text>}}that is pretty much{{<u size="20.0pt">}}impermeable{{</u>}}, trapping water inside (and outside like this picture) and also reducing
{{<text bg="lime">}}microbial attack{{</text>}}.

![Beyond the Human Eye: Plant Cuticles](beyond_the_human_eye_plant_cuticles.jpeg)

They also evolved some handy structures called **{{<text size="20.0pt" fg="#ED7D31">}}stomata{{</text>}}** {{<text size="20.0pt" fg="#ED7D31">}}{{</text>}}that can open and close to control the rates at which gases are coming in and out of the plant. This is what they look like close up.

![#116 Homeostasis in plants | Biology Notes for A level](116_homeostasis_in_plants_biology_notes_.jpeg)



{{<text size="22.0pt" bg="fuchsia">}}Gravity{{</text>}}

*{{<text size="18.0pt" fg="#FFC000">}}Another bloody nightmare{{</text>}}*. Trying to get water to the top of you is SO DIFFICULT, especially when you used to be bathed in the
 stuff.

But here on land it is also super helpful to be tall because it means you get the *{{<highlight size="22.0pt" bg="yellow">}}sunlight{{</highlight>}}* {{<text size="22.0pt">}}{{</text>}}that all the plants want.

![Freedom Grow GIF by ADWEEK - Find & Share on GIPHY](freedom_grow_gif_by_adweek_-_find_share_.gif)

This meant that the walls of the xylem were reinforced with ***{{<text size="22.0pt" fg="#A9D18E" bg="green">}}lignin{{</text>}}*** {{<text size="22.0pt" fg="#A9D18E">}}{{</text>}}to make them **{{<text size="18.0pt" fg="#ED7D31">}}stronger{{</text>}}**. This allowed water to get all the way to the top of the plant.

Important to note! Not all land plants evolved vascular systems --- like{{<text size="18.0pt" fg="#70AD47">}}mosses{{</text>}} but they can't exactly grow very tall can they.

**{{<text size="20.0pt" bg="teal">}}Roots{{</text>}}** {{<text size="20.0pt">}}{{</text>}}were also pretty useful here as they stopped the now tall plants from *{{<text size="18.0pt" fg="#5B9BD5">}}falling over{{</text>}}*. Whilst also allowing for **{{<text size="16.0pt" fg="#4472C4">}}more nutrients to be extracted{{</text>}}** {{<text size="16.0pt" fg="#4472C4">}}{{</text>}}from the soil.

![ How engineered roots could help fight climate change](_how_engineered_roots_could_help_fight_c.gif)

By about ***{{<text size="18.0pt" fg="#A9D18E">}}375 million years ago{{</text>}}*** {{<text size="18.0pt" fg="#A9D18E">}}{{</text>}}plants had root-like structures that reached almost **{{<highlight size="18.0pt" bg="yellow">}}a metre{{</highlight>}}** {{<text size="18.0pt">}}{{</text>}}down into the soil.

And look how well plants have done since overcoming all of these tricksome problems.

Blows the mind.

I"m gonna love you and{{<text size="16.0pt" fg="#70AD47">}}leaf{{</text>}}you,

Flora xxx