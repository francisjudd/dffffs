+++
author = "Flora Judd"
title = "Birds on the Move"
date = "2021-04-12"
description = "Exploring how researchers teach captive-born birds to migrate."
tags = ["Northern Bald Ibis","Captive Breeding","Migration Tactics","Research Techniques","Robotic Kites","Wildlife Conservation","Behavioral Studies","Adaptation","Species Recovery","Conservation Strategies"]
categories = ["Animals","Conservation","Birds","Migratory Behavior"]
image = "human-led_migration_flight_with_a_group_.png"
+++

Hello and welcome to today's
 ***{{<text size="20.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}*** ,

*Quick note* --- these facts are going to take a bit of a break while I prepare for my exams (big eek). There might be a couple here and there but I"m afraid not with
 the same consistency as before. It will be sporadic and therefore more exciting ? Let's go with that.

Today we are having another look at **{{<text size="18.0pt" fg="#ED7D31">}}weird shit that happens when you keep animals in captivity for too long{{</text>}}**.

Last time it was *{{<text size="16.0pt" bg="fuchsia">}}The Gastrointestinal Enigma of Pandas{{</text>}}* {{<text size="16.0pt">}}{{</text>}}(the definite article needed to be capitalised) and today it is **{{<highlight size="16.0pt" bg="yellow">}}The Questionable Migration Tactics of the Northern Bald Ibis{{</highlight>}}** okay I made that one up but you get the point.

Now the northern bald ibis is a *{{<text size="16.0pt" fg="#70AD47">}}freaky lookin bird{{</text>}}* {{<text size="16.0pt" fg="#70AD47">}}{{</text>}}(a face only a mother could love I"m afraid) and it looks like this.

![Northern Bald Ibis l Threatened Bird - Our Breathing Planet](northern_bald_ibis_l_threatened_bird_-_o.png)

They are not doing so well and so **{{<text size="18.0pt" fg="#00B0F0">}}captive breeding programmes{{</text>}}** {{<text size="18.0pt" fg="#00B0F0">}}{{</text>}}have been set up in Austria where they live. But you see they normally *{{<text size="20.0pt" fg="#4472C4">}}migrate{{</text>}}* {{<text size="20.0pt" fg="#4472C4">}}{{</text>}}from **{{<text size="20.0pt" fg="#8FAADC">}}Austria to Italy{{</text>}}**.

So when you want to release the birds from captivity you have a bit of a problem because they normally learn that *{{<text size="18.0pt" fg="#FFC000">}}migratory route{{</text>}}* {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}from their **{{<text size="20.0pt" fg="#ED7D31">}}parents{{</text>}}** but what do you do if their parents were **{{<text size="24.0pt" fg="#70AD47">}}raised in captivity too{{</text>}}** ?

How are they going to learn the migratory route?

Bit of a **{{<text size="14.0pt" fg="#002060">}}conundrum{{</text>}}** {{<text size="14.0pt" fg="#002060">}}{{</text>}}really.

![June 6 - in pictures](june_6_-_in_pictures.jpeg)
*How will I learn!?!*

Well the researchers weren't going to give up in the face of a **{{<text size="18.0pt" fg="#00B0F0">}}challenge{{</text>}}**.

![Human-led migration flight with a group of juvenile human ...](human-led_migration_flight_with_a_group_.png)

They decided to ***{{<text size="26.0pt" fg="#BDD7EE" bg="blue">}}fly with them{{</text>}}*** !!

So the researchers put some **{{<text size="20.0pt" fg="#5B9BD5">}}robots in a kite{{</text>}}** {{<text size="20.0pt" fg="#5B9BD5">}}{{</text>}}and flew the birds from Austria to Italy.

And it looks like they had a{{<highlight size="22.0pt" bg="aqua">}}great time{{</highlight>}}.

![Reversing local extinction: scientists bring the northern ...](reversing_local_extinction_scientists_br.jpeg)

*{{<text size="20.0pt" fg="#ED7D31">}}It's never too late to teach an old bird new migratory routes{{</text>}}*.

Hope you"ve had a good Monday.

Love Flora xx

Don't worry --- I didn't forget. Here's the **GIF**.

![Turtle pushing turtle | HilariousGifs.com](turtle_pushing_turtle_hilariousgifscom.gif)
*genuine footage of me and Clare at the pool (guess which one is which)*