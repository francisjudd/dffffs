+++
author = "Flora Judd"
title = "Cane Toads and Evolution"
date = "2021-04-01"
description = "Exploring how cane toads are reshaping snake evolution in Australia."
tags = ["Cane Toads","Evolutionary Impact","Selective Pressure","Predation","Adaptation","Australian Black Snake","Invasive Species","Ecosystem Dynamics","Body Size","Snake Evolution"]
categories = ["Evolution","Wildlife","Ecology","Invasive Species"]
image = "filered-bellied_black_snake_pseudechis_p.jpeg"
+++

Hello and welcome to your *{{<text size="18.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}* ,

Today we are going to have a look at the effect that us **{{<text size="16.0pt" fg="#ED7D31">}}lovely humans{{</text>}}** {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}are having on the natural world and some of the ways that we have *{{<text size="16.0pt" fg="#92D050">}}changed the course of evolution{{</text>}}* {{<text size="16.0pt" fg="#92D050">}}{{</text>}}without meaning to.

***{{<text size="16.0pt" fg="#4472C4">}}Cane toads{{</text>}}*** {{<text size="16.0pt" fg="#4472C4">}}{{</text>}}were introduced in Australia to try and counteract the **{{<text size="18.0pt" fg="#FFC000">}}cane beetles{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}that were munching through all of Australia's fabulous sugarcane. It had turned out pretty well in Puerto Rico so they took
 **{{<text size="16.0pt" fg="#70AD47">}}102 cane toads{{</text>}}** {{<text size="16.0pt" fg="#70AD47">}}{{</text>}}from Hawaii and introduced them into cane fields.

![425-Pound Fattt Fuccc Attempts 2 Kidnap 10-Year-Old - Life ...](425-pound_fattt_fuccc_attempts_2_kidnap_.gif)


There are now over ***{{<highlight size="22.0pt" fg="#ED7D31" bg="yellow">}}200 million{{</highlight>}}*** {{<text size="22.0pt" fg="#ED7D31">}}{{</text>}}of them.

Whoopsies.

Now these cane toads are **{{<text size="18.0pt" bg="fuchsia">}}extremely poisonous{{</text>}}** , which was one of the reasons why they were so successful at spreading. Nothing had evolved resistance to
 eat them.

This has had some pretty interesting effects in driving evolution other species around them. Especially in a couple of species of
 ***{{<text size="18.0pt" fg="white" bg="black">}}Australian black snake{{</text>}}*** {{<text size="18.0pt" fg="white">}}{{</text>}}( *Pseudechis porphyriacus* and *Dendrelaphis punctulatus* if you're feeling fancy).

![File:Red-bellied Black Snake (Pseudechis porphyriacus ...](filered-bellied_black_snake_pseudechis_p.jpeg)

Now it's not what you think --- the snakes are *{{<text size="16.0pt" fg="#4472C4">}}not gaining resistance{{</text>}}* {{<text size="16.0pt" fg="#4472C4">}}{{</text>}}to the cane toad's toxins. Oh no. Instead they are showing a remarkably rapid
 **{{<text size="16.0pt" fg="#5B9BD5">}}increase in body size{{</text>}}** {{<text size="16.0pt" fg="#5B9BD5">}}{{</text>}}(now that's a mood) accompanied by a **{{<text size="18.0pt" fg="#0070C0">}}decrease in relative head size{{</text>}}**.

*{{<highlight size="14.0pt" bg="yellow">}}What is going on here?{{</highlight>}}* *{{<text size="14.0pt">}}{{</text>}}*

This change was seen in **{{<text size="16.0pt" fg="#ED7D31">}}fewer than 23 snake generations{{</text>}}** {{<text size="16.0pt" fg="#ED7D31">}}{{</text>}}and for evolution that is *{{<text size="18.0pt" fg="#FFC000">}}RAPID{{</text>}}* {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}so it must be some quite *{{<text size="16.0pt" fg="#92D050">}}strong selective pressures{{</text>}}* {{<text size="16.0pt" fg="#92D050">}}{{</text>}}that the snakes are experiencing.

Now these cane toads, as I mentioned, are **{{<text size="16.0pt" fg="#0070C0">}}VERY toxic{{</text>}}** {{<text size="16.0pt" fg="#0070C0">}}{{</text>}}and so a mature can toad will likely kill any snake that tries to eat one. A juvenile might not kill them, but it isn't going to be pleasant.

Interestingly, these snakes are known as **{{<text size="18.0pt" fg="#FFC000">}}gape-limited predators{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}which literally means they will only not eat things because they are simply
 *{{<text size="16.0pt" fg="#00B0F0">}}too big to fit in their mouths{{</text>}}*.

![snake eating toad | Nature at Its Best | Pinterest | Snake ...](snake_eating_toad_nature_at_its_best_pin.jpeg)
Bit ambitious there Larry.

Therefore, the smaller their heads are, the *{{<highlight size="16.0pt" bg="yellow">}}less likely they are to eat a toad that is big enough to kill them{{</highlight>}}*.

And on top of THAT a bigger body size helps too, as it takes a
 *{{<text size="18.0pt" bg="fuchsia">}}larger amount of toxin to kill a larger snake{{</text>}}*.

I love this **{{<text size="18.0pt" fg="#FFC000">}}weird reverse evolution{{</text>}}** {{<text size="18.0pt" fg="#FFC000">}}{{</text>}}of not evolving to be able to cope with eating the poisonous thing but instead evolving a body that doesn't fit the poisonous thing inside.

And the scientists involved compared these snakes to another two species that were
 **{{<text size="18.0pt" fg="#70AD47">}}not exposed to toads{{</text>}}** ( *Hemiaspis signata* and *Tropidonophis mairii* ) and they showed
 *{{<text size="16.0pt" fg="#00B0F0">}}NO TRENDS{{</text>}}* {{<text size="16.0pt" fg="#00B0F0">}}{{</text>}}in either of the traits.

This makes us pretty darn sure that the changes we are seeing in those other snakes are a form of
 ***{{<highlight size="18.0pt" bg="aqua">}}adaptation to the toxic cane toads{{</highlight>}}***.

Pretty neat, eh?

If you"d like to learn more here's a highly scientific and informative film you can watch
![Cane Toads: The Conquest - Alchetron, the free social ...](cane_toads_the_conquest_-_alchetron_the_.jpeg)

Thank goodness they included that warning at the bottom. If you can't read it, it says
**{{<highlight bg="yellow">}}WARNING: LICKING THIS TOAD CAN BE HAZARDOUS TO YOUR HEALTH{{</highlight>}}.**

Wow. What scientific content.

Lots of love,

Flora