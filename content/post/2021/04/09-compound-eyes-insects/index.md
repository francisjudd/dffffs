+++
author = "Flora Judd"
title = "Insect Vision Uncovered"
date = "2021-04-09"
description = "Exploring the fascinating world of insect compound eyes and their unique characteristics."
tags = ["Compound Eyes","Insects","Ommatidia","Vision Systems","Light Polarization","Field of Vision","Photoreceptors","Insect Biology","Diffractive Optics","Adaptations"]
categories = ["Insects","Science","Biology","Vision Studies"]
image = "compound_eye_taken_with_an_electron_micr.jpeg"
+++

Hello and welcome to your *{{<text size="22.0pt" fg="#7030A0">}}Daily Fun Fact From Flora{{</text>}}*
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}Today we are going to have a look at the{{</text>}} ***{{<text size="22.0pt" fg="#ED7D31" bg="white">}}compound eyes{{</text>}}*** {{<text size="22.0pt" fg="#ED7D31" bg="white">}}{{</text>}}{{<text fg="#202122" bg="white">}}of insects.{{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}Now they look like this{{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
![Seeing With Insect Eyes | A Moment of Science - Indiana Public Media](seeing_with_insect_eyes_a_moment_of_scie.jpeg)
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}(very cool){{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}And they are made up of{{</text>}} *{{<text size="18.0pt" fg="#FFC000" bg="white">}}thousands{{</text>}}* {{<text size="18.0pt" fg="#FFC000" bg="white">}}{{</text>}}{{<text fg="#202122" bg="white">}}of little individual photoreceptors that are called{{</text>}} **{{<highlight size="24.0pt" fg="#202122" bg="yellow">}}ommatidia{{</highlight>}}** {{<text fg="#202122" bg="white">}}.{{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
![Compound Eye | Taken with an electron microscope. | St Stev | Flickr](compound_eye_taken_with_an_electron_micr.jpeg)
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}The input from of these little units, the ommatidia, is{{</text>}} **{{<text size="22.0pt" fg="#A9D18E" bg="white">}}combined{{</text>}}** {{<text size="22.0pt" fg="#A9D18E" bg="white">}}{{</text>}}{{<text fg="#202122" bg="white">}}to form an{{</text>}} ***{{<text size="22.0pt" fg="#F4B183" bg="white">}}image{{</text>}}*** {{<text fg="#202122" bg="white">}}.
 Because the insect eye is{{</text>}}{{<text size="22.0pt" fg="#548235" bg="white">}}convex{{</text>}}{{<text fg="#202122" bg="white">}}it means that they all point in slightly different directions, giving the insect a pretty{{</text>}} *{{<text size="20.0pt" fg="#0070C0" bg="white">}}wide field{{</text>}}* {{<text size="20.0pt" fg="#0070C0" bg="white">}}{{</text>}}{{<text fg="#202122" bg="white">}}of vision.{{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}These eyes can even detect the{{</text>}} **{{<text size="20.0pt" fg="#8FAADC" bg="white">}}polarisation of light{{</text>}}** {{<text size="20.0pt" fg="#8FAADC" bg="white">}}{{</text>}}{{<text fg="#202122" bg="white">}}in some cases (but that's another day's fact).{{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}Now because the{{</text>}} ***{{<text size="22.0pt" fg="#0070C0" bg="white">}}lenses are so small{{</text>}}*** {{<text fg="#202122" bg="white">}}, the effects of{{</text>}}{{<text size="20.0pt" fg="#202122" bg="fuchsia">}}diffraction{{</text>}}{{<text size="20.0pt" fg="#202122" bg="white">}}{{</text>}}{{<text fg="#202122" bg="white">}}mean that the image that is produced does not have very{{</text>}} **{{<text size="20.0pt" fg="#00B0F0" bg="white">}}high resolution{{</text>}}** {{<text fg="#202122" bg="white">}}. This means that it is not very good at making out fine detail.{{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}This can only be countered by increasing the{{</text>}} **{{<text size="24.0pt" fg="#FFC000" bg="white">}}size{{</text>}}** {{<text size="24.0pt" fg="#FFC000" bg="white">}}{{</text>}}{{<text fg="#202122" bg="white">}}and the{{</text>}} **{{<text size="24.0pt" fg="#FFC000" bg="white">}}number{{</text>}}** {{<text size="24.0pt" fg="#FFC000" bg="white">}}{{</text>}}{{<text fg="#202122" bg="white">}}of these ommatidia which can happen but only as far the eye can be physiologically maintained. It wouldn't be very useful having eyes that had a{{</text>}} *{{<text size="18.0pt" fg="#ED7D31" bg="white">}}fabulous resolution{{</text>}}* {{<text size="18.0pt" fg="#202122" bg="white">}}{{</text>}}{{<text fg="#202122" bg="white">}}but were{{</text>}} **{{<text size="22.0pt" fg="#92D050" bg="white">}}so massive{{</text>}}** {{<text size="22.0pt" fg="#92D050" bg="white">}}{{</text>}}{{<text fg="#202122" bg="white">}}that you couldn't{{</text>}} **{{<text size="20.0pt" fg="#202122" bg="white">}}walk{{</text>}}** {{<text size="20.0pt" fg="#202122" bg="white">}}{{</text>}}{{<text fg="#202122" bg="white">}}or{{</text>}} **{{<text size="24.0pt" fg="#202122" bg="white">}}fly{{</text>}}** {{<text fg="#202122" bg="white">}}.{{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
![Fly GIFs - Get the best GIF on GIPHY](fly_gifs_-_get_the_best_gif_on_giphy.gif)
{{<text fg="#202122" bg="white">}}{{</text>}}
**{{<text size="22.0pt" fg="#8FAADC" bg="white">}}If
 *we* wanted to have compound eyes{{</text>}}** {{<text fg="#202122" bg="white">}}, but keep the{{</text>}} *{{<text size="26.0pt" fg="#00B0F0" bg="white">}}resolution{{</text>}}* {{<text size="26.0pt" fg="#202122" bg="white">}}{{</text>}}{{<text fg="#202122" bg="white">}}that we see with the same. We would need to have compound eyes that were {{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}Guess how big {{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}Wrong {{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
![11 Incredible Eye Macros](11_incredible_eye_macros.jpeg){{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}} **{{<highlight size="72.0pt" fg="red" bg="yellow">}}22 metres{{</highlight>}}** {{<text size="72.0pt" fg="red" bg="white">}}{{</text>}}{{<text fg="#202122" bg="white">}}in diameter.{{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
***{{<text size="18.0pt" fg="#202122" bg="white">}}HOW NUTS IS THAT?!{{</text>}}***
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}So even if you need glasses --- you're doing a whole lot better than a fly.{{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}Lots of love,{{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}Flora{{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
{{<text fg="#202122" bg="white">}}PS this is your new segment of a gif of an animal falling over because it's the energy we all need right now (thank you Clare for inspiring it){{</text>}}
{{<text fg="#202122" bg="white">}}{{</text>}}
![Panda falls from tree unhurt --- Find and Share Funny Animated Gifs | Funnygifs fails, Funny gif, Funny animals](panda_falls_from_tree_unhurt_find_and_sh.gif)