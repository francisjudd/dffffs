## Contribute

To contribute to this collection of facts, you need to install Hugo Extended.

- Clone the repository to your local machine using `git clone`.
- Make sure Hugo Extended is installed on your local machine.
  - use `choco install hugo-extended`
- Create or edit content within your local repository.
- Test your changes locally by running `hugo server` and visiting the generated local server address.
- Commit your changes with a meaningful message using `git commit -m "Your message"`.
- Push the changes to your forked repository using `git push`.
- Submit a merge request to the repository for your contributions to be reviewed.
- Once approved, your contributions will be live on the blog hosted by GitLab Pages.

For detailed instructions on setting up and deploying a Hugo site on GitLab Pages, refer to the official Hugo documentation and additional resources.