import fs from "fs-extra";
import { render } from "dom-serializer";
import { ParsedMail } from "mailparser";
import OpenAI from "openai";
import path from "path";
import { Eml2Mail } from "./src/eml2mail";
import { HTML2IMG } from "./src/html2img";
import { HTML2MD } from "./src/html2md";
import { Mail2Summary, Summary } from "./src/inferSummary";
import { Mail2HTML } from "./src/mail2html";

/**
 * Creates Front-Matter metadata for the email message.
 */
async function createFrontMatter(
  message: ParsedMail,
  summary: Summary
): Promise<string> {
  const date = message.date?.toISOString().split("T")[0] || "";
  const imgName = path.parse(summary.featuredImageName);

  return `+++
author = "Flora Judd"
title = "${summary.title}"
date = "${date}"
description = "${summary.description}"
tags = ${JSON.stringify(summary.keywords)}
categories = ${JSON.stringify(summary.categories)}
image = "${imgName.name.slice(0, 40)}${imgName.ext}"
+++`;
}

// ==========================
// EML File Processing
// ==========================

/**
 * Processes a single .eml file, converting it to HTML and Markdown.
 */
async function processEmlFile(
  emlFilePath: string,
  outputFolder: string,
  openai: OpenAI
) {
  try {
    const message = await new Eml2Mail(emlFilePath).build();
    const htmlDocument = await new Mail2HTML(message).build();
    if (!htmlDocument) {
      throw new Error("No htmlDocument returned...");
    }

    const imgHandler = new HTML2IMG(htmlDocument);
    const inlineImgRecord = await imgHandler.build();
    const inlineImages = Object.values(inlineImgRecord);
    const mdHandler = new HTML2MD(htmlDocument, inlineImages);
    const markdownContent = await mdHandler.build();
    const summary = await new Mail2Summary(
      emlFilePath,
      message,
      markdownContent,
      inlineImages,
      openai
    ).build();
    const frontMatter = await createFrontMatter(message, summary);

    const finalContent = `${frontMatter}\n\n${markdownContent}`;

    const outputHeirarchy = getOutputHeirarchy(message, summary.filename);
    const outputFilePath = path.join(outputFolder, ...outputHeirarchy);

    await fs.ensureDir(outputFilePath);
    await fs.ensureDir(Mail2Summary.htmlDir);

    await Promise.all([
      fs.writeFile(path.join(outputFilePath, "index.md"), finalContent),
      fs.writeFile(path.join(Mail2Summary.htmlDir, `${outputHeirarchy.join('-')}.html`), render(htmlDocument)),
      imgHandler.flushQueue(outputFilePath),
    ]);

    console.log(`Converted: ${emlFilePath}`);
  } catch (error) {
    console.error(`Error processing ${emlFilePath}: ${error.message}`);
  }
}

function getOutputHeirarchy(message: ParsedMail, filename: string) {
  const dateStr = message.date?.toISOString().split("T")[0] || "";
  const dateParts = dateStr.split("-");
  const day = dateParts.pop();
  const outputHeirarchy = [...dateParts, `${day}-${filename}`];
  return outputHeirarchy;
}

/**
 * Processes all .eml files in a folder.
 */
async function processEmlFolder(inputFolder: string, outputFolder: string) {
  const openai: OpenAI = new OpenAI({
    apiKey: fs.readFileSync("./api-key.txt", "utf-8").trim(),
  });

  Mail2Summary.htmlDir = "./raw-html";
  Mail2Summary.summaryDir = "./ai-summary";
  Mail2Summary.taxonomyDir = "./ai-taxonomy";

  try {
    await Mail2Summary.readFromDir();

    await Mail2Summary.secondPassKeywords(openai);

    await Mail2Summary.mapSummaries();

    const emlFiles = await fs.readdir(inputFolder);
    const emlPaths = emlFiles
      .filter((file) => file.endsWith(".eml"))
      .map((file) => path.join(inputFolder, file));

    if (emlPaths.length === 0) {
      console.log("No .eml files found.");
      return;
    }

    await fs.ensureDir(outputFolder);

    await Promise.all(
      emlPaths.map((emlFilePath) =>
        processEmlFile(emlFilePath, outputFolder, openai)
      )
    );

    console.log("Processing complete.");
  } catch (error) {
    console.error(`Error processing folder ${inputFolder}: ${error.message}`);
  }
}

// ==========================
// Run the process
// ==========================
await processEmlFolder(process.argv[2], process.argv[3]);
