import { createHash } from "crypto";
import fs from "fs-extra";
import { ParsedMail } from "mailparser";
import OpenAI from "openai";
import path, { join } from "path";
import { ProcessedImage } from "./html2img";

export interface Summary {
  filename: string;
  featuredImageName: string;
  categories: string[];
  keywords: string[];
  title: string;
  description: string;
  coords: {
    hash: string;
    emlname: string;
  };
}

export class Mail2Summary {
  static htmlDir: string;
  static summaryDir: string;
  static taxonomyDir = "./ai-taxonomy";
  static completionsDir = "./ai-completions";

  static async readFromDir() {
    await fs.ensureDir(this.summaryDir);

    const summaryFiles = await fs.readdir(this.summaryDir);

    const summaryPaths = summaryFiles.map((file) =>
      path.join(this.summaryDir, file)
    );

    this.summaries = [];

    if (summaryPaths.length === 0) {
      console.log("No summary files found.");
    } else {
      this.summaries = await Promise.all(
        summaryPaths.map((filename) => {
          return fs.readFile(filename, "utf8").then((txt) => JSON.parse(txt));
        })
      );
    }

    this.read = true;
  }

  static async writeSummary(
    coords: { hash: string; emlname: string },
    summary: Summary
  ) {
    if (!this.summaryDir) {
      throw new Error("No summary directory provided");
    }
    await fs.ensureDir(this.summaryDir);
    await fs.writeFile(
      path.join(this.summaryDir, summary.filename + ".json"),
      JSON.stringify({ ...summary, coords }, undefined, 2),
      "utf8"
    );
  }

  static async secondPassKeywords(openAI: OpenAI): Promise<void> {
    const questionContent = JSON.stringify(
      this.summaries.map((summary) => {
        return {
          keywords: summary.keywords,
          categories: summary.categories,
          title: summary.title,
          description: summary.description,
        };
      })
    );

    const completion = await openAI.chat.completions.create({
      model: "gpt-4o-mini",
      response_format: {
        // See /docs/guides/structured-outputs
        type: "json_schema",
        json_schema: {
          name: "summary_schema",
          schema: {
            type: "object",
            properties: {
              mappedResponse: {
                type: "array",
                items: {
                  type: "object",
                  properties: {
                    title: {
                      description: "the provided title",
                      type: "string",
                    },
                    description: {
                      description: "the provided description",
                      type: "string",
                    },
                    categories: {
                      items: {
                        type: "string",
                      },
                      description:
                        "the categories you have extracted and mapped",
                      type: "array",
                    },
                    keywords: {
                      items: {
                        type: "string",
                      },
                      description: "the keywords you have extracted and mapped",
                      type: "array",
                    },
                  },
                  additionalProperties: false,
                },
              },
              allKeywords: {
                "type": "array",
                "items": {
                  "type": "string"
                }
              },
              allCategories: {
                "type": "array",
                "items": {
                  "type": "string"
                }
              },
              additionalProperties: false,
            },
          },
        },
      },
      messages: [
        {
          role: "system",
          content: `
Adopt the persona of a blog post editor and you are tasked with improving the taxonomy of the blog content. Your response should be valid
JSON data.

Aim for 10 keywords and categories per post, erring on the side of more.
Try to emulate the style of the post author.
Do not wrap the JSON in markdown quotes.
Do not modify the provided titles and descriptions.
Feel free to modify the words used if there is a better synonym that matches more posts.

Use the following proceedure as a guideline to answering the user question:

  1. Read all the post summaries
  2. Come up with a list of all the keywords you think are helpful across all the posts. Try to remove duplicates. Feel free to modify the words used if there is a better synonym that matches more posts. Use consistent capitalization to improve linking.
  3. Come up with a list of all the categories you think are helpful across all the posts. Feel free to modify the words used if there is a better synonym that matches more posts. Use consistent capitalization to improve linking.
  4. Map each provided summary to use keywords and categories from your sanitized lists.

`,
        },
        {
          role: "user",
          content: `
Please improve the keywords and categories to provide better linking across these posts.

---
${questionContent}
---
`,
        },
      ],
    });

    await fs.ensureDir(Mail2Summary.taxonomyDir);
    await fs.writeFile(
      join(Mail2Summary.taxonomyDir, "response.txt"),
      completion.choices[0].message.content ||
        JSON.stringify(completion, null, 2),
      "utf8"
    );
  }

  static async mapSummaries(): Promise<void> {
    const mapped = (await fs
      .readFile(join(Mail2Summary.taxonomyDir, "response.txt"), "utf8")
      .then((txt) => JSON.parse(txt))) as {
      mappedResponse: [
        {
          keywords: string[];
          categories: string[];
          title: string;
          description: string;
        },
      ];
    };
    this.summaries = this.summaries.map((summary) => {
      const mappedSummary = mapped.mappedResponse.find(
        (s) =>
          s.title === summary.title && s.description === summary.description
      );

      if (!mappedSummary) {
        console.warn("Could not find mapped summary for", summary.title);

        return summary;
      }

      return {
        ...summary,
        keywords: mappedSummary.keywords,
        categories: mappedSummary.categories,
      };
    });
  }

  static summaries: Summary[] = [];
  private static read = false;

  constructor(
    protected emlFilePath: string,
    protected message: ParsedMail,
    protected markdownContent: string,
    protected featuredImages: ProcessedImage[],
    protected openAI: OpenAI
  ) {}

  async build(): Promise<Summary> {
    if (!Mail2Summary.read) {
      await Mail2Summary.readFromDir();
    }

    const hasher = createHash("sha256", {});
    hasher.update(this.message.html || this.message.text || "noop-msg");
    const hash = hasher.digest("hex");

    let summary = Mail2Summary.summaries.find((s) => s.coords.hash === hash);
    const shouldWriteSummary = !summary;

    if (!summary) {
      summary = await this.generateSummary(
        hash,
        this.markdownContent,
        this.featuredImages.map((img) => img.filename)
      );
    }

    if (shouldWriteSummary) {
      await Mail2Summary.writeSummary(
        {
          hash,
          emlname: this.emlFilePath,
        },
        summary as Summary
      );
    }

    return summary;
  }

  async generateSummary(
    hash: string,
    markdownContent: string,
    images: string[]
  ): Promise<Summary> {
    const completion = await this.openAI.chat.completions.create({
      model: "gpt-4o-mini",
      response_format: {
        // See /docs/guides/structured-outputs
        type: "json_schema",
        json_schema: {
          name: "summary_schema",
          schema: {
            type: "object",
            properties: {
              title: {
                description:
                  "a short, pithy title in the same style as the post, using as few words as possible - that is ideally fewer than 5 words",
                type: "string",
              },
              description: {
                description:
                  "a short, pithy description in the same style as the post, using as few words as possible - that is ideally fewer than 20 words",
                type: "string",
              },
              categories: {
                items: {
                  type: "string",
                },
                description:
                  "a short list of categories that the post falls into",
                type: "array",
              },
              keywords: {
                items: {
                  type: "string",
                },
                description:
                  "a list of no more than 10 keywords, extracted from the post, or concepts related to the post",
                type: "array",
              },
              filename: {
                description:
                  "a short appropriate human-readable filename for the post",
                type: "string",
              },
              featuredImageName: {
                description:
                  "the most appropriate image from the post to use as the headline from the provided list of image names.",
                type: "string",
              },
            },
            additionalProperties: false,
          },
        },
      },
      messages: [
        {
          role: "system",
          content: `
Adopt the persona of a blog post editor and you are tasked with summarizing the raw content for 
a blog post that is in the form of a hugo template markdown file. Your response should be valid
JSON data.

Refrain from describing or referencing any image or email related jargon e.g. gif, email.
Avoid using the words "daily", "fun", or "fact".
Try to emulate the style of the post author.
Do not wrap the JSON in markdown quotes.

Use the following proceedure as a guideline to answering the user question:

  1. read the whole post
  2. come up with a short, pithy "title" in the same style as the post, using as few words as possible - that is ideally fewer than 5 words.
  3. come up with a short, pithy "description", in the same style as the post using as few words as possible - that is ideally fewer than 20 words.
  4. extract a list of no more than 10 "keywords" from the post, or concepts related to the post.
  5. provide a short list of "categories" that the post falls into.
  6. provide a short appropriate human-readable "filename" for the post.
  7. select the most appropriate image from the post to use as the headline ("featuredImageName") from the provided list of image names.

`,
        },
        {
          role: "user",
          content: `
Please provide a summary of this blog post.

---
${markdownContent}
---

The image names are:
  - ${images.join("\n  - ")}

`,
        },
      ],
    });

    await fs.ensureDir(Mail2Summary.completionsDir);
    await fs.writeFile(
      join(Mail2Summary.completionsDir, hash + ".txt"),
      completion.choices[0].message.content ||
        JSON.stringify(completion, null, 2),
      "utf8"
    );

    let summary: Summary;

    try {
      summary = JSON.parse(completion.choices[0].message.content || "{}");
    } catch (e) {
      console.info("AI response", completion.choices);
      throw new Error("Failed to parse AI response...");
    }

    return summary;
  }
}
