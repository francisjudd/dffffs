import { createHash } from "crypto";
import { ChildNode } from "domhandler";
import fs from "fs-extra";
import path from "path";

export interface ProcessedImage {
  src: string;
  alt: string;
  filename: string;
}

export class HTML2IMG {
  protected imgs: ProcessedImage[] = [];
  protected imgWriteQueue: Array<(imgDir: string) => Promise<void>> = [];

  constructor(protected rootDoc: { children: ChildNode[] }) {}

  async build() {
    return this.processBase64Images(this.rootDoc);
  }

  async flushQueue(imgDir: string): Promise<void> {
    await Promise.all(this.imgWriteQueue.map((task) => task(imgDir))).then(() =>
      this.imgWriteQueue.splice(0),
    );
  }

  protected createHash(input: string) {
    return createHash("sha256").update(input).digest("hex");
  }

  /**
   * Processes Base64 images, saves them to the file system, and updates the src attributes.
   */
  protected processBase64Images(
    htmlDoc: { children: ChildNode[] },
    parsed: Record<string, ProcessedImage> = {},
  ): Record<string, ProcessedImage> {
    for (const node of htmlDoc.children) {
      if (node.type === "tag" && node.name === "img" && node.attribs?.src) {
        const src = node.attribs.src;
        const srcHash = this.createHash(src);

        if (parsed[srcHash]) {
          continue;
        } else if (src.startsWith("data:image/")) {
          const matches = src.match(/^data:image\/([a-zA-Z]+);base64,([^"]+)$/);
          if (matches) {
            const extension = matches[1];
            const base64Data = matches[2];
            const buffer = Buffer.from(base64Data, "base64");
            const altText = node.attribs.alt;
            const fileName =
              this.generateFilenameFromAlt(
                altText,
                this.createHash(base64Data).slice(0, 16),
              ) + `.${extension}`;

            parsed[srcHash] = {
              src,
              alt: node.attribs.alt?.replace(/\n|\r|\s{2,}/g, ""),
              filename: fileName,
            };

            this.imgs.push(parsed[srcHash]);
            this.imgWriteQueue.push(async (imgDir: string) => {
              const filePath = path.join(imgDir, fileName);
              await fs.writeFile(filePath, buffer);
            });
          }
        }
      }

      // Process nested children recursively
      if ("children" in node && node.children) {
        this.processBase64Images(node, parsed);
      }
    }

    return parsed;
  }

  /**
   * Generates a unique filename from alt text using a hash.
   */
  protected generateFilenameFromAlt(
    altText: string | undefined,
    hash: string,
  ): string {
    return (
      altText
        ?.replace(/[^\w\s-]/g, "")
        .replace(/\n|\r/g, "")
        .replace(/\s+/g, "_")
        .toLowerCase() || hash
    ).slice(0, 40);
  }
}
