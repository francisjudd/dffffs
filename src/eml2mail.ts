import fs from "fs-extra";
import { simpleParser } from "mailparser";

export class Eml2Mail {
  constructor(protected emlFilePath: string) {}

  async build() {
    return simpleParser(await fs.readFile(this.emlFilePath));
  }
}
