import { parseDocument } from "htmlparser2";
import { ParsedMail } from "mailparser";

export class Mail2HTML {
  constructor(protected mail: ParsedMail) {}

  async build() {
    const htmlContent = this.mail.html || this.mail.text;

    if (!htmlContent) {
      console.log(`No HTML content found in ${this.mail.subject}, skipping...`);
      return;
    }

    return parseDocument(htmlContent);
  }
}
