import { ChildNode, Document, Element } from "domhandler";

export class HTML2MD {
  protected ALLOWED_STYLES = [
    "font-size",
    "color",
    "background-color",
    "background",
    "width",
    "height",
  ];
  protected DEFAULT_ALT_TEXT = "image";

  constructor(
    protected rootDoc: Document,
    protected imgs: { src: string; alt: string; filename: string }[]
  ) {}

  async build() {
    this.renameSavedImages(this.rootDoc);
    return this.convertHtmlToMarkdown(this.rootDoc);
  }

  protected renameSavedImages(htmlDoc: { children: ChildNode[] }): void {
    for (const node of htmlDoc.children) {
      if (node.type === "tag" && node.name === "img" && node.attribs?.src) {
        const src = node.attribs.src;
        if (src.startsWith("data:image/")) {
          const matches = src.match(/^data:image\/([a-zA-Z]+);base64,([^"]+)$/);
          if (matches) {
            const img = this.imgs.find((img) => img.src === src);
            if (img) {
              node.attribs.alt = img.alt;
              node.attribs.src = img.filename; // Replace Base64 with file path
            }
          }
        }
      }

      // Process nested children recursively
      if ("children" in node && node.children) {
        this.renameSavedImages(node);
      }
    }
  }

  /**
   * Converts the entire HTML document to Markdown format.
   */
  protected convertHtmlToMarkdown(htmlDoc: Document): string {
    return htmlDoc.children
      .map((childNode) => this.renderChildren(childNode))
      .join("")
      .replace(/’(s\b|re\b|t\b)/g, "'$1")
      .replace(/’|‘/g, '"')
      .replace(/–/g, "---")
      .replace(/[^\x00-\x7f]/g, " ")
      .replace(/ +/g, " ")
      .replace(/\* \./g, "*.")
      .trim();
  }

  /**
   * Converts style attributes to Hugo shortcode attributes (size, fg, bg).
   */
  convertStyleToTextAttributes(style: string): {
    isHighlight: boolean;
    attrs: string;
  } {
    const fontSizeMatch = style.match(/font-size:\s*([^;]+);?/i);
    const colorMatch = style.match(/(?<!background-?)color:\s*([^;]+);?/i);
    const bgColorMatch = style.match(/background(?:-color)?:\s*([^;]+);?/i);

    const sizeValue =
      fontSizeMatch && !["11.0pt", "11pt"].includes(fontSizeMatch[1].trim())
        ? fontSizeMatch[1].trim()
        : undefined;
    const fgValue =
      colorMatch && !["black", "#212121"].includes(colorMatch[1].trim())
        ? colorMatch[1].trim()
        : undefined;
    const bgValue =
      bgColorMatch && bgColorMatch[1].trim()
        ? bgColorMatch[1].trim()
        : undefined;

    // Check if bg color is yellow or aqua and transform accordingly
    const bgHighlightMatch = bgValue?.match(/(yellow|aqua)/i);

    const size = sizeValue ? ` size="${sizeValue}"` : "";
    const fg = fgValue ? ` fg="${fgValue}"` : "";
    const bg = bgValue ? ` bg="${bgValue}"` : "";

    return { attrs: `${size}${fg}${bg}`, isHighlight: !!bgHighlightMatch };
  }

  /**
   * Sanitizes style attributes, allowing only the specified styles.
   */
  sanitizeStyle(style: string, allowedStyles: string[]): string {
    return style
      .split(";")
      .map((part) => {
        const [key, value] = part.split(":").map((str) => str.trim());
        return allowedStyles.includes(key) ? `${key}:${value}` : "";
      })
      .filter(Boolean)
      .join(";");
  }

  /**
   * Recursively renders child nodes to Markdown format.
   */
  renderChildren(node: ChildNode): string {
    if (node.type === "tag") {
      switch (node.name) {
        case "a":
          return this.handleAnchorTag(node);
        case "strong":
        case "b":
          return ` **${this.renderNodeContent(node)}** `;
        case "em":
        case "i":
          return ` *${this.renderNodeContent(node)}* `;
        case "u":
          return this.handleUnderlineTag(node, this.ALLOWED_STYLES);
        case "span":
          return this.handleSpanTag(node, this.ALLOWED_STYLES);
        case "img":
          return this.handleImageTag(node);
        default:
          return this.renderNodeContent(node);
      }
    }
    if (node.type === "text" && !node.data.startsWith("<!--")) {
      return node.data || "";
    }
    return "";
  }

  /**
   * Processes the content of a node and renders it to Markdown.
   */
  renderNodeContent(node: Element): string {
    if (node.children) {
      return node.children
        .map((childNode) => this.renderChildren(childNode))
        .join("")
        .trim();
    }
    return "";
  }

  hasImgNode(node: Element | ChildNode): Element | undefined {
    if (node.type === "tag" && node.name === "img") {
      return node;
    }

    if ("children" in node && node.children) {
      return node.children
        .map((childNode) => this.hasImgNode(childNode))
        .filter((e): e is Element => !!e)[0];
    }

    return;
  }

  /**
   * Handles <u> tags that wrap around <span> elements with styles.
   * Collapses <u><span> into {{<u size="...">}} if appropriate styles are present.
   */
  handleUnderlineTag(node: Element, allowedStyles: string[]): string {
    // If <u> wraps a <span>, extract the style attributes from <span>
    const firstChild = node.children?.[0];

    if (
      firstChild &&
      firstChild.type === "tag" &&
      firstChild.name === "span" &&
      firstChild.attribs?.style
    ) {
      const sanitizedStyle = this.sanitizeStyle(
        firstChild.attribs.style,
        allowedStyles
      );
      const textAttributes = this.convertStyleToTextAttributes(sanitizedStyle);

      // If there are any style attributes, collapse the span into <u>
      if (textAttributes.attrs) {
        const tag = textAttributes.isHighlight ? "highlight-u" : "u";
        return `{{<${tag}${textAttributes.attrs}>}}${this.renderNodeContent(firstChild)}{{</${tag}>}}`;
      }
    }

    // Default case: render <u> with its children content
    const childrenContent = this.renderNodeContent(node);
    return `{{<u>}}${childrenContent}{{</u>}}`;
  }

  /**
   * Handles <span> tags, applying transformations based on background colors or default.
   */
  handleSpanTag(node: Element, allowedStyles: string[], tag = "text"): string {
    if (node.attribs?.style) {
      const sanitizedStyle = this.sanitizeStyle(
        node.attribs.style,
        allowedStyles
      );
      const textAttributes = this.convertStyleToTextAttributes(sanitizedStyle);

      // General case for spans without specific background color
      if (textAttributes.attrs) {
        if (textAttributes.isHighlight) tag = "highlight";
        return `{{<${tag}${textAttributes.attrs}>}}${this.renderNodeContent(node)}{{</${tag}>}}`;
      }
    }

    // Default case: just return the content if no styles are present
    return this.renderNodeContent(node);
  }

  /**
   * Handles <img> tags and converts them to Markdown image syntax.
   */
  handleImageTag(node: Element): string {
    const src = node.attribs?.src;
    const alt = node.attribs?.alt || this.DEFAULT_ALT_TEXT;
    return src ? `![${alt}](${src})` : "";
  }

  /**
   *
   */
  handleAnchorTag(node: Element): string {
    const href = node.attribs?.href;

    let renderedContent = "";
    let injectedContent = "";
    let shouldWrap = true;
    let shouldIncludeRenderedContent = true;

    const hasImg = this.hasImgNode(node);
    const renderedChildNodes = hasImg
      ? this.renderChildren(hasImg)
      : this.renderNodeContent(node);

    if (href.includes("youtube") || href.includes("youtu.be")) {
      const video_id_match = href.match(
        /https:\/\/www\.youtube\.com\/watch\?.*?v=([a-zA-Z0-9_\-]+).*?|https:\/\/youtu\.be\/([a-zA-Z0-9_\-]+)/
      );

      const video_id = video_id_match
        ? video_id_match[1] || video_id_match[2]
        : undefined;

      if (video_id) {
        injectedContent = `{{< youtube ${video_id} >}}`;
      }

      if (renderedChildNodes.includes(href)) {
        shouldIncludeRenderedContent = false;
      }
    }

    if (href.startsWith("mailto:")) {
      shouldWrap = false;
    }

    renderedContent = shouldWrap
      ? `<a href="${href}">${renderedChildNodes}</a>`
      : renderedChildNodes;

    return `${shouldIncludeRenderedContent ? renderedContent : ""}${injectedContent ? `\n${injectedContent}` : ""}`;
  }
}
